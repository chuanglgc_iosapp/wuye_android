package com.chuanglgc.wuye.utils;


import android.util.Log;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.chuanglgc.wuye.EmployeeApplication;

//高德定位
public class GaoDeLocation {
    //声明AMapLocationClientOption对象
    public AMapLocationClientOption mLocationOption = null;
    //声明AMapLocationClient类对象

    //声明定位回调监听器
    public AMapLocationListener mLocationListener = new AMapLocationListener() {
        @Override
        public void onLocationChanged(AMapLocation amapLocation) {
            if (amapLocation != null) {
                if (amapLocation.getErrorCode() == 0) {
//可在其中解析amapLocation获取相应内容。
                    amapLocation.getLocationType();//获取当前定位结果来源，如网络定位结果，详见定位类型表
                    amapLocation.getLatitude();//获取纬度
                    amapLocation.getLongitude();//获取经度
                    amapLocation.getAccuracy();//获取精度信息
                    amapLocation.getAddress();//地址，如果option中设置isNeedAddress为false，则没有此结果，网络定位结果中会有地址信息，GPS定位不返回地址信息。
                    amapLocation.getCountry();//国家信息
                    amapLocation.getProvince();//省信息
                    amapLocation.getCity();//城市信息
                    amapLocation.getDistrict();//城区信息
                    amapLocation.getStreet();//街道信息
                    amapLocation.getStreetNum();//街道门牌号信息
                    amapLocation.getCityCode();//城市编码
                    amapLocation.getAdCode();//地区编码
                    amapLocation.getAoiName();//获取当前定位点的AOI信息
                    amapLocation.getBuildingId();//获取当前室内定位的建筑物Id
                    amapLocation.getFloor();//获取当前室内定位的楼层
                    amapLocation.getGpsAccuracyStatus();//获取GPS的当前状态
                    double v = DistanceOfTwoPoints(amapLocation.getLatitude(), amapLocation.getLongitude()
                            , 39.95002, 119.568221);
                    Toast.makeText(EmployeeApplication.getInstance(),v+"",Toast.LENGTH_LONG).show();
                    LogUtil.e("当前位置", amapLocation.toString());
                    LogUtil.e("相差多少mi", v);
                } else {
                    //定位失败时，可通过ErrCode（错误码）信息来确定失败的原因，errInfo是错误信息，详见错误码表。
                    Log.e("AmapError", "location Error, ErrCode:"
                            + amapLocation.getErrorCode() + ", errInfo:"
                            + amapLocation.getErrorInfo());
                }
            }

        }
    };

  public void startLocation(){

//初始化定位
       AMapLocationClient mLocationClient = new AMapLocationClient(EmployeeApplication.getInstance());
//设置定位回调监听
       mLocationClient.setLocationListener(mLocationListener);


//初始化AMapLocationClientOption对象
       mLocationOption = new AMapLocationClientOption();

       //设置定位模式为AMapLocationMode.Hight_Accuracy，高精度模式。
       mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);

       //设置定位间隔,单位毫秒,默认为2000ms，最低1000ms。
       mLocationOption.setInterval(1000);

       //设置是否返回地址信息（默认返回地址信息）
       mLocationOption.setNeedAddress(true);
       //单位是毫秒，默认30000毫秒，建议超时时间不要低于8000毫秒。
       mLocationOption.setHttpTimeOut(20000);

       //给定位客户端对象设置定位参数
       mLocationClient.setLocationOption(mLocationOption);
//启动定位
       mLocationClient.startLocation();

   }
    private static final double EARTH_RADIUS = 6378.137;
    /**
     * 依据两点间经纬度坐标（double值），计算两点间距离，
     *
     * @param lat1
     * @param lng1
     * @param lat2
     * @param lng2
     * @return 距离：单位为公里
     */
    public  double DistanceOfTwoPoints(double lat1,double lng1,
                                             double lat2,double lng2) {
        double radLat1 = rad(lat1);
        double radLat2 = rad(lat2);
        double a = radLat1 - radLat2;
        double b = rad(lng1) - rad(lng2);
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
                + Math.cos(radLat1) * Math.cos(radLat2)
                * Math.pow(Math.sin(b / 2), 2)));
        s = s * EARTH_RADIUS;
        s = Math.round(s * 10000);

        return s;
    }
    private static double rad(double d) {
        return d * Math.PI / 180.0;
    }

}
