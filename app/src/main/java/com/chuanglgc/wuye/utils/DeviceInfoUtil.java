package com.chuanglgc.wuye.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.telephony.TelephonyManager;

import com.chuanglgc.wuye.EmployeeApplication;

import java.util.UUID;

/**
 *
 * 设备信息获取
 */

public class DeviceInfoUtil {

    /**
     * 在Application里面会保存着设备信息
     * @return
     */
    public static String getDID() {
        @SuppressLint("MissingPermission") String deviceId = ((TelephonyManager) EmployeeApplication.getInstance()
                .getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();

        return deviceId;

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static String getUserAgent() {

       // StringBuilder userAgent = new StringBuilder();
        //应用版本
       // String versionName = getVersionName();
        //屏幕分辨率
      /*  WindowManager wm = (WindowManager) OwnerApplication.INSTANCE.getSystemService(Context.WINDOW_SERVICE);
        android.view.Display display = wm.getDefaultDisplay();
        Point point = new Point();
        display.getRealSize(point);
        int x = point.x;
        int y = point.y;
        //userAgent.append(url);
        //userAgent.append(url+";");
       // userAgent.append(versionName + ";");*/
       // userAgent.append(Build.BRAND + ";");
        //userAgent.append(Build.MODEL + ";");
       // userAgent.append("Android " + Build.VERSION.RELEASE + ";");
       // userAgent.append(x + "*" + y);
       // String manufacturer = Build.MANUFACTURER;

        return "Android " + Build.VERSION.RELEASE ;
    }

    @SuppressLint("MissingPermission")
    public static String getUUID() {

        final TelephonyManager tm = (TelephonyManager) EmployeeApplication.getInstance().getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, androidId;

        tmDevice = "" + tm.getDeviceId();

        tmSerial = "" + tm.getSimSerialNumber();

        androidId = "" + android.provider.Settings.Secure.getString(EmployeeApplication.getInstance().getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());

        String uniqueId = deviceUuid.toString();//把-换成_ 激光推送不能使用-
        return uniqueId.replace("-","_");


    }

    public static String getVersionName() {
        String pkName = EmployeeApplication.getInstance().getPackageName();
        String versionName = "";
        try {
            versionName = EmployeeApplication.getInstance().getPackageManager().getPackageInfo(pkName, 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return versionName;

    }

    /**
     * 获取手机厂商
     * @return
     */
    public static String getDeviceBrand() {
        return Build.BRAND;
    }


}
