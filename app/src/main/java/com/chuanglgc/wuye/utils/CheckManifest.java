package com.chuanglgc.wuye.utils;

/**
 * Created by Json on 2017/11/16.
 */

import android.Manifest;
import android.app.Activity;
import android.os.Build;
import android.widget.Toast;

import com.tbruyelle.rxpermissions2.RxPermissions;

/**
 * 动态授予访问权限 可根据自己需求添加
 */

public class CheckManifest {
    public static void initManifest(final Activity activity) {
        //检查权限
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            new RxPermissions(activity).request(Manifest.permission.INTERNET
                    , Manifest.permission.ACCESS_NETWORK_STATE
                    , Manifest.permission.READ_PHONE_STATE)
                    .subscribe(new io.reactivex.functions.Consumer<Boolean>() {
                        @Override
                        public void accept(Boolean aBoolean) throws Exception {
                            if (!aBoolean) {
                                Toast.makeText(activity, "请确认权限", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }
}
