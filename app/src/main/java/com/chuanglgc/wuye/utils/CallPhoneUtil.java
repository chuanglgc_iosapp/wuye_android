package com.chuanglgc.wuye.utils;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import com.tbruyelle.rxpermissions2.RxPermissions;

public class CallPhoneUtil {
    public static void callPhone(Activity context, String phoneNumber){
        new RxPermissions(context).request(Manifest.permission.CALL_PHONE)
                .subscribe(new io.reactivex.functions.Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        if (!aBoolean) {
                            Toast.makeText(context, "请确认权限", Toast.LENGTH_SHORT).show();
                        }else {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_CALL);
                            intent.setData(Uri.parse("tel:" + phoneNumber));
                            context.startActivity(intent);
                        }
                    }
                });

    }
}


