package com.chuanglgc.wuye.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chuanglgc.wuye.R;


public class LoadingBelowSearchUtils {


    private static Dialog loadingDialog;

    /**
     * @param context
     * @param msg       提示信息
     * @param isHasShow 是否有阴影效果
     * @return
     */
    public static Dialog createLoadingDialog(Context context, String msg, boolean isHasShow) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.dialog_loading, null);// 得到加载view
        LinearLayout layout = (LinearLayout) v.findViewById(R.id.dialog_loading_view);// 加载布局
        ProgressBar pb1= (ProgressBar) v.findViewById(R.id.progressBar1);
        TextView tipTextView = (TextView) v.findViewById(R.id.tipTextView);// 提示文字

        if (msg.equals("")) {
            tipTextView.setText("加载中");// 设置加载信息
            tipTextView.setTextColor(context.getResources().getColor(R.color.textnormal));
        }else {
            tipTextView.setText(msg);// 设置加载信息
        }

        if (isHasShow) {
            // 判断是否有阴影效果  请求时间短的为false 使用透明一闪而过  反之有阴影
            loadingDialog = new Dialog(context, R.style.MyDialogStyleShadow);
        } else {
            loadingDialog = new Dialog(context, R.style.MyDialogStyleTranslucent);
            v.findViewById(R.id.rl_root).setBackground(null);//設置無背景
        }

        loadingDialog.setCancelable(true); // 是否可以按“返回键”消失
        loadingDialog.setCanceledOnTouchOutside(false); // 点击加载框以外的区域

        //设置dialog显示大小,位置 高度减去状态栏的高度 不然会变成默认系统颜色
        int statusBarHeight1 = DisplayUtil.dp2px(context, context.getResources().getDimension(R.dimen.padding_top));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                - statusBarHeight1);
        loadingDialog.setContentView(layout, layoutParams);
        loadingDialog.show();
        Window window = loadingDialog.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height =DisplayUtil.getScreenHeight(context)- statusBarHeight1-DisplayUtil.dp2px(context,45);
        window.setGravity(Gravity.CENTER);
        window.setAttributes(lp);
        window.setWindowAnimations(R.style.PopWindowAnimStyle);

        return loadingDialog;
    }


    public static void closeDialog(Dialog mDialogUtils) {
        if (mDialogUtils != null && mDialogUtils.isShowing()) {
            mDialogUtils.dismiss();
        }
    }

}