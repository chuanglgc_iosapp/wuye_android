/*
 * Create on 2017-7-26 上午11:33
 * FileName: LogUtil.java
 * Author: Ren Yaowei
 * Blog: http://www.renyaowei.top
 * Email renyaowei@foxmail.com
 */

package com.chuanglgc.wuye.utils;

import android.util.Log;

/**
 * Created by Mr_Shadow on 2017/7/26.
 * <p>
 * 输出log日志  并且可控  正式发布时把DEBUG改为false
 */

public class LogUtil {
    private LogUtil() {

    }

    static String tag = "宅司令";
    static boolean DEBUG = true;


    /**
     * Verbose日志
     *
     * @param msg 消息
     */
    public static void v(Object msg) {
        if (!DEBUG)
            return;
        Log.v(tag, msg.toString());
    }

    /**
     * Verbose日志
     *
     * @param tag 标签
     * @param msg 消息
     */
    public static void v(String tag, Object msg) {
        if (!DEBUG)
            return;
        Log.v(tag, msg.toString());
    }


    /**
     * Debug日志
     *
     * @param msg 消息
     */
    public static void d(Object msg) {
        if (!DEBUG)
            return;
        Log.d(tag, msg.toString());
    }

    /**
     * Debug日志
     *
     * @param tag 标签
     * @param msg 消息
     */
    public static void d(String tag, Object msg) {
        if (!DEBUG)
            return;
        Log.d(tag, msg.toString());
    }


    /**
     * Info日志
     *
     * @param msg 消息
     */
    public static void i(Object msg) {
        if (!DEBUG)
            return;
        Log.i(tag, msg.toString());
    }

    /**
     * Info日志
     *
     * @param tag 标签
     * @param msg 消息
     */
    public static void i(String tag, Object msg) {
        if (!DEBUG)
            return;
        Log.i(tag, msg.toString());
    }


    /**
     * Warn日志
     *
     * @param msg 消息
     */
    public static void w(Object msg) {
        if (!DEBUG)
            return;
        Log.w(tag, msg.toString());
    }

    /**
     * Warn日志
     *
     * @param tag 标签
     * @param msg 消息
     */
    public static void w(String tag, Object msg) {
        if (!DEBUG)
            return;
        Log.w(tag, msg.toString());
    }


    /**
     * Error日志
     *
     * @param msg 消息
     */
    public static void e(Object msg) {
        if (!DEBUG)
            return;
        Log.e(tag, msg.toString());
    }

    /**
     * Error日志
     *
     * @param tag 标签
     * @param msg 消息
     */
    public static void e(String tag, Object msg) {
        if (!DEBUG)
            return;
        Log.e(tag, msg.toString());
    }


}
