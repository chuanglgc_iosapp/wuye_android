package com.chuanglgc.wuye.base;

import java.lang.ref.WeakReference;

public class BasePersenter<T> {

    public WeakReference<T> weakReference;

    /**
     * 使用弱引用加载 加载view层接口
     * @param iView
     */
   public void attatch(T iView){
        if (weakReference==null){
            weakReference=new WeakReference<T>(iView);
        }
    }

    /**
     * 释放软引用，避免内存泄漏
     */
    public void detach(){
        weakReference.clear();
    }
}
