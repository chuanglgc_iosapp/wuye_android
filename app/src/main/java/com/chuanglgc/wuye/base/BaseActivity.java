package com.chuanglgc.wuye.base;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.utils.StateBarConfig;
import com.chuanglgc.wuye.utils.StatusBarUtil;

import butterknife.ButterKnife;
import butterknife.Unbinder;



public abstract class BaseActivity<V, T extends BasePersenter<V>> extends AppCompatActivity {
    protected T persenter;
    private Unbinder bind;
    private View statusBarView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
   /*     //沉浸式状态栏设置
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            setTranslucentStatus(true);
            SystemBarTintManager tintManager = new SystemBarTintManager(this);
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setStatusBarTintResource(R.color.tran);//通知栏所需颜色
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Window window = getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                        | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(Color.TRANSPARENT);
            }
        }*/
        if (StateBarConfig.isStatusBarLight) {
            StatusBarUtil.setStatusBarLightMode(getWindow(),this);
        }
        if (StateBarConfig.isStatusBar()) {
            initStatusBar();
            getWindow().getDecorView().addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    initStatusBar();
                }
            });
        }
        setContentView(getLayoutId());
        bind = ButterKnife.bind(this);
        init();
        EmployeeApplication.getInstance().putActivity(this);
    }

    private void initStatusBar() {
        if (statusBarView == null) {
            int identifier = getResources().getIdentifier("statusBarBackground", "id", "android");
            statusBarView = getWindow().findViewById(identifier);
        }
        if (statusBarView != null) {
            if (StateBarConfig.isStatusBarLight) {
                statusBarView.setBackgroundDrawable(null);
            }
            statusBarView.setBackgroundResource(StateBarConfig.statusDrawable);
        }
    }
    @TargetApi(19)
    private void setTranslucentStatus(boolean on) {
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    /**
     * 初始化view 并创建Persenter用来控制 mv层之间交互
     */
    protected void init() {
        persenter = oncreatPersenter();
        if (persenter != null)
            persenter.attatch((V) this);
        initView();
        initData();
        initListener();
    }

    public void startActivity(Class<BaseActivity> clz) {
        startActivity(clz, null);
    }

    /**
     *携带数据的页面跳转
     * @param clz
     * @param bundle
     */
    public void startActivity(Class<?> clz, Bundle bundle) {
        Intent intent = new Intent();
        intent.setClass(this, clz);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }
    protected abstract int getLayoutId();

    protected abstract void initView();

    protected abstract void initListener();

    protected abstract void initData();

    /**
     * 创建Persenter 用来控制 M V层交互
     *
     * @return
     */
    protected abstract T oncreatPersenter();

    /**
     * 即时释放软引用和ButterKnife
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bind != null) bind.unbind();
        if (persenter != null) persenter.detach();
        EmployeeApplication.getInstance().removeActivity(this);
    }
}