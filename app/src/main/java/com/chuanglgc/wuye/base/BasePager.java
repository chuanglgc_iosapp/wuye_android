package com.chuanglgc.wuye.base;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.Unbinder;


public abstract class BasePager implements View.OnClickListener {
    public final Context mActivity;
    public final LayoutInflater inflater;
    public View rootView;
   // private final Unbinder bind;


    public BasePager(Context context) {
        this.mActivity = context;
        inflater = LayoutInflater.from(context);
        rootView = initView(inflater);

      //  bind = ButterKnife.bind(this,rootView);

        initData();
        initListener();
    }

    public void onDestroy() {
       // bind.unbind();
    }


    public abstract void initListener();

    public abstract void initData();

    public abstract View initView(LayoutInflater inflater);

}
