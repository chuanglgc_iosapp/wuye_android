package com.chuanglgc.wuye.network;


import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.utils.MD5Utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Constant {
    //测试
   //public static String BASE_URL = "http://192.168.0.210/platform/api/v3/wuye/";
    //public static String BASE_URL = "http://139.224.191.27/cms2.0_dev/";
    //线上
    public static String BASE_URL = "http://139.224.191.27/platform/api/v3/wuye/";
    public static String USER_KEY="USER_INFO";
    public static String USER_NAME="USER_NAME";
    public static String PASSWORD="PASSWORD";
    public static Map<String, String> addParamsMap = new HashMap<>();
    public static String client_id = "E5174DE6007E4A66A148";
    public static String clientSecret = "C88DB249249942CD8DA5453A4C745145";

    public static String salt = "583dd02cf5f347d3c5af512c6536e176";
    /**
     * 获取尾部加密数据
     *
     * @param url 方法名称
     * @return
     */
    public static Map getAddParams(String url) {
        addParamsMap.clear();
        String api_token = MD5Utils.md5(url + client_id + getDateToString() + clientSecret);
        LogUtil.e("apitoken", api_token);
        LogUtil.e("client_id", client_id);
        LogUtil.e("mod", url);
        addParamsMap.put("api_token", api_token);
        addParamsMap.put("client_id", client_id);
        addParamsMap.put("mod", url);
        return addParamsMap;
    }
    /**
     * 获取当前时间
     *
     * @return
     */
    public static String getDateToString() {
        Date d = new Date();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        LogUtil.e("时间", sf.format(d));
        return sf.format(d);
    }

}
