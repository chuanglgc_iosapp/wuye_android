package com.chuanglgc.wuye.network;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.utils.LogUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private static Retrofit retrofit;
    private static APIService service;
    private static OkHttpClient okHttpClient;

    /**
     * 获取自定义Retrofit关联的接口
     *
     * @return APIService
     */
    public static APIService getAPIService() {
        if (service == null) {
            service = getRetrofit().create(APIService.class);
        }
        return service;
    }

    /**
     * 构造Retrofit
     *
     * @return Retrofit
     */
    private static Retrofit getRetrofit() {
        if (retrofit == null) {
            //使用Builder模式创建Retrofit
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constant.BASE_URL)//这里的BASE_URL加上@POST的地址形成完整的请求地址
                    .addConverterFactory(GsonConverterFactory.create(getDataGson()))//添加自定义Date数据序列化
                    .addConverterFactory(GsonConverterFactory.create())
                   // .addConverterFactory(ScalarsConverterFactory.create())//添加字符串解析器
                    .client(getClient())
                    .build();
        }

        return retrofit;
    }




    /**
     * 添加自定义的Gson，解析日期类型的数据,日期转换格式,保证能够转换所有的格式
     *
     * @return 自定义的Gson
     */
    private static Gson getDataGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        //自定义序列化日期类型的数据
        gsonBuilder.registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
            @Override
            public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
                return new JsonPrimitive(src.getTime());
            }
        });
        //自定义反序列化日期类型的数据
        gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            @Override
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                return new Date(json.getAsJsonPrimitive().getAsLong());
            }
        });
        return gsonBuilder.create();
    }


    /**
     * 构造OKHttp客户端，相关参数设置
     *
     * @return OKHttp客户端
     */

    private static OkHttpClient getClient() {
        File cacheFile = new File(EmployeeApplication.getInstance().getExternalCacheDir(), "Cache");
        //设置缓存大小
        Cache cache = new Cache(cacheFile, 50 * 1024 * 1024);
        if (okHttpClient == null) {
            okHttpClient = new OkHttpClient.Builder()
                    /*//设置验证用来证实回复的证书应用于回复的主机名
                    .hostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    })*/
                    //添加Log信息拦截器 resposne调用一次
                    .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request request = chain.request();
                            RequestBody body = request.body();
                            if (body instanceof FormBody){
                                for (int i=0;i<((FormBody) body).size();i++){
                                    LogUtil.e("请求体",((FormBody) body).encodedName(i)+"_________"+((FormBody) body).encodedValue(i));

                                }

                            }
                            return chain.proceed(request);
                        }
                    })
                  /* //添加的是网络拦截器，他会在在request和resposne是分别被调用一次
                    .addNetworkInterceptor(new MyCacheInterceptor())*/
                    .retryOnConnectionFailure(true)//连接失败后是否重新连接
                    .connectTimeout(30, TimeUnit.SECONDS)//超时时间15S
                    .cache(cache)
                    .build();


        }
        return okHttpClient;
    }
}
