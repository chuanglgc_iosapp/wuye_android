package com.chuanglgc.wuye.network;

import com.chuanglgc.wuye.model.AttedanceModel;
import com.chuanglgc.wuye.model.BuildModel;
import com.chuanglgc.wuye.model.CarModel;
import com.chuanglgc.wuye.model.CarVisitorModel;
import com.chuanglgc.wuye.model.CycleEventDetailModel;
import com.chuanglgc.wuye.model.CycleEventModel;
import com.chuanglgc.wuye.model.CycleTaskModel;
import com.chuanglgc.wuye.model.DailyPhotoModel;
import com.chuanglgc.wuye.model.DailyTaskModel;
import com.chuanglgc.wuye.model.DailyWorkTypeModel;
import com.chuanglgc.wuye.model.DepartmentServiceModel;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.EventHandleDetailModel;
import com.chuanglgc.wuye.model.EventManagerModel;
import com.chuanglgc.wuye.model.HeadPicModel;
import com.chuanglgc.wuye.model.LoginModel;
import com.chuanglgc.wuye.model.MasterAttedanModel;
import com.chuanglgc.wuye.model.MasterAttendMonthModel;
import com.chuanglgc.wuye.model.MasterCheckWorkInfoModel;
import com.chuanglgc.wuye.model.MessageModel;
import com.chuanglgc.wuye.model.NearAreaOrDeviceModel;
import com.chuanglgc.wuye.model.NearBigBodyTypeModel;
import com.chuanglgc.wuye.model.NearBigDeviceNumModel;
import com.chuanglgc.wuye.model.NearBigDeviceTypeModel;
import com.chuanglgc.wuye.model.NearDailyClassModel;
import com.chuanglgc.wuye.model.NearDailyWorkModel;
import com.chuanglgc.wuye.model.OwnerEventDetailModel;
import com.chuanglgc.wuye.model.OwnerEventModel;
import com.chuanglgc.wuye.model.OwnerInfoModel;
import com.chuanglgc.wuye.model.ParkAreaModel;
import com.chuanglgc.wuye.model.ParkNumberModel;
import com.chuanglgc.wuye.model.PartnerModel;
import com.chuanglgc.wuye.model.PersonalStatisticsModel;
import com.chuanglgc.wuye.model.PhotoModel;
import com.chuanglgc.wuye.model.PressMoneyDetailModel;
import com.chuanglgc.wuye.model.PressMoneyModel;
import com.chuanglgc.wuye.model.RepairPicModel;
import com.chuanglgc.wuye.model.RepairTypeModel;
import com.chuanglgc.wuye.model.RoomModel;
import com.chuanglgc.wuye.model.ScheduleModel;
import com.chuanglgc.wuye.model.ServiceModel;
import com.chuanglgc.wuye.model.SuperviseDailyDetailModel;
import com.chuanglgc.wuye.model.SuperviseDailyModel;
import com.chuanglgc.wuye.model.SuperviseHandleDetailModel;
import com.chuanglgc.wuye.model.SuperviseHandleModel;
import com.chuanglgc.wuye.model.SuperviseOwnerDetailModel;
import com.chuanglgc.wuye.model.SuperviseOwnerModel;
import com.chuanglgc.wuye.model.UnitModel;
import com.chuanglgc.wuye.model.VisitRecordModel;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.QueryMap;


public interface APIService {

    @FormUrlEncoded
    @POST("EmpApp/Emp/empLogin")
    Call<Result<LoginModel>> login(@FieldMap Map<String, String> params);//登陆

    @FormUrlEncoded
    @POST("EmpApp/Emp/updatePassword")
    Call<Result> changePassword(@FieldMap Map<String, String> params);//修改密码

    @FormUrlEncoded
    @POST("EmpApp/Emp/queryPermissionMenu")
    Call<Result<List<LoginModel.MenuBean>>> queryPermissionMenu(@FieldMap Map<String, String> params);//查询权限列表

    @Multipart
    @POST("EmpApp/Emp/uploadHeadImage")
    Call<Result<HeadPicModel>> uploadHeadImage(@Part("emp_id") String id, @Part() MultipartBody.Part part);//上传头像

    @FormUrlEncoded
    @POST("EmpApp/Repair/queryRepairTask")
    Call<Result<List<OwnerEventModel>>> queryRepairTask(@FieldMap Map<String, String> params);//查询业主事件


    @FormUrlEncoded
    @POST("EmpApp/Repair/queryRepairTaskDetail")
    Call<Result<OwnerEventDetailModel>> queryRepairTaskDetail(@FieldMap Map<String, String> params);//查询业主事件详情

    @FormUrlEncoded
    @POST("EmpApp/Repair/queryFeedbackContent")
    Call<Result<List<VisitRecordModel>>> queryFeedbackContent(@FieldMap Map<String, String> params);//查询业主事件回访记录

    @FormUrlEncoded
    @POST("EmpApp/Repair/acceptRepairTask")
    Call<Result> acceptRepairTask(@FieldMap Map<String, String> params);//接受业主事件

    @Multipart
    @POST("EmpApp/Repair/uploadFinishedRepairImage")
    Call<Result<PhotoModel>> uploadFinishedRepairImage(@Part("community_id") String community, @Part("task_id") String taskID, @Part() MultipartBody.Part pic);//上传业主事件图片

    @FormUrlEncoded
    @POST("EmpApp/Repair/finishRepairTask")
    Call<Result> finishRepairTask(@FieldMap Map<String, String> params);//解决业主事件

    @FormUrlEncoded
    @POST("EmpApp/Repair/writeFeedbackContent")
    Call<Result> writeFeedbackContent(@FieldMap Map<String, String> params);//回访业主事件

    @FormUrlEncoded
    @POST("EmpApp/Cycle/queryCycleTaskListUnassigned")
    Call<Result<List<CycleEventModel>>> queryCycleTask(@FieldMap Map<String, String> params);//查询周期行事务

    @FormUrlEncoded
    @POST("EmpApp/Cycle/queryCycleTaskDetailUnassigned")
    Call<Result<CycleEventDetailModel>> queryCycleTaskDetail(@FieldMap Map<String, String> params);//查询周期事务详情

    @FormUrlEncoded
    @POST("EmpApp/Cycle/queryDirector")
    Call<Result<List<EventAllotModel>>> queryDirector(@FieldMap Map<String, String> params);//查询周期事务负责人

    @FormUrlEncoded
    @POST("EmpApp/Cycle/queryCooperator")
    Call<Result<List<PartnerModel>>> queryPartner(@FieldMap Map<String, String> params);//查询周期事务配合人

    @FormUrlEncoded
    @POST("EmpApp/Cycle/assignCycleTask")
    Call<Result> assignCycleTask(@FieldMap Map<String, String> params);//周期事务分配

    @FormUrlEncoded
    @POST("EmpApp/Cycle/queryCycleTaskList")
    Call<Result<List<CycleTaskModel>>> queryCycleTaskList(@FieldMap Map<String, String> params);//查询事务处理列表

    @FormUrlEncoded
    @POST("EmpApp/Cycle/queryCycleTaskDetail")
    Call<Result<EventHandleDetailModel>> queryHandleDetailTaskDetail(@FieldMap Map<String, String> params);//查询事务处理详情

    @FormUrlEncoded
    @POST("EmpApp/Cycle/acceptCycleTask")
    Call<Result> acceptCycleTask(@FieldMap Map<String, String> params);//接受任务

    @Multipart
    @POST("EmpApp/Cycle/uploadFinishedCycleTaskImage")
    Call<Result<PhotoModel>> uploadFinishedCycleTaskImage(@QueryMap Map<String, String> params, @Part() MultipartBody.Part pic);//上传图片

    @FormUrlEncoded
    @POST("EmpApp/Cycle/finishCycleTask")
    Call<Result> finishCycleTask(@FieldMap Map<String, String> params);//完成周期工作

    /*考勤打卡*/
    @FormUrlEncoded
    @POST("EmpApp/Emp/queryCheckList")
    Call<Result<List<AttedanceModel>>> queryCheckList(@FieldMap Map<String, String> params);//完成周期工作

    @FormUrlEncoded
    @POST("EmpApp/Emp/empCheckByApp")
    Call<Result<List<AttedanceModel>>> empCheckByApp(@FieldMap Map<String, String> params);//打卡

    @FormUrlEncoded
    @POST("EmpApp/Emp/queryScheduleList")
    Call<Result<List<ScheduleModel>>> queryScheduleList(@FieldMap Map<String, String> params);//查询班次

    @FormUrlEncoded
    @POST("EmpApp/Emp/queryCheckStatistics")
    Call<Result<PersonalStatisticsModel>> queryCheckStatistics(@FieldMap Map<String, String> params);//查询个人考勤统计

    /*临时分配*/

    @FormUrlEncoded
    @POST("EmpApp/Temporary/queryClasses")
    Call<Result<List<NearDailyClassModel>>> queryClasses(@FieldMap Map<String, String> params);//查询临时日常分配工作班次

    @FormUrlEncoded
    @POST("EmpApp/Temporary/queryDailyWork")
    Call<Result<List<NearDailyWorkModel>>> queryDailyWork(@Field("community_id") String params);//查询临时日常分配工作内容

    @FormUrlEncoded
    @POST("EmpApp/Temporary/queryDailyWorkType")
    Call<Result<List<DailyWorkTypeModel>>> queryDailyWorkType(@Field("community_id") String params);//查询临时日常分配类型

    @FormUrlEncoded
    @POST("EmpApp/Temporary/queryAreaOrDevice")
    Call<Result<List<NearAreaOrDeviceModel>>> queryAreaOrDevice(@FieldMap Map<String, String> params);//查询临时日常分配区域

    @FormUrlEncoded
    @POST("EmpApp/Temporary/assignTempDailyTask")
    Call<Result> assignTempDailyTask(@FieldMap Map<String, String> params);//临时日常分配区域

    /*临时大事分配*/
    @FormUrlEncoded
    @POST("EmpApp/Temporary/queryCycleWork")
    Call<Result<List<NearDailyWorkModel>>> queryBigCycleWork(@Field("community_id") String params);//临时大事分配 工作内容

    @FormUrlEncoded
    @POST("EmpApp/Temporary/queryDeviceType")
    Call<Result<List<NearBigDeviceTypeModel>>> queryDeviceType(@Field("community_id") String params);//临时大事分配 设备类型

    @FormUrlEncoded
    @POST("EmpApp/Temporary/queryBodyType")
    Call<Result<List<NearBigBodyTypeModel>>> queryBodyType(@Field("community_id") String params);//临时大事分配 主体类型

    @FormUrlEncoded
    @POST("EmpApp/Temporary/queryDeviceNumber")
    Call<Result<List<NearBigDeviceNumModel>>> queryDeviceNumber(@FieldMap Map<String, String> params);//临时大事分配  设备编号

    @FormUrlEncoded
    @POST("EmpApp/Temporary/assignTempCycleTask")
    Call<Result> assignTempCycleTask(@FieldMap Map<String, String> params);//临时大事分配

    @FormUrlEncoded
    @POST("EmpApp/Temporary/assignTempAnyTask")
    Call<Result> assignTempAnyTask(@FieldMap Map<String, String> params);//临时其他分配

    /*事件管理*/
    @FormUrlEncoded
    @POST("EmpApp/Repair/searchRepairRecord")
    Call<Result<List<EventManagerModel>>> searchRepairRecord(@FieldMap Map<String, String> params);//查询事件管理 列表

    @Multipart
    @POST("EmpApp/Repair/uploadRepairImage")
    Call<Result<RepairPicModel>>uploadRepairImage(@Part("community_id")String id, @Part() MultipartBody.Part part);

    @FormUrlEncoded
    @POST("EmpApp/Repair/addRepair")
    Call<Result> addRepair(@FieldMap Map<String, String> params);//添加时间

    @FormUrlEncoded
    @POST("EmpApp/Repair/assistRepairTask")
    Call<Result> assistRepairTask(@FieldMap Map<String, String> params);//分配


    /*消息*/
    @FormUrlEncoded
    @POST("EmpApp/Emp/queryNoticeList")
    Call<Result<List<MessageModel>>> queryNoticeList(@FieldMap Map<String, String> params);

    /*每日工作*/
    @FormUrlEncoded
    @POST("EmpApp/Daily/queryDailyTaskList")
    Call<Result<List<DailyTaskModel>>> queryDailyTaskList(@FieldMap Map<String, String> params);//查询每日工作

    @FormUrlEncoded
    @POST("EmpApp/Daily/acceptDailyTask")
    Call<Result> acceptDailyTask(@FieldMap Map<String, String> params);//接受每日工作

    @Multipart
    @POST("EmpApp/Daily/uploadDailyTaskImage")
    Call<Result<DailyPhotoModel>> uploadDailyTaskImage(@PartMap Map<String, String> params, @Part() MultipartBody.Part part);//上传每日工作照片

    @FormUrlEncoded
    @POST("EmpApp/Daily/finishDailyTask")
    Call<Result> finishDailyTask(@FieldMap Map<String, String> params);//@Part("image_info") RequestBody body

    /*车辆查询*/
    @FormUrlEncoded
    @POST("EmpApp/Visitor/queryCar")
    Call<Result<CarModel>> queryCar(@FieldMap Map<String, String> params);//车辆查询

    @FormUrlEncoded
    @POST("EmpApp/Visitor/queryParkArea")
    Call<Result<List<ParkAreaModel>>> queryParkArea(@Field("community_id") String params);//车位区域查询

    @FormUrlEncoded
    @POST("EmpApp/Visitor/queryParkNum")
    Call<Result<List<ParkNumberModel>>> queryParkNum(@FieldMap Map<String, String> params);//车位编号查询

    /*车辆放行*/
    @FormUrlEncoded
    @POST("EmpApp/Visitor/queryVisitorEntry")
    Call<Result<List<CarVisitorModel>>> queryVisitorEntry(@FieldMap Map<String, String> params);//车辆放行记录

    @FormUrlEncoded
    @POST("EmpApp/Visitor/letPassVisitor")
    Call<Result> letPassVisitor(@FieldMap Map<String, String> params);//车辆放行记录

    @Multipart
    @POST("EmpApp/Visitor/uploadTempTrafficVehicleImage")
    Call<Result<HeadPicModel>> addVisitorCarPic(@Part("community_id")String communityId,@Part() MultipartBody.Part part);//放行车辆车牌号

    @FormUrlEncoded
    @POST("EmpApp/Visitor/addVisitor")
    Call<Result> addVisitor(@FieldMap Map<String, String> params);//车辆放行记录


    /*监督评价*/
    @FormUrlEncoded
    @POST("EmpApp/Supervision/queryDailyWorkStatusList")
    Call<Result<List<SuperviseDailyModel>>> queryDailyWorkStatusList(@FieldMap Map<String, String> params);//监督评价 查询每日工作（人、工作状态）

    @FormUrlEncoded
    @POST("EmpApp/Supervision/queryDailyWorkDetail")
    Call<Result<SuperviseDailyDetailModel>> queryDailyWorkDetail(@FieldMap Map<String, String> params);//监督评价 查询每日工作详情

    @FormUrlEncoded
    @POST("EmpApp/Supervision/evaluateWork")
    Call<Result> evaluateWork(@FieldMap Map<String, String> params);//监督评价 查询每日工作详情

    @FormUrlEncoded
    @POST("EmpApp/Supervision/queryCycleTaskList")
    Call<Result<List<SuperviseHandleModel>>> querySuperviseCycleTaskList(@FieldMap Map<String, String> params);//监督评价 查询事务处理（人、工作状态）

    @FormUrlEncoded
    @POST("EmpApp/Supervision/queryCycleTaskDetail")
    Call<Result<SuperviseHandleDetailModel>> querySuperviseCycleTaskDetail(@FieldMap Map<String, String> params);//监督评价 查询事务处理 详情

    @FormUrlEncoded
    @POST("EmpApp/Supervision/addAssistantPersonOnCycleTask")
    Call<Result> addAssistantPersonOnCycleTask(@FieldMap Map<String, String> params);//监督评价 事务处理追加协助

    @FormUrlEncoded
    @POST("EmpApp/Supervision/reassignCycleTask")
    Call<Result> reassignCycleTask(@FieldMap Map<String, String> params);//监督评价 事务处理再分配


    @FormUrlEncoded
    @POST("EmpApp/Supervision/queryRepairTask")
    Call<Result<List<SuperviseOwnerModel>>> querySuperviseRepairTask(@FieldMap Map<String, String> params);//监督评价 查询业主事件（人、工作状态）

    @FormUrlEncoded
    @POST("EmpApp/Supervision/queryRepairTaskDetail")
    Call<Result<SuperviseOwnerDetailModel>> querySuperviseOwnerDetail(@FieldMap Map<String, String> params);//监督评价 查询业主事件 详情

    @FormUrlEncoded
    @POST("EmpApp/Supervision/addAssistantPersonOnRepairTask")
    Call<Result> addAssistantPersonOnRepairTask(@FieldMap Map<String, String> params);//监督评价 查询业主事件 追加协助

    @FormUrlEncoded
    @POST("EmpApp/Supervision/reassignRepairTask")
    Call<Result> reassignRepairTask(@FieldMap Map<String, String> params);//监督评价 查询业主事件 再分配

    /*员工考勤*/


    @FormUrlEncoded
    @POST("EmpApp/Emp/queryEmpCheckAttendance")
    Call<Result<MasterAttedanModel>> queryEmpCheckAttendance(@FieldMap Map<String, String> params);//查询主管下管理的员工考勤

    @FormUrlEncoded
    @POST("EmpApp/Emp/queryEmpClockinWorkBydate")
    Call<Result<MasterCheckWorkInfoModel>> queryEmpClockinWorkBydate(@FieldMap Map<String, String> params);//根据日期查询每个出勤人员的上班打卡情况

    @FormUrlEncoded
    @POST("EmpApp/Emp/queryEmpCheckStatistics")
    Call<Result<MasterAttendMonthModel>> queryEmpCheckStatistics(@FieldMap Map<String, String> params);

    /*业主查询*/
    @FormUrlEncoded
    @POST("EmpApp/Owner/queryHouseholder")
    Call<Result<OwnerInfoModel>> queryHouseholder(@FieldMap Map<String, String> params);//查询业主信息

    /*催费事务*/
    @FormUrlEncoded
    @POST("EmpApp/UrgePay/queryUrgePayList")
    Call<Result<List<PressMoneyModel>>> queryUrgePayList(@FieldMap Map<String, String> params);//查询催费列表

    @FormUrlEncoded
    @POST("EmpApp/UrgePay/queryUrgePayDetails")
    Call<Result<PressMoneyDetailModel>> queryUrgePayDetails(@FieldMap Map<String, String> params);//催费记录详情

    @FormUrlEncoded
    @POST("EmpApp/UrgePay/addUrgePayInfo")
    Call<Result> addUrgePayInfo(@FieldMap Map<String, String> params);//催费详情记录


  /*------------公共查询------------*/

    @FormUrlEncoded
    @POST("EmpApp/Public/queryBuild")
    Call<Result<List<BuildModel>>> queryBuild(@FieldMap Map<String, String> params);//查询栋

    @FormUrlEncoded
    @POST("EmpApp/Public/queryUnit")
    Call<Result<List<UnitModel>>> queryUnit(@FieldMap Map<String, String> params);//查询单元

    @FormUrlEncoded
    @POST("EmpApp/Public/queryRoom")
    Call<Result<List<RoomModel>>> queryRoom(@FieldMap Map<String, String> params);//查询门牌号

    @FormUrlEncoded
    @POST("EmpApp/Repair/queryRepairTypeList")
    Call<Result<List<RepairTypeModel>>> queryRepairTypeList(@Field("community_id") String parm);//查询事件类型

    @FormUrlEncoded
    @POST("EmpApp/Public/queryServiceProvider")
    Call<Result<List<ServiceModel>>> queryServiceProvider(@Field("community_id") String parm);//查询服务类型

    @FormUrlEncoded
    @POST("EmpApp/Public/queryDepartmentByServiceType")
    Call<Result<List<DepartmentServiceModel>>> queryDepartmentByServiceType(@FieldMap Map<String, String> params);//根据服务商类型查询部门工种

    @FormUrlEncoded
    @POST("EmpApp/Public/queryLeaderByWorkType")
    Call<Result<List<EventAllotModel>>> queryLeaderByWorkType(@FieldMap Map<String, String> params);//根据部门查询负责人
}
