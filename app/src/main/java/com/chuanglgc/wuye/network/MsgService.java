package com.chuanglgc.wuye.network;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.activity.message_notify.MessageNotifyActivity;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;

//接受推送后的通知栏
public class MsgService extends Service {
    private Notification getNotification(String s) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        //使用RemoteViews进行自定义通知栏
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.service_msg);

        Intent goMsg = new Intent(this, MessageNotifyActivity.class);
        PendingIntent intentMes = PendingIntent.getActivity(this, 0, goMsg, FLAG_UPDATE_CURRENT);

       // remoteViews.setOnClickPendingIntent(R.id.rl_root, canclePendingIntent);

        builder.setContent(remoteViews);
        remoteViews.setTextViewText(R.id.tv_info, s);
        builder.setSmallIcon(R.mipmap.logo);
        builder.setWhen(SystemClock.currentThreadTimeMillis());
        builder.setContentIntent(intentMes);
        builder.setAutoCancel(true);
        builder.setDefaults(Notification.DEFAULT_ALL);
        return builder.build();
    }

    /**
     * 获取到系统NotificationManager
     *
     * @return
     */
    private NotificationManager getNotificationManager() {
        return (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
                String info = intent.getStringExtra("info");
                getNotificationManager().notify(1, getNotification(info));
        }
        return super.onStartCommand(intent, flags, startId);
    }

}