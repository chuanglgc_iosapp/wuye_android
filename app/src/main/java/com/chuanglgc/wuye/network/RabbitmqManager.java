package com.chuanglgc.wuye.network;

import android.content.Intent;
import android.media.MediaPlayer;
import android.util.Log;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.activity.MainActivity;
import com.chuanglgc.wuye.model.MsgModle;
import com.google.gson.Gson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;


public class RabbitmqManager {
    public static String TGA = RabbitmqManager.class.getName();
    private ConnectionFactory factory = new ConnectionFactory();
    private Thread subscribeThread;
    private Thread publishThread;
    private BlockingDeque<String> queue = new LinkedBlockingDeque<String>();
    private static RabbitmqManager mInstance = null;
    private String queueName = "user_wuye_app_qu_" + EmployeeApplication.getInstance().getUserInfoFromCache().getPhone_number();
    private MainActivity activity;
    private MediaPlayer player;


    public static RabbitmqManager getInstance() {
        if (null == mInstance) {
            mInstance = new RabbitmqManager();
        }
        return mInstance;
    }

    /**
     * 释放单例, 及其所引用的资源
     */
    public static void release() {
        try {
            if (mInstance != null) {
                mInstance.onClose();
                mInstance = null;
            }
        } catch (Exception e) {
        }
    }

    /**
     * 设置连接属性
     *
     * @param url          地址
     * @param username     用户名
     * @param password     密码
     * @param mainActivity
     */
    public void setupConnectionFactory(String url, int port, String username, String password, MainActivity mainActivity) {
        this.activity = mainActivity;
        try {
//            factory.setAutomaticRecoveryEnabled(false);
            //factory.setUri(url);
            factory.setHost(url);
            factory.setPort(port);
            factory.setVirtualHost(EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            factory.setUsername(username);
            factory.setPassword(password);
            //factory.setAutomaticRecoveryEnabled(true);
            //factory.setNetworkRecoveryInterval(10000);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    /**
     * 发送消息
     *
     * @param message
     */
    public void publishMessage(String message) {
        try {
            Log.d("", "[q] " + message);
            queue.putLast(message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 订阅消息
     *
     * @param RABBITMQ_EXCHANGE
     * @param RABBITMQ_ROUTINGKEY
     */
    public void subscribe(final String RABBITMQ_EXCHANGE, final String RABBITMQ_ROUTINGKEY) {
        subscribeThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Connection connection = factory.newConnection();
                        Channel channel = connection.createChannel();
                        channel.basicQos(1);
                        AMQP.Queue.DeclareOk q = channel.queueDeclare(queueName, true, false, false, null);// 声明共享队列
                        channel.queueBind(queueName, RABBITMQ_EXCHANGE, RABBITMQ_ROUTINGKEY);
                        QueueingConsumer consumer = new QueueingConsumer(channel);
                        channel.basicConsume(queueName, true, consumer);
                        while (true) {
                            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                            String message = new String(delivery.getBody());
                            Intent intent = new Intent(activity, MsgService.class);
                            Gson gson = new Gson();
                            MsgModle msgModle = gson.fromJson(message, MsgModle.class);
                            intent.putExtra("info", msgModle.getData());
                            activity.startService(intent);
                        }
                    } catch (InterruptedException e) {
                        break;
                    } catch (Exception e1) {
                        try {
                            Thread.sleep(5000); //sleep and then try again
                        } catch (InterruptedException e) {
                            break;
                        }
                    }
                }
            }
        });
        subscribeThread.start();
    }

    /**
     * 建立连接
     *
     * @param RABBITMQ_EXCHANGE
     * @param RABBITMQ_ROUTINGKEY
     */
    public void publishToAMQP(final String RABBITMQ_EXCHANGE, final String RABBITMQ_ROUTINGKEY) {
        publishThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Connection connection = factory.newConnection();
                        Channel ch = connection.createChannel();
                        ch.confirmSelect();
                        while (true) {
                            String message = queue.takeFirst();
                            try {
                                ch.basicPublish(RABBITMQ_EXCHANGE, RABBITMQ_ROUTINGKEY, null, message.getBytes());
                                Log.e("", "[s] " + message);
                                ch.waitForConfirmsOrDie();
                            } catch (Exception e) {
                                Log.e("", "[f] " + message);
                                queue.putFirst(message);
                                throw e;
                            }
                        }
                    } catch (InterruptedException e) {
                        break;
                    } catch (Exception e) {
                        Log.e("", "Connection broken: " + e.getClass().getName());
                        try {
                            Thread.sleep(5000); //sleep and then try again
                        } catch (InterruptedException e1) {
                            break;
                        }
                    }
                }
            }
        });
        publishThread.start();
    }

    /**
     * 关闭连接
     */
    public void onClose() {
        if (publishThread!=null)publishThread.interrupt();
        if (subscribeThread!=null)subscribeThread.interrupt();
    }

}
