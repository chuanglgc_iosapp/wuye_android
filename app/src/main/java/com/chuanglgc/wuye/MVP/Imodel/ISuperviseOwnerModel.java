package com.chuanglgc.wuye.MVP.Imodel;


import com.chuanglgc.wuye.model.SuperviseOwnerDetailModel;


import java.util.HashMap;

public interface ISuperviseOwnerModel {
    void requestOwnerDetail(HashMap<String, String> map, IRequestListener<SuperviseOwnerDetailModel> listener);//查询每日工作详情

    void requestMaster(HashMap<String, String> map, IRequestListener listener);//负责人

    void requestPartner(HashMap<String, String> map, IRequestListener listener);//获取配合人


    void requestAllot(HashMap<String, String> map, IRequestListener listener);//再分配


    void requestHelp(HashMap<String, String> map, IRequestListener listener);//追加协助

    void requestEvaluateWork(HashMap<String, String> map, IRequestListener listener);//提交评价

}
