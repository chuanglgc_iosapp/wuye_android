package com.chuanglgc.wuye.MVP.IView;


import com.chuanglgc.wuye.model.EventHandleDetailModel;
import com.chuanglgc.wuye.model.PhotoModel;

public interface IHandleEventDetailView {
    void getTaskDetail(EventHandleDetailModel model);

    void commitTaskResult(String result);

    void upLoadPhotoResult(PhotoModel model);

    void acceptTaskResult(String result);

    void requestFailResult(String result);
}
