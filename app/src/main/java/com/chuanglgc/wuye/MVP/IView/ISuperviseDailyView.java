package com.chuanglgc.wuye.MVP.IView;



import com.chuanglgc.wuye.model.SuperviseDailyDetailModel;

public interface ISuperviseDailyView {
    void getDailyDetilModel(SuperviseDailyDetailModel model);

    void getEvalueResult(String result);

    void getNetError(String error);
}
