package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.IOwnerEventFinishView;
import com.chuanglgc.wuye.MVP.Imodel.IOwnerEventFinishModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.modelImpl.OwnerEventFinishImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.PhotoModel;
import com.chuanglgc.wuye.network.Result;

import java.util.Map;

import okhttp3.MultipartBody;


public class OwnerEventFinishPersent extends BasePersenter<IOwnerEventFinishView> {
    private  IOwnerEventFinishModel iModel = new OwnerEventFinishImpl();
    private IOwnerEventFinishView iView;
    //上传图片
    public void upLoadPhoto(String community,String taskID, MultipartBody.Part parts) {
        if (iView == null) iView = weakReference.get();
        iModel.uploadFinishedRepairImage(community,taskID, parts, new IRequestListener<PhotoModel>() {

            @Override
            public void onSuccess(Result<PhotoModel> result) {
                if (result.isResult()) {
                    iView.getUploadPhotoResult(result.getData());
                } else {
                    iView.requestNetFail(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.requestNetFail(wrong);
            }
        });
    }
   //解决业主事件
    public void finishOwnerEvent(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.finishOwnerEvent(map, new IRequestListener() {
            @Override
            public void onSuccess(Result result) {
                if (result.isResult()){
                    iView.getFinishEventResult(result.getMsg());
                }else {
                    iView.requestNetFail(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.requestNetFail(wrong);
            }
        });
    }
}
