package com.chuanglgc.wuye.MVP.modelImpl;


import com.chuanglgc.wuye.MVP.Imodel.IPressDetailModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.model.PressMoneyDetailModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PressMoneyDetailImpl implements IPressDetailModel {
    @Override
    public void requestPressDetail(Map<String, String> map, IRequestListener<PressMoneyDetailModel> listener) {
        RestClient.getAPIService().queryUrgePayDetails(map).enqueue(new Callback<Result<PressMoneyDetailModel>>() {
            @Override
            public void onResponse(Call<Result<PressMoneyDetailModel>> call, Response<Result<PressMoneyDetailModel>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<PressMoneyDetailModel>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    @Override
    public void addPressRecord(Map<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().addUrgePayInfo(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code()==200){
                    listener.onSuccess(response.body());
                }else {
                    listener.onFauiler(response.code()+"");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }
}
