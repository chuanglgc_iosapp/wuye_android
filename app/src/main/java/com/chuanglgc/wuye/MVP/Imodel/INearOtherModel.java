package com.chuanglgc.wuye.MVP.Imodel;


import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.PartnerModel;

import java.util.List;
import java.util.Map;

public interface INearOtherModel {
    void requestMaster(Map<String, String> map, IRequestListener<List<EventAllotModel>> listener);

    void requestPartner(Map<String, String> map, IRequestListener<List<PartnerModel>> listener);

    void assignTempDailyTask(Map<String, String> map, IRequestListener listener);
}
