package com.chuanglgc.wuye.MVP.Imodel;


import java.util.Map;

import okhttp3.MultipartBody;

public interface IOwnerEventFinishModel {

    void uploadFinishedRepairImage(String community,String taskID, MultipartBody.Part parts, IRequestListener listener);

    void finishOwnerEvent(Map<String, String> map, IRequestListener listener);
}
