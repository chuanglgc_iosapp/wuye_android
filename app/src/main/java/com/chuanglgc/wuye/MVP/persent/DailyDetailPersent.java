package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.IDailyDetailView;
import com.chuanglgc.wuye.MVP.Imodel.IDailyDetailModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.modelImpl.DailyDetailImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.DailyPhotoModel;
import com.chuanglgc.wuye.model.PostDailyPhotoModel;
import com.chuanglgc.wuye.network.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class DailyDetailPersent extends BasePersenter<IDailyDetailView> {

    IDailyDetailView iView;
    private IDailyDetailModel iModel = new DailyDetailImpl();
   //接受日常工作
    public void acceptDailyTask(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.acceptTask(map, new IRequestListener() {
            @Override
            public void onSuccess(Result result) {
                if (result.isResult()) {
                    iView.acceptResult(result.getMsg());
                } else {
                    iView.requestNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.requestNetError(wrong);
            }
        });
    }
    //上传日常任务图片
    public void uploadDailyPhoto(Map<String, String> map,  MultipartBody.Part part) {
        if (iView == null) iView = weakReference.get();
        iModel.uploadPhoto(map, part, new IRequestListener<DailyPhotoModel>() {
            @Override
            public void onSuccess(Result<DailyPhotoModel> result) {
                if (result.isResult()) {
                    iView.uploadPhotoResult(result.getData());
                } else {
                    iView.requestNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.requestNetError(wrong);
            }
        });
    }

    //完成日常工作
    public void finishDailyTask(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.finishTask(map, new IRequestListener() {
            @Override
            public void onSuccess(Result result) {
                if (result.isResult()) {
                    iView.finishResult(result.getMsg());
                } else {
                    iView.requestNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.requestNetError(wrong);
            }
        });
    }
}
