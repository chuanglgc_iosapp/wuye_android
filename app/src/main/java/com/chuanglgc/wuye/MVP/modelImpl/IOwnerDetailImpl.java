package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.IOwnerDetailModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.model.OwnerEventDetailModel;
import com.chuanglgc.wuye.model.VisitRecordModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class IOwnerDetailImpl implements IOwnerDetailModel {


    //接受业主事件

    @Override
    public void acceptOwnerEvent(Map<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().acceptRepairTask(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                listener.onFauiler(t.toString() + "");
            }
        });
    }

    /**
     * 请求任务详情
     *
     * @param map
     * @param listener
     */
    @Override
    public void requestTaskDetail(Map<String, String> map, IRequestListener<OwnerEventDetailModel> listener) {
        RestClient.getAPIService().queryRepairTaskDetail(map).enqueue(new Callback<Result<OwnerEventDetailModel>>() {
            @Override
            public void onResponse(Call<Result<OwnerEventDetailModel>> call, Response<Result<OwnerEventDetailModel>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<OwnerEventDetailModel>> call, Throwable t) {
                listener.onFauiler(t + "");
            }
        });
    }

    @Override
    public void queryFeedbackContent(Map<String, String> map, IRequestListener<List<VisitRecordModel>> listener) {
        RestClient.getAPIService().queryFeedbackContent(map).enqueue(new Callback<Result<List<VisitRecordModel>>>() {
            @Override
            public void onResponse(Call<Result<List<VisitRecordModel>>> call, Response<Result<List<VisitRecordModel>>> response) {
                if (response.code()==200){
                    listener.onSuccess(response.body());
                }else {
                    listener.onFauiler(response.code()+"");
                }
            }

            @Override
            public void onFailure(Call<Result<List<VisitRecordModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }
}
