package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.ILoginView;
import com.chuanglgc.wuye.MVP.IView.INormalView;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.Imodel.IRequestModel;
import com.chuanglgc.wuye.MVP.modelImpl.ICycleEventDetailImpl;
import com.chuanglgc.wuye.MVP.modelImpl.ILoginModeImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.CycleEventDetailModel;
import com.chuanglgc.wuye.network.Result;

import java.util.Map;


public class CycleEventDetailPersent extends BasePersenter<INormalView> {
    private IRequestModel iModel = new ICycleEventDetailImpl();
    private INormalView iView;

    public void requestDetail(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestInfo(map, new IRequestListener<CycleEventDetailModel>() {
            @Override
            public void onSuccess(Result<CycleEventDetailModel> result) {
                if (result.isResult()) {
                    iView.getNetResult(result.getData());
                } else {
                    iView.getNetFauiler(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetFauiler(wrong);
            }
        });
    }
}
