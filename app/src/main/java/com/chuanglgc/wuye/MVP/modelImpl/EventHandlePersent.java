package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.IView.IEventHandleView;
import com.chuanglgc.wuye.MVP.Imodel.IEventHandleModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.DepartmentServiceModel;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.ServiceModel;
import com.chuanglgc.wuye.network.Result;

import java.util.HashMap;
import java.util.List;


public class EventHandlePersent extends BasePersenter<IEventHandleView> {
    private IEventHandleModel iModel = new EventHandleImpl();
    private IEventHandleView iView;

    //请求服务商
    public void reqeustService(String community) {
        if (iView == null) iView = weakReference.get();
        iModel.queryServiceProvider(community, new IRequestListener<List<ServiceModel>>() {
            @Override
            public void onSuccess(Result<List<ServiceModel>> result) {
                if (result.isResult()) {
                    iView.getServiceProvider(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    public void requestDepartment(HashMap<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.queryDepartmentByServiceType(map, new IRequestListener<List<DepartmentServiceModel>>() {
            @Override
            public void onSuccess(Result<List<DepartmentServiceModel>> result) {
                if (result.isResult()) {
                    iView.getDepartmentByServiceType(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    public void requestLeaderByWorkType(HashMap<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.queryLeaderByWorkType(map, new IRequestListener<List<EventAllotModel>>() {
            @Override
            public void onSuccess(Result<List<EventAllotModel>> result) {
                if (result.isResult()) {
                    iView.getLeaderByWorkType(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    public void handleEvent(HashMap<String, String> map) {
        if (iView == null) weakReference.get();
        iModel.assistRepairTask(map, new IRequestListener() {
            @Override
            public void onSuccess(Result result) {
                if (result.isResult()) {
                    iView.getRepairTask(result.getMsg());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }
}
