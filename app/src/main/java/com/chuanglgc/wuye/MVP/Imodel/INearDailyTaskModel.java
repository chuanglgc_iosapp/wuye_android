package com.chuanglgc.wuye.MVP.Imodel;

import com.chuanglgc.wuye.model.DailyWorkTypeModel;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.NearAreaOrDeviceModel;
import com.chuanglgc.wuye.model.NearDailyClassModel;
import com.chuanglgc.wuye.model.NearDailyWorkModel;
import com.chuanglgc.wuye.model.PartnerModel;

import java.util.List;
import java.util.Map;

//临时日常分配接口
public interface INearDailyTaskModel {
    void requestMaster(Map<String, String> map, IRequestListener<List<EventAllotModel>> listener);

    void requestClass(Map<String, String> map, IRequestListener<List<NearDailyClassModel>> listener);

    void requestWorkContent(String community, IRequestListener<List<NearDailyWorkModel>> listener);

    void requestDailyWorkType(String community, IRequestListener<List<DailyWorkTypeModel>> listener);

    void requestAreaOrDevice(Map<String, String> map, IRequestListener<List<NearAreaOrDeviceModel>> listener);

    void assignTempDailyTask(Map<String,String>map,IRequestListener listener);


}
