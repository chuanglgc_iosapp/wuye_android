package com.chuanglgc.wuye.MVP.IView;


import com.chuanglgc.wuye.model.DepartmentServiceModel;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.ServiceModel;

import java.util.List;

public interface IEventHandleView {

    void getServiceProvider(List<ServiceModel> modelList);//查询服务上类型

    void getDepartmentByServiceType(List<DepartmentServiceModel> modelList);//查询归属部门

    void getLeaderByWorkType(List<EventAllotModel> modelList);//部门查询负责人

    void getRepairTask(String result);//分配

    void getNetError(String error);
}
