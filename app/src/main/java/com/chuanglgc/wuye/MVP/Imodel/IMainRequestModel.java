package com.chuanglgc.wuye.MVP.Imodel;

import java.util.Map;

import okhttp3.MultipartBody;


public interface IMainRequestModel {
    //请求信息
    void requestInfo(Map<String, String> map, IRequestListener iRequestListener);
    void uploadPic(String id, MultipartBody.Part part, IRequestListener iRequestListener);
}
