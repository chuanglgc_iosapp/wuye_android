package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.IAttendanceModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.model.AttedanceModel;
import com.chuanglgc.wuye.model.CheckModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AttendanceImpl implements IAttendanceModel {
    @Override
    public void queryCheckList(Map<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().queryCheckList(map).enqueue(new Callback<Result<List<AttedanceModel>>>() {
            @Override
            public void onResponse(Call<Result<List<AttedanceModel>>> call, Response<Result<List<AttedanceModel>>> response) {
                if (response.code()==200){
                    listener.onSuccess(response.body());
                }else {
                    listener.onFauiler(response.code()+"");
                }
            }

            @Override
            public void onFailure(Call<Result<List<AttedanceModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    @Override
    public void queryCheckByApp(Map<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().empCheckByApp(map).enqueue(new Callback<Result<List<AttedanceModel>>>() {
            @Override
            public void onResponse(Call<Result<List<AttedanceModel>>> call, Response<Result<List<AttedanceModel>>> response) {
                if (response.code()==200){
                    listener.onSuccess(response.body());
                }else {
                    listener.onFauiler(response.code()+"");
                }
            }

            @Override
            public void onFailure(Call<Result<List<AttedanceModel>>> call, Throwable t) {
                listener.onFauiler(t.toString()+"");
            }
        });
    }
}
