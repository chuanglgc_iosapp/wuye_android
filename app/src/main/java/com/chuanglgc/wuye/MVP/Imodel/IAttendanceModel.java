package com.chuanglgc.wuye.MVP.Imodel;

import java.util.Map;



public interface IAttendanceModel {
    void queryCheckList(Map<String,String>map ,IRequestListener listener);

    void queryCheckByApp(Map<String,String>map,IRequestListener listener);
}
