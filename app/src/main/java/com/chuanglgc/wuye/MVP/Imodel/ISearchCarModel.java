package com.chuanglgc.wuye.MVP.Imodel;


import com.chuanglgc.wuye.model.CarModel;

import java.util.HashMap;
import java.util.List;

public interface ISearchCarModel {
    void queryCardInfo(HashMap<String, String> map, IRequestListener<CarModel> listener);//查询车辆信息

    void queryParkArea(HashMap<String, String> map, IRequestListener<List<String>> listener);//查询车位区域

    void queryParkNumber(HashMap<String, String> map, IRequestListener<List<String>> listener);//查询车位编号
}
