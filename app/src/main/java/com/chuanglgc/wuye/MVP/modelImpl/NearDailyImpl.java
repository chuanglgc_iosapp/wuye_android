package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.INearDailyTaskModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.model.DailyWorkTypeModel;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.NearAreaOrDeviceModel;
import com.chuanglgc.wuye.model.NearDailyClassModel;
import com.chuanglgc.wuye.model.NearDailyWorkModel;
import com.chuanglgc.wuye.model.ScheduleModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NearDailyImpl implements INearDailyTaskModel {
    @Override
    public void requestMaster(Map<String, String> map, IRequestListener<List<EventAllotModel>> listener) {
        RestClient.getAPIService().queryDirector(map).enqueue(new Callback<Result<List<EventAllotModel>>>() {
            @Override
            public void onResponse(Call<Result<List<EventAllotModel>>> call, Response<Result<List<EventAllotModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<EventAllotModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    @Override
    public void requestClass(Map<String, String> map, IRequestListener<List<NearDailyClassModel>> listener) {
        RestClient.getAPIService().queryClasses(map).enqueue(new Callback<Result<List<NearDailyClassModel>>>() {
            @Override
            public void onResponse(Call<Result<List<NearDailyClassModel>>> call, Response<Result<List<NearDailyClassModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<NearDailyClassModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    //请求工作内容
    @Override
    public void requestWorkContent(String community, IRequestListener<List<NearDailyWorkModel>> listener) {
        RestClient.getAPIService().queryDailyWork(community).enqueue(new Callback<Result<List<NearDailyWorkModel>>>() {
            @Override
            public void onResponse(Call<Result<List<NearDailyWorkModel>>> call, Response<Result<List<NearDailyWorkModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<NearDailyWorkModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });

    }

    @Override
    public void requestDailyWorkType(String community, IRequestListener<List<DailyWorkTypeModel>> listener) {
        RestClient.getAPIService().queryDailyWorkType(community).enqueue(new Callback<Result<List<DailyWorkTypeModel>>>() {
            @Override
            public void onResponse(Call<Result<List<DailyWorkTypeModel>>> call, Response<Result<List<DailyWorkTypeModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<DailyWorkTypeModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    @Override
    public void requestAreaOrDevice(Map<String, String> map, IRequestListener<List<NearAreaOrDeviceModel>> listener) {
        RestClient.getAPIService().queryAreaOrDevice(map).enqueue(new Callback<Result<List<NearAreaOrDeviceModel>>>() {
            @Override
            public void onResponse(Call<Result<List<NearAreaOrDeviceModel>>> call, Response<Result<List<NearAreaOrDeviceModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<NearAreaOrDeviceModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }
    //分配日常工作
    @Override
    public void assignTempDailyTask(Map<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().assignTempDailyTask(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }


}
