package com.chuanglgc.wuye.MVP.persent;


import com.chuanglgc.wuye.MVP.IView.ISuperviseHandleView;

import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.Imodel.ISuperviseHandleModel;
import com.chuanglgc.wuye.MVP.modelImpl.SuperviseHandleImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.PartnerModel;
import com.chuanglgc.wuye.model.SuperviseDailyDetailModel;
import com.chuanglgc.wuye.model.SuperviseHandleDetailModel;
import com.chuanglgc.wuye.model.SuperviseHandleModel;
import com.chuanglgc.wuye.network.Result;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SuperviseHandlePersent extends BasePersenter<ISuperviseHandleView> {
    private ISuperviseHandleView iView;
    private ISuperviseHandleModel iModel = new SuperviseHandleImpl();
    //查询详情
    public void requestDetailInfo(HashMap<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestHandleDetail(map, new IRequestListener<SuperviseHandleDetailModel>() {
            @Override
            public void onSuccess(Result<SuperviseHandleDetailModel> result) {
                if (result.isResult()) {
                    iView.getHandleDetail(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    //再分配
    public void requestMaster(HashMap<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestMaster(map, new IRequestListener<List<EventAllotModel>>() {
            @Override
            public void onSuccess(Result<List<EventAllotModel>> result) {
                if (result.isResult()) {
                    iView.getMasterList(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    //获取配合人
    public void requsetPartner(HashMap<String, String> map) {
        iModel.requestPartner(map, new IRequestListener<List<PartnerModel>>() {
            @Override
            public void onSuccess(Result<List<PartnerModel>> result) {
                if (result.isResult()) {
                    iView.getPartner(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    //评价
    public void requestEvalue(HashMap<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestEvaluateWork(map, new IRequestListener() {
            @Override
            public void onSuccess(Result result) {
                if (result.isResult()) {
                    iView.getEvalueResult(result.getMsg());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }
    //再分配
    public void requestAllot(HashMap<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestAllot(map, new IRequestListener() {
            @Override
            public void onSuccess(Result result) {
                if (result.isResult()) {
                    iView.getAllotTaskResult(result.getMsg());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }
    //追加协助
    public void requestHelp(HashMap<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestHelp(map, new IRequestListener() {
            @Override
            public void onSuccess(Result result) {
                if (result.isResult()) {
                    iView.getHelpTaskResult(result.getMsg());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }
}
