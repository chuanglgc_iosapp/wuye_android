package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.IHandleEventDetailModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.model.EventHandleDetailModel;
import com.chuanglgc.wuye.model.PhotoModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LogUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HandleEventDetailImpl implements IHandleEventDetailModel {
    /*
     *查询事务处理详情
     */
    @Override
    public void queryCycleTaskDetail(Map<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().queryHandleDetailTaskDetail(map).enqueue(new Callback<Result<EventHandleDetailModel>>() {
            @Override
            public void onResponse(Call<Result<EventHandleDetailModel>> call, Response<Result<EventHandleDetailModel>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<EventHandleDetailModel>> call, Throwable t) {
                listener.onFauiler(t.toString());
                LogUtil.e("返回结果", t.toString());
            }
        });
    }

    @Override
    public void acceptCycleTask(Map<String, String> map, IRequestListener listener) {
         RestClient.getAPIService().acceptCycleTask(map).enqueue(new Callback<Result>() {
             @Override
             public void onResponse(Call<Result> call, Response<Result> response) {
                 if (response.code()==200){
                     listener.onSuccess(response.body());
                 }else {
                     listener.onFauiler(response.code()+"");
                 }
             }

             @Override
             public void onFailure(Call<Result> call, Throwable t) {
                 listener.onFauiler(t.toString()+"");
             }
         });

    }

    //上传图片
    @Override
    public void uploadFinishedCycleTaskImage(Map<String, String> map, MultipartBody.Part part, IRequestListener listener) {
        RestClient.getAPIService().uploadFinishedCycleTaskImage(map, part).enqueue(new Callback<Result<PhotoModel>>() {
            @Override
            public void onResponse(Call<Result<PhotoModel>> call, Response<Result<PhotoModel>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<PhotoModel>> call, Throwable t) {
                listener.onFauiler(t.toString() + "");
            }
        });

    }

    //完成周期性事务
    @Override
    public void finishCycleTask(Map<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().finishCycleTask(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                listener.onFauiler(t.toString() + "");
            }
        });
    }


}
