package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.ICycleEventAllotView;
import com.chuanglgc.wuye.MVP.Imodel.ICycleEventAllotModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.modelImpl.CycleEventAllotImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.PartnerModel;
import com.chuanglgc.wuye.network.Result;

import java.util.List;
import java.util.Map;


public class CycleEventAllotPersent extends BasePersenter<ICycleEventAllotView> {
    private ICycleEventAllotView iView;
    private ICycleEventAllotModel iModel = new CycleEventAllotImpl();

    /**
     * 请求负责人
     *
     * @param map
     */
    public void queryMaster(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestMaster(map, new IRequestListener<List<EventAllotModel>>() {
            @Override
            public void onSuccess(Result<List<EventAllotModel>> result) {
                if (result.isResult()) {
                    iView.getAllotPerson(result.getData());
                } else {
                    iView.onRequestError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.onRequestError(wrong);
            }
        });
    }

    /**
     * 请求配合人
     *
     * @param map
     */
    public void queryPartner(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestPartner(map, new IRequestListener<List<PartnerModel>>() {
            @Override
            public void onSuccess(Result<List<PartnerModel>> result) {
                if (result.isResult()) {
                    iView.getAllotPartner(result.getData());
                } else {
                    iView.onRequestError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.onRequestError(wrong);
            }
        });
    }

    /**
     * 分配任务
     *
     * @param map
     */
    public void allotTask(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestAllot(map, new IRequestListener() {
            @Override
            public void onSuccess(Result result) {
                iView.getAllotResult(result.getMsg());
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getAllotResult(wrong);
            }
        });
    }

}
