package com.chuanglgc.wuye.MVP.IView;

import com.chuanglgc.wuye.model.HeadPicModel;
import com.chuanglgc.wuye.model.LoginModel;
import com.chuanglgc.wuye.model.PhotoModel;
import com.chuanglgc.wuye.network.Result;

import java.util.List;


public interface IMainView {
    void getFunction(List<LoginModel.MenuBean>menuBeanResult);
    void getUpPicResult(HeadPicModel model);
    void getFunctionErrow(String error);
}
