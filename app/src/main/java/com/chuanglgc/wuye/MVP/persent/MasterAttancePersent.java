package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.IMasterAttendView;
import com.chuanglgc.wuye.MVP.Imodel.IMasterAttendModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.modelImpl.MasterAttendImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.MasterAttedanModel;
import com.chuanglgc.wuye.model.MasterCheckWorkInfoModel;
import com.chuanglgc.wuye.network.Result;

import java.util.HashMap;

public class MasterAttancePersent extends BasePersenter<IMasterAttendView> {
    IMasterAttendModel iModel = new MasterAttendImpl();
    private IMasterAttendView iView;

    //查询员工考勤
    public void requestAttendance(HashMap<String, String> map) {
        if (iView == null) iView=weakReference.get();
        iModel.queryEmpCheckAttendance(map, new IRequestListener<MasterAttedanModel>() {
            @Override
            public void onSuccess(Result<MasterAttedanModel> result) {
                if (result.isResult()) {
                    iView.getEmpCheckAttendance(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    //查询员工个人考勤
    public void requestAttendWork(HashMap<String, String> map) {
        if (iView == null) iView=weakReference.get();
        iModel.queryEmpClockinWorkBydate(map, new IRequestListener<MasterCheckWorkInfoModel>() {
            @Override
            public void onSuccess(Result<MasterCheckWorkInfoModel> result) {
                if (result.isResult()) {
                    iView.getEmpClockinWorkBydate(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }
}
