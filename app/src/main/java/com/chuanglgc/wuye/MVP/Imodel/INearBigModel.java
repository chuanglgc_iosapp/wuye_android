package com.chuanglgc.wuye.MVP.Imodel;

import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.NearBigBodyTypeModel;
import com.chuanglgc.wuye.model.NearBigDeviceNumModel;
import com.chuanglgc.wuye.model.NearBigDeviceTypeModel;
import com.chuanglgc.wuye.model.NearDailyWorkModel;
import com.chuanglgc.wuye.model.PartnerModel;

import java.util.List;
import java.util.Map;

public interface INearBigModel {

    void requestMaster(Map<String, String> map, IRequestListener<List<EventAllotModel>> listener);

    void requestPartner(Map<String, String> map, IRequestListener<List<PartnerModel>> listener);

    void requestWorkContent(String community, IRequestListener<List<NearDailyWorkModel>> listener);

    void requestDeviceType(String community, IRequestListener<List<NearBigDeviceTypeModel>> listener);//获取设备类型

    void requestCycleWorkType(String community, IRequestListener<List<NearBigBodyTypeModel>> listener);//请求主体类型

    void requestDeviceNumber(Map<String, String> map, IRequestListener<List<NearBigDeviceNumModel>> listener);//获取设备编号

    void assignTempDailyTask(Map<String, String> map, IRequestListener listener);
}
