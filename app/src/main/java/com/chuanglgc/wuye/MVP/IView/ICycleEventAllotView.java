package com.chuanglgc.wuye.MVP.IView;


import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.PartnerModel;

import java.util.List;

public interface ICycleEventAllotView {

     void getAllotPerson(List<EventAllotModel> list);
     void getAllotPartner(List<PartnerModel> list);
     void getAllotResult(String result);

     void onRequestError(String wrong);

}
