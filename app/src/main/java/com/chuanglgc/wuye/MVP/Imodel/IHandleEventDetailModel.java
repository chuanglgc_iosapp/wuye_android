package com.chuanglgc.wuye.MVP.Imodel;
import java.util.Map;
import okhttp3.MultipartBody;



public interface IHandleEventDetailModel {
    void queryCycleTaskDetail(Map<String, String> map, IRequestListener listener);

    void acceptCycleTask(Map<String, String> map, IRequestListener listener);

    void uploadFinishedCycleTaskImage(Map<String, String> map, MultipartBody.Part parts, IRequestListener listener);

    void finishCycleTask(Map<String, String> map, IRequestListener listener);
}
