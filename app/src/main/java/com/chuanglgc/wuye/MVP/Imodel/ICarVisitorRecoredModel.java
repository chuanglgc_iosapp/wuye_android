package com.chuanglgc.wuye.MVP.Imodel;


import com.chuanglgc.wuye.model.HeadPicModel;

import java.util.Map;

import okhttp3.MultipartBody;

public interface ICarVisitorRecoredModel {
    void requestCommitRecord(Map<String,String>map,IRequestListener listener);

    void requestCommitCarPicResult(String commnutiyID, MultipartBody.Part body,IRequestListener<HeadPicModel> listener);

}
