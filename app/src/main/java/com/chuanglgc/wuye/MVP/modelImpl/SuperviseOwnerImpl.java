package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.Imodel.ISuperviseOwnerModel;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.PartnerModel;
import com.chuanglgc.wuye.model.SuperviseOwnerDetailModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SuperviseOwnerImpl implements ISuperviseOwnerModel {

    @Override
    public void requestOwnerDetail(HashMap<String, String> map, IRequestListener<SuperviseOwnerDetailModel> listener) {
        RestClient.getAPIService().querySuperviseOwnerDetail(map).enqueue(new Callback<Result<SuperviseOwnerDetailModel>>() {
            @Override
            public void onResponse(Call<Result<SuperviseOwnerDetailModel>> call, Response<Result<SuperviseOwnerDetailModel>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<SuperviseOwnerDetailModel>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    @Override
    public void requestMaster(HashMap<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().queryDirector(map).enqueue(new Callback<Result<List<EventAllotModel>>>() {
            @Override
            public void onResponse(Call<Result<List<EventAllotModel>>> call, Response<Result<List<EventAllotModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<EventAllotModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }


    //再分配
    @Override
    public void requestAllot(HashMap<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().reassignRepairTask(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    @Override
    public void requestPartner(HashMap<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().queryPartner(map).enqueue(new Callback<Result<List<PartnerModel>>>() {
            @Override
            public void onResponse(Call<Result<List<PartnerModel>>> call, Response<Result<List<PartnerModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<PartnerModel>>> call, Throwable t) {
                listener.onFauiler(t.toString() + "");
            }
        });
    }

    @Override
    public void requestHelp(HashMap<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().addAssistantPersonOnRepairTask(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    @Override
    public void requestEvaluateWork(HashMap<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().evaluateWork(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }
}
