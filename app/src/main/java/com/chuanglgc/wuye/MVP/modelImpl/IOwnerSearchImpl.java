package com.chuanglgc.wuye.MVP.modelImpl;


import com.chuanglgc.wuye.MVP.Imodel.IOwnerSearchModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.model.BuildModel;
import com.chuanglgc.wuye.model.EventManagerModel;
import com.chuanglgc.wuye.model.RepairTypeModel;
import com.chuanglgc.wuye.model.RoomModel;
import com.chuanglgc.wuye.model.UnitModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LogUtil;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IOwnerSearchImpl implements IOwnerSearchModel {
    //查询栋
    @Override
    public void queryBuild(Map<String, String> map, IRequestListener<List<BuildModel>> listener) {
        RestClient.getAPIService().queryBuild(map).enqueue(new Callback<Result<List<BuildModel>>>() {
            @Override
            public void onResponse(Call<Result<List<BuildModel>>> call, Response<Result<List<BuildModel>>> response) {
                LogUtil.e("路径", call.request().url());
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<BuildModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
                LogUtil.e("路径", call.request().url());
            }
        });
    }

    //查询单元
    @Override
    public void queryUnit(Map<String, String> map, IRequestListener<List<UnitModel>> listener) {
        RestClient.getAPIService().queryUnit(map).enqueue(new Callback<Result<List<UnitModel>>>() {
            @Override
            public void onResponse(Call<Result<List<UnitModel>>> call, Response<Result<List<UnitModel>>> response) {
                LogUtil.e("获取单元", response.code());
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<UnitModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });

    }

    //查询门牌号
    @Override
    public void queryRoom(Map<String, String> map, IRequestListener<List<RoomModel>> listener) {
        RestClient.getAPIService().queryRoom(map).enqueue(new Callback<Result<List<RoomModel>>>() {
            @Override
            public void onResponse(Call<Result<List<RoomModel>>> call, Response<Result<List<RoomModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<RoomModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    //查询事件类型
    @Override
    public void queryRepairTypeList(String communityId, IRequestListener<List<RepairTypeModel>> listener) {
        RestClient.getAPIService().queryRepairTypeList(communityId).enqueue(new Callback<Result<List<RepairTypeModel>>>() {
            @Override
            public void onResponse(Call<Result<List<RepairTypeModel>>> call, Response<Result<List<RepairTypeModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<RepairTypeModel>>> call, Throwable t) {
                listener.onFauiler(t.toString() + "");
            }
        });
    }
}
