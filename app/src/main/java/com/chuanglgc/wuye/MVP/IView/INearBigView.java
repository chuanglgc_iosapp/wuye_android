package com.chuanglgc.wuye.MVP.IView;


import com.chuanglgc.wuye.model.DailyWorkTypeModel;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.NearAreaOrDeviceModel;
import com.chuanglgc.wuye.model.NearBigBodyTypeModel;
import com.chuanglgc.wuye.model.NearBigDeviceNumModel;
import com.chuanglgc.wuye.model.NearBigDeviceTypeModel;
import com.chuanglgc.wuye.model.NearDailyClassModel;
import com.chuanglgc.wuye.model.NearDailyWorkModel;
import com.chuanglgc.wuye.model.PartnerModel;

import java.util.List;

public interface INearBigView {

    void getMasterList(List<EventAllotModel> masterList);//获取负责人

    void getPartnerList(List<PartnerModel> partnerList);//获取配合人

    void getWorkContentList(List<NearDailyWorkModel> workModels);//获取工作内容

    void getDeviceTypeList(List<NearBigDeviceTypeModel> workTypeList);//获取设备类型

    void getCycleWorkType(List<NearBigBodyTypeModel> bodyTypeList);//获取主体类型

    void getDeviceNumberList(List<NearBigDeviceNumModel> areaOrDeviceList);//获取设备编号

    void getAllotResult(String result);//分配任务

    void getNetError(String error);
}
