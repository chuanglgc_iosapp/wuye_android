package com.chuanglgc.wuye.MVP.IView;


import com.chuanglgc.wuye.model.HeadPicModel;

public interface ICarVisitorRecordView {
    void getCommitRecordResutl(String isSuccess);

    void getCommitCarPicResult(HeadPicModel model);

    void getRequestNetError(String error);
}
