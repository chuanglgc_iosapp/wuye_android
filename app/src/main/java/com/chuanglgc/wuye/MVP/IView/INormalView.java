package com.chuanglgc.wuye.MVP.IView;

import com.chuanglgc.wuye.model.LoginModel;

//公用的model T model类型
public interface INormalView<T> {
    void getNetResult(T model);

    void getNetFauiler(String wrong);
}
