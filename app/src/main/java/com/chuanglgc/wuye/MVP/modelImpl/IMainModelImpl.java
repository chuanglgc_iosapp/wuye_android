package com.chuanglgc.wuye.MVP.modelImpl;


import com.chuanglgc.wuye.MVP.Imodel.IMainRequestModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.Imodel.IRequestModel;
import com.chuanglgc.wuye.model.HeadPicModel;
import com.chuanglgc.wuye.model.LoginModel;
import com.chuanglgc.wuye.model.PhotoModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LogUtil;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IMainModelImpl implements IMainRequestModel {
    @Override
    public void requestInfo(Map<String, String> map, IRequestListener iRequestListener) {
        RestClient.getAPIService().queryPermissionMenu(map).enqueue(new Callback<Result<List<LoginModel.MenuBean>>>() {
            @Override
            public void onResponse(Call<Result<List<LoginModel.MenuBean>>> call, Response<Result<List<LoginModel.MenuBean>>> response) {
                 if (response.code()==200){
                     if (response.body().isResult()){
                         iRequestListener.onSuccess(response.body());
                     }
                 }
                LogUtil.e("错误",response.code());
            }

            @Override
            public void onFailure(Call<Result<List<LoginModel.MenuBean>>> call, Throwable t) {
                LogUtil.e("错误",t);
            }
        });
    }

    @Override
    public void uploadPic(String id,MultipartBody.Part part, IRequestListener iRequestListener) {
        RestClient.getAPIService().uploadHeadImage(id,part).enqueue(new Callback<Result<HeadPicModel>>() {
            @Override
            public void onResponse(Call<Result<HeadPicModel>> call, Response<Result<HeadPicModel>> response) {
                if (response.code()==200){
                    if (response.body().isResult()){
                        iRequestListener.onSuccess(response.body());
                    }else {
                        iRequestListener.onFauiler(response.body().getMsg());
                    }
                }else {
                    iRequestListener.onFauiler(response.code()+"");
                }
                LogUtil.e("错误",response.code());
            }

            @Override
            public void onFailure(Call<Result<HeadPicModel>> call, Throwable t) {
                iRequestListener.onFauiler(t.toString());
            }
        });
    }


}
