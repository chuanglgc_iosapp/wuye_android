package com.chuanglgc.wuye.MVP.Imodel;


import com.chuanglgc.wuye.model.MasterAttedanModel;
import com.chuanglgc.wuye.model.MasterCheckWorkInfoModel;

import java.util.HashMap;

public interface IMasterAttendModel {
    void queryEmpCheckAttendance(HashMap<String, String> map, IRequestListener<MasterAttedanModel> listener);

    void queryEmpClockinWorkBydate(HashMap<String, String> map, IRequestListener<MasterCheckWorkInfoModel> listener);
}
