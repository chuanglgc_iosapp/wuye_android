package com.chuanglgc.wuye.MVP.IView;


import com.chuanglgc.wuye.model.BuildModel;
import com.chuanglgc.wuye.model.RepairTypeModel;
import com.chuanglgc.wuye.model.RoomModel;
import com.chuanglgc.wuye.model.UnitModel;

import java.util.List;

public interface IOwnerSearchView {
    void getBuildList(List<BuildModel> buildModelList);

    void getUnitList(List<UnitModel> unitModelList);

    void getRoomList(List<RoomModel> roomModelList);

    void getRepairTypeList(List<RepairTypeModel> repairTypeModels);


    void getNetInfoError(String error);

}
