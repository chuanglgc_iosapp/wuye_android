package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.IOwnerDetailView;
import com.chuanglgc.wuye.MVP.Imodel.IOwnerDetailModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.modelImpl.IOwnerDetailImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.OwnerEventDetailModel;
import com.chuanglgc.wuye.model.VisitRecordModel;
import com.chuanglgc.wuye.network.Result;

import java.util.List;
import java.util.Map;

//业主事件 和事件管理详情  公用
public class OwnerEventDetailPersent extends BasePersenter<IOwnerDetailView> {
    private IOwnerDetailModel iOwnerDetailModel = new IOwnerDetailImpl();
    private IOwnerDetailView iOwnerDetailView;

    //接受业主事件
    public void acceptOwnerEvent(Map<String, String> map) {
        if (iOwnerDetailView == null) {
            iOwnerDetailView = weakReference.get();
        }
        iOwnerDetailModel.acceptOwnerEvent(map, new IRequestListener() {
            @Override
            public void onSuccess(Result result) {
                if (result.isResult()) {
                    iOwnerDetailView.getAcceptResult(result.getMsg());
                } else {
                    iOwnerDetailView.getOwnerDetailErrow(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iOwnerDetailView.getOwnerDetailErrow(wrong);
            }
        });
    }

    /**
     * 请求任务详情
     */
    public void requestTaskDetail(Map<String, String> map) {
        if (iOwnerDetailView == null) {
            iOwnerDetailView = weakReference.get();
        }
        iOwnerDetailModel.requestTaskDetail(map, new IRequestListener<OwnerEventDetailModel>() {
            @Override
            public void onSuccess(Result<OwnerEventDetailModel> result) {
                if (result.isResult()) {
                    iOwnerDetailView.getOwnerDetail(result.getData());
                } else {
                    iOwnerDetailView.getOwnerDetailErrow(result.getCode() + "");
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iOwnerDetailView.getOwnerDetailErrow(wrong + "");
            }
        });
    }

    /**
     * 请求回访记录
     */
    public void requestVisitRecord(Map<String, String> map) {
        if (iOwnerDetailView == null) {
            iOwnerDetailView = weakReference.get();
        }
        iOwnerDetailModel.queryFeedbackContent(map, new IRequestListener<List<VisitRecordModel>>() {
            @Override
            public void onSuccess(Result<List<VisitRecordModel>> result) {
                if (result.isResult()) {
                    iOwnerDetailView.getVisitRecords(result.getData());
                } else {
                    iOwnerDetailView.getOwnerDetailErrow(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iOwnerDetailView.getOwnerDetailErrow(wrong);
            }
        });
    }
}
