package com.chuanglgc.wuye.MVP.IView;

import com.chuanglgc.wuye.model.LoginModel;
import com.chuanglgc.wuye.network.Result;


public interface ILoginView {
    void getLoginResult(LoginModel loginModel);

    void getLoginFauiler(String wrong);
}
