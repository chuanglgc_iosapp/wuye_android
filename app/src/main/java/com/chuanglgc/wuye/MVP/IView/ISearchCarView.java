package com.chuanglgc.wuye.MVP.IView;


import com.chuanglgc.wuye.model.CarModel;

import java.util.List;

public interface ISearchCarView {
    void getCardInfo(CarModel carModel);//获取车辆信息

    void getyParkArea(List<String> parkArea);//获取车位区域

    void getParkNumber(List<String> parkNumber);//获取车位编号

    void getNetError(String error);
}
