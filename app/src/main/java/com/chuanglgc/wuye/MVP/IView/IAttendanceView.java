package com.chuanglgc.wuye.MVP.IView;


import com.chuanglgc.wuye.model.AttedanceModel;
import com.chuanglgc.wuye.model.CheckModel;

import java.util.List;

public interface IAttendanceView {
      void getAttendModel(List<AttedanceModel>list);
      void getAttendError(String result);

      void getCheckResult(List<AttedanceModel> checkModels);

      void getNetErrow(String result);
}
