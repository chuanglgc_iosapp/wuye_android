package com.chuanglgc.wuye.MVP.IView;


import com.chuanglgc.wuye.model.OwnerEventDetailModel;
import com.chuanglgc.wuye.model.VisitRecordModel;

import java.util.List;

public interface IOwnerDetailView {

    void getAcceptResult(String result);

    void getOwnerDetail(OwnerEventDetailModel model);//获取任务详情

    void getVisitRecords(List<VisitRecordModel> visitModels);//获取回访记录

    void getOwnerDetailErrow(String errow);//返回错误
}
