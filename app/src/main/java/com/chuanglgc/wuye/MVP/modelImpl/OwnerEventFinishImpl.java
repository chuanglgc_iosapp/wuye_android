package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.IOwnerEventFinishModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.model.PhotoModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;

import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OwnerEventFinishImpl implements IOwnerEventFinishModel {


    @Override
    public void uploadFinishedRepairImage(String community,String taskID, MultipartBody.Part parts, IRequestListener listener) {
        RestClient.getAPIService().uploadFinishedRepairImage(community,taskID, parts).enqueue(new Callback<Result<PhotoModel>>() {
            @Override
            public void onResponse(Call<Result<PhotoModel>> call, Response<Result<PhotoModel>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<PhotoModel>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    @Override
    public void finishOwnerEvent(Map<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().finishRepairTask(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }
}
