package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.ISuperviseDailyView;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.Imodel.ISuperviseDailyModel;
import com.chuanglgc.wuye.MVP.modelImpl.SupserviseDailyImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.SuperviseDailyDetailModel;
import com.chuanglgc.wuye.network.Result;

import java.util.HashMap;

public class SuserviseDailyPersent extends BasePersenter<ISuperviseDailyView> {
    private ISuperviseDailyView iView;
    private ISuperviseDailyModel iModel = new SupserviseDailyImpl();

    //获取每日工作详情
    public void requestDailyDetail(HashMap<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestDailyDetail(map, new IRequestListener<SuperviseDailyDetailModel>() {
            @Override
            public void onSuccess(Result<SuperviseDailyDetailModel> result) {
                if (result.isResult()) {
                    iView.getDailyDetilModel(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    //评价
    public void requestEvalue(HashMap<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestEvaluateWork(map, new IRequestListener() {
            @Override
            public void onSuccess(Result result) {
                if (result.isResult()) {
                    iView.getEvalueResult(result.getMsg());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }
}
