package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.IDailyDetailModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.model.DailyPhotoModel;
import com.chuanglgc.wuye.model.PostDailyPhotoModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DailyDetailImpl implements IDailyDetailModel {
    @Override
    public void acceptTask(Map<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().acceptDailyTask(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    @Override
    public void finishTask(Map<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().finishDailyTask(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }


            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    @Override
    public void uploadPhoto(Map<String, String> map, MultipartBody.Part part, IRequestListener listener) {
        RestClient.getAPIService().uploadDailyTaskImage(map, part).enqueue(new Callback<Result<DailyPhotoModel>>() {
            @Override
            public void onResponse(Call<Result<DailyPhotoModel>> call, Response<Result<DailyPhotoModel>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<DailyPhotoModel>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }
}
