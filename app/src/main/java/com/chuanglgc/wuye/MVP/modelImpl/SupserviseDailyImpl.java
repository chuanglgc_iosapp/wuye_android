package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.Imodel.ISuperviseDailyModel;
import com.chuanglgc.wuye.model.SuperviseDailyDetailModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SupserviseDailyImpl implements ISuperviseDailyModel {

    @Override
    public void requestDailyDetail(HashMap<String, String> map, IRequestListener<SuperviseDailyDetailModel> listener) {
        RestClient.getAPIService().queryDailyWorkDetail(map).enqueue(new Callback<Result<SuperviseDailyDetailModel>>() {
            @Override
            public void onResponse(Call<Result<SuperviseDailyDetailModel>> call, Response<Result<SuperviseDailyDetailModel>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<SuperviseDailyDetailModel>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    @Override
    public void requestEvaluateWork(HashMap<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().evaluateWork(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code()==200){
                    listener.onSuccess(response.body());
                }else {
                    listener.onFauiler(response.code()+"");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }
}
