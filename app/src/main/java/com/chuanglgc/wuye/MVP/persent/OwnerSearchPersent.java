package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.IOwnerSearchView;
import com.chuanglgc.wuye.MVP.Imodel.IOwnerSearchModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.modelImpl.IOwnerSearchImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.BuildModel;
import com.chuanglgc.wuye.model.RepairTypeModel;
import com.chuanglgc.wuye.model.RoomModel;
import com.chuanglgc.wuye.model.UnitModel;
import com.chuanglgc.wuye.network.Result;

import java.util.List;
import java.util.Map;


public class OwnerSearchPersent extends BasePersenter<IOwnerSearchView> {
    private IOwnerSearchView iView;
    private IOwnerSearchModel iModel = new IOwnerSearchImpl();

    public void QueryBuild(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.queryBuild(map, new IRequestListener<List<BuildModel>>() {
            @Override
            public void onSuccess(Result<List<BuildModel>> result) {
                if (result.isResult()) {
                    iView.getBuildList(result.getData());
                } else {
                    iView.getNetInfoError(result.getMsg());
                }

            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetInfoError(wrong);
            }
        });
    }

    public void QueryUnit(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.queryUnit(map, new IRequestListener<List<UnitModel>>() {
            @Override
            public void onSuccess(Result<List<UnitModel>> result) {
                if (result.isResult()) {
                    iView.getUnitList(result.getData());
                } else {
                    iView.getNetInfoError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetInfoError(wrong);
            }
        });
    }

    public void QueryRoom(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.queryRoom(map, new IRequestListener<List<RoomModel>>() {
            @Override
            public void onSuccess(Result<List<RoomModel>> result) {
                if (result.isResult()) {
                    iView.getRoomList(result.getData());
                } else {
                    iView.getNetInfoError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetInfoError(wrong);
            }
        });
    }

    public void queryRepairType(String communityId) {
        if (iView == null) iView = weakReference.get();
        iModel.queryRepairTypeList(communityId, new IRequestListener<List<RepairTypeModel>>() {
            @Override
            public void onSuccess(Result<List<RepairTypeModel>> result) {
                if (result.isResult()) {
                    iView.getRepairTypeList(result.getData());
                } else {
                    iView.getNetInfoError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetInfoError(wrong);
            }
        });
    }
}
