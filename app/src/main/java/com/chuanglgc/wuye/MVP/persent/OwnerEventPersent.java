package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.ILoginView;
import com.chuanglgc.wuye.MVP.IView.INormalView;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.Imodel.IRequestModel;
import com.chuanglgc.wuye.MVP.modelImpl.ILoginModeImpl;
import com.chuanglgc.wuye.MVP.modelImpl.IOwnerEventImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.OwnerEventModel;
import com.chuanglgc.wuye.network.Result;

import java.util.List;
import java.util.Map;


public class OwnerEventPersent extends BasePersenter<INormalView> {
    private IRequestModel iRequestModel = new IOwnerEventImpl();
    private INormalView iNormalView;

    public void requestOwnerEvent(Map<String, String> map) {
        if (iNormalView == null) {
            iNormalView = weakReference.get();
        }
        iRequestModel.requestInfo(map, new IRequestListener<List<OwnerEventModel>>() {
                    @Override
                    public void onSuccess(Result<List<OwnerEventModel>> result) {
                        if (result.isResult()) {
                            iNormalView.getNetResult(result.getData());
                        } else {
                            iNormalView.getNetFauiler(result.getMsg());
                        }
                    }

                    @Override
                    public void onFauiler(String wrong) {
                        iNormalView.getNetFauiler(wrong);
                    }
                }
        );
    }
}
