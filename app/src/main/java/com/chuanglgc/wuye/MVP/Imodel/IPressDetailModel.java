package com.chuanglgc.wuye.MVP.Imodel;


import com.chuanglgc.wuye.model.PressMoneyDetailModel;

import java.util.Map;

public interface IPressDetailModel {
    void requestPressDetail(Map<String, String> map, IRequestListener<PressMoneyDetailModel> listener);

    void addPressRecord(Map<String, String> map, IRequestListener  listener);
}
