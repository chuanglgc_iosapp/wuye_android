package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.Imodel.ISearchCarModel;
import com.chuanglgc.wuye.model.CarModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LogUtil;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SearchCarImpl implements ISearchCarModel {
    @Override
    public void queryCardInfo(HashMap<String, String> map, IRequestListener<CarModel> listener) {
        RestClient.getAPIService().queryCar(map).enqueue(new Callback<Result<CarModel>>() {
            @Override
            public void onResponse(Call<Result<CarModel>> call, Response<Result<CarModel>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<CarModel>> call, Throwable t) {
              listener.onFauiler(t.toString());
            }
        });
    }

    @Override
    public void queryParkArea(HashMap<String, String> map, IRequestListener<List<String>> listener) {

    }

    @Override
    public void queryParkNumber(HashMap<String, String> map, IRequestListener<List<String>> listener) {

    }
}
