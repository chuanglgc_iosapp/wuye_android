package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.Imodel.IRequestModel;

import com.chuanglgc.wuye.model.CycleEventModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LogUtil;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ICycleEventImpl implements IRequestModel {
    @Override
    public void requestInfo(Map<String, String> map, IRequestListener iRequestListener) {
        RestClient.getAPIService().queryCycleTask(map).enqueue(new Callback<Result<List<CycleEventModel>>>() {
            @Override
            public void onResponse(Call<Result<List<CycleEventModel>>> call, Response<Result<List<CycleEventModel>>> response) {
                LogUtil.e("周期事务",response.code()+response.body().getMsg());
                if (response.code() == 200) {
                    iRequestListener.onSuccess(response.body());
                }else {
                    iRequestListener.onFauiler(response.code()+"");
                }
            }

            @Override
            public void onFailure(Call<Result<List<CycleEventModel>>> call, Throwable t) {
                iRequestListener.onFauiler(t.toString());
            }
        });
    }
}
