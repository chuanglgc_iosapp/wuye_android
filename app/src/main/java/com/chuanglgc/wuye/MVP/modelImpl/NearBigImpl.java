package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.INearBigModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.NearBigBodyTypeModel;
import com.chuanglgc.wuye.model.NearBigDeviceNumModel;
import com.chuanglgc.wuye.model.NearBigDeviceTypeModel;
import com.chuanglgc.wuye.model.NearDailyWorkModel;
import com.chuanglgc.wuye.model.PartnerModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NearBigImpl implements INearBigModel {
    @Override
    public void requestMaster(Map<String, String> map, IRequestListener<List<EventAllotModel>> listener) {
        RestClient.getAPIService().queryDirector(map).enqueue(new Callback<Result<List<EventAllotModel>>>() {
            @Override
            public void onResponse(Call<Result<List<EventAllotModel>>> call, Response<Result<List<EventAllotModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<EventAllotModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    @Override
    public void requestPartner(Map<String, String> map, IRequestListener<List<PartnerModel>> listener) {
        RestClient.getAPIService().queryPartner(map).enqueue(new Callback<Result<List<PartnerModel>>>() {
            @Override
            public void onResponse(Call<Result<List<PartnerModel>>> call, Response<Result<List<PartnerModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<PartnerModel>>> call, Throwable t) {
                listener.onFauiler(t.toString() + "");
            }
        });

    }

    //工作内容
    @Override
    public void requestWorkContent(String community, IRequestListener<List<NearDailyWorkModel>> listener) {
        RestClient.getAPIService().queryBigCycleWork(community).enqueue(new Callback<Result<List<NearDailyWorkModel>>>() {
            @Override
            public void onResponse(Call<Result<List<NearDailyWorkModel>>> call, Response<Result<List<NearDailyWorkModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<NearDailyWorkModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });

    }

    //请求设备类型
    @Override
    public void requestDeviceType(String community, IRequestListener<List<NearBigDeviceTypeModel>> listener) {
        RestClient.getAPIService().queryDeviceType(community).enqueue(new Callback<Result<List<NearBigDeviceTypeModel>>>() {
            @Override
            public void onResponse(Call<Result<List<NearBigDeviceTypeModel>>> call, Response<Result<List<NearBigDeviceTypeModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<NearBigDeviceTypeModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    //主体类型 房产类 区域类
    @Override
    public void requestCycleWorkType(String community, IRequestListener<List<NearBigBodyTypeModel>> listener) {
        RestClient.getAPIService().queryBodyType(community).enqueue(new Callback<Result<List<NearBigBodyTypeModel>>>() {
            @Override
            public void onResponse(Call<Result<List<NearBigBodyTypeModel>>> call, Response<Result<List<NearBigBodyTypeModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<NearBigBodyTypeModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    //设备编号
    @Override
    public void requestDeviceNumber(Map<String, String> map, IRequestListener<List<NearBigDeviceNumModel>> listener) {
        RestClient.getAPIService().queryDeviceNumber(map).enqueue(new Callback<Result<List<NearBigDeviceNumModel>>>() {
            @Override
            public void onResponse(Call<Result<List<NearBigDeviceNumModel>>> call, Response<Result<List<NearBigDeviceNumModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<NearBigDeviceNumModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }
   //分配
    @Override
    public void assignTempDailyTask(Map<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().assignTempCycleTask(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }
}
