package com.chuanglgc.wuye.MVP.IView;


import com.chuanglgc.wuye.model.DailyPhotoModel;

public interface IDailyDetailView {
    void finishResult(String result);

    void acceptResult(String result);

    void uploadPhotoResult(DailyPhotoModel model);

    void requestNetError(String error);
}
