package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.ICarVisitorRecoredModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.model.HeadPicModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;

import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CarVisitorRecoredImpl implements ICarVisitorRecoredModel {


    @Override
    public void requestCommitRecord(Map<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().addVisitor(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                listener.onFauiler(t.getMessage());
            }
        });
    }

    @Override
    public void requestCommitCarPicResult(String commnutiyID, MultipartBody.Part body, IRequestListener<HeadPicModel> listener) {
        RestClient.getAPIService().addVisitorCarPic(commnutiyID, body).enqueue(new Callback<Result<HeadPicModel>>() {
            @Override
            public void onResponse(Call<Result<HeadPicModel>> call, Response<Result<HeadPicModel>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<HeadPicModel>> call, Throwable t) {
                listener.onFauiler(t.getMessage());
            }
        });
    }
}
