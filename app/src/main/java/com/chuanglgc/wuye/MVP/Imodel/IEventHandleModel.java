package com.chuanglgc.wuye.MVP.Imodel;


import com.chuanglgc.wuye.model.DepartmentServiceModel;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.ServiceModel;

import java.util.HashMap;
import java.util.List;

public interface IEventHandleModel {

    void queryServiceProvider(String communityId, IRequestListener<List<ServiceModel>> listener);//查询服务上类型

    void queryDepartmentByServiceType(HashMap<String, String> map, IRequestListener<List<DepartmentServiceModel>> listener);//查询归属部门

    void queryLeaderByWorkType(HashMap<String, String> map, IRequestListener<List<EventAllotModel>> listener);//部门查询负责人

    void assistRepairTask(HashMap<String, String> map, IRequestListener listener);//分配
}
