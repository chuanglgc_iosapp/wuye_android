package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.INearOtherModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.PartnerModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NearOtherImpl implements INearOtherModel {
    @Override
    public void requestMaster(Map<String, String> map, IRequestListener<List<EventAllotModel>> listener) {
        RestClient.getAPIService().queryDirector(map).enqueue(new Callback<Result<List<EventAllotModel>>>() {
            @Override
            public void onResponse(Call<Result<List<EventAllotModel>>> call, Response<Result<List<EventAllotModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<EventAllotModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    @Override
    public void requestPartner(Map<String, String> map, IRequestListener<List<PartnerModel>> listener) {
        RestClient.getAPIService().queryPartner(map).enqueue(new Callback<Result<List<PartnerModel>>>() {
            @Override
            public void onResponse(Call<Result<List<PartnerModel>>> call, Response<Result<List<PartnerModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<PartnerModel>>> call, Throwable t) {
                listener.onFauiler(t.toString() + "");
            }
        });

    }

   //分配
    @Override
    public void assignTempDailyTask(Map<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().assignTempAnyTask(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }
}
