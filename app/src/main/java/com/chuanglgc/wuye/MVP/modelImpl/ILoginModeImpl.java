package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.Imodel.IRequestModel;
import com.chuanglgc.wuye.model.LoginModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ILoginModeImpl implements IRequestModel {
    @Override
    public void requestInfo(Map<String, String> map, IRequestListener iLoginlLisenter) {
        RestClient.getAPIService().login(map).enqueue(new Callback<Result<LoginModel>>() {
            @Override
            public void onResponse(Call<Result<LoginModel>> call, Response<Result<LoginModel>> response) {
                if (response.code() == 200) {
                    Result body  = response.body();
                    if (body.isResult()) {
                        iLoginlLisenter.onSuccess(response.body());
                    } else {
                        iLoginlLisenter.onFauiler(body.getMsg());
                    }
                } else {
                    iLoginlLisenter.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<LoginModel>> call, Throwable t) {
                iLoginlLisenter.onFauiler(t + "");
            }
        });
    }
}
