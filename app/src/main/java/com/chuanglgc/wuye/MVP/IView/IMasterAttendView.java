package com.chuanglgc.wuye.MVP.IView;


import com.chuanglgc.wuye.model.MasterAttedanModel;
import com.chuanglgc.wuye.model.MasterCheckWorkInfoModel;


public interface IMasterAttendView {
    void getEmpCheckAttendance(MasterAttedanModel model);

    void getEmpClockinWorkBydate(MasterCheckWorkInfoModel model);

    void getNetError(String error);
}
