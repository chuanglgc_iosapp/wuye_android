package com.chuanglgc.wuye.MVP.IView;


import com.chuanglgc.wuye.model.PressMoneyDetailModel;

public interface IPressDetailView {
    void getPressDetail(PressMoneyDetailModel moneyDetailModel);

    void addPressResult(String result);

    void getNetError(String  error);
}
