package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.INearBigView;
import com.chuanglgc.wuye.MVP.Imodel.INearBigModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.modelImpl.NearBigImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.NearBigBodyTypeModel;
import com.chuanglgc.wuye.model.NearBigDeviceNumModel;
import com.chuanglgc.wuye.model.NearBigDeviceTypeModel;
import com.chuanglgc.wuye.model.NearDailyWorkModel;
import com.chuanglgc.wuye.model.PartnerModel;
import com.chuanglgc.wuye.network.Result;

import java.util.List;
import java.util.Map;


public class NearBigPersent extends BasePersenter<INearBigView> {
    private INearBigView iView;
    private INearBigModel iModel = new NearBigImpl();

    public void requestMaster(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestMaster(map, new IRequestListener<List<EventAllotModel>>() {
            @Override
            public void onSuccess(Result<List<EventAllotModel>> result) {
                if (result.isResult()) {
                    iView.getMasterList(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    //获取配合人
    public void requsetPartner(Map<String, String> map) {
        iModel.requestPartner(map, new IRequestListener<List<PartnerModel>>() {
            @Override
            public void onSuccess(Result<List<PartnerModel>> result) {
                if (result.isResult()) {
                    iView.getPartnerList(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    //查询工作内容
    public void requestWorkContent(String community) {
        if (iView == null) iView = weakReference.get();
        iModel.requestWorkContent(community, new IRequestListener<List<NearDailyWorkModel>>() {
            @Override
            public void onSuccess(Result<List<NearDailyWorkModel>> result) {
                if (result.isResult()) {
                    iView.getWorkContentList(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    //请求主体类型
    public void requestCycleWorkType(String community) {
        if (iView == null) iView = weakReference.get();
        iModel.requestCycleWorkType(community, new IRequestListener<List<NearBigBodyTypeModel>>() {
            @Override
            public void onSuccess(Result<List<NearBigBodyTypeModel>> result) {
                if (result.isResult()) {
                    iView.getCycleWorkType(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }
    //请求设备类型
    public void requestDevicekType(String community) {
        if (iView == null) iView = weakReference.get();
        iModel.requestDeviceType(community, new IRequestListener<List<NearBigDeviceTypeModel>>() {
            @Override
            public void onSuccess(Result<List<NearBigDeviceTypeModel>> result) {
                if (result.isResult()) {
                    iView.getDeviceTypeList(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }
    //请求设备类型
    public void requestDevicekNum(Map<String,String>map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestDeviceNumber(map, new IRequestListener<List<NearBigDeviceNumModel>>() {
            @Override
            public void onSuccess(Result<List<NearBigDeviceNumModel>> result) {
                if (result.isResult()) {
                    iView.getDeviceNumberList(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }
    //分配
    public void allotTask(Map<String,String>map) {
        if (iView == null) iView = weakReference.get();
        iModel.assignTempDailyTask(map, new IRequestListener() {
            @Override
            public void onSuccess(Result result) {
                if (result.isResult()) {
                    iView.getAllotResult(result.getMsg());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

}
