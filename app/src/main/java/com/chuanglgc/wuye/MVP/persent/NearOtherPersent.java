package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.INearOtherView;
import com.chuanglgc.wuye.MVP.Imodel.INearOtherModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.modelImpl.NearOtherImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.PartnerModel;
import com.chuanglgc.wuye.network.Result;

import java.util.List;
import java.util.Map;


public class NearOtherPersent extends BasePersenter<INearOtherView> {
    private INearOtherView iView;
    private INearOtherModel iModel = new NearOtherImpl();

    public void requestMaster(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestMaster(map, new IRequestListener<List<EventAllotModel>>() {
            @Override
            public void onSuccess(Result<List<EventAllotModel>> result) {
                if (result.isResult()) {
                    iView.getMasterList(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    //获取配合人
    public void requsetPartner(Map<String, String> map) {
        iModel.requestPartner(map, new IRequestListener<List<PartnerModel>>() {
            @Override
            public void onSuccess(Result<List<PartnerModel>> result) {
                if (result.isResult()) {
                    iView.getPartnerList(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    //分配
    public void allotTask(Map<String,String>map) {
        if (iView == null) iView = weakReference.get();
        iModel.assignTempDailyTask(map, new IRequestListener() {
            @Override
            public void onSuccess(Result result) {
                if (result.isResult()) {
                    iView.getAllotResult(result.getMsg());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

}
