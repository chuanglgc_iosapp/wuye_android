package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.IMasterAttendModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.model.MasterAttedanModel;
import com.chuanglgc.wuye.model.MasterCheckWorkInfoModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MasterAttendImpl implements IMasterAttendModel {
    @Override
    public void queryEmpCheckAttendance(HashMap<String, String> map, IRequestListener<MasterAttedanModel> listener) {
        RestClient.getAPIService().queryEmpCheckAttendance(map).enqueue(new Callback<Result<MasterAttedanModel>>() {
            @Override
            public void onResponse(Call<Result<MasterAttedanModel>> call, Response<Result<MasterAttedanModel>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<MasterAttedanModel>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    @Override
    public void queryEmpClockinWorkBydate(HashMap<String, String> map, IRequestListener<MasterCheckWorkInfoModel> listener) {
        RestClient.getAPIService().queryEmpClockinWorkBydate(map).enqueue(new Callback<Result<MasterCheckWorkInfoModel>>() {
            @Override
            public void onResponse(Call<Result<MasterCheckWorkInfoModel>> call, Response<Result<MasterCheckWorkInfoModel>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<MasterCheckWorkInfoModel>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }
}
