package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.IHandleEventDetailView;
import com.chuanglgc.wuye.MVP.Imodel.IHandleEventDetailModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.modelImpl.HandleEventDetailImpl;
import com.chuanglgc.wuye.adapter.MssageAdapter;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.EventHandleDetailModel;
import com.chuanglgc.wuye.model.PhotoModel;
import com.chuanglgc.wuye.network.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class HandleEventDetailPersent extends BasePersenter<IHandleEventDetailView> {
    private IHandleEventDetailModel iModel = new HandleEventDetailImpl();
    private IHandleEventDetailView iView;

    //接受任务
    public void acceptCycleTask(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.acceptCycleTask(map, new IRequestListener() {
            @Override
            public void onSuccess(Result result) {
                if (result.isResult()) {
                    iView.acceptTaskResult(result.getMsg());
                } else {
                    iView.requestFailResult(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.requestFailResult(wrong);
            }
        });
    }

    //查询事务详情
    public void queryCycleTaskDetail(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.queryCycleTaskDetail(map, new IRequestListener<EventHandleDetailModel>() {
            @Override
            public void onSuccess(Result<EventHandleDetailModel> result) {
                if (result.isResult()) {
                    iView.getTaskDetail(result.getData());
                } else {
                    iView.requestFailResult(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.requestFailResult(wrong);
            }
        });
    }

    //上传图片
    public void uploadPic(Map<String, String> map, MultipartBody.Part part) {
        if (iView == null) iView = weakReference.get();
        iModel.uploadFinishedCycleTaskImage(map, part, new IRequestListener<PhotoModel>() {
            @Override
            public void onSuccess(Result<PhotoModel> result) {
                if (result.isResult()) {
                    iView.upLoadPhotoResult(result.getData());
                } else {
                    iView.requestFailResult(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.requestFailResult(wrong);
            }
        });
    }

    //完成事务
    public void finishTask(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.finishCycleTask(map, new IRequestListener() {
            @Override
            public void onSuccess(Result result) {
                if (result.isResult()) {
                    iView.commitTaskResult(result.getMsg());
                } else {
                    iView.requestFailResult(result.getMsg());
                }

            }
            @Override
            public void onFauiler(String wrong) {
                iView.requestFailResult(wrong);
            }
        });
    }
}
