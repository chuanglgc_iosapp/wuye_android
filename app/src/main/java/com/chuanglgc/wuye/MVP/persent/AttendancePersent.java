package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.IAttendanceView;
import com.chuanglgc.wuye.MVP.Imodel.IAttendanceModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.modelImpl.AttendanceImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.AttedanceModel;
import com.chuanglgc.wuye.model.CheckModel;
import com.chuanglgc.wuye.network.Result;

import java.util.List;
import java.util.Map;

public class AttendancePersent extends BasePersenter<IAttendanceView> {
    private IAttendanceModel iMode = new AttendanceImpl();
    private IAttendanceView iView;

    //查询打卡集合
    public void queryCheckList(Map<String, String> map) {
        if (iView==null)iView=weakReference.get();
        iMode.queryCheckList(map, new IRequestListener<List<AttedanceModel>>() {
            @Override
            public void onSuccess(Result<List<AttedanceModel>> result) {
                if (result.isResult()){
                    iView.getAttendModel(result.getData());
                }else {
                    iView.getAttendError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetErrow(wrong);
            }
        });
    }

    public void checkWork(Map<String,String>map){
        if (iView==null)iView=weakReference.get();
        iMode.queryCheckByApp(map, new IRequestListener<List<AttedanceModel>>() {
            @Override
            public void onSuccess(Result<List<AttedanceModel>> result) {
                if (result.isResult()){
                    iView.getCheckResult(result.getData());
                }else {
                    iView.getNetErrow(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetErrow(wrong);
            }
        });
    }
}
