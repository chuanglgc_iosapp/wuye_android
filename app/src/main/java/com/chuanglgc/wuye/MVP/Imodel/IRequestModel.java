package com.chuanglgc.wuye.MVP.Imodel;

import java.util.Map;


public interface IRequestModel {
    //请求信息
    void requestInfo(Map<String,String> map, IRequestListener iRequestListener);
}
