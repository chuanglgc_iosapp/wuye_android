package com.chuanglgc.wuye.MVP.Imodel;

import com.chuanglgc.wuye.network.Result;


public interface IRequestListener<T> {
    //成功返回bean类
    void onSuccess(Result<T> result);
    //不成功返回错误码
    void onFauiler(String wrong);
}
