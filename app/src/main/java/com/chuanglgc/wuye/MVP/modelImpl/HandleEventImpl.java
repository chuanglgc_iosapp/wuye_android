package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.Imodel.IRequestModel;
import com.chuanglgc.wuye.model.CycleTaskModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LogUtil;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HandleEventImpl implements IRequestModel {
    @Override
    public void requestInfo(Map<String, String> map, IRequestListener iRequestListener) {
        RestClient.getAPIService().queryCycleTaskList(map).enqueue(new Callback<Result<List<CycleTaskModel>>>() {
            @Override
            public void onResponse(Call<Result<List<CycleTaskModel>>> call, Response<Result<List<CycleTaskModel>>> response) {
                if (response.code()==200){
                    iRequestListener.onSuccess(response.body());
                }else {
                    iRequestListener.onFauiler(response.code()+"");
                }
            }

            @Override
            public void onFailure(Call<Result<List<CycleTaskModel>>> call, Throwable t) {
                iRequestListener.onFauiler(t.toString());
            }
        });
    }
}
