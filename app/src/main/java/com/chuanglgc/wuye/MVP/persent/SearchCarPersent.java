package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.ISearchCarView;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.Imodel.ISearchCarModel;
import com.chuanglgc.wuye.MVP.modelImpl.SearchCarImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.CarModel;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LogUtil;

import java.util.HashMap;
import java.util.List;


public class SearchCarPersent extends BasePersenter<ISearchCarView> {
    ISearchCarModel iModel = new SearchCarImpl();
    ISearchCarView iView;

    public void queryParkArea(HashMap<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.queryParkArea(map, new IRequestListener<List<String>>() {
            @Override
            public void onSuccess(Result<List<String>> result) {
                if (result.isResult()) {
                    iView.getyParkArea(result.getData());

                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    public void queryParkNumber(HashMap<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.queryParkNumber(map, new IRequestListener<List<String>>() {
            @Override
            public void onSuccess(Result<List<String>> result) {
                if (result.isResult()) {
                    iView.getParkNumber(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    public void queryCarInfo(HashMap<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.queryCardInfo(map, new IRequestListener<CarModel>() {
            @Override
            public void onSuccess(Result<CarModel> result) {
                LogUtil.e("进来","11111111111111");
                if (result.isResult()) {
                    LogUtil.e("进来ssss",result.getData().getPark_number().size()>0);
                    LogUtil.e("进来ssss",result.getData().getPlate_number().size()>0);
                    if (result.getData().getPark_number().size()>0|result.getData().getPlate_number().size()>0){
                        iView.getCardInfo(result.getData());
                    }else if (result.getData().getPlate_number().size()==0&&result.getData().getPark_number().size()==0){
                        LogUtil.e("进来","@@@@@@@@@@@@@@@@@@@");
                        iView.getNetError("该车位还未有户主");
                    }
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }
}
