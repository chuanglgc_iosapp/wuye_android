package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.Imodel.IRequestModel;
import com.chuanglgc.wuye.model.LoginModel;
import com.chuanglgc.wuye.model.OwnerEventModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LogUtil;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class IOwnerEventImpl implements IRequestModel {
    @Override
    public void requestInfo(Map<String, String> map, IRequestListener iRequestListener) {
        RestClient.getAPIService().queryRepairTask(map).enqueue(new Callback<Result<List<OwnerEventModel>>>() {
            @Override
            public void onResponse(Call<Result<List<OwnerEventModel>>> call, Response<Result<List<OwnerEventModel>>> response) {

                if (response.code() == 200) {
                    Result<List<OwnerEventModel>> body = response.body();
                    if (body.isResult()) {
                        iRequestListener.onSuccess(body);
                    } else {
                        iRequestListener.onFauiler(response.message());
                    }
                } else {
                    iRequestListener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<OwnerEventModel>>> call, Throwable t) {
                iRequestListener.onFauiler(t + "");
            }
        });
    }
}
