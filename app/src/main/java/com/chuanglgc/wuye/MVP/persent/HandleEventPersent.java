package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.INormalView;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.Imodel.IRequestModel;
import com.chuanglgc.wuye.MVP.modelImpl.HandleEventImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.CycleTaskModel;
import com.chuanglgc.wuye.network.Result;

import java.util.List;
import java.util.Map;


public class HandleEventPersent extends BasePersenter<INormalView> {
    private IRequestModel iModel = new HandleEventImpl();
    private INormalView iView;

    public void queryCycleTaskList(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestInfo(map, new IRequestListener<List<CycleTaskModel>>() {
            @Override
            public void onSuccess(Result<List<CycleTaskModel>> result) {
                if (result.isResult()) {
                    iView.getNetResult(result.getData());
                } else {
                    iView.getNetFauiler(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetFauiler(wrong);
            }
        });
    }

}
