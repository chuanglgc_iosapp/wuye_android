package com.chuanglgc.wuye.MVP.IView;


import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.NearBigBodyTypeModel;
import com.chuanglgc.wuye.model.NearBigDeviceNumModel;
import com.chuanglgc.wuye.model.NearBigDeviceTypeModel;
import com.chuanglgc.wuye.model.NearDailyWorkModel;
import com.chuanglgc.wuye.model.PartnerModel;

import java.util.List;

public interface INearOtherView {

    void getMasterList(List<EventAllotModel> masterList);//获取负责人

    void getPartnerList(List<PartnerModel> partnerList);//获取配合人

    void getAllotResult(String result);//分配任务

    void getNetError(String error);
}
