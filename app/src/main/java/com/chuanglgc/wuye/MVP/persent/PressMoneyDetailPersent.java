package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.IPressDetailView;
import com.chuanglgc.wuye.MVP.Imodel.IPressDetailModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.modelImpl.PressMoneyDetailImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.PressMoneyDetailModel;
import com.chuanglgc.wuye.network.Result;

import java.util.Map;



public class PressMoneyDetailPersent extends BasePersenter<IPressDetailView> {
    private IPressDetailModel iModel = new PressMoneyDetailImpl();
    private IPressDetailView iView;

    public void requestPressMoneyDetail(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestPressDetail(map, new IRequestListener<PressMoneyDetailModel>() {
            @Override
            public void onSuccess(Result<PressMoneyDetailModel> result) {
                if (result.isResult()) {
                    iView.getPressDetail(result.getData());
                } else {
                    onFauiler(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    public void addPressRecord(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.addPressRecord(map, new IRequestListener() {
            @Override
            public void onSuccess(Result result) {
                if (result.isResult()) {
                    iView.addPressResult(result.getMsg());
                } else {
                    onFauiler(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }
}
