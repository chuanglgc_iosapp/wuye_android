package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.Imodel.IRequestModel;
import com.chuanglgc.wuye.model.CycleEventDetailModel;
import com.chuanglgc.wuye.model.CycleEventModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LogUtil;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ICycleEventDetailImpl implements IRequestModel {
    @Override
    public void requestInfo(Map<String, String> map, IRequestListener iRequestListener) {
        RestClient.getAPIService().queryCycleTaskDetail(map).enqueue(new Callback<Result<CycleEventDetailModel>>() {
            @Override
            public void onResponse(Call<Result<CycleEventDetailModel>> call, Response<Result<CycleEventDetailModel>> response) {
                if (response.code() == 200) {
                    iRequestListener.onSuccess(response.body());
                } else {
                    iRequestListener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<CycleEventDetailModel>> call, Throwable t) {
                iRequestListener.onFauiler(t + "");
            }
        });
    }
}
