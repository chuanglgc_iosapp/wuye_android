package com.chuanglgc.wuye.MVP.IView;


import com.chuanglgc.wuye.model.PhotoModel;

public interface IOwnerEventFinishView {
    void getUploadPhotoResult(PhotoModel photoModel);

    void getFinishEventResult(String result);

    void requestNetFail(String errow);
}
