package com.chuanglgc.wuye.MVP.Imodel;


import com.chuanglgc.wuye.model.SuperviseDailyDetailModel;

import java.util.HashMap;

public interface ISuperviseDailyModel {
    void requestDailyDetail(HashMap<String, String> map, IRequestListener<SuperviseDailyDetailModel> listener);//查询每日工作详情

    void requestEvaluateWork(HashMap<String, String> map, IRequestListener listener);//提交评价
}
