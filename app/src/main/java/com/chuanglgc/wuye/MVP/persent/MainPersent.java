package com.chuanglgc.wuye.MVP.persent;

import android.widget.ImageView;

import com.chuanglgc.wuye.MVP.IView.IMainView;
import com.chuanglgc.wuye.MVP.Imodel.IMainRequestModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.modelImpl.IMainModelImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.HeadPicModel;
import com.chuanglgc.wuye.model.LoginModel;
import com.chuanglgc.wuye.model.PhotoModel;
import com.chuanglgc.wuye.network.Result;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;


public class MainPersent extends BasePersenter<IMainView> {
    private IMainRequestModel iRequestModel = new IMainModelImpl();
    private IMainView iMainView;

    public void refreshFunction(Map<String, String> map) {
        if (iMainView == null) iMainView = weakReference.get();
        iRequestModel.requestInfo(map, new IRequestListener<List<LoginModel.MenuBean>>() {
            @Override
            public void onSuccess(Result<List<LoginModel.MenuBean>> result) {
                if (result.isResult()) {
                    iMainView.getFunction(result.getData());
                } else {
                    iMainView.getFunctionErrow(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iMainView.getFunctionErrow(wrong);

            }
        });
    }

    public void uploadPic(String id,MultipartBody.Part part) {
        if (iMainView == null) iMainView = weakReference.get();
        iRequestModel.uploadPic(id,part, new IRequestListener<HeadPicModel>() {
            @Override
            public void onSuccess(Result<HeadPicModel> result) {
                if (result.isResult()) {
                    iMainView.getUpPicResult(result.getData());
                } else {
                    iMainView.getFunctionErrow(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iMainView.getFunctionErrow(wrong);
            }
        });
    }
}
