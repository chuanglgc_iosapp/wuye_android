package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.INearDailyView;
import com.chuanglgc.wuye.MVP.Imodel.INearDailyTaskModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.modelImpl.NearDailyImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.DailyWorkTypeModel;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.NearAreaOrDeviceModel;
import com.chuanglgc.wuye.model.NearDailyClassModel;
import com.chuanglgc.wuye.model.NearDailyWorkModel;
import com.chuanglgc.wuye.network.Result;

import java.util.List;
import java.util.Map;


public class NearDailyPersent extends BasePersenter<INearDailyView> {
    private INearDailyTaskModel iModel = new NearDailyImpl();
    private INearDailyView iView;

    public void requestMaster(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestMaster(map, new IRequestListener<List<EventAllotModel>>() {
            @Override
            public void onSuccess(Result<List<EventAllotModel>> result) {
                if (result.isResult()) {
                    iView.getMasterList(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    //查询班次
    public void requestClass(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestClass(map, new IRequestListener<List<NearDailyClassModel>>() {
            @Override
            public void onSuccess(Result<List<NearDailyClassModel>> result) {
                if (result.isResult()) {
                    iView.getClassList(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    //查询工作内容
    public void requestWorkContent(String community) {
        if (iView == null) iView = weakReference.get();
        iModel.requestWorkContent(community, new IRequestListener<List<NearDailyWorkModel>>() {
            @Override
            public void onSuccess(Result<List<NearDailyWorkModel>> result) {
                if (result.isResult()) {
                    iView.getWorkContentList(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    //请求工作类型
    public void requestDailyWorkType(String community) {
        if (iView == null) iView = weakReference.get();
        iModel.requestDailyWorkType(community, new IRequestListener<List<DailyWorkTypeModel>>() {
            @Override
            public void onSuccess(Result<List<DailyWorkTypeModel>> result) {
                if (result.isResult()) {
                    iView.getDailyWorkType(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }

    //请求区域或设备信息
    public void requestAreaOrDevice(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestAreaOrDevice(map, new IRequestListener<List<NearAreaOrDeviceModel>>() {
            @Override
            public void onSuccess(Result<List<NearAreaOrDeviceModel>> result) {
                if (result.isResult()) {
                    iView.getAreaList(result.getData());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }
   //分配日常工作
    public void allotTask(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.assignTempDailyTask(map, new IRequestListener() {
            @Override
            public void onSuccess(Result result) {
                if (result.isResult()) {
                    iView.getAllotResult(result.getMsg());
                } else {
                    iView.getNetError(result.getMsg());
                }
            }
            @Override
            public void onFauiler(String wrong) {
                iView.getNetError(wrong);
            }
        });
    }
}
