package com.chuanglgc.wuye.MVP.Imodel;

import com.chuanglgc.wuye.model.BuildModel;
import com.chuanglgc.wuye.model.RoomModel;
import com.chuanglgc.wuye.model.UnitModel;

import java.util.List;
import java.util.Map;


public interface IEventSearchModel {
    void queryBuild(Map<String,String > map, IRequestListener<List<BuildModel>>listener);//查询栋
    void queryUnit(Map<String,String >map, IRequestListener<List<UnitModel>>listener);//查询栋
    void queryRoom(Map<String,String >map, IRequestListener<List<RoomModel>>listener);//查询栋
}


