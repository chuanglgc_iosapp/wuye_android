package com.chuanglgc.wuye.MVP.persent;


import com.chuanglgc.wuye.MVP.IView.ICarVisitorRecordView;
import com.chuanglgc.wuye.MVP.Imodel.ICarVisitorRecoredModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.modelImpl.CarVisitorRecoredImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.HeadPicModel;
import com.chuanglgc.wuye.network.Result;

import java.util.Map;

import okhttp3.MultipartBody;

public class CarVisitorRecordPersent extends BasePersenter<ICarVisitorRecordView> {
    ICarVisitorRecordView iView;
    ICarVisitorRecoredModel iModel = new CarVisitorRecoredImpl();

    //提交访客车辆记录
    public void requestCommitRecord(Map<String, String> map) {
        if (iView == null) iView = weakReference.get();
        iModel.requestCommitRecord(map, new IRequestListener() {
            @Override
            public void onSuccess(Result result) {
                iView.getCommitRecordResutl(result.getMsg());
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getRequestNetError(wrong);
            }
        });
    }

    public void requestCommitCarPic(String communityId, MultipartBody.Part body) {
        if (iView == null) iView = weakReference.get();
        iModel.requestCommitCarPicResult(communityId, body, new IRequestListener<HeadPicModel>() {
            @Override
            public void onSuccess(Result<HeadPicModel> result) {
                if (result.isResult()) {
                    iView.getCommitCarPicResult(result.getData());
                } else {
                    onFauiler(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iView.getRequestNetError(wrong);
            }
        });
    }
}
