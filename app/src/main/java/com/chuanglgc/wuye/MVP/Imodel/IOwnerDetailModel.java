package com.chuanglgc.wuye.MVP.Imodel;


import com.chuanglgc.wuye.model.OwnerEventDetailModel;
import com.chuanglgc.wuye.model.VisitRecordModel;

import java.util.List;
import java.util.Map;

public interface IOwnerDetailModel {
    void acceptOwnerEvent(Map<String, String> map, IRequestListener listener);

    void requestTaskDetail(Map<String, String> map, IRequestListener<OwnerEventDetailModel> listener);

    void queryFeedbackContent(Map<String, String> map, IRequestListener<List<VisitRecordModel>> listener);

}
