package com.chuanglgc.wuye.MVP.Imodel;

import com.chuanglgc.wuye.model.PostDailyPhotoModel;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public interface IDailyDetailModel {
    void acceptTask(Map<String, String> map, IRequestListener listener);

    void finishTask(Map<String, String> map, IRequestListener listener);

    void uploadPhoto(Map<String,String>map, MultipartBody.Part part,IRequestListener listener);
}
