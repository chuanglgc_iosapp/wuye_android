package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.INormalView;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.Imodel.IRequestModel;
import com.chuanglgc.wuye.MVP.modelImpl.ICycleEventImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.CycleEventModel;
import com.chuanglgc.wuye.network.Result;

import java.util.List;
import java.util.Map;


public class CycleEventPersent extends BasePersenter<INormalView> {
    private IRequestModel iRequestModel = new ICycleEventImpl();
    private INormalView iNormalView;

    public void requestCycleEvent(Map<String, String> map) {
        if (iNormalView == null) iNormalView = weakReference.get();
        iRequestModel.requestInfo(map, new IRequestListener<List<CycleEventModel>>() {
            @Override
            public void onSuccess(Result<List<CycleEventModel>> result) {
                if (result.isResult()) {
                    iNormalView.getNetResult(result.getData());
                } else {
                    iNormalView.getNetFauiler(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                iNormalView.getNetFauiler(wrong);
            }
        });
    }
}
