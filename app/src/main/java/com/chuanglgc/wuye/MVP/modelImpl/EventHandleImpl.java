package com.chuanglgc.wuye.MVP.modelImpl;

import com.chuanglgc.wuye.MVP.Imodel.IEventHandleModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.model.DepartmentServiceModel;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.ServiceModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EventHandleImpl implements IEventHandleModel {
    @Override
    public void queryServiceProvider(String communityId, IRequestListener<List<ServiceModel>> listener) {
        RestClient.getAPIService().queryServiceProvider(communityId).enqueue(new Callback<Result<List<ServiceModel>>>() {
            @Override
            public void onResponse(Call<Result<List<ServiceModel>>> call, Response<Result<List<ServiceModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<ServiceModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    @Override
    public void queryDepartmentByServiceType(HashMap<String, String> map, IRequestListener<List<DepartmentServiceModel>> listener) {
        RestClient.getAPIService().queryDepartmentByServiceType(map).enqueue(new Callback<Result<List<DepartmentServiceModel>>>() {
            @Override
            public void onResponse(Call<Result<List<DepartmentServiceModel>>> call, Response<Result<List<DepartmentServiceModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<DepartmentServiceModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }

    @Override
    public void queryLeaderByWorkType(HashMap<String, String> map, IRequestListener<List<EventAllotModel>> listener) {
        RestClient.getAPIService().queryLeaderByWorkType(map).enqueue(new Callback<Result<List<EventAllotModel>>>() {
            @Override
            public void onResponse(Call<Result<List<EventAllotModel>>> call, Response<Result<List<EventAllotModel>>> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result<List<EventAllotModel>>> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }


    @Override
    public void assistRepairTask(HashMap<String, String> map, IRequestListener listener) {
        RestClient.getAPIService().assistRepairTask(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code() == 200) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFauiler(response.code() + "");
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                listener.onFauiler(t.toString());
            }
        });
    }
}
