package com.chuanglgc.wuye.MVP.IView;


import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.PartnerModel;
import com.chuanglgc.wuye.model.SuperviseHandleDetailModel;

import java.util.List;

public interface ISuperviseHandleView {

    void getMasterList(List<EventAllotModel> marsterList);//获取负责人

    void getPartner(List<PartnerModel> partnerModels);//获取配合人

    void getAllotTaskResult(String result);//再分配

    void getHelpTaskResult(String result);//追加协助


    void getHandleDetail(SuperviseHandleDetailModel handleModel);//获取详情

    void getEvalueResult(String result);//获取评价

    void getNetError(String error);


}
