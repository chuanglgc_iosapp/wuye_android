package com.chuanglgc.wuye.MVP.IView;


import com.chuanglgc.wuye.model.DailyWorkTypeModel;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.NearAreaOrDeviceModel;
import com.chuanglgc.wuye.model.NearDailyClassModel;
import com.chuanglgc.wuye.model.NearDailyWorkModel;


import java.util.List;

public interface INearDailyView {
    void getMasterList(List<EventAllotModel> masterList);

    void getClassList(List<NearDailyClassModel> partnerModelList);

    void getWorkContentList(List<NearDailyWorkModel> workModels);

    void getDailyWorkType(List<DailyWorkTypeModel> typeModelList);

    void getAreaList(List<NearAreaOrDeviceModel> areaOrDeviceList);

    void getAllotResult(String result);

    void getNetError(String error);
}
