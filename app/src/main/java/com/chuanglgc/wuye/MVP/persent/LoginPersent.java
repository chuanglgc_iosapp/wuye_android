package com.chuanglgc.wuye.MVP.persent;

import com.chuanglgc.wuye.MVP.IView.ILoginView;
import com.chuanglgc.wuye.MVP.Imodel.IRequestModel;
import com.chuanglgc.wuye.MVP.Imodel.IRequestListener;
import com.chuanglgc.wuye.MVP.modelImpl.ILoginModeImpl;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.LoginModel;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LogUtil;

import java.util.Map;

public class LoginPersent extends BasePersenter<ILoginView> {
    private IRequestModel iLoginMode = new ILoginModeImpl();
    private ILoginView iLoginView;

    /**
     * 请求登陆
     * @param map
     */
    public void requestLogin(Map<String,String> map) {
         if (iLoginView==null) {
             iLoginView = weakReference.get();
         }
        iLoginMode.requestInfo(map, new IRequestListener<LoginModel>() {
            @Override
            public void onSuccess(Result<LoginModel> result) {
                LogUtil.e("请求数据",result.getData());
                if (result.isResult()) {
                    iLoginView.getLoginResult(result.getData());
                } else {
                    iLoginView.getLoginFauiler(result.getMsg());
                }
            }

            @Override
            public void onFauiler(String wrong) {
                LogUtil.e("请求数据",wrong);
                iLoginView.getLoginFauiler(wrong);
            }
        });
    }

}
