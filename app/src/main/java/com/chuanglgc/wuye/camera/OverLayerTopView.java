package com.chuanglgc.wuye.camera;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.chuanglgc.wuye.utils.DisplayUtil;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;

;


/**
 * 相机矩形取景框设置
 */

public class OverLayerTopView extends AppCompatImageView {
    //取景框四周的线的画笔
    private Paint mRectBorderPaint;
    //阴影部分的画笔
    private Paint mShadePaint;
    //矩形取景框四角的短线的画笔
    private Paint mLinePaint;
    //文字的画笔
    private Paint wordPaint;
    //中间矩形区域画板
    private Rect mCenterRect;
    //屏幕的宽和高
    private int screenWidth, screenHeight;

    /**
     * 上一次两指距离
     */
    private float oldDist = 1f;

    private static final String TIPS = "请对准车牌号进行拍照";
    private Paint paintFocus;
    private Subscription subscription;

    public OverLayerTopView(Context context) {
        this(context, null, 0);
    }

    public OverLayerTopView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public OverLayerTopView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    /**
     * 初始化控件及参数
     *
     * @param context 全局常量
     */
    private void init(Context context) {
        initPaint();
        screenWidth = DisplayUtil.getScreenWidth(context);
        screenHeight = DisplayUtil.getScreenHeight(context);
    }

    /**
     * 初始化画笔
     */
    private void initPaint() {
        //中间矩形取景框的边界
        mRectBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mRectBorderPaint.setColor(Color.BLUE);
        mRectBorderPaint.setStyle(Paint.Style.STROKE);
        mRectBorderPaint.setStrokeWidth(5f);
        mRectBorderPaint.setAlpha(0);//透明度

        //阴影区域的画笔
        mShadePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mShadePaint.setColor(Color.GRAY);
        mShadePaint.setStyle(Paint.Style.FILL);
        mShadePaint.setAlpha(100);

        //矩形四角的短线
        mLinePaint = new Paint();
        mLinePaint.setColor(Color.YELLOW);
        mLinePaint.setAlpha(180);
        mLinePaint.setStrokeWidth(15f);

        //顶部文字提示信息
        wordPaint = new Paint(Paint.ANTI_ALIAS_FLAG);//抗锯齿
        wordPaint.setColor(Color.WHITE);//字体颜色
        wordPaint.setTextAlign(Paint.Align.CENTER);//居中显示
        wordPaint.setStrokeWidth(8f);//画笔的宽度
        wordPaint.setTextSize(45);//字体大小

        //获取焦点的框
        paintFocus = new Paint();
        paintFocus.setColor(Color.YELLOW);
        paintFocus.setStyle(Paint.Style.STROKE);
        paintFocus.setStrokeWidth(5f);


    }

    /**
     * 设置取景框的矩形区域大小
     *
     * @param mCenterRect 取景框矩形
     */
    public void setCenterRect(Rect mCenterRect) {
        this.mCenterRect = mCenterRect;
    }

    /**
     * 取消中间取景框
     *
     * @param mCenterRect 取景框
     */
    public void setDismissCenterRect(Rect mCenterRect) {
        this.mCenterRect = null;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //判断取景框矩形是否为空
        if (mCenterRect == null) return;
        //绘制阴影区域
        drawShadeRect(canvas);
        //绘制四角的短线
        drawLine(canvas);
        //绘制中间矩形取景框
        canvas.drawRect(mCenterRect, mRectBorderPaint);

        drawTipText(canvas);


        drawFocus(canvas);

        super.onDraw(canvas);
    }
   //设置焦点
    private void drawFocus(Canvas canvas) {
        float centerX = rectF.centerX();
        float centerY = rectF.centerY();
        canvas.scale(scale, scale, centerX, centerY);
        canvas.drawRect(rectF, paintFocus);
        canvas.drawLine(rectF.left, centerY, rectF.left + lineSize, centerY, paintFocus);
        canvas.drawLine(rectF.right, centerY, rectF.right - lineSize, centerY, paintFocus);
        canvas.drawLine(centerX, rectF.top, centerX, rectF.top + lineSize, paintFocus);
        canvas.drawLine(centerX, rectF.bottom, centerX, rectF.bottom - lineSize, paintFocus);
    }

    /**
     * 画文字提示
     *
     * @param canvas 画布
     */
    private void drawTipText(Canvas canvas) {
        canvas.drawText(TIPS, mCenterRect.centerX(), mCenterRect.top - DisplayUtil.dp2px(getContext(), 20), wordPaint);
    }

    /**
     * 绘制阴影区
     *
     * @param canvas 画布
     */
    private void drawShadeRect(Canvas canvas) {
        canvas.drawRect(0, 0, screenWidth, mCenterRect.top - 2, mShadePaint);//顶部
        canvas.drawRect(0, mCenterRect.bottom + 2, screenWidth, screenHeight, mShadePaint);//左侧
        canvas.drawRect(0, mCenterRect.top - 2, mCenterRect.left - 2, mCenterRect.bottom + 2, mShadePaint);//下部
        canvas.drawRect(mCenterRect.right + 2, mCenterRect.top - 2, screenWidth, mCenterRect.bottom + 2, mShadePaint);//右侧
    }

    /**
     * 绘制取景框四个角的短线
     */
    private void drawLine(Canvas canvas) {
        //左下
        canvas.drawRect(mCenterRect.left - 4, mCenterRect.bottom, mCenterRect.left + 50, mCenterRect.bottom + 4, mLinePaint);//底部
        canvas.drawRect(mCenterRect.left - 4, mCenterRect.bottom - 50, mCenterRect.left, mCenterRect.bottom, mLinePaint);//左侧
        //左上
        canvas.drawRect(mCenterRect.left - 4, mCenterRect.top - 4, mCenterRect.left + 50, mCenterRect.top, mLinePaint);//顶部
        canvas.drawRect(mCenterRect.left - 4, mCenterRect.top, mCenterRect.left, mCenterRect.top + 50, mLinePaint);//左侧
        //右上
        canvas.drawRect(mCenterRect.right - 50, mCenterRect.top - 4, mCenterRect.right + 4, mCenterRect.top, mLinePaint);//顶部
        canvas.drawRect(mCenterRect.right, mCenterRect.top, mCenterRect.right + 4, mCenterRect.top + 50, mLinePaint);//右侧
        //右下
        canvas.drawRect(mCenterRect.right - 50, mCenterRect.bottom, mCenterRect.right + 4, mCenterRect.bottom + 4, mLinePaint);//右侧
        canvas.drawRect(mCenterRect.right, mCenterRect.bottom - 50, mCenterRect.right + 4, mCenterRect.bottom, mLinePaint);//底部
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = MotionEventCompat.getActionMasked(event);
        if (event.getPointerCount() == 1 && action == MotionEvent.ACTION_DOWN) {
            float x = event.getX();
            float y = event.getY();
            setFoucsPoint(x, y);
            if (listener != null) {
                listener.handleFocus(x, y);
            }
        } else if (event.getPointerCount() >= 2) {
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_POINTER_DOWN:
                    oldDist = getFingerSpacing(event);
                    break;
                case MotionEvent.ACTION_MOVE:
                    float newDist = getFingerSpacing(event);
                    if (newDist > oldDist) {
                        if (this.listener != null) {
                            this.listener.handleZoom(true);
                        }
                    } else if (newDist < oldDist) {
                        if (this.listener != null) {
                            this.listener.handleZoom(false);
                        }
                    }
                    oldDist = newDist;
                    break;
            }
        }
        return true;
    }

    /**
     * 计算两点触控距离
     *
     * @param event
     * @return
     */
    private float getFingerSpacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }

    private RectF rectF = new RectF();
    /**
     * focus size
     */
    private int focusSize = 80;

    private int lineSize = focusSize / 4;
    /**
     * 动画时长
     */
    private static final int ANIM_MILS = 600;
    /**
     * 动画每多久刷新一次
     */
    private static final int ANIM_UPDATE = 30;


    private float scale;
    /**
    /**
     * 设置当前触摸点
     * @param x
     * @param y
     */
    private void setFoucsPoint(float x, float y) {
        if (subscription != null) {
            subscription.unsubscribe();

        }
        rectF.set(x - focusSize, y - focusSize, x + focusSize, y + focusSize);
        final int count = ANIM_MILS / ANIM_UPDATE;
        subscription = Observable.interval(ANIM_UPDATE, TimeUnit.MILLISECONDS).take(count).subscribe(new Subscriber<Long>() {
            @Override
            public void onCompleted() {
                scale = 0;
                postInvalidate();
            }

            @Override
            public void onError(Throwable e) {
                scale = 0;
                postInvalidate();
            }

            @Override
            public void onNext(Long aLong) {
                float current = aLong== null ? 0 : aLong.longValue();
                scale = 1 - current / count;
                if (scale <= 0.5f) {
                    scale = 0.5f;
                }
                postInvalidate();
            }
        });
    }


    private OnViewTouchListener listener;

    public void setOnViewTouchListener(OnViewTouchListener listener) {
        this.listener = listener;
    }

    public void removeOnZoomListener() {
        this.listener = null;
    }

    public interface OnViewTouchListener {
        /**
         * 对焦
         *
         * @param x
         * @param y
         */
        void handleFocus(float x, float y);

        /**
         * 缩放
         *
         * @param zoom true放大反之
         */
        void handleZoom(boolean zoom);
    }
}
