package com.chuanglgc.wuye.camera;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.utils.DisplayUtil;


public class CameraActivity extends Activity {

    private MyCamera view;
    private ImageView ivCommit;
 //   private ImageView ivFlash;
    private ImageView ivTakePhoto;
    private TextView tvFlash;
    private String photoPath;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        view = findViewById(R.id.camera);
        ivCommit = findViewById(R.id.iv_commit);
        OverLayerTopView mOverLayerView = findViewById(R.id.over_layer_view);
        //ivFlash = findViewById(R.id.flash);
        tvFlash = findViewById(R.id.tv_flash);
        ivTakePhoto = findViewById(R.id.iv_take_photo);
        tvFlash.setVisibility(view.isSupportFlashCamera(this) ? View.VISIBLE : View.GONE);
        int left = DisplayUtil.getScreenWidth(this)/5;
        int right = DisplayUtil.getScreenWidth(this)-left;

        int top = DisplayUtil.getScreenHeight(this)/2-DisplayUtil.getScreenHeight(this)/10;
        int bottom =  DisplayUtil.getScreenHeight(this)/2 +DisplayUtil.getScreenHeight(this)/10;
        mOverLayerView.setCenterRect(new Rect(left, top, right, bottom));
        view.start();

        mOverLayerView.setOnViewTouchListener(new OverLayerTopView.OnViewTouchListener() {
            @Override
            public void handleFocus(float x, float y) {
                if (view.isPreView) view.handleFocusMetering(x, y);
            }

            @Override
            public void handleZoom(boolean zoom) {
                view.handleZoom(zoom);
            }
        });
        view.setOnCameraSuccessListener(new MyCamera.OnCameraSuccessListener() {
            @Override
            public void onSuccess(String path) {
                Log.e("保存路径", path);
                photoPath=path;
                ivCommit.setVisibility(View.VISIBLE);
                ivTakePhoto.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFuiler(String exception) {
                Log.e("保存路径", exception);
            }
        });
    }

    //取消
    public void cancle(View v) {
        if (ivCommit.getVisibility() == View.VISIBLE) {
            ivCommit.setVisibility(View.INVISIBLE);
            ivTakePhoto.setVisibility(View.VISIBLE);
            view.start();
        } else {
            finish();
        }

    }

    //拍照
    public void takePhoto(View v) {
        view.cutPic();
    }

    //提交
    public void commit(View v) {
        if (photoPath!=null){
            Intent intent = new Intent();
            intent.putExtra("photoPath", photoPath);
            setResult(5,intent);
            finish();
        }

    }

    public void changeFlash(View v) {
        view.changeCameraFlash();
        setCameraFlashState();
    }

    /**
     * 设置闪光状态
     */
    private void setCameraFlashState() {
        int flashState = view.getCameraFlash();
        switch (flashState) {
            case 0: //自动
                tvFlash.setSelected(true);
                tvFlash.setText("自动");
                break;
            case 1://open
                tvFlash.setSelected(true);
                tvFlash.setText("开启");
                break;
            case 2: //close
                tvFlash.setSelected(false);
                tvFlash.setText("关闭");
                break;
        }
    }

}
