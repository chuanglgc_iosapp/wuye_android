package com.chuanglgc.wuye.camera;

import android.content.Context;
import android.content.pm.FeatureInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.ImageView;
import android.widget.Toast;

import com.chuanglgc.wuye.utils.LogUtil;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;


public class MyCamera extends SurfaceView implements SurfaceHolder.Callback {
    public static Camera camera;
    private ImageView imageView;
    private Camera.Parameters parameters;
    public boolean isPreView = false;

    /**
     * 相机闪光状态
     */
    private int cameraFlash;

    public MyCamera(Context context) {
        super(context);
        init();
    }


    private void init() {
        getHolder().addCallback(this);
    }

    public MyCamera(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    @Override
    public void surfaceCreated(final SurfaceHolder holder) {
        release();
        open(holder);
    }

    private void open(SurfaceHolder holder) {
        camera = Camera.open();
        try {
            camera.setPreviewDisplay(holder);
            //模拟器需要设置
            parameters = camera.getParameters();
            List<Camera.Size> photoSizes = parameters.getSupportedPictureSizes();//获取系统可支持的图片尺寸
            for (Camera.Size size : photoSizes) {//因为我需要的是4：3的图片，所以设置为4：3图片尺寸。
                if (size.width / 4 == size.height / 3) {
                    parameters.setPictureSize(size.width, size.height);
                    Log.d("setPictureSize", "SET width:" + size.width + " height " + size.height);
                    break;
                }
            }
            parameters.setJpegQuality(100);
            parameters.setRotation(90);//防止保存的图片旋转


            Point bestPreviewSizeValue1 = findBestPreviewSizeValue(parameters.getSupportedPreviewSizes());
            parameters.setPreviewSize(bestPreviewSizeValue1.x, bestPreviewSizeValue1.y);
            //进行横竖屏判断然后对图像进行校正
            //如果是竖屏
            if (getContext().getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
                camera.setDisplayOrientation(90);
            } else {//如果是横屏
                camera.setDisplayOrientation(0);
            }
            camera.setParameters(parameters);
            camera.startPreview();
            isPreView = true;
            camera.cancelAutoFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //拍照
    public void cutPic() {
        if (camera != null) {
            camera.autoFocus(new Camera.AutoFocusCallback() {
                @Override
                public void onAutoFocus(boolean success, Camera c) {
                    if (success) { // 成功
                        camera.cancelAutoFocus();
                        Log.e("对焦成功", "myAutoFocusCallback: ");
                        isPreView = false;
                        camera.takePicture(sc, pc, jpgcall);
                    }else {
                        Log.e("对焦失败", "myAutoFocusCallback: ");
                    }
                }
            });
        }
    }

    public void setImage(ImageView imageView) {
        this.imageView = imageView;
    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    public void start() {
        if (camera != null){
            camera.startPreview();
            isPreView = true;
        }

    }


    /**
     * 对焦
     *
     * @param x
     * @param y
     */
    public void handleFocusMetering(float x, float y) {
        if (camera == null) return;
        camera.cancelAutoFocus();
        Camera.Parameters params = camera.getParameters();
        Camera.Size previewSize = params.getPreviewSize();
        Rect focusRect = calculateTapArea(x, y, 1f, previewSize);
        Rect meteringRect = calculateTapArea(x, y, 1.5f, previewSize);
        //camera.cancelAutoFocus();

        if (params.getMaxNumFocusAreas() > 0) {
            List<Camera.Area> focusAreas = new ArrayList<>();
            focusAreas.add(new Camera.Area(focusRect, 1000));
            params.setFocusAreas(focusAreas);
        } else {
            LogUtil.i("focus areas not supported");
        }
        if (params.getMaxNumMeteringAreas() > 0) {
            List<Camera.Area> meteringAreas = new ArrayList<>();
            meteringAreas.add(new Camera.Area(meteringRect, 1000));
            params.setMeteringAreas(meteringAreas);
        } else {
            LogUtil.i("metering areas not supported");
        }
        final String currentFocusMode = params.getFocusMode();
        params.setFocusMode(currentFocusMode);
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        camera.setParameters(params);

        camera.autoFocus(new Camera.AutoFocusCallback() {
            @Override
            public void onAutoFocus(boolean success, Camera camera) {
                if (success)camera.cancelAutoFocus();
                LogUtil.e("对焦成功",success+"");

               Camera.Parameters params = camera.getParameters();
                params.setFocusMode(currentFocusMode);
                camera.setParameters(params);
            }
        });
    }


    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        release();
    }

    public void release() {
        if (camera != null) {
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    private Rect calculateTapArea(float x, float y, float coefficient, Camera.Size previewSize) {
        float focusAreaSize = 300;
        int areaSize = Float.valueOf(focusAreaSize * coefficient).intValue();
        int centerX = (int) (x / previewSize.width - 1000);
        int centerY = (int) (y / previewSize.height - 1000);
        int left = clamp(centerX - areaSize / 2, -1000, 1000);
        int top = clamp(centerY - areaSize / 2, -1000, 1000);
        RectF rectF = new RectF(left, top, left + areaSize, top + areaSize);
        return new Rect(Math.round(rectF.left), Math.round(rectF.top), Math.round(rectF.right), Math.round(rectF.bottom));
    }

    private int clamp(int x, int min, int max) {
        if (x > max) {
            return max;
        }
        if (x < min) {
            return min;
        }
        return x;
    }

    private Camera.PictureCallback jpgcall = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) { // 保存图片的操作
            Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);

            int difH = bmp.getHeight() / 10;
            int y = bmp.getHeight() / 2 - difH;

            int width = bmp.getWidth();
            LogUtil.e("宽度",width);
            LogUtil.e("宽度1",bmp.getHeight());
            int left= width/4;
            int right=width-width/4*2;

            Bitmap rectBitmap = Bitmap.createBitmap(bmp, left, y, right, difH * 2);//截取

            String fileName = System.currentTimeMillis() + ".jpg";
            String filePath = Environment.getExternalStorageDirectory()
                    .toString()
                    + File.separator
                    + "zsl"
                    + File.separator
                    + fileName;
            if (imageView != null) imageView.setImageBitmap(rectBitmap);
            save(rectBitmap, filePath, fileName);
        }

    };

    //保存相册
    private void save(Bitmap bitmap, String filePath, String fileName) {
        File file = new File(filePath);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();// 创建文件夹
        }
        try {
            BufferedOutputStream bos = new BufferedOutputStream(
                    new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos); // 向缓冲区之中压缩图片
            bos.flush();
            bos.close();
            if (iListener != null) iListener.onSuccess(filePath);
        } catch (Exception e) {
            if (iListener != null) iListener.onFuiler(e.getMessage());
        }
    }

    private Camera.ShutterCallback sc = new Camera.ShutterCallback() {
        @Override
        public void onShutter() {
            // 按下快门之后进行的操作
        }
    };
    private Camera.PictureCallback pc = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

        }
    };


    /**
     * 改变闪光状态
     */
    public void changeCameraFlash() {
        if (!isSupportFlashCamera(getContext())) {
            Toast.makeText(getContext(), "您的手机不支闪光", Toast.LENGTH_SHORT).show();
            return;
        }
        if (camera != null) {
            Camera.Parameters parameters = camera.getParameters();
            if (parameters != null) {
                int newState = cameraFlash;
                switch (cameraFlash) {
                    case 0: //自动
                        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                        newState = 1;
                        break;
                    case 1://open
                        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        newState = 2;
                        break;
                    case 2: //close
                        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                        newState = 0;
                        break;
                }
                cameraFlash = newState;
                //  CameraUtils.setCameraFlash(context, newState);
                camera.setParameters(parameters);
            }
        }
    }

    /**
     * 是否支持闪光
     *
     * @param context
     * @return
     */
    boolean isSupportFlashCamera(Context context) {
        FeatureInfo[] features = context.getPackageManager().getSystemAvailableFeatures();
        for (FeatureInfo info : features) {
            if (PackageManager.FEATURE_CAMERA_FLASH.equals(info.name))
                return true;
        }
        return false;
    }

    public int getCameraFlash() {
        return cameraFlash;
    }

    /**
     * 通过对比得到与宽高比最接近的尺寸（如果有相同尺寸，优先选择）
     *
     * @return 得到与原宽高比例最接近的尺寸
     */
    protected static Point findBestPreviewSizeValue(List<Camera.Size> sizeList) {
        int bestX = 0;
        int bestY = 0;
        int size = 0;
        for (Camera.Size nowSize : sizeList) {
            int newX = nowSize.width;
            int newY = nowSize.height;
            int newSize = Math.abs(newX * newX) + Math.abs(newY * newY);
            float ratio = (float) (newY * 1.0 / newX);
            if (newSize >= size && ratio != 0.75) {//确保图片是16:9
                bestX = newX;
                bestY = newY;
                size = newSize;
            } else if (newSize < size) {
                continue;
            }
        }
        if (bestX > 0 && bestY > 0) {
            return new Point(bestX, bestY);
        }
        return null;
    }

    /**
     * 缩放
     */
    public void handleZoom(boolean isZoomIn) {
        if (camera == null) return;
        Camera.Parameters params = camera.getParameters();
        if (params == null) return;
        if (params.isZoomSupported()) {
            int maxZoom = params.getMaxZoom();
            int zoom = params.getZoom();
            if (isZoomIn && zoom < maxZoom) {
                zoom++;
            } else if (zoom > 0) {
                zoom--;
            }
            params.setZoom(zoom);
            camera.setParameters(params);
        } else {
            LogUtil.i("zoom not supported");
        }
    }

    private OnCameraSuccessListener iListener;

    public void setOnCameraSuccessListener(OnCameraSuccessListener listener) {
        this.iListener = listener;
    }

    interface OnCameraSuccessListener {
        void onSuccess(String path);

        void onFuiler(String exception);
    }
}
