package com.chuanglgc.wuye;

import android.app.Application;

import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.model.LoginModel;
import com.chuanglgc.wuye.network.Constant;
import com.chuanglgc.wuye.utils.CacheUtil;
import com.chuanglgc.wuye.utils.StateBarConfig;

import java.util.ArrayList;
import java.util.List;


public class EmployeeApplication extends Application {
    private static EmployeeApplication INSTANCE;
    private CacheUtil cacheUtil;
    private List<BaseActivity> activityList = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        StateBarConfig.setToolbarDrawable(R.color.white);
        StateBarConfig.setStatusbarDrawable(R.color.white);
        StateBarConfig.setBackDrawable(R.color.black);
        StateBarConfig.setIsStatusBarLight(true);//初始化statebar颜色和字体 在BaseActivity中设置
    }

    public static EmployeeApplication getInstance() {
        return INSTANCE;
    }

    public CacheUtil getCacheUtil() {
        if (cacheUtil == null) {
            cacheUtil = CacheUtil.get(INSTANCE.getCacheDir());
        }
        return cacheUtil;
    }

    /**
     * 从缓存中获取用户信息
     *
     * @return LoginModel
     */
    public LoginModel getUserInfoFromCache() {
        LoginModel userInfoModel = (LoginModel) getCacheUtil().getSerializableObj(Constant.USER_KEY);
        return userInfoModel;
    }

    public List<BaseActivity> getActivityList() {
        return activityList;
    }

    public  void putActivity(BaseActivity activity) {
        if (!activityList.contains(activity)) {
            activityList.add(activity);
        }
    }
    /**
     * 移除Activity
     *
     * @param activity
     */
    public void removeActivity(BaseActivity activity) {
        if (activity != null &&activityList.contains(activity)) {
            activityList.remove(activity);
        }
    }

    /**
     * 清除所有Activity
     */
    public void removeAllActivity() {
        for (BaseActivity activity : activityList) {
            if (activity != null && !activity.isFinishing())
                activity.finish();
        }
    }
}
