package com.chuanglgc.wuye.activity.event_owner;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.IOwnerDetailView;
import com.chuanglgc.wuye.MVP.persent.OwnerEventDetailPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.dialog.EventInfoDialog;
import com.chuanglgc.wuye.model.OwnerEventDetailModel;
import com.chuanglgc.wuye.model.VisitRecordModel;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;
import com.zhy.android.percent.support.PercentRelativeLayout;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OwnerEventDetailActivity extends BaseActivity<IOwnerDetailView, OwnerEventDetailPersent> implements View.OnClickListener, IOwnerDetailView {
    @BindView(R.id.toolbar)
    MyToolbar myToolbar;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.rl_time)
    PercentRelativeLayout rlTime;
    @BindView(R.id.tv_room)
    TextView tvRoom;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.tv_detail)
    TextView tvDetail;

    @BindView(R.id.tv_date_histroy)
    TextView tvDateHistroy;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_more)
    TextView tvMore;
    @BindView(R.id.rc_left)
    RelativeLayout rcLeft;
    @BindView(R.id.iv_round)
    ImageView ivRound;
    @BindView(R.id.bt_visitor)
    TextView btVisitor;
    @BindView(R.id.bt_finish)
    TextView btFinish;
    @BindView(R.id.rl_power)
    PercentRelativeLayout rlPower;
    @BindView(R.id.rl_info_foot)
    PercentRelativeLayout rlInfo;
    @BindView(R.id.tv_master)
    TextView tvMaster;
    @BindView(R.id.tv_mate)
    TextView tvMate;
    @BindView(R.id.bt_accept)
    MyPhotoBtton btAccept;
    @BindView(R.id.iv_line)
    ImageView ivLine;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_person)
    TextView tvPerson;
    @BindView(R.id.tv_call)
    TextView tvCall;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.tv_title_time)
    TextView tvTitleTime;
    private String power;
    private String taskId;
    private String repaireId;
    private List<VisitRecordModel> visitModels;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_event_owner_detail;
    }

    /**
     * 初始化时候判断权限 当为负责人的时候才有回访和解决按钮
     * 当在处理中的时候 有rl_info 事件详情
     * 在未接受的时候显示接受
     */
    @Override
    protected void initView() {
        Toolbar toolbar = myToolbar.getToolbar();
        if (toolbar != null) {
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> {
                finish();
            });
        }
        power = getIntent().getStringExtra("power");
        taskId = getIntent().getStringExtra("task_id");
        repaireId = getIntent().getStringExtra("repaire_id");
        initBtPower();//设置权限
        requestDetail();//请求详情信息

    }

    /**
     * 请求回访记录
     */
    private void requestVisitRecord() {
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("repair_id", repaireId);
        persenter.requestVisitRecord(map);
    }

    /**
     * 请求事件详情
     */
    private void requestDetail() {
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("task_id", taskId);
        map.put("repair_id", repaireId);
        LogUtil.e("请求id", taskId + "~~~" + repaireId);
        persenter.requestTaskDetail(map);
    }

    /**
     * 判断权限
     */
    private void initBtPower() {
        if (power.equals("1")) {//未接受 2处理中
            ivLine.setVisibility(View.GONE);//line图片显示消失
            btVisitor.setVisibility(View.GONE);//回访消失
            btFinish.setVisibility(View.GONE);//解决消失
            rlInfo.setVisibility(View.INVISIBLE);//信息消失
            btAccept.setVisibility(View.VISIBLE);//接受显示
        } else {
            requestVisitRecord();//在处理中请求回访信息
        }

    }


    @Override
    protected void initData() {

    }

    @Override
    protected OwnerEventDetailPersent oncreatPersenter() {
        return new OwnerEventDetailPersent();
    }

    @Override
    protected void initListener() {
        tvMore.setOnClickListener(this);
        btFinish.setOnClickListener(this);
        btVisitor.setOnClickListener(this);
        btAccept.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_more:
                showMoreInfoDialog();
                break;
            case R.id.bt_finish:
                Intent intentFinish = new Intent(this, OwnerEventFinishActivity.class);
                intentFinish.putExtra("task_id", taskId);
                intentFinish.putExtra("repair_id", repaireId);
                startActivityForResult(intentFinish, 1);
                break;
            case R.id.bt_visitor:
                Intent intentVisitor = new Intent(this, OwnerEventVisitorActivity.class);
                // intentVisitor.putExtra("task_id", taskId);
                intentVisitor.putExtra("repair_id", repaireId);
                startActivityForResult(intentVisitor, 5);
                break;
            case R.id.bt_accept:
                acceptEvent();
                break;
        }
    }

    //接受业主事件
    private void acceptEvent() {
        HashMap<String, String> map = new HashMap<>();
        map.put("task_id", taskId);
        map.put("repair_id", repaireId);
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        persenter.acceptOwnerEvent(map);
    }

    //接受任务回调
    @Override
    public void getAcceptResult(String result) {
        Toast.makeText(this, "接受成功", Toast.LENGTH_SHORT).show();
        setResult(100);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == 10) {
            setResult(100);
            finish();
        }
        if (requestCode == 5 && resultCode == 5) {
            requestVisitRecord();
        }
    }

    //显示显示更多窗口
    private void showMoreInfoDialog() {
        if (visitModels != null && visitModels.size() != 0) {
            EventInfoDialog dialog = new EventInfoDialog(this, visitModels);
            dialog.show();
        }
    }


    /**
     * 获取业主事件详情 model
     */
    @Override
    public void getOwnerDetail(OwnerEventDetailModel model) {
        tvDate.setText(model.getRepair_date());
        tvMaster.setText(model.getLeading_person());
        if (!TextUtils.isEmpty(model.getAssistant_person()) && !model.getAssistant_person().equals("")) {
            String partner = "、" + model.getAssistant_person();
            tvMate.setText(partner);
        }
        tvRoom.setText(model.getRepair_address());
        tvType.setText(model.getRepair_type_name());
        tvDetail.setText(model.getRepair_content());
        tvPhone.setText(model.getRepair_user_phone());
        tvName.setText(model.getRepair_user_name());
        tvDate.setText(model.getRepair_date());
        tvTitleTime.setText(model.getRepair_time());


    }

    //获取回访记录
    @Override
    public void getVisitRecords(List<VisitRecordModel> visitModels) {
        if (visitModels != null && visitModels.size() > 0) {
            //设置页面显示的条目
            VisitRecordModel model = visitModels.get(0);
            tvDateHistroy.setText(model.getFeedback_date());
            tvTime.setText(model.getFeedback_time());
            tvPerson.setText(model.getContact_person());
            tvCall.setText(model.getContact_info());
            tvContent.setText(model.getFeedback_content());
            this.visitModels = visitModels;//用于查看更多

        } else {
            rlInfo.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void getOwnerDetailErrow(String errow) {
        Toast.makeText(this, errow, Toast.LENGTH_SHORT).show();
    }

}
