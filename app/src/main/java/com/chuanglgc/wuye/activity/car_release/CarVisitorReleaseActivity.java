package com.chuanglgc.wuye.activity.car_release;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.CarVisitorModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.CallPhoneUtil;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;
import com.chuanglgc.wuye.widget.photoview.ShowImageActivity;
import com.zhy.android.percent.support.PercentLinearLayout;

import java.util.HashMap;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CarVisitorReleaseActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    MyToolbar toolbar;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_car_number)
    TextView tvCarNumber;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_info)
    TextView tvInfo;
    @BindView(R.id.bt_commit)
    MyPhotoBtton btCommit;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.iv_line)
    ImageView ivLine;
    @BindView(R.id.iv_carNumber)
    ImageView ivCarNumber;
    @BindView(R.id.ll_pic_root)
    PercentLinearLayout llPicRoot;
    @BindView(R.id.iv4)
    ImageView ivCallPhone;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_car_release_visitor;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = toolbar.getToolbar();
        mytoolbar.setTitle("" );
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });

        Intent intent = getIntent();
        CarVisitorModel model = (CarVisitorModel) intent.getSerializableExtra("model" );

        String carImage = model.getVisitor_image();
        if (carImage != null && !carImage.equals("" )) {
            Glide.with(this).load(carImage).into(ivCarNumber);
            ivCarNumber.setOnClickListener(v -> {
                ShowImageActivity.start(this, carImage);
            });
        } else {
            ivLine.setVisibility(View.GONE);
            llPicRoot.setVisibility(View.GONE);
        }
        tvDate.setText(model.getEntry_date());
        tvTime.setText(model.getEntry_time());
        tvAddress.setText(model.getVisit_address());
        tvCarNumber.setText(model.getPlate_number());

        if (model.getRemark() == null || model.getRemark().equals("" )) {
            tvInfo.setText("暂无备注" );
        } else {
            tvInfo.setText(model.getRemark());
        }

        tvPhone.setText(model.getVisitor_phone());
        tvName.setText(model.getVisitor_name());
        btCommit.setOnClickListener(v -> {
            requestReleseCar(model.getVisitor_id());
        });

        ivCallPhone.setOnClickListener(v -> CallPhoneUtil.callPhone(this,model.getVisitor_phone()));

    }

    private void requestReleseCar(String visitor_id) {
        Dialog loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("visit_id", visitor_id);
        map.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        RestClient.getAPIService().letPassVisitor(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code() == 200) {
                    Result body = response.body();
                    if (body.isResult()) {
                        Toast.makeText(CarVisitorReleaseActivity.this, body.getMsg(), Toast.LENGTH_SHORT).show();
                        setResult(8);
                        finish();
                    } else {
                        Toast.makeText(CarVisitorReleaseActivity.this, body.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(CarVisitorReleaseActivity.this, response.code(), Toast.LENGTH_SHORT).show();
                }
                loadingDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Toast.makeText(CarVisitorReleaseActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
                loadingDialog.dismiss();
            }
        });
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }



}
