package com.chuanglgc.wuye.activity.event_supervise;

import android.app.Dialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.ISuperviseHandleView;
import com.chuanglgc.wuye.MVP.persent.SuperviseHandlePersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.HandleFloorNumberAdapter;
import com.chuanglgc.wuye.adapter.event.EventPhotoAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.dialog.CycleDiviceDialog;
import com.chuanglgc.wuye.dialog.EvaluateDialog;
import com.chuanglgc.wuye.dialog.PartnerDialog;
import com.chuanglgc.wuye.dialog.SecondChekDialog;
import com.chuanglgc.wuye.dialog.SpinnerDialog;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.PartnerModel;
import com.chuanglgc.wuye.model.SuperviseHandleDetailModel;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;
import com.zhy.android.percent.support.PercentLinearLayout;
import com.zhy.android.percent.support.PercentRelativeLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;


public class SuperviseHandleDetailActivity extends BaseActivity<ISuperviseHandleView, SuperviseHandlePersent> implements View.OnClickListener
        , ISuperviseHandleView {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.rl_time)
    PercentRelativeLayout rlTime;

    @BindView(R.id.rcl_number)
    TextView rclNumber;
    @BindView(R.id.rcl_pic)
    RecyclerView rclPic;
    @BindView(R.id.ll_pic_root)
    PercentLinearLayout llPicRoot;
    @BindView(R.id.bt_help)
    TextView btHelp;
    @BindView(R.id.bt_handle)
    TextView btHandle;
    @BindView(R.id.bt_evalues)
    MyPhotoBtton btEvalues;
    @BindView(R.id.rl_bt_root)
    RelativeLayout rlBtRoot;
    @BindView(R.id.ll_noevalue_root)
    PercentLinearLayout llNoevalueRoot;
    @BindView(R.id.tv_more)
    TextView tvMore;
    @BindView(R.id.rl_line)
    RelativeLayout rlLine;

    @BindView(R.id.tv_body)
    TextView tvBody;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.tv_level)
    TextView tvLevel;
    @BindView(R.id.tv_master)
    TextView tvMaster;
    @BindView(R.id.tv_mate)
    TextView tvMate;
    @BindView(R.id.tv_detail)
    TextView tvDetail;
    @BindView(R.id.rl_left)
    RelativeLayout rlLeft;
    @BindView(R.id.frame_rcl)
    FrameLayout frameRcl;
    @BindView(R.id.iv_line_one)
    ImageView ivLineOne;
    @BindView(R.id.ivv)
    ImageView ivv;
    @BindView(R.id.tv_taskName)
    TextView tvTaskName;
    @BindView(R.id.ll_device_root)
    PercentRelativeLayout llDeviceRoot;
    @BindView(R.id.tv_level_near)
    TextView tvLevelNear;
    @BindView(R.id.ll_near_root)
    PercentLinearLayout llNearRoot;
    @BindView(R.id.ll_type_root)
    PercentRelativeLayout llTypeRoot;
    @BindView(R.id.tv_time)
    TextView tvTime;
    private List<String> moreList;

    private List<EventAllotModel> marsterList;
    private List<PartnerModel> partnerList;
    private String partnerSleName;
    private Dialog loadingDialog;
    private int masterPostion;
    private String taskId;
    private HandleFloorNumberAdapter adapter;
    private EvaluateDialog evaluateDialog;
    private ArrayList<Integer> partnerIdList;
    private String leaderId;
    private String work_status;
    private String taskName;
    private String devices;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_supervise_handle_detail;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = myToolbar.getToolbar();
        mytoolbar.setTitle("");

        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });

        String state = getIntent().getStringExtra("state");
        taskId = getIntent().getStringExtra("task_id");
        //分三种类型：周期性、临时大事、临时其他
        // 当为临时其他时候只显示 事务级别 备注 处理人
        // 当为临时其他的时候显示 在数据请求成功设置临时其他的优先级 tvLevelNear

        taskName = getIntent().getStringExtra("task_name");
        String taskTime = getIntent().getStringExtra("task_time");//接口中没有该字段  自己传
        tvTime.setText(taskTime);
        if (taskName.equals("临时其他")) {
            llTypeRoot.setVisibility(View.GONE);
            llDeviceRoot.setVisibility(View.GONE);
            llNearRoot.setVisibility(View.VISIBLE);
        }

        work_status = "";
        LogUtil.e("状态", state + "||" + taskId);
        switch (state) {
            case "处理中"://处理中
                work_status = "1";
                rlBtRoot.setVisibility(View.INVISIBLE);
                llPicRoot.setVisibility(View.INVISIBLE);
                myToolbar.setTvState("处理中...", getResources().getColor(R.color.bluelight));
                break;
            case "未接受"://未接受
                llNoevalueRoot.setVisibility(View.VISIBLE);
                btEvalues.setVisibility(View.INVISIBLE);
                rlLine.setVisibility(View.GONE);
                work_status = "2";
                myToolbar.setTvState("未接受...", getResources().getColor(R.color.colorAccent));
                break;
            case "待评价":
                work_status = "3";
                llNoevalueRoot.setVisibility(View.INVISIBLE);
                btEvalues.setVisibility(View.VISIBLE);
                rlLine.setVisibility(View.VISIBLE);
                llPicRoot.setVisibility(View.VISIBLE);
                myToolbar.setTvState("待评价...", getResources().getColor(R.color.yellow));
                break;
        }

        requestDetail();

    }

    private void requestDetail() {
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("cycle_task_id", taskId);
        map.put("work_status", work_status);
        showLoading();
        persenter.requestDetailInfo(map);
    }

    private void initRclPhoto(List<String> picList) {
        rclPic.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        EventPhotoAdapter photoAdapter = new EventPhotoAdapter(picList);
        photoAdapter.bindToRecyclerView(rclPic);
    }


    @Override
    protected void initListener() {
        tvMore.setOnClickListener(this);
        btEvalues.setOnClickListener(this);
        btHandle.setOnClickListener(this);
        btHelp.setOnClickListener(this);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected SuperviseHandlePersent oncreatPersenter() {
        return new SuperviseHandlePersent();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_more:
                if (devices != null && !devices.equals("")) {
                    CycleDiviceDialog diviceDialog = new CycleDiviceDialog(this, devices);
                }
                break;
            case R.id.bt_evalues:
                showEvaluateDialog();
                break;
            case R.id.bt_help:
                requestPartner();
                break;
            case R.id.bt_handle:
                requestMaster();
                break;
        }
    }


    //请求配合人
    private void requestPartner() {
        if (TextUtils.isEmpty(tvMaster.getText().toString().trim())) {
            Toast.makeText(this, "请选择负责人", Toast.LENGTH_SHORT).show();
        } else {
            HashMap<String, String> partnerMap = new HashMap<>();
            partnerMap.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            if (marsterList != null) {
                partnerMap.put("director_id", marsterList.get(masterPostion).getEmp_id());
            } else if (leaderId != null) {
                partnerMap.put("leaderid", leaderId);
            }
            showLoading();
            persenter.requsetPartner(partnerMap);
        }
    }

    //显示loading
    private void showLoading() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
    }

    //请求负责人
    private void requestMaster() {
        showLoading();
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        persenter.requestMaster(map);
    }

    //评价每日工作
    private void evalueTaks(String evluate, int stars) {
        showLoading();
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("daily_task_id", taskId);
        map.put("task_type", "2");
        map.put("star_quantity", stars + "");
        map.put("evaluate_content", evluate);
        persenter.requestEvalue(map);
    }


    /**
     * 显示二次确认负责人dialog
     */
    private void showSeconedCommitDialog(String name) {
        SecondChekDialog chekDialog = new SecondChekDialog(this);
        chekDialog.setContent(name);
        chekDialog.setOnNegativeListener(v -> {
            chekDialog.dismiss();
        });
        chekDialog.setOnPositiveListener(v -> {
            requestAllot();
            chekDialog.dismiss();
        });
        chekDialog.show();

    }

    //请求分配负责人
    private void requestAllot() {
        showLoading();
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("cycle_task_id", taskId);
        if (marsterList != null) {
            map.put("leaderid", marsterList.get(masterPostion).getEmp_id());
        } else {
            map.put("leaderid", leaderId);
        }
        LogUtil.e("分配人id1", marsterList.get(masterPostion).getEmp_id());
        persenter.requestAllot(map);
    }


    /**
     * 配合人二次确认
     */
    private void showPartnerSeconedCommitDialog(String sleName) {
        SecondChekDialog chekDialog = new SecondChekDialog(this);
        if (TextUtils.isEmpty(sleName) || sleName.equals("")) {//无选中任何条目
            chekDialog.setContent("没有选中任何条目");
        } else {
            chekDialog.setContent("添加协助人:" + sleName);
            LogUtil.e("所选则协助人",sleName);
            chekDialog.setOnPositiveListener(v -> {
                requestHelp();//请求接口
                chekDialog.dismiss();
            });
        }
        chekDialog.setOnNegativeListener(v -> {
            chekDialog.dismiss();
        });
        chekDialog.show();

    }

    //追加协助
    private void requestHelp() {
        showLoading();
        String personID = "";
        for (int i = 0; i < partnerIdList.size(); i++) {
            if (i == 0) {
                personID = partnerList.get(partnerIdList.get(0)).getEmp_id();
            } else {
                personID += "," + partnerList.get(partnerIdList.get(i)).getEmp_id();
            }
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("cycle_task_id", taskId);
        map.put("assistant_person", personID);
        persenter.requestHelp(map);
    }

    /**
     * 评价dialog
     */
    private void showEvaluateDialog() {
        evaluateDialog = new EvaluateDialog(this);
        evaluateDialog.setOnCommitEvluateListener((evluate, stars) -> {
            evalueTaks(evluate, stars);
        });
    }

    //获取再分配列表
    @Override
    public void getMasterList(List<EventAllotModel> marsterList) {
        ArrayList<String> listMaster = new ArrayList<>();
        for (EventAllotModel model : marsterList) {
            listMaster.add(model.getEmp_name());
        }
        SpinnerDialog masterDialog = new SpinnerDialog(this, listMaster);
        masterDialog.setOnSpinnerItemClick((itemName, position) -> {
            this.marsterList = marsterList;
            masterPostion = position;
            tvMaster.setText(itemName);
            showSeconedCommitDialog("分配负责人:" + itemName);
            masterDialog.dismiss();

        });
        if (loadingDialog != null) loadingDialog.dismiss();
    }

    //获取追加协助列表
    @Override
    public void getPartner(List<PartnerModel> partnerList) {
        ArrayList<String> listPartner = new ArrayList<>();
        for (PartnerModel model : partnerList) {
            listPartner.add(model.getEmp_name());
        }
        PartnerDialog contentDialog = new PartnerDialog(this, listPartner);
        contentDialog.setOnCommitClick(((itemName, sleIdList) -> {
            this.partnerList = partnerList;
            partnerSleName = itemName.replace(",", "、");
            this.partnerIdList = sleIdList;

            contentDialog.dismiss();
            showPartnerSeconedCommitDialog(partnerSleName);
        }));
        if (loadingDialog != null) loadingDialog.dismiss();
    }

    //再分配成功
    @Override
    public void getAllotTaskResult(String result) {
        if (!leaderId.equals(marsterList.get(masterPostion).getEmp_id())) {//如果分配的负责人不一样
            tvMate.setText("");//将配合人设置为null
        }
        setResult(2);
        finish();
        if (loadingDialog != null) loadingDialog.dismiss();
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
    }

    //追加协助
    @Override
    public void getHelpTaskResult(String result) {
        requestDetail();
        if (loadingDialog != null) loadingDialog.dismiss();
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
    }

    //获取详情
    @Override
    public void getHandleDetail(SuperviseHandleDetailModel handleModel) {
        tvLevel.setText(handleModel.getCycle_task_priority());
        tvDate.setText(handleModel.getCycle_task_date());
        tvType.setText(handleModel.getCycle_task_facilities_type());
        if (handleModel.getCycle_task_remark() != null && !handleModel.getCycle_task_remark().equals("")) {
            tvDetail.setText(handleModel.getCycle_task_remark());
        } else {
            tvDetail.setText("无备注");
        }

        tvMaster.setText(handleModel.getShip_name());
        tvTaskName.setText(handleModel.getCycle_task_content());
        tvBody.setText(handleModel.getCycle_task_type());
        this.leaderId = handleModel.getShip_id();
        if (handleModel.getAssistant_person() != null && !handleModel.getAssistant_person().equals("")) {
            tvMate.setText("、" + handleModel.getAssistant_person());
        }
        String facilitiesList = handleModel.getCycle_task_facilities_id();//设备列表
        if (!TextUtils.isEmpty(facilitiesList) && !facilitiesList.equals("")) {
            // String[] more = facilitiesList.split(",");
            //moreList = Arrays.asList(more);
            //判断如果为空则Fragment不显示
            devices = facilitiesList.replace(",", "   ");
            rclNumber.setText(devices);
            if (rclNumber.getLineCount() >= 4) {
                tvMore.setVisibility(View.VISIBLE);
            }
        } else {
            frameRcl.setVisibility(View.GONE);
            ivLineOne.setVisibility(View.GONE);
        }
        List<String> picList = handleModel.getTask_image1();
        if (picList != null && picList.size() > 0) {
            initRclPhoto(picList);
        } else {
            llPicRoot.setVisibility(View.GONE);
            rlLine.setVisibility(View.GONE);
        }
        //当为临时其他的时候显示 设置临时其他的优先级
        if (taskName.equals("临时其他")) {
            tvLevelNear.setText(handleModel.getCycle_task_priority());
        }
        if (loadingDialog != null) loadingDialog.dismiss();
    }



    //获取评价信息
    @Override
    public void getEvalueResult(String result) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
        if (loadingDialog != null) loadingDialog.dismiss();
        if (evaluateDialog != null) evaluateDialog.dismiss();
        setResult(2);
        finish();
    }

    @Override
    public void getNetError(String error) {
        if (loadingDialog != null) loadingDialog.dismiss();
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

}
