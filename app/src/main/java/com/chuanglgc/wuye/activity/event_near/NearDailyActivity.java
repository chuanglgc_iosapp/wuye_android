package com.chuanglgc.wuye.activity.event_near;

import android.app.Dialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.INearDailyView;
import com.chuanglgc.wuye.MVP.persent.NearDailyPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.dialog.PartnerDialog;
import com.chuanglgc.wuye.dialog.SpinnerDialog;
import com.chuanglgc.wuye.model.DailyWorkTypeModel;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.NearAreaOrDeviceModel;
import com.chuanglgc.wuye.model.NearDailyClassModel;
import com.chuanglgc.wuye.model.NearDailyWorkModel;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;


public class NearDailyActivity extends BaseActivity<INearDailyView, NearDailyPersent> implements View.OnClickListener, INearDailyView {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.tv_master)
    TextView tvMaster;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.sel_content)
    RelativeLayout selContent;
    @BindView(R.id.tv_record)
    EditText tvRecord;
    @BindView(R.id.bt_photo)
    MyPhotoBtton btPhoto;
    @BindView(R.id.tv_content_id)
    TextView tvContentId;
    @BindView(R.id.rb_area)
    RadioButton rbOne;
    @BindView(R.id.rb_divice)
    RadioButton rbTow;
    @BindView(R.id.radio_group)
    RadioGroup radioGroup;
    @BindView(R.id.tv_class)
    TextView tvClass;
    @BindView(R.id.sel_class)
    RelativeLayout selClass;
    @BindView(R.id.iv_work)
    ImageView ivWork;
    @BindView(R.id.iv_content_id)
    ImageView ivContentId;
    @BindView(R.id.sel_content_id)
    RelativeLayout selContentId;
    @BindView(R.id.iv_text)
    ImageView ivText;
    private Dialog loadingDialog;
    private int masterPostion;
    private int classPosition;
    private int workPosition;
    private List<EventAllotModel> masterList;
    private List<NearDailyClassModel> classList;
    private List<NearDailyWorkModel> workList;
    private List<DailyWorkTypeModel> typeList;
    private List<NearAreaOrDeviceModel> areaList;
    private int areaPosition;
    private String areaSleName;
    private ArrayList<Integer> areSleIdList;
    private String rbContentId;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_near_daily;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = myToolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        persenter.requestDailyWorkType(EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());//请求工作类型
    }


    @Override
    protected void initData() {

    }

    @Override
    protected NearDailyPersent oncreatPersenter() {
        return new NearDailyPersent();
    }


    @Override
    protected void initListener() {
        tvContent.setOnClickListener(this);
        tvMaster.setOnClickListener(this);
        btPhoto.setOnClickListener(this);
        tvClass.setOnClickListener(this);
        tvContentId.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_master:
                requestMasterList();
                break;
            case R.id.tv_content:
                requestWorkConent();
                break;
            case R.id.bt_photo:
                allotTask();
                break;
            case R.id.tv_class:
                requestClass();
                break;
            case R.id.tv_content_id:
                requestAreaOrDevice();
                break;
        }
    }

    //分配任务
    private void allotTask() {
        if (TextUtils.isEmpty(tvMaster.getText().toString().trim())) {
            Toast.makeText(this, "请选择负责人", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(tvClass.getText().toString().trim())) {
            Toast.makeText(this, "请选择班次", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(tvContent.getText().toString().trim())) {
            Toast.makeText(this, "请选择工作内容", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(tvContentId.getText().toString().trim())) {
            Toast.makeText(this, "请" + tvContentId.getHint().toString(), Toast.LENGTH_SHORT).show();
        } else {
            HashMap<String, String> map = new HashMap<>();
            map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            map.put("leading_person", masterList.get(masterPostion).getEmp_id());
            map.put("schedule_id", classList.get(classPosition).getClasses_id());
            map.put("daily_task_id", workList.get(workPosition).getWork_id());
            map.put("work_type_num", rbContentId);//设置工作类型id  仪表类或 区域类
            String deviceId = "";

            for (int i = 0; i < areSleIdList.size(); i++) {
                if (i == 0) {
                    deviceId = deviceId + areaList.get(areSleIdList.get(i)).getArea_or_device_id();
                } else {
                    String append = "," + areaList.get(areSleIdList.get(i)).getArea_or_device_id();
                    deviceId = deviceId + append;
                }
            }
            map.put("area_or_device_id", deviceId);
            map.put("area_or_device_name", areaSleName);
            map.put("daily_task_requirements", tvRecord.getText().toString().trim());
            LogUtil.e("名称", map.get("community_id"));
            LogUtil.e("名称", map.get("leading_person"));
            LogUtil.e("名称", map.get("schedule_id"));
            LogUtil.e("名称", map.get("daily_task_id"));
            LogUtil.e("名称", map.get("area_or_device_id"));
            LogUtil.e("名称", map.get("area_or_device_name"));
            LogUtil.e("名称", map.get("daily_task_requirements"));
            persenter.allotTask(map);
        }


    }

    //请求区域或设备信息
    private void requestAreaOrDevice() {
        if (workList == null) {
            Toast.makeText(this, "请选择工作内容", Toast.LENGTH_SHORT).show();
        } else {
            if (loadingDialog == null) {
                loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
            } else {
                loadingDialog.show();
            }
            HashMap<String, String> map = new HashMap<>();
            map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            map.put("work_name", workList.get(workPosition).getWork_name());
            int radioButtonId = radioGroup.getCheckedRadioButtonId();
            if (radioButtonId == R.id.rb_area) {
                map.put("area_or_device", typeList.get(0).getWork_type_num());
            } else if (radioButtonId == R.id.rb_divice) {
                map.put("area_or_device", typeList.get(1).getWork_type_num());
            }
            LogUtil.e("请求区域或设备",workList.get(workPosition).getWork_name());
            LogUtil.e("请求区域或设备",map.get("area_or_device"));
            persenter.requestAreaOrDevice(map);
        }

    }

    //查询班次
    private void requestClass() {
        if (masterList == null) {
            Toast.makeText(this, "请先选择负责人", Toast.LENGTH_SHORT).show();
        } else {
            if (loadingDialog == null) {
                loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
            } else {
                loadingDialog.show();
            }
            HashMap<String, String> map = new HashMap<>();
            map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            map.put("emp_id", masterList.get(masterPostion).getEmp_id());
            persenter.requestClass(map);
        }

    }

    //请求工作内容
    private void requestWorkConent() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
        persenter.requestWorkContent(EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
    }

    //请求负责人数据
    private void requestMasterList() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        persenter.requestMaster(map);
    }

    //获取负责人
    @Override
    public void getMasterList(List<EventAllotModel> masterList) {
        ArrayList<String> listMaster = new ArrayList<>();
        for (EventAllotModel model : masterList) {
            listMaster.add(model.getEmp_name());
        }
        SpinnerDialog masterDialog = new SpinnerDialog(this, listMaster);
        masterDialog.setOnSpinnerItemClick((itemName, position) -> {
            this.masterList = masterList;
            masterPostion = position;
            tvMaster.setText(itemName);
            tvClass.setText("");
            masterDialog.dismiss();

        });
        if (loadingDialog != null) loadingDialog.dismiss();
    }

    //获取班次列表
    @Override
    public void getClassList(List<NearDailyClassModel> classList) {
        ArrayList<String> listClass = new ArrayList<>();
        for (NearDailyClassModel model : classList) {
            listClass.add(model.getClasses_name());
        }
        SpinnerDialog contentDialog = new SpinnerDialog(this, listClass);
        contentDialog.setOnSpinnerItemClick((itemName, position) -> {
            this.classList = classList;
            classPosition = position;
            tvClass.setText(itemName);
            contentDialog.dismiss();

        });
        if (loadingDialog != null) loadingDialog.dismiss();
    }

    //工作内容
    @Override
    public void getWorkContentList(List<NearDailyWorkModel> workModels) {
        ArrayList<String> listWork = new ArrayList<>();
        for (NearDailyWorkModel model : workModels) {
            listWork.add(model.getWork_name());
        }
        SpinnerDialog workDialog = new SpinnerDialog(this, listWork);
        workDialog.setOnSpinnerItemClick((itemName, position) -> {
            this.workList = workModels;
            workPosition = position;
            workDialog.dismiss();
            tvContent.setText(itemName);
            tvContentId.setText("");
        });
        if (loadingDialog != null) loadingDialog.dismiss();
    }

    /**
     * 获取工作类型 设置RadioButton名称并设置cheked时，区域动态改变hint  目前只有两个类型
     */
    @Override
    public void getDailyWorkType(List<DailyWorkTypeModel> typeModelList) {
        //设置RadioButton 名称
        for (int i = 0; i < typeModelList.size(); i++) {
            if (i == 0) {
                rbOne.setText(typeModelList.get(0).getWork_type_name());
            } else if (i == 1) {
                rbTow.setText(typeModelList.get(1).getWork_type_name());
            }
        }

        this.typeList = typeModelList;
        //动态设置hint
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_area) {
                    rbContentId = typeList.get(0).getWork_type_num();//设置工作类型Id
                    tvContentId.setHint("选择" + typeModelList.get(0).getWork_type_name());
                } else if (checkedId == R.id.rb_divice) {
                    tvContentId.setHint("选择" + typeModelList.get(1).getWork_type_name());
                    rbContentId = typeList.get(1).getWork_type_num();
                }
                tvContent.setText("");
                tvContentId.setText("");
            }
        });
        radioGroup.check(R.id.rb_area);//默认选中第一个
    }

    //获取区域类行或仪表类的数据
    @Override
    public void getAreaList(List<NearAreaOrDeviceModel> areaOrDeviceList) {
        ArrayList<String> listArea = new ArrayList<>();
        for (NearAreaOrDeviceModel model : areaOrDeviceList) {
            listArea.add(model.getArea_or_device_name());
        }
        PartnerDialog contentDialog = new PartnerDialog(this, listArea);
        contentDialog.setOnCommitClick(((itemName, sleIdList) -> {
            areaList = areaOrDeviceList;
            areaSleName = itemName;
            LogUtil.e("选择名称",itemName);
            areSleIdList = sleIdList;
            tvContentId.setText(itemName.replace(",","，"));
            contentDialog.dismiss();
        }));
        if (loadingDialog != null) loadingDialog.dismiss();
    }

    //分配任务成功回调
    @Override
    public void getAllotResult(String result) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
        finish();
    }


    @Override
    public void getNetError(String error) {
        if (loadingDialog != null) loadingDialog.dismiss();
        LogUtil.e("工作类型", error);
    }
}
