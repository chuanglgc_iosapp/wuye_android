package com.chuanglgc.wuye.activity.owner_search;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.IOwnerSearchView;
import com.chuanglgc.wuye.MVP.persent.OwnerSearchPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.dialog.SpinnerDialog;
import com.chuanglgc.wuye.model.BuildModel;
import com.chuanglgc.wuye.model.OwnerInfoModel;
import com.chuanglgc.wuye.model.RepairTypeModel;
import com.chuanglgc.wuye.model.RoomModel;
import com.chuanglgc.wuye.model.UnitModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OwnerSearchActivity extends BaseActivity<IOwnerSearchView, OwnerSearchPersent> implements View.OnClickListener, IOwnerSearchView {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.tv_dong)
    TextView tvDong;
    @BindView(R.id.sel_dong)
    RelativeLayout selDong;
    @BindView(R.id.tv_danyuan)
    TextView tvDanyuan;
    @BindView(R.id.tv_door)
    TextView tvDoor;

    @BindView(R.id.sel_danyuan)
    RelativeLayout selDanyuan;
    @BindView(R.id.sel_door)
    RelativeLayout selDoor;
    @BindView(R.id.bt_search)
    MyPhotoBtton btSearch;
    private ArrayList<String> listBuild;
    private ArrayList<String> listUnit;
    private ArrayList<String> listRoom;
    private List<BuildModel> buildModelList;
    private List<UnitModel> unitModelList;
    private Dialog loadingDialog;
    private int buildPosition = -1;
    private int unitPosition = -1;
    private int roomPosition = -1;
    private List<RoomModel> roomModelList;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_owner_search;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = myToolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });

    }


    @Override
    protected void initData() {

    }

    @Override
    protected OwnerSearchPersent oncreatPersenter() {
        return new OwnerSearchPersent();
    }

    @Override
    protected void initListener() {
        selDong.setOnClickListener(this);
        selDanyuan.setOnClickListener(this);
        selDoor.setOnClickListener(this);
        btSearch.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sel_dong:
                queryBuild();
                break;
            case R.id.sel_danyuan:
                queryUnit();
                break;
            case R.id.sel_door:
                queryDoor();
                break;
            case R.id.bt_search:
                String dong = tvDong.getText().toString().trim();
                String danyuan = tvDanyuan.getText().toString().trim();
                String door = tvDoor.getText().toString().trim();
                if (dong.equals("")) {
                    Toast.makeText(this, "请选择栋数", Toast.LENGTH_SHORT).show();
                } else if (danyuan.equals("")) {
                    Toast.makeText(this, "请选择单元数", Toast.LENGTH_SHORT).show();
                } else if (door.equals("")) {
                    Toast.makeText(this, "请选择门牌号", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(this, OwnerInfoDetailActivity.class);
                    intent.putExtra("build_id", buildModelList.get(buildPosition).getBuild_id());
                    intent.putExtra("unit_id", unitModelList.get(unitPosition).getUnit_id());
                    intent.putExtra("room_id", roomModelList.get(roomPosition).getRoom_name());
                    startActivity(intent);
                }
                break;
        }

    }

    /**
     * 获取房间
     */
    private void queryDoor() {
        if (tvDong.getText().toString().equals("")) {
            Toast.makeText(this, "请选择栋数", Toast.LENGTH_SHORT).show();
        } else if (tvDanyuan.getText().toString().equals("")) {
            Toast.makeText(this, "请选择单元", Toast.LENGTH_SHORT).show();
        } else {
            HashMap<String, String> doorMap = new HashMap<>();
            doorMap.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            doorMap.put("build_id", buildModelList.get(buildPosition).getBuild_id());
            doorMap.put("unit_id", unitModelList.get(unitPosition).getUnit_id());
            persenter.QueryRoom(doorMap);
            if (loadingDialog == null) {
                loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
            } else {
                loadingDialog.show();
            }
        }
    }

    /**
     * 获取单元
     */
    private void queryUnit() {
        if (tvDong.getText().toString().equals("")) {
            Toast.makeText(this, "请选择栋数", Toast.LENGTH_SHORT).show();
        } else {
            Map<String, String> mapUnit = new HashMap<>();
            mapUnit.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            mapUnit.put("build_id", buildModelList.get(buildPosition).getBuild_id());
            persenter.QueryUnit(mapUnit);
            if (loadingDialog == null) {
                loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
            } else {
                loadingDialog.show();
            }
        }
    }

    /**
     * 查询栋
     */
    private void queryBuild() {
        loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        Map<String, String> mapBuild = new HashMap<>();
        mapBuild.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        persenter.QueryBuild(mapBuild);
    }


    /**
     * 获取栋
     */
    @Override
    public void getBuildList(List<BuildModel> buildModelList) {
        if (loadingDialog != null) loadingDialog.dismiss();

        listBuild = new ArrayList<>();
        for (BuildModel model : buildModelList) {
            listBuild.add(model.getBuild_name());
        }
        SpinnerDialog spinnerDialog = new SpinnerDialog(this, listBuild);
        spinnerDialog.setOnSpinnerItemClick((itemName, position) -> {
            spinnerDialog.dismiss();
            this.buildModelList = buildModelList;
            this.buildPosition = position;
            tvDong.setText(itemName);
            tvDanyuan.setText("");
            tvDoor.setText("");
        });
        spinnerDialog.show();
    }

    /**
     * 获取单元
     *
     * @param unitModelList
     */
    @Override
    public void getUnitList(List<UnitModel> unitModelList) {
        if (loadingDialog != null) loadingDialog.dismiss();

        LogUtil.e("获取单元", unitModelList.size());
        listUnit = new ArrayList<>();
        for (UnitModel model : unitModelList) {
            listUnit.add(model.getUnit_name());
        }

        SpinnerDialog spinnerDialog = new SpinnerDialog(this, listUnit);
        spinnerDialog.setOnSpinnerItemClick((itemName, position) -> {
            spinnerDialog.dismiss();
            this.unitModelList = unitModelList;
            this.unitPosition = position;
            tvDanyuan.setText(itemName);
            tvDoor.setText("");
        });
        spinnerDialog.show();
    }

    /**
     * 获取房间号
     *
     * @param roomModelList
     */
    @Override
    public void getRoomList(List<RoomModel> roomModelList) {
        if (loadingDialog != null) loadingDialog.dismiss();

        listRoom = new ArrayList<>();
        for (RoomModel model : roomModelList) {
            listRoom.add(model.getRoom_name());
        }

        SpinnerDialog spinnerDialog = new SpinnerDialog(this, listRoom);
        spinnerDialog.setOnSpinnerItemClick((itemName, position) -> {
            spinnerDialog.dismiss();
            this.roomModelList = roomModelList;
            this.roomPosition = position;
            tvDoor.setText(itemName);
        });
        spinnerDialog.show();
    }

    @Override
    public void getRepairTypeList(List<RepairTypeModel> repairTypeModels) {

    }

    //获取网络数据失败
    @Override
    public void getNetInfoError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        if (loadingDialog != null) loadingDialog.dismiss();
    }


}
