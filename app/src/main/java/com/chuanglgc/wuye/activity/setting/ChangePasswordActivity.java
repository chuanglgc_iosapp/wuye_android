package com.chuanglgc.wuye.activity.setting;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.activity.LoginActivity;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.network.Constant;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.utils.MD5Utils;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.HashMap;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.et_newpassword)
    EditText etNewpassword;
    @BindView(R.id.et_checkpassword)
    EditText etCheckpassword;
    @BindView(R.id.bt_commit)
    MyPhotoBtton btCommit;
    @BindView(R.id.et_oldpassword)
    EditText etOldpassword;
    @BindView(R.id.iv_current)
    ImageView ivCurrent;
    @BindView(R.id.iv_new)
    ImageView ivNew;
    @BindView(R.id.iv_check)
    ImageView ivCheck;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_change_password;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = myToolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
    }


    private void changePassword() {
        String oldPassword = etOldpassword.getText().toString().trim();
        String newPassword = etNewpassword.getText().toString().trim();
        String checkPassword = etCheckpassword.getText().toString().trim();
        if (oldPassword.equals("")){
            Toast.makeText(this,"请输入当前密码",Toast.LENGTH_SHORT).show();
        }else if (newPassword.equals("")||newPassword.length()<6){
            Toast.makeText(this,"新密码最少设置六位",Toast.LENGTH_SHORT).show();
        }else if (checkPassword.equals("")){
            Toast.makeText(this,"请输入确认密码",Toast.LENGTH_SHORT).show();
        }else {
            if (newPassword.equals(checkPassword)) {
                String password = MD5Utils.md5(Constant.salt + newPassword);//密码+salt 在MD5
                HashMap<String, String> map = new HashMap<>();
                map.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
                map.put("old_password", MD5Utils.md5(Constant.salt + oldPassword));
                map.put("new_password", password);
                map.put("confirm_password", password);
                RestClient.getAPIService().changePassword(map).enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {
                        if (response.code() == 200) {
                            Result body = response.body();
                            if (body.isResult()) {
                                EmployeeApplication.getInstance().removeAllActivity();
                                Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                                startActivity(intent);
                            }
                            Toast.makeText(ChangePasswordActivity.this, body.getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {

                    }
                });
            }else {
                Toast.makeText(this,"请确认新密码是否一致",Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    protected void initData() {

    }

    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }


    @Override
    protected void initListener() {
        btCommit.setOnClickListener(this);
        ivCurrent.setOnClickListener(this);
        ivCheck.setOnClickListener(this);
        ivNew.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_commit:
                changePassword();
                break;
            case R.id.iv_current:
                int inputType = etOldpassword.getInputType();
                if (inputType == (InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)) {//如果不可见
                    etOldpassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);//密码可见
                    ivCurrent.setSelected(true);
                } else {
                    etOldpassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);//密码不可见
                    ivCurrent.setSelected(false);
                }
                break;
            case R.id.iv_new:
                int inputTypeNew = etNewpassword.getInputType();
                if (inputTypeNew == (InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)) {//如果不可见
                    etNewpassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);//密码可见
                    ivNew.setSelected(true);
                } else {
                    etNewpassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);//密码不可见
                    ivNew.setSelected(false);
                }
                break;
            case R.id.iv_check:
                int inputTypeCheck = etCheckpassword.getInputType();
                if (inputTypeCheck == (InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)) {//如果不可见
                    etCheckpassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);//密码可见
                    ivCheck.setSelected(true);
                } else {
                    etCheckpassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);//密码不可见
                    ivCheck.setSelected(false);
                }
                break;
        }
    }
}
