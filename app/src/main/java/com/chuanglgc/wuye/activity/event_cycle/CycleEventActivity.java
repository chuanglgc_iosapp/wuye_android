package com.chuanglgc.wuye.activity.event_cycle;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.INormalView;
import com.chuanglgc.wuye.MVP.persent.CycleEventPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.CycleEventAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.CycleEventModel;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import butterknife.BindView;


public class CycleEventActivity extends BaseActivity<INormalView, CycleEventPersent> implements INormalView<List<CycleEventModel>> {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.rcl_cycle)
    RecyclerView rclCycle;
    @BindView(R.id.bt_photo)
    MyPhotoBtton btPhoto;
    private CycleEventAdapter cycleEventAdapter;
    private ArrayList<String> listEvent;
    private List<CycleEventModel> eventModel;
    private Dialog loadingDialog;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_cycle;
    }

    @Override
    protected void initView() {
        Toolbar toolbar = myToolbar.getToolbar();
        if (toolbar != null) {
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> {
                finish();
            });
        }
        requestCycleEvent();


    }

    //请求列表
    private void requestCycleEvent() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        persenter.requestCycleEvent(map);
    }

    /**
     * 初始化rlc
     *
     * @param models
     */
    private void initRclCycleEvent(List<CycleEventModel> models) {
        eventModel = models;
        if (cycleEventAdapter == null) {
            cycleEventAdapter = new CycleEventAdapter(models);
            rclCycle.setLayoutManager(new LinearLayoutManager(this));
            View view = new View(this);//避免与按钮重叠
            view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, DisplayUtil.dp2px(this, 100)));
            cycleEventAdapter.addFooterView(view);
            cycleEventAdapter.bindToRecyclerView(rclCycle);
        } else {
            cycleEventAdapter.setNewData(models);
        }
    }

    @Override
    protected void initListener() {
        //获取所选的条目
        btPhoto.setOnClickListener(v -> {

            if (cycleEventAdapter != null) {
                StringBuilder sb = new StringBuilder();
                TreeMap<Integer, Boolean> sleMap = cycleEventAdapter.getSleMap();
                Iterator iter = sleMap.entrySet().iterator();
                while (iter.hasNext()) {
                    Map.Entry entry = (Map.Entry) iter.next();
                    Integer key = (Integer) entry.getKey();
                    Boolean val = (Boolean) entry.getValue();
                    if (val) {
                        sb.append(eventModel.get(key).getCycle_task_id() + ",");//每个id用逗号隔开
                    }
                }
                if (TextUtils.isEmpty(sb.toString())) {
                    Toast.makeText(this, "请选择任务进行分配", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(this, CycleEventAllotActivity.class);
                    intent.putExtra("slePostion", sb.toString());
                    startActivityForResult(intent, 2);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 2) {
            requestCycleEvent();
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    protected CycleEventPersent oncreatPersenter() {
        return new CycleEventPersent();
    }


    @Override
    public void getNetResult(List<CycleEventModel> models) {
        initRclCycleEvent(models);
        loadingDialog.dismiss();
    }

    @Override
    public void getNetFauiler(String wrong) {
        Toast.makeText(this, wrong, Toast.LENGTH_SHORT).show();
        loadingDialog.dismiss();
        initRclCycleEvent(new ArrayList<CycleEventModel>());
    }
}
