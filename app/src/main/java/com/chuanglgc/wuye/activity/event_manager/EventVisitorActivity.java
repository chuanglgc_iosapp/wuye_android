package com.chuanglgc.wuye.activity.event_manager;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.dialog.DatePickerDialog;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EventVisitorActivity extends BaseActivity implements DatePickerDialog.TimePickerDialogInterface {
    @BindView(R.id.toolbar)
    MyToolbar toolbar;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_person)
    TextView tvPerson;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_record)
    TextView tvRecord;
    @BindView(R.id.bt_commit)
    TextView btCommit;
    @BindView(R.id.tv_date)
    TextView tvDate;
    private Dialog loadingDialog;
    private String searchDate;
    private String repaireId;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_event_visitor;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = toolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        Intent intent = getIntent();
        // taskId = intent.getStringExtra("task_id");
        repaireId = intent.getStringExtra("repair_id");
        tvDate.setOnClickListener(v -> {

            showCalendary();
        });

        Calendar c = Calendar.getInstance();
        tvTime.setOnClickListener(v -> showTimePickerDialog(0, tvTime, c));
    }

    public void showTimePickerDialog(int themeResId, final TextView tv, Calendar calendar) {
        // Calendar c = Calendar.getInstance();
        // 创建一个TimePickerDialog实例，并把它显示出来
        // 解释一哈，Activity是context的子类
        new TimePickerDialog(this, themeResId,
                // 绑定监听器
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        tv.setText(hourOfDay + ":" + minute);
                    }
                }
                // 设置初始时间
                , calendar.get(Calendar.HOUR_OF_DAY)
                , calendar.get(Calendar.MINUTE)
                // true表示采用24小时制
                , true).show();
    }

    @Override
    protected void initListener() {
        btCommit.setOnClickListener(v -> {
            String person = tvPerson.getText().toString().trim();
            String phone = tvPhone.getText().toString().trim();
            String record = tvRecord.getText().toString().trim();
            String time = tvTime.getText().toString().trim();
            if (TextUtils.isEmpty(searchDate)) {
                Toast.makeText(this, "请填写回访日期", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(time)) {
                Toast.makeText(this, "请填写回访时间", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(person)) {
                Toast.makeText(this, "请填写联系人", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(phone)) {
                Toast.makeText(this, "请填写联系人电话", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(record)) {
                Toast.makeText(this, "请填写回访记录", Toast.LENGTH_SHORT).show();
            } else {
                HashMap<String, String> map = new HashMap<>();
                map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
                map.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
                map.put("repair_id", repaireId);
                map.put("contact_person", person);
                map.put("contact_phone", phone);
                map.put("feedback_content", record);
                map.put("feedback_time", time);
                map.put("feedback_date", searchDate);
                loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
                RestClient.getAPIService().writeFeedbackContent(map).enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {
                        if (response.code() == 200) {
                            if (response.body().isResult()) {
                                Toast.makeText(EventVisitorActivity.this, "填写成功", Toast.LENGTH_SHORT).show();
                                setResult(5);
                                loadingDialog.dismiss();
                                finish();

                            } else {
                                loadingDialog.dismiss();
                                Toast.makeText(EventVisitorActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            loadingDialog.dismiss();
                            Toast.makeText(EventVisitorActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {
                        loadingDialog.dismiss();
                        Toast.makeText(EventVisitorActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

        });

    }

    @Override
    protected void initData() {

    }

    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }

    private void showCalendary() {
        DatePickerDialog pickerDialog = new DatePickerDialog(this);
        pickerDialog.showDatePickerDialog();
    }

    @Override
    public void positiveListener(String date, String current) {
        tvDate.setText(current);
        searchDate = current;
    }

    @Override
    public void negativeListener() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
