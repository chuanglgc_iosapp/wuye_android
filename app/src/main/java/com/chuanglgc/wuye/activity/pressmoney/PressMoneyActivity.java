package com.chuanglgc.wuye.activity.pressmoney;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.PressMoneyAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.PressMoneyModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PressMoneyActivity extends BaseActivity {
    @BindView(R.id.tv_search)
    TextView tvSearch;
    @BindView(R.id.ll_search_goods)
    RelativeLayout llSearchGoods;
    @BindView(R.id.rcy_press)
    RecyclerView rcyPress;
    @BindView(R.id.toolbar)
    MyToolbar toolbar;
    @BindView(R.id.iv_empty)
    ImageView ivEmpty;
    private PressMoneyAdapter pressMoneyAdapter;
    private OnItemClickListener onItemClickListener;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_press_money;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = toolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        queryPressMoney("", "", "");
    }

    @Override
    protected void initListener() {
        tvSearch.setOnClickListener(v -> {
            Intent intent = new Intent(this, PressSearchActivity.class);
            startActivityForResult(intent, 1);
        });
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == 6) {//查询页面返回查询条件
            String buildId = data.getStringExtra("build_id");

            String unitId = data.getStringExtra("unit_id");
            String roomId = data.getStringExtra("room_id");

            queryPressMoney(buildId, unitId, roomId);
        }
    }

    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }

    //查询催费列表
    private void queryPressMoney(String build, String unit, String room) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("build", build);
        map.put("unit", unit);
        map.put("room", room);
        RestClient.getAPIService().queryUrgePayList(map).enqueue(new Callback<Result<List<PressMoneyModel>>>() {
            @Override
            public void onResponse(Call<Result<List<PressMoneyModel>>> call, Response<Result<List<PressMoneyModel>>> response) {
                if (response.code() == 200) {
                    if (response.body().isResult()) {
                        initRcl(response.body().getData());
                    } else {
                        Toast.makeText(PressMoneyActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(PressMoneyActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Result<List<PressMoneyModel>>> call, Throwable t) {
                Toast.makeText(PressMoneyActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initRcl(List<PressMoneyModel> data) {
        if (data.size() == 0) {
            rcyPress.setVisibility(View.GONE);
            ivEmpty.setVisibility(View.VISIBLE);
        }
        if (pressMoneyAdapter == null) {
            rcyPress.setLayoutManager(new LinearLayoutManager(this));
            pressMoneyAdapter = new PressMoneyAdapter(data);
            pressMoneyAdapter.bindToRecyclerView(rcyPress);
        } else {
            pressMoneyAdapter.setNewData(data);
        }
        if (onItemClickListener != null) rcyPress.removeOnItemTouchListener(onItemClickListener);
        onItemClickListener = new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(PressMoneyActivity.this, PerssDetailActivity.class);
                intent.putExtra("estate_id", data.get(position).getEstate_id());
                intent.putExtra("arreascpst_id", data.get(position).getArreascost_id());
                startActivity(intent);
            }
        };
        rcyPress.addOnItemTouchListener(onItemClickListener);
    }


}
