package com.chuanglgc.wuye.activity.event_manager;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.IOwnerSearchView;
import com.chuanglgc.wuye.MVP.persent.OwnerSearchPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.dialog.DatePickerDialog;
import com.chuanglgc.wuye.dialog.SpinnerDialog;
import com.chuanglgc.wuye.model.BuildModel;
import com.chuanglgc.wuye.model.RepairTypeModel;
import com.chuanglgc.wuye.model.RoomModel;
import com.chuanglgc.wuye.model.UnitModel;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

public class EventSearchActivity extends BaseActivity<IOwnerSearchView, OwnerSearchPersent> implements View.OnClickListener
        , DatePickerDialog.TimePickerDialogInterface, IOwnerSearchView {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.tv_dong)
    TextView tvDong;
    @BindView(R.id.sel_dong)
    RelativeLayout selDong;
    @BindView(R.id.tv_danyuan)
    TextView tvDanyuan;
    @BindView(R.id.sel_danyuan)
    RelativeLayout selDanyuan;
    @BindView(R.id.tv_room)
    TextView tvRoom;
    @BindView(R.id.sel_room)
    RelativeLayout selRoom;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.sel_type)
    RelativeLayout selType;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.sel_time)
    RelativeLayout selTime;
    @BindView(R.id.bt_search)
    MyPhotoBtton btSearch;
    private Dialog loadingDialog;
    private List<BuildModel> buildModelList;

    private List<UnitModel> unitModelList;

    private List<RoomModel> roomModelList;

    private List<RepairTypeModel> repairTypeModels;
    private int buildPosition = -1;
    private int unitPosition = -1;
    private int roomPosition = -1;
    private int repairTypeId = -1;
    private String returDate;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_event_search;
    }

    @Override
    protected void initView() {
        Toolbar toolbar = myToolbar.getToolbar();
        if (toolbar != null) {
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> {
                finish();
            });
        }
    }

    @Override
    protected void initListener() {
        tvDong.setOnClickListener(this);
        tvDanyuan.setOnClickListener(this);
        tvRoom.setOnClickListener(this);
        tvType.setOnClickListener(this);
        tvTime.setOnClickListener(this);
        btSearch.setOnClickListener(this);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected OwnerSearchPersent oncreatPersenter() {
        return new OwnerSearchPersent();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_dong:
                queryBuild();
                break;
            case R.id.tv_danyuan:
                queryUnit();
                break;
            case R.id.tv_room:
                queryRoom();
                break;
            case R.id.tv_type:
                queryEventType();
                break;
            case R.id.tv_time:
                showCalendary();
                break;
            case R.id.bt_search:
                returnSearchCondition();

                break;

        }
    }

    //结束此页面 返回给EventManagerActivity 查询数据 并查询 条件不是必选
    private void returnSearchCondition() {
     /*   if (roomPosition!=-1){*/
        Intent intent = new Intent();
        if (!tvDong.getText().toString().equals(""))
            intent.putExtra("build_id", buildModelList.get(buildPosition).getBuild_id());
        if (!tvDanyuan.getText().toString().equals(""))
            intent.putExtra("unit_id", unitModelList.get(unitPosition).getUnit_id());
        if (!tvRoom.getText().toString().equals(""))
            intent.putExtra("room_id", roomModelList.get(roomPosition).getRoom_id());
        if (repairTypeId != -1)
            intent.putExtra("repair_type_id", repairTypeModels.get(repairTypeId).getRepair_type_id());
        intent.putExtra("repair_time", returDate);
        setResult(6, intent);
        finish();
      /*  }else {
            Toast.makeText(this,"请选择详细地址进行查询",Toast.LENGTH_SHORT).show();
        }*/

    }

    /**
     * 显示日历
     */
    private void showCalendary() {
        DatePickerDialog pickerDialog = new DatePickerDialog(this);
        pickerDialog.showDatePickerDialog();
    }


    @Override
    public void positiveListener(String date, String currentDate) {
        tvTime.setText(date);
        this.returDate = currentDate;

    }

    @Override
    public void negativeListener() {

    }

    //查询门牌号
    private void queryRoom() {
        if (buildModelList == null || buildModelList.size() == 0) {
            Toast.makeText(this, "请选择栋数", Toast.LENGTH_SHORT).show();
        } else if (unitModelList == null || unitModelList.size() == 0) {
            Toast.makeText(this, "请选择单元", Toast.LENGTH_SHORT).show();
        } else {
            HashMap<String, String> doorMap = new HashMap<>();
            doorMap.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            doorMap.put("build_id", buildModelList.get(buildPosition).getBuild_id());
            doorMap.put("unit_id", unitModelList.get(unitPosition).getUnit_id());
            persenter.QueryRoom(doorMap);
            if (loadingDialog == null) {
                loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
            } else {
                loadingDialog.show();
            }
        }
    }

    //查询单元
    private void queryUnit() {
        if (buildModelList == null || buildModelList.size() == 0) {
            Toast.makeText(this, "请选择栋数", Toast.LENGTH_SHORT).show();
        } else {
            Map<String, String> mapUnit = new HashMap<>();
            mapUnit.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            mapUnit.put("build_id", buildModelList.get(buildPosition).getBuild_id());
            persenter.QueryUnit(mapUnit);
            if (loadingDialog == null) {
                loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
            } else {
                loadingDialog.show();
            }
        }
    }

    /**
     * 查询栋
     */
    private void queryBuild() {
        loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        Map<String, String> mapBuild = new HashMap<>();
        mapBuild.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        persenter.QueryBuild(mapBuild);
    }

    //查询事件类型
    private void queryEventType() {
        loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        persenter.queryRepairType(EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
    }


    //获取栋的信息
    @Override
    public void getBuildList(List<BuildModel> buildModelList) {
        if (loadingDialog != null) loadingDialog.dismiss();

        ArrayList<String> listBuild = new ArrayList<>();
        for (BuildModel model : buildModelList) {
            listBuild.add(model.getBuild_name());
        }
        SpinnerDialog spinnerDialog = new SpinnerDialog(this, listBuild);
        spinnerDialog.setOnSpinnerItemClick((itemName, position) -> {
            spinnerDialog.dismiss();
            this.buildModelList = buildModelList;
            this.buildPosition = position;
            tvDong.setText(itemName);
        });
        spinnerDialog.show();
    }

    //获取单元
    @Override
    public void getUnitList(List<UnitModel> unitModelList) {
        if (loadingDialog != null) loadingDialog.dismiss();

        LogUtil.e("获取单元", unitModelList.size());
        ArrayList<String> listUnit = new ArrayList<>();
        for (UnitModel model : unitModelList) {
            listUnit.add(model.getUnit_name());
        }

        SpinnerDialog spinnerDialog = new SpinnerDialog(this, listUnit);
        spinnerDialog.setOnSpinnerItemClick((itemName, position) -> {
            spinnerDialog.dismiss();
            this.unitModelList = unitModelList;
            this.unitPosition = position;
            tvDanyuan.setText(itemName);
        });
        spinnerDialog.show();
    }

    //获取房间
    @Override
    public void getRoomList(List<RoomModel> roomModelList) {
        if (loadingDialog != null) loadingDialog.dismiss();

        ArrayList<String> listRoom = new ArrayList<>();
        for (RoomModel model : roomModelList) {
            listRoom.add(model.getRoom_name());
        }

        SpinnerDialog spinnerDialog = new SpinnerDialog(this, listRoom);
        spinnerDialog.setOnSpinnerItemClick((itemName, position) -> {
            spinnerDialog.dismiss();
            this.roomModelList = roomModelList;
            this.roomPosition = position;
            tvRoom.setText(itemName);
        });
        spinnerDialog.show();
    }

    //获取事件类型
    @Override
    public void getRepairTypeList(List<RepairTypeModel> repairTypeModels) {
        if (loadingDialog != null) loadingDialog.dismiss();

        ArrayList<String> listType = new ArrayList<>();
        for (RepairTypeModel model : repairTypeModels) {
            listType.add(model.getRepair_type_name());
        }
        SpinnerDialog spinnerDialog = new SpinnerDialog(this, listType);
        spinnerDialog.setOnSpinnerItemClick((itemName, position) -> {
            repairTypeId = position;
            this.repairTypeModels = repairTypeModels;
            spinnerDialog.dismiss();
            tvType.setText(itemName);
        });
        spinnerDialog.show();

    }

    //获取网络数据失败
    @Override
    public void getNetInfoError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        if (loadingDialog != null) loadingDialog.dismiss();
    }


}
