package com.chuanglgc.wuye.activity;

import android.content.Intent;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.dialog.DiffLoginDialog;
import com.chuanglgc.wuye.model.LoginModel;
import com.chuanglgc.wuye.network.Constant;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.DeviceInfoUtil;
import com.chuanglgc.wuye.utils.MD5Utils;
import com.chuanglgc.wuye.utils.SpUtils;

import java.util.HashMap;

import cn.jpush.android.api.JPushInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//别处登陆  广播监听到 跳转到此界面
public class DifferentLoginActivity extends BaseActivity {
    @Override
    protected int getLayoutId() {
        return R.layout.activity_different_login;
    }

    @Override
    protected void initView() {
        DiffLoginDialog diffLoginDialog = new DiffLoginDialog(this);
        diffLoginDialog.setOnNegativeListener(v -> {
            //清除极光推送的别名
            String userId = EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id();
            JPushInterface.deleteAlias(DifferentLoginActivity.this,Integer.valueOf(userId));
            //退出所有Activity
            EmployeeApplication.getInstance().removeAllActivity();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);

        });
        diffLoginDialog.setOnPositiveListener(v -> {
            loginUser();
            diffLoginDialog.dismiss();
        });
        diffLoginDialog.show();
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }


    /**
     * 从缓存中拿到账号密码登陆
     * 成功更新本地缓存并跳转到MainActitiy  并设置极光推送的别名
     * 失败 跳转到登陆界面
     */
    private void loginUser() {
        String password = (String) SpUtils.get(this, Constant.PASSWORD, "");
        LoginModel userInfoFromCache = EmployeeApplication.getInstance().getUserInfoFromCache();
        HashMap<String, String> map = new HashMap<>();
        map.put("phone_number", userInfoFromCache.getPhone_number());
        map.put("login_password", MD5Utils.md5(Constant.salt + password));

        //获取设备厂商
        String deviceBrand = DeviceInfoUtil.getDeviceBrand();
        String uuid = DeviceInfoUtil.getUUID();
        String userAgent = DeviceInfoUtil.getUserAgent();

        map.put("device_manufacturer", deviceBrand);
        map.put("device_uuid", uuid);
        map.put("device_platform", userAgent);
        RestClient.getAPIService().login(map).enqueue(new Callback<Result<LoginModel>>() {
            @Override
            public void onResponse(Call<Result<LoginModel>> call, Response<Result<LoginModel>> response) {
                if (response.code() == 200) {
                    Result<LoginModel> body = response.body();
                    if (body.isResult()) {
                        //登陆成功注册激光推送的别名
                        String dName = "WY_" + DeviceInfoUtil.getUUID();
                        //先注册昵称，推送时候会根据这个昵称去推送到个人
                        String userId = EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id();
                        JPushInterface.setAlias(DifferentLoginActivity.this,Integer.valueOf(userId),dName);
                        Toast.makeText(DifferentLoginActivity.this, "账号登录成功", Toast.LENGTH_SHORT).show();

                        LoginModel model = body.getData();
                        EmployeeApplication.getInstance().getCacheUtil().putSerializableObj(Constant.USER_KEY, model);
                        int activitSize = EmployeeApplication.getInstance().getActivityList().size();
                        //判断了在退出状态下处理
                        if (activitSize==1){
                            Intent intent = new Intent(DifferentLoginActivity.this, MainActivity.class);
                            startActivity(intent);
                        }
                        finish();
                    } else {
                        Intent intent = new Intent(DifferentLoginActivity.this, LoginActivity.class);
                        startActivity(intent);
                        EmployeeApplication.getInstance().removeAllActivity();
                    }
                } else {
                    Intent intent = new Intent(DifferentLoginActivity.this, LoginActivity.class);
                    startActivity(intent);
                    EmployeeApplication.getInstance().removeAllActivity();
                }
            }

            @Override
            public void onFailure(Call<Result<LoginModel>> call, Throwable t) {
                Intent intent = new Intent(DifferentLoginActivity.this, LoginActivity.class);
                startActivity(intent);
                EmployeeApplication.getInstance().removeAllActivity();
            }
        });

    }

}
