package com.chuanglgc.wuye.activity.master_attendance;

import android.app.Dialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.IMasterAttendView;
import com.chuanglgc.wuye.MVP.persent.MasterAttancePersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.AttendanceClassAdapter;
import com.chuanglgc.wuye.adapter.MasterAttendanceClassAdapter;
import com.chuanglgc.wuye.adapter.MasterAttendanceDayAdapter;
import com.chuanglgc.wuye.adapter.MasterAttendanceLateAdapter;
import com.chuanglgc.wuye.adapter.MasterAttendanceLeaveAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.dialog.DatePickerDialog;
import com.chuanglgc.wuye.dialog.EmployeeInfoDialog;
import com.chuanglgc.wuye.model.MasterAttedanModel;
import com.chuanglgc.wuye.model.MasterCheckWorkInfoModel;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.AttendanceUpDownView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;


public class MasterAttendanceActivity extends BaseActivity<IMasterAttendView, MasterAttancePersent> implements View.OnClickListener,
        DatePickerDialog.TimePickerDialogInterface, IMasterAttendView {
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.iv_calendary)
    ImageView ivCalendary;
    @BindView(R.id.sel_time)
    RelativeLayout selTime;
    @BindView(R.id.rl_date_root)
    RelativeLayout rlDateRoot;
    @BindView(R.id.updown_day)
    AttendanceUpDownView updownDay;
    @BindView(R.id.rcl_day)
    RecyclerView rclDay;
    @BindView(R.id.updown_class)
    AttendanceUpDownView updownClass;
    @BindView(R.id.rcl_class)
    RecyclerView rclClass;
    @BindView(R.id.updown_late)
    AttendanceUpDownView updownLate;
    @BindView(R.id.rcl_late)
    RecyclerView rclLate;
    @BindView(R.id.updown_leave)
    AttendanceUpDownView updownLeave;
    @BindView(R.id.rcl_leave)
    RecyclerView rclLeave;
    @BindView(R.id.iv_total)
    ImageView ivTotal;
    @BindView(R.id.mytoolbar)
    Toolbar mytoolbar;
    private MasterAttendanceDayAdapter dayNumberAdapter;
    private ArrayList<String> listdayNumber;
    private List<MasterAttedanModel.AttendanceBean.AttendanceEmpnameBean> attendanceEmpList;
    private OnItemClickListener onItemClickListener;
    private OnItemClickListener dayItemClickListener;
    private List<MasterAttedanModel.ShiftBean.ShiftTimeBean> classList;
    private List<MasterAttedanModel.LateBean.LateInfoBean> lateList;
    private List<MasterAttedanModel.LeaveEarlyBean.EarlyInfoBean> leaveList;
    private MasterAttendanceClassAdapter classAdapter;
    private MasterAttendanceLateAdapter lateAdapter;
    private MasterAttendanceLeaveAdapter leaveEarlyAdapter;
    private String selDate = "";
    private Dialog loadingDialog;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_master_attendcance;
    }

    @Override
    protected void initView() {
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        SimpleDateFormat sDateFormat1 = new SimpleDateFormat("yyyy.MM.dd");
        String currentDate = sDateFormat1.format(new Date());
        tvTime.setText(currentDate);
        selDate = currentDate;
        requestAttendance("");//查询员工考勤
    }

    //请求员工考勤
    private void requestAttendance(String date) {
        closeAllUp(null);
        showLoading();
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        map.put("attendancetime", date);
        persenter.requestAttendance(map);
    }

    //请求员工考勤详情
    private void requestAttendanceInfo(String empID, String date) {
        LogUtil.e("数据", empID + "||" + date);
        showLoading();
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("emp_id", empID);
        map.put("attendancetime", date);
        persenter.requestAttendWork(map);
    }

    private void showLoading() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    protected MasterAttancePersent oncreatPersenter() {
        return new MasterAttancePersent();
    }


    @Override
    protected void initListener() {
        updownDay.setOnClickListener(this);
        updownClass.setOnClickListener(this);
        updownLate.setOnClickListener(this);
        updownLeave.setOnClickListener(this);

        rlDateRoot.setOnClickListener(this);
        ivTotal.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.updown_day:
                closeAllUp(updownDay);
                if (updownDay.setUpDown()) {
                    showRclDay();
                } else {
                    rclDay.setVisibility(View.GONE);
                }
                break;
            case R.id.updown_class:
                closeAllUp(updownClass);
                if (updownClass.setUpDown()) {
                    showRclClass();
                } else {
                    rclClass.setVisibility(View.GONE);
                }
                break;
            case R.id.updown_late:
                closeAllUp(updownLate);
                if (updownLate.setUpDown()) {
                    showRclLate();
                } else {
                    rclLate.setVisibility(View.GONE);
                }

                break;
            case R.id.updown_leave:
                closeAllUp(updownLeave);
                if (updownLeave.setUpDown()) {
                    showRlcLeaveEearly();
                } else {
                    rclLeave.setVisibility(View.GONE);
                }
                break;
            case R.id.rl_date_root:
                showCalendary();//显示日历
                break;
            case R.id.iv_total:
                startActivity(MasterAttedanceMothActiivty.class, null);
                break;
        }
    }

    /**
     * 关闭其他的Rcl 选择器设置为false
     *
     * @param updown
     */
    private void closeAllUp(AttendanceUpDownView updown) {
        if (updown != updownDay) {
            rclDay.setVisibility(View.GONE);
            updownDay.setSelectFalse();
        }
        if (updown != updownClass) {
            rclClass.setVisibility(View.GONE);
            updownClass.setSelectFalse();
        }
        if (updown != updownLeave) {
            rclLeave.setVisibility(View.GONE);
            updownLeave.setSelectFalse();
        }
        if (updown != updownLate) {
            rclLate.setVisibility(View.GONE);
            updownLate.setSelectFalse();
        }

    }

    /**
     * 显示日历
     */
    private void showCalendary() {
        DatePickerDialog pickerDialog = new DatePickerDialog(this);
        pickerDialog.showDatePickerDialog();
    }


    /**
     * 设置早退
     */
    private void showRlcLeaveEearly() {
        if (leaveList != null) {
            if (leaveEarlyAdapter == null) {
                leaveEarlyAdapter = new MasterAttendanceLeaveAdapter(leaveList);
                rclLeave.setLayoutManager(new LinearLayoutManager(this));
                leaveEarlyAdapter.bindToRecyclerView(rclLeave);
            } else {
                leaveEarlyAdapter.setNewData(leaveList);
            }
            rclLeave.setVisibility(View.VISIBLE);
        }

    }

    /**
     * 设置迟到
     */
    private void showRclLate() {
        if (lateList != null) {
            if (lateAdapter == null) {
                lateAdapter = new MasterAttendanceLateAdapter(lateList);
                rclLate.setLayoutManager(new LinearLayoutManager(this));
                lateAdapter.bindToRecyclerView(rclLate);
            } else {
                lateAdapter.setNewData(lateList);
            }
            rclLate.setVisibility(View.VISIBLE);
        }

    }

    /**
     * 设置出勤班次
     */
    private void showRclClass() {
        if (classList != null) {
            if (classAdapter == null) {
                rclClass.setLayoutManager(new LinearLayoutManager(this));
                classAdapter = new MasterAttendanceClassAdapter(classList);
                classAdapter.bindToRecyclerView(rclClass);
            } else {
                classAdapter.setNewData(classList);
            }
        }
        rclClass.setVisibility(View.VISIBLE);
    }

    /**
     * 设置出勤人数数
     */
    private void showRclDay() {
        if (attendanceEmpList != null) {
            if (dayNumberAdapter == null) {
                rclDay.setLayoutManager(new LinearLayoutManager(this));
                dayNumberAdapter = new MasterAttendanceDayAdapter(attendanceEmpList);
                dayNumberAdapter.bindToRecyclerView(rclDay);
            } else {
                dayNumberAdapter.setNewData(attendanceEmpList);
            }
            if (dayItemClickListener != null)
                rclDay.removeOnItemTouchListener(dayItemClickListener);
            dayItemClickListener = new OnItemClickListener() {
                @Override
                public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                    requestAttendanceInfo(attendanceEmpList.get(position).getEmp_id(), selDate);
                }
            };
            rclDay.addOnItemTouchListener(dayItemClickListener);
            rclDay.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void positiveListener(String date, String currentDate) {
        this.selDate = currentDate;//弹出dialog的时候使用
        tvTime.setText(currentDate);
        requestAttendance(currentDate);
    }

    @Override
    public void negativeListener() {

    }


    @Override
    public void getEmpCheckAttendance(MasterAttedanModel model) {
        loadingDialog.dismiss();
        //出勤人数
        MasterAttedanModel.AttendanceBean attendance = model.getAttendance();
        updownDay.setCount(attendance.getAttendance_totalperson() + "");
        attendanceEmpList = attendance.getAttendance_empname();

        //出勤班次
        MasterAttedanModel.ShiftBean shiftBean = model.getShift();
        updownClass.setCount(shiftBean.getShift_totalcount() + "");
        classList = shiftBean.getShift_time();

        //迟到
        MasterAttedanModel.LateBean lateBean = model.getLate();
        lateList = lateBean.getLate_info();
        updownLate.setCount(lateBean.getLate_totalcount() + "");

        //早退
        MasterAttedanModel.LeaveEarlyBean leaveEarlyBean = model.getLeave_early();
        updownLeave.setCount(leaveEarlyBean.getEarly_total() + "");
        leaveList = leaveEarlyBean.getEarly_info();
    }

    @Override
    public void getEmpClockinWorkBydate(MasterCheckWorkInfoModel model) {
        LogUtil.e("数据", model.getEmp_id());
        LogUtil.e("数据", model.getEmp_name());
        LogUtil.e("数据", model.getWeekday());
        LogUtil.e("数据", model.getWork_info().size());
        EmployeeInfoDialog employeeInfoDialog = new EmployeeInfoDialog(this, model);
        employeeInfoDialog.show();
        loadingDialog.dismiss();
    }

    /**
     * 展示员工信息的Dialog
     */
    private void showEmpInfoDialog() {

    }

    @Override
    public void getNetError(String error) {
        loadingDialog.dismiss();
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}
