package com.chuanglgc.wuye.activity.event_dailywork;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.DailyWorkAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.DailyTaskModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class DailyWorkActivity extends BaseActivity {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.rcl_work)
    RecyclerView rclWork;
    private Dialog loadingDialog;
    private OnItemClickListener onItemClickListener;
    private DailyWorkAdapter workAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_dailywork;
    }

    @Override
    protected void initView() {
        Toolbar toolbar = myToolbar.getToolbar();
        if (toolbar != null) {
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> {
                finish();
            });
        }
        queryDailyTaskList();
    }

    private void initRclDaily(List<DailyTaskModel> data) {
        if (data==null||data.size()==0){
            Toast.makeText(this,"今天没有工作",Toast.LENGTH_SHORT).show();
        }
        if (workAdapter==null){
            rclWork.setLayoutManager(new LinearLayoutManager(this));
            workAdapter = new DailyWorkAdapter(data);
            workAdapter.bindToRecyclerView(rclWork);
        }else {
            workAdapter.setNewData(data);
        }

        //重新设置点击事件
        if (onItemClickListener != null) rclWork.removeOnItemTouchListener(onItemClickListener);
        onItemClickListener = new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(DailyWorkActivity.this, DailyDetailActivity.class);
                intent.putExtra("model", data.get(position));
                startActivityForResult(intent,5);
            }
        };
        rclWork.addOnItemTouchListener(onItemClickListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==5&&resultCode==5){
            queryDailyTaskList();
        }
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    //查询每日工作
    private void queryDailyTaskList() {
        loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        RestClient.getAPIService().queryDailyTaskList(map).enqueue(new Callback<Result<List<DailyTaskModel>>>() {
            @Override
            public void onResponse(Call<Result<List<DailyTaskModel>>> call, Response<Result<List<DailyTaskModel>>> response) {
                if (response.code() == 200) {
                    Result<List<DailyTaskModel>> body = response.body();
                    if (body.isResult()) {
                        initRclDaily(body.getData());
                    } else {
                        Toast.makeText(DailyWorkActivity.this, body.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(DailyWorkActivity.this, response.code()+"", Toast.LENGTH_SHORT).show();
                }
                loadingDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Result<List<DailyTaskModel>>> call, Throwable t) {
                Toast.makeText(DailyWorkActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
                loadingDialog.dismiss();
            }
        });
    }

    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }


}
