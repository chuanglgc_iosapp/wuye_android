package com.chuanglgc.wuye.activity.event_manager;

import android.app.Dialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.IEventHandleView;
import com.chuanglgc.wuye.MVP.modelImpl.EventHandlePersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.dialog.SpinnerDialog;
import com.chuanglgc.wuye.model.DepartmentServiceModel;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.ServiceModel;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

//事件分配
public class EventAssignActivity extends BaseActivity<IEventHandleView, EventHandlePersent> implements View.OnClickListener, IEventHandleView {
    @BindView(R.id.toolbar)
    MyToolbar myToolbar;
    @BindView(R.id.tv_service)
    TextView tvService;
    @BindView(R.id.sel_service)
    RelativeLayout selService;
    @BindView(R.id.tv_class)
    TextView tvClass;
    @BindView(R.id.sel_class)
    RelativeLayout selClass;
    @BindView(R.id.tv_person)
    TextView tvPerson;
    @BindView(R.id.sel_person)
    RelativeLayout selPerson;
    @BindView(R.id.bt_commit)
    MyPhotoBtton btCommit;
    @BindView(R.id.iv_person)
    ImageView ivPerson;
    private Dialog loadingDialog;
    private String departmentId;
    private String masterId;
    private String serviceId;
    private String repairID;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_event_assign;
    }

    @Override
    protected void initView() {
        Toolbar toolbar = myToolbar.getToolbar();
        if (toolbar != null) {
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> {
                finish();
            });
        }
        repairID = getIntent().getStringExtra("repair_id");
    }


    @Override
    protected void initData() {

    }

    @Override
    protected EventHandlePersent oncreatPersenter() {
        return new EventHandlePersent();
    }

    @Override
    protected void initListener() {
        btCommit.setOnClickListener(this);
        tvService.setOnClickListener(this);
        tvClass.setOnClickListener(this);
        tvPerson.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_commit:
                requestHandleEvent();
                break;
            case R.id.tv_service:
                requestService();
                break;
            case R.id.tv_class:
                requestDepartment();
                break;
            case R.id.tv_person:
                requestLeader();
                break;
        }
    }

    //请求服务商类型
    private void requestService() {
        showDialog();
        Log.e("小区id",EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        persenter.reqeustService(EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
    }

    //查询部门工种
    private void requestDepartment() {
        if (TextUtils.isEmpty(tvService.getText())){
            Toast.makeText(this,"请选择服务商类型",Toast.LENGTH_SHORT).show();
        }else {
            showDialog();
            HashMap<String, String> map = new HashMap<>();
            map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            map.put("service_type", serviceId);
            persenter.requestDepartment(map);
        }

    }

    //请求负责人
    private void requestLeader() {
        if (TextUtils.isEmpty(tvClass.getText())){
            Toast.makeText(this,"请选择归属部门",Toast.LENGTH_SHORT).show();
        }else {
            showDialog();
            HashMap<String, String> map = new HashMap<>();
            map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            map.put("department_id", departmentId);
            persenter.requestLeaderByWorkType(map);
        }

    }

    private void requestHandleEvent() {
        if (TextUtils.isEmpty(serviceId)) {
            Toast.makeText(this, "请选择服务商类型", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(departmentId)) {
            Toast.makeText(this, "请选择归属部门", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(masterId) && serviceId.equals("1")) {
            Toast.makeText(this, "请选择负责人", Toast.LENGTH_SHORT).show();
        } else {
            showDialog();
            HashMap<String, String> map = new HashMap<>();
            map.put("service_type", serviceId);
            if (serviceId.equals("1")) {
                map.put("emp_id", masterId);
            } else {
                map.put("emp_id", departmentId);
            }
            map.put("task_id", repairID);
            map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            persenter.handleEvent(map);
        }

    }

    void showDialog() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
    }

    private void getMasterList(List<EventAllotModel> modelList) {
        ArrayList<String> list = new ArrayList<>();
        for (EventAllotModel model : modelList) {
            list.add(model.getEmp_name());
        }
        SpinnerDialog persoinSpinner = new SpinnerDialog(this, list);
        persoinSpinner.setOnSpinnerItemClick((itemName, position) -> {
            tvPerson.setText(itemName);
            this.masterId = modelList.get(position).getEmp_id();
            persoinSpinner.dismiss();
        });
        persoinSpinner.show();
    }

    private void getDepartmentList(List<DepartmentServiceModel> modelList) {
        ArrayList<String> list = new ArrayList<>();
        for (DepartmentServiceModel model : modelList) {
            list.add(model.getService_provider_name());
        }
        SpinnerDialog classSpinner = new SpinnerDialog(this, list);
        classSpinner.setOnSpinnerItemClick((itemName, position) -> {
            tvClass.setText(itemName);
            this.departmentId = modelList.get(position).getService_provider_id();
            classSpinner.dismiss();
            tvPerson.setText("");
        });

    }

    private void getServiceTypeList(List<ServiceModel> modelList) {
        ArrayList<String> list = new ArrayList<>();
        for (ServiceModel model : modelList) {
            list.add(model.getService_type_name());
        }
        SpinnerDialog serviceSpinner = new SpinnerDialog(this, list);
        serviceSpinner.setOnSpinnerItemClick((itemName, position) -> {
            tvService.setText(itemName);
            this.serviceId = modelList.get(position).getService_type_num();
            if (serviceId.equals("2")) {//如果是第三方 就没有负责人
                ivPerson.setVisibility(View.INVISIBLE);
                selPerson.setVisibility(View.INVISIBLE);
                tvClass.setText("");
                tvClass.setHint("归属服务商");
            } else {
                ivPerson.setVisibility(View.VISIBLE);
                selPerson.setVisibility(View.VISIBLE);
                tvClass.setText("");
                tvPerson.setText("");
                tvClass.setHint("归属部门");
            }
            serviceSpinner.dismiss();
        });
        serviceSpinner.show();
    }


    @Override
    public void getServiceProvider(List<ServiceModel> modelList) {
        loadingDialog.dismiss();
        getServiceTypeList(modelList);
    }

    @Override
    public void getDepartmentByServiceType(List<DepartmentServiceModel> modelList) {
        loadingDialog.dismiss();
        getDepartmentList(modelList);
    }

    @Override
    public void getLeaderByWorkType(List<EventAllotModel> modelList) {
        loadingDialog.dismiss();
        getMasterList(modelList);
    }

    @Override
    public void getRepairTask(String result) {
        loadingDialog.dismiss();
        setResult(2);
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void getNetError(String error) {
        loadingDialog.dismiss();
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

}
