package com.chuanglgc.wuye.activity.car_release;


import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.ICarVisitorRecordView;
import com.chuanglgc.wuye.MVP.persent.CarVisitorRecordPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.camera.CameraActivity;
import com.chuanglgc.wuye.model.HeadPicModel;
import com.chuanglgc.wuye.utils.IsRightOrNotNumber;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;
import com.chuanglgc.wuye.widget.photoview.ShowImageActivity;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;
import java.util.HashMap;

import butterknife.BindView;
import io.reactivex.functions.Consumer;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class CarVisitorRecordActivity extends BaseActivity<ICarVisitorRecordView, CarVisitorRecordPersent> implements ICarVisitorRecordView {
    @BindView(R.id.toolbar)
    MyToolbar toolbar;
    @BindView(R.id.tv_car_number)
    EditText tvCarNumber;
    @BindView(R.id.tv_address)
    EditText tvAddress;
    @BindView(R.id.tv_name)
    EditText tvName;
    @BindView(R.id.tv_phone)
    EditText tvPhone;
    @BindView(R.id.tv_record)
    EditText tvRecord;
    @BindView(R.id.bt_commit)
    MyPhotoBtton btCommit;
    @BindView(R.id.iv_take_photo)
    ImageView ivTakePhoto;
    private String photoPath;
    private Dialog loadingDialog;
    private String carNumber;
    private String address;
    private String name;
    private String phone;
    private String record;
    private File photoFile=null;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_car_visitor_record;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = toolbar.getToolbar();
        TextView tvState = toolbar.getTvState();
        tvState.setText("拍照");
        tvState.setTextColor(getResources().getColor(R.color.colorAccent));

        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        tvState.setOnClickListener(v -> {
            new RxPermissions(this).request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                    , Manifest.permission.READ_EXTERNAL_STORAGE
                    , Manifest.permission.CAMERA)
                    .subscribe(new Consumer<Boolean>() {
                        @Override
                        public void accept(Boolean aBoolean) throws Exception {
                            if (!aBoolean) {
                                Toast.makeText(CarVisitorRecordActivity.this, "请确认权限", Toast.LENGTH_SHORT).show();
                            } else {
                                Intent intent = new Intent(CarVisitorRecordActivity.this, CameraActivity.class);
                                startActivityForResult(intent, 5);
                            }
                        }
                    });
        });


    }

    @Override
    protected void initListener() {
        btCommit.setOnClickListener(v -> addRecordCarVisitor());
        ivTakePhoto.setOnClickListener(v -> {
            ShowImageActivity.start(this, photoPath);
        });
    }

    //添加访客记录
    private void addRecordCarVisitor() {
        carNumber = tvCarNumber.getText().toString().trim();
        address = tvAddress.getText().toString().trim();
        name = tvName.getText().toString().trim();
        phone = tvPhone.getText().toString().trim();
        record = tvRecord.getText().toString().trim();
        if (TextUtils.isEmpty(carNumber)) {
            Toast.makeText(this, "请填写访客车牌号", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(address)) {
            Toast.makeText(this, "请填写访客地址", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(name)) {
            Toast.makeText(this, "请填写访客名字", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(phone)|| !IsRightOrNotNumber.isMobile(phone)) {
            Toast.makeText(this, "请填写正确联系电话", Toast.LENGTH_SHORT).show();
        } else if (photoPath == null) {
            Toast.makeText(this, "请提交车牌号照片", Toast.LENGTH_SHORT).show();
        } else {
            commitCarPic();
        }
    }

    /**
     * 上传车牌号图片
     */
    private void commitCarPic() {
        loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        File file = new File(photoPath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpeg"), file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("photo", file.getName(), requestBody);
        this.photoFile=file;
        persenter.requestCommitCarPic(EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id(), part);
      /*  LuBanUtils.compressFile(this,file,file1 -> {
            RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpeg"), file1);
            MultipartBody.Part part = MultipartBody.Part.createFormData("photo", file1.getName(), requestBody);
            this.photoFile=file1;
            persenter.requestCommitCarPic(EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id(), part);
        });*/


    }

    @Override
    protected void initData() {

    }

    @Override
    protected CarVisitorRecordPersent oncreatPersenter() {
        return new CarVisitorRecordPersent();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 5 && resultCode == 5) {
            photoPath = data.getStringExtra("photoPath");
            ivTakePhoto.setVisibility(View.VISIBLE);
            ivTakePhoto.setBackground(new BitmapDrawable(photoPath));
        }
    }


    @Override
    public void getCommitRecordResutl(String isSuccess) {
        loadingDialog.dismiss();
        Intent intent = new Intent();
        intent.putExtra("carPic",photoPath);
        setResult(5,intent);
        finish();
    }

    @Override
    public void getCommitCarPicResult(HeadPicModel model) {
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        map.put("plate_number", carNumber);
        map.put("visitor_address", address);
        map.put("visitor_name", name);
        map.put("visitor_phone", phone);
        map.put("remark", record);
        map.put("image_url", model.getImage_url());
        persenter.requestCommitRecord(map);

        if (photoFile!=null&&photoFile.exists()){
            photoFile.delete();
        }
    }

    @Override
    public void getRequestNetError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        loadingDialog.dismiss();
    }
}
