package com.chuanglgc.wuye.activity.attendance;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.IAttendanceView;
import com.chuanglgc.wuye.MVP.persent.AttendancePersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.AttendanceAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.broadcast.MainBroadcast;
import com.chuanglgc.wuye.model.AttedanceModel;
import com.chuanglgc.wuye.model.CheckModel;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.google.gson.JsonArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

//考勤打卡
public class AttendanceActivity extends BaseActivity<IAttendanceView, AttendancePersent> implements View.OnClickListener
        , com.chuanglgc.wuye.dialog.DatePickerDialog.TimePickerDialogInterface
        , IAttendanceView {


    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_total)
    ImageView ivTotal;
    @BindView(R.id.mytoolbar)
    Toolbar mytoolbar;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.sel_time)
    RelativeLayout selTime;
    @BindView(R.id.rcl_day)
    RecyclerView rclDay;
    @BindView(R.id.iv_calendary)
    ImageView ivCalendary;
    private AttendanceAdapter adapter;
    private String currentDate;
    private Dialog loadingDialog;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_attendance;
    }

    @Override
    protected void initView() {
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        SimpleDateFormat sDateFormat1 = new SimpleDateFormat("yyyy.MM.dd");
        currentDate = sDateFormat1.format(new Date());
        tvTime.setText(currentDate);
        queryCheckListByDate(currentDate);

    }

    /**
     * 查找当前事件的打卡记录
     *
     * @param currentDate 所查找事件 2018.01.01
     */
    private void queryCheckListByDate(String currentDate) {
        HashMap<String, String> map = new HashMap<>();
        LogUtil.e("查询日期", currentDate);
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        map.put("date_condition", currentDate);
        persenter.queryCheckList(map);
    }

    //打卡
    public void checkCard(String schedule) {
        showLoading();
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        map.put("schedule_id", schedule);
        LogUtil.e("打卡参数", schedule);
      /*  map.put("latitude", );
        map.put("longitude", );*/
        persenter.checkWork(map);
    }

    private void showLoading() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    protected AttendancePersent oncreatPersenter() {
        return new AttendancePersent();
    }

    @Override
    protected void initListener() {
        ivTotal.setOnClickListener(this);
        tvTime.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_time:
                showCalendary();
                break;
            case R.id.iv_total:
                startActivity(AttendanceCountActivity.class, null);
                break;
        }
    }

    private void showCalendary() {
        com.chuanglgc.wuye.dialog.DatePickerDialog pickerDialog = new com.chuanglgc.wuye.dialog.DatePickerDialog(this);
        pickerDialog.showDatePickerDialog();
    }


    @Override
    public void positiveListener(String date, String currentDate) {
        tvTime.setText(currentDate);
        queryCheckListByDate(currentDate);
    }

    @Override
    public void negativeListener() {

    }

    /**
     * 获取考勤打卡列表
     */
    @Override
    public void getAttendModel(List<AttedanceModel> list) {
        LogUtil.e("考勤记录", list.size());
        if (list.size() == 0) {
            initRcl(null);//如果没有记录 给空  与获取失败一致
        } else {
            initRcl(list);
        }

        if (loadingDialog != null) loadingDialog.dismiss();
    }

    @Override
    public void getAttendError(String result) {
        if (result.equals("未找到任何数据")) {
            initRcl(null);
        }
        if (loadingDialog != null) loadingDialog.dismiss();
    }
      //获取打卡状态
    @Override
    public void getCheckResult(List<AttedanceModel> checkModels) {
        if (checkModels.size() > 0) {
            AttedanceModel attedanceModel = checkModels.get(checkModels.size() - 1);
            Intent intent = new Intent("MainBroadcast");
            if (attedanceModel.getClock_status()==null) {
                intent.putExtra("isCheck", "未打卡");
            } else {
                intent.putExtra("isCheck", attedanceModel.getClock_status());
                LogUtil.e("打卡状态",attedanceModel.getClock_status());
            }
            sendBroadcast(intent);
        }
        if (loadingDialog != null) loadingDialog.dismiss();
        initRcl(checkModels);
    }

    /**
     * 判断是否为null并且为当天时间
     * 如果为null添加一条记录 用来打卡
     * 不是当天显示null 管理员未排班
     *
     * @param list
     */
    private void initRcl(List<AttedanceModel> list) {
        String curTime = tvTime.getText().toString().trim();

        if (list == null && curTime.equals(currentDate)) {
            list = new ArrayList<AttedanceModel>();
            list.add(null);
            LogUtil.e("可以打卡", currentDate + "||" + curTime);
        } else if (list != null && list.size() > 0 && list.get(list.size() - 1).isCheck_button_flag()) {//判断最后一个条目是否有可打卡的标志位
            List<AttedanceModel.ScheduleCheckBean> schedule_check = list.get(list.size() - 1).getSchedule_check();
            if (schedule_check.size() == 2) {
                list.add(null);
            }
        } else if (list == null || list.size() == 0) {
            Toast.makeText(this, "管理员当天未给你排班", Toast.LENGTH_SHORT).show();
        }
        if (adapter == null) {
            rclDay.setLayoutManager(new LinearLayoutManager(this));
            adapter = new AttendanceAdapter(list);
            adapter.bindToRecyclerView(rclDay);
        } else {
            adapter.setNewData(list);
        }

    }


    @Override
    public void getNetErrow(String result) {
        if (loadingDialog != null) loadingDialog.dismiss();
        LogUtil.e("打卡数据", result);
    }
}
