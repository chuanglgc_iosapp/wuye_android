package com.chuanglgc.wuye.activity.event_near;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.INearOtherView;
import com.chuanglgc.wuye.MVP.persent.NearOtherPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.dialog.PartnerDialog;
import com.chuanglgc.wuye.dialog.SpinnerDialog;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.PartnerModel;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class NearEventOtherActivity extends BaseActivity<INearOtherView, NearOtherPersent> implements View.OnClickListener, INearOtherView {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.tv_master)
    TextView tvMaster;
    @BindView(R.id.sel_mast)
    RelativeLayout selMast;
    @BindView(R.id.tv_mate)
    TextView tvMate;
    @BindView(R.id.sel_mate)
    RelativeLayout selMate;
    @BindView(R.id.tv_record)
    EditText tvRecord;
    @BindView(R.id.bt_photo)
    MyPhotoBtton btPhoto;
    @BindView(R.id.rb_hurry)
    RadioButton rbHurry;
    @BindView(R.id.rb_normal)
    RadioButton rbNormal;
    @BindView(R.id.rg_level)
    RadioGroup rgLevel;
    private List<EventAllotModel> masterList;
    private int masterPostion;
    private List<PartnerModel> partnerList;
    private String partnerSleName;
    private Dialog loadingDialog;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_near_event_other;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = myToolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        rgLevel.check(R.id.rb_normal);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected NearOtherPersent oncreatPersenter() {
        return new NearOtherPersent();
    }


    @Override
    protected void initListener() {
        tvMaster.setOnClickListener(this);
        tvMate.setOnClickListener(this);
        btPhoto.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_master:
                requestMaster();
                break;
            case R.id.tv_mate:
                requestPartner();
                break;
            case R.id.bt_photo:
                allotTask();
                break;
        }
    }

    private void allotTask() {
        if (masterList == null) {
            Toast.makeText(this, "请选择负责人", Toast.LENGTH_SHORT).show();
        } else {
            HashMap<String, String> map = new HashMap<>();
            map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            map.put("leading_person", masterList.get(masterPostion).getEmp_id());
            if (!tvMate.getText().toString().trim().equals(""))map.put("assistant_person", partnerSleName);
            map.put("any_task_requirements", tvRecord.getText().toString().trim());
            //设置优先级  1 紧急 2 一般
            int rbId = rgLevel.getCheckedRadioButtonId();
            if (rbId == R.id.rb_hurry) {
                map.put("any_task_priority", "1");
            } else if (rbId == R.id.rb_normal) {
                map.put("any_task_priority", "2");
            }
            persenter.allotTask(map);
        }
    }

    //显示loading
    private void showLoading() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
    }

    //请求配合人
    private void requestPartner() {
        if (TextUtils.isEmpty(tvMaster.getText().toString().trim())) {
            Toast.makeText(this, "请选择负责人", Toast.LENGTH_SHORT).show();
        } else {
            HashMap<String, String> partnerMap = new HashMap<>();
            partnerMap.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            partnerMap.put("director_id", masterList.get(masterPostion).getEmp_id());
            persenter.requsetPartner(partnerMap);
        }
    }


    //请求负责人
    private void requestMaster() {
        showLoading();
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        persenter.requestMaster(map);
    }

    @Override
    public void getMasterList(List<EventAllotModel> masterList) {
        ArrayList<String> listMaster = new ArrayList<>();
        for (EventAllotModel model : masterList) {
            listMaster.add(model.getEmp_name());
        }
        SpinnerDialog masterDialog = new SpinnerDialog(this, listMaster);
        masterDialog.setOnSpinnerItemClick((itemName, position) -> {
            this.masterList = masterList;
            masterPostion = position;
            tvMaster.setText(itemName);
            masterDialog.dismiss();

        });
        if (loadingDialog != null) loadingDialog.dismiss();
    }

    //获取配合人
    @Override
    public void getPartnerList(List<PartnerModel> partnerList) {
        ArrayList<String> listPartner = new ArrayList<>();
        for (PartnerModel model : partnerList) {
            listPartner.add(model.getEmp_name());
        }
        PartnerDialog contentDialog = new PartnerDialog(this, listPartner);
        contentDialog.setOnCommitClick(((itemName, sleIdList) -> {
            this.partnerList = partnerList;
            partnerSleName = itemName.replace(",","、");
            tvMate.setText(partnerSleName);
            contentDialog.dismiss();
        }));
        if (loadingDialog != null) loadingDialog.dismiss();
    }


    @Override
    public void getAllotResult(String result) {
        Toast.makeText(this, "分配成功", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void getNetError(String error) {
        LogUtil.e("其他分配", error);
    }

}
