package com.chuanglgc.wuye.activity.event_supervise;

import android.app.Dialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.ISuperviseOwnerView;
import com.chuanglgc.wuye.MVP.persent.SuperviseOwnerPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.event.EventPhotoAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.dialog.EvaluateDialog;
import com.chuanglgc.wuye.dialog.EventOwnerInfoDialog;
import com.chuanglgc.wuye.dialog.PartnerDialog;
import com.chuanglgc.wuye.dialog.SecondChekDialog;
import com.chuanglgc.wuye.dialog.SpinnerDialog;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.PartnerModel;
import com.chuanglgc.wuye.model.SuperviseOwnerDetailModel;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;
import com.zhy.android.percent.support.PercentRelativeLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;


public class SuperviseOwnerDetailActivity extends BaseActivity<ISuperviseOwnerView, SuperviseOwnerPersent> implements View.OnClickListener
        , ISuperviseOwnerView {
    @BindView(R.id.toolbar)
    MyToolbar myToolbar;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.rl_time)
    PercentRelativeLayout rlTime;
    @BindView(R.id.tv_room)
    TextView tvRoom;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.tv_master)
    TextView tvMaster;
    @BindView(R.id.tv_mate)
    TextView tvMate;
    @BindView(R.id.tv_detail)
    TextView tvDetail;
    @BindView(R.id.rcy_iv)
    RecyclerView rcyPic;
    @BindView(R.id.tv_more)
    TextView tvMore;
    @BindView(R.id.rc_left)
    RelativeLayout rcLeft;
    @BindView(R.id.iv_round)
    ImageView ivRound;
    @BindView(R.id.bt_help)
    TextView btHelp;
    @BindView(R.id.bt_handle)
    TextView btHandle;
    @BindView(R.id.ll_noevalue_root)
    PercentRelativeLayout llNoevalueRoot;
    @BindView(R.id.bt_evalues)
    MyPhotoBtton btEvalues;
    @BindView(R.id.rl_bt_root)
    RelativeLayout rlBtRoot;
    @BindView(R.id.rl_pic_root)
    PercentRelativeLayout rlPicRoot;
    @BindView(R.id.rl_info_foot)
    PercentRelativeLayout rlInfoFoot;
    @BindView(R.id.iv_line)
    ImageView ivLine;
    @BindView(R.id.rl_line)
    RelativeLayout rlLine;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_info_date)
    TextView tvInfoDate;
    @BindView(R.id.tv_info_time)
    TextView tvInfoTime;
    @BindView(R.id.tv_info_name)
    TextView tvInfoName;
    @BindView(R.id.tv_info_room)
    TextView tvInfoRoom;
    @BindView(R.id.tv_info_phone)
    TextView tvInfoPhone;
    @BindView(R.id.tv_info_detail)
    TextView tvInfoDetail;
    private ArrayList<String> list;
    private String taskId;
    private int masterPostion;
    private List<EventAllotModel> marsterList;
    private Dialog loadingDialog;
    private List<PartnerModel> partnerList;
    private String partnerSleName;
    private ArrayList<Integer> partnerIdList;
    private String leaderId;
    private List<SuperviseOwnerDetailModel.FeedbackBean> feedbackList;
    private ArrayList<String> picList = new ArrayList<>();
    private String state;
    private String workStatus;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_supervise_owner_detail;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = myToolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });

        state = getIntent().getStringExtra("state");
        taskId = getIntent().getStringExtra("task_id");
        LogUtil.e("业主ID", taskId + state);
        workStatus = "";
        switch (state) {
            case "处理中"://处理中
                workStatus = "1";
                myToolbar.setTvState("处理中...", getResources().getColor(R.color.bluelight));
                rlBtRoot.setVisibility(View.INVISIBLE);
                break;
            case "未接受"://未接受
                workStatus = "2";
                myToolbar.setTvState("未接受...", getResources().getColor(R.color.colorAccent));
                rlInfoFoot.setVisibility(View.GONE);
                llNoevalueRoot.setVisibility(View.VISIBLE);
                btEvalues.setVisibility(View.INVISIBLE);
                ivLine.setVisibility(View.INVISIBLE);
                break;
            case "待评价"://待评价
                workStatus = "3";
                myToolbar.setTvState("待评价...", getResources().getColor(R.color.yellow));
                llNoevalueRoot.setVisibility(View.INVISIBLE);
                btEvalues.setVisibility(View.VISIBLE);
                break;
        }
        requestDetail();

    }

    //请求详情
    private void requestDetail() {
        showLoading();
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("task_id", taskId);
        map.put("work_status", workStatus);
        persenter.requestDetailInfo(map);
    }


    @Override
    protected void initData() {

    }

    @Override
    protected SuperviseOwnerPersent oncreatPersenter() {
        return new SuperviseOwnerPersent();
    }

    @Override
    protected void initListener() {
        tvMore.setOnClickListener(this);
        btEvalues.setOnClickListener(this);
        btHandle.setOnClickListener(this);
        btHelp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_more:
                showMoreInfoDialog();//展示更多条目
                break;
            case R.id.bt_help:
                requestPartner();
                break;
            case R.id.bt_handle:
                requestMaster();
                break;
            case R.id.bt_evalues:
                showEvaluateDialog();
                break;
        }
    }

    //请求配合人
    private void requestPartner() {
        if (TextUtils.isEmpty(tvMaster.getText().toString().trim())) {
            Toast.makeText(this, "请选择负责人", Toast.LENGTH_SHORT).show();
        } else {
            showLoading();
            HashMap<String, String> partnerMap = new HashMap<>();
            partnerMap.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            if (marsterList != null) {
                partnerMap.put("director_id", marsterList.get(masterPostion).getEmp_id());
            } else if (leaderId != null) {
                partnerMap.put("leaderid", leaderId);
            }
            persenter.requsetPartner(partnerMap);
        }
    }

    //显示loading
    private void showLoading() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
    }

    //请求负责人
    private void requestMaster() {
        showLoading();
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        persenter.requestMaster(map);
    }

    //评价每日工作
    private void evalueTaks(String evluate, int stars) {
        showLoading();
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("daily_task_id", taskId);
        map.put("task_type", "3");
        map.put("star_quantity", stars + "");
        map.put("evaluate_content", evluate);
        persenter.requestEvalue(map);
    }

    /**
     * 当待评价时候显示
     */
    private void initRclPhoto() {
        rlLine.setVisibility(View.VISIBLE);
        rlPicRoot.setVisibility(View.VISIBLE);
        rcyPic.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        EventPhotoAdapter photoAdapter = new EventPhotoAdapter(picList);
        photoAdapter.bindToRecyclerView(rcyPic);
    }

    private void showMoreInfoDialog() {
        if (feedbackList != null) {
            EventOwnerInfoDialog dialog = new EventOwnerInfoDialog(this, feedbackList);
            dialog.show();
        }
    }


    /**
     * 显示二次确认负责人dialog
     */
    private void showSeconedCommitDialog(String sleName) {
        SecondChekDialog chekDialog = new SecondChekDialog(this);
        chekDialog.setContent(sleName);
        chekDialog.setOnNegativeListener(v -> {
            chekDialog.dismiss();
        });
        chekDialog.setOnPositiveListener(v -> {
            requestAllot();//分配负责人
            chekDialog.dismiss();
        });
        chekDialog.show();
    }


    //请求分配负责人
    private void requestAllot() {
        showLoading();
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("task_id", taskId);
        if (marsterList != null) {
            map.put("leaderid", marsterList.get(masterPostion).getEmp_id());
        } else {
            map.put("leaderid", leaderId);
        }
        persenter.requestAllot(map);
    }

    /**
     * 配合人二次确认
     */
    private void showPartnerSeconedCommitDialog(String sleName) {
        SecondChekDialog chekDialog = new SecondChekDialog(this);
        if (TextUtils.isEmpty(sleName) || sleName.equals("")) {//无选中任何条目
            chekDialog.setContent("没有选中任何条目");
        } else {
            chekDialog.setContent("添加协助人:" + sleName);
            chekDialog.setOnPositiveListener(v -> {
                requestHelp();//请求接口
                chekDialog.dismiss();
            });
        }
        chekDialog.setOnNegativeListener(v -> {
            chekDialog.dismiss();
        });
        chekDialog.show();
    }

    //追加协助
    private void requestHelp() {
        showLoading();
        String personID = "";
        for (int i = 0; i < partnerIdList.size(); i++) {
            if (i == 0) {
                personID = partnerList.get(partnerIdList.get(0)).getEmp_id();
            } else {
                personID += "," + partnerList.get(partnerIdList.get(i)).getEmp_id();
            }
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("task_id", taskId);
        map.put("assistant_person", personID);
        persenter.requestHelp(map);
    }

    /**
     * 评价dialog
     */
    private void showEvaluateDialog() {
        EvaluateDialog evaluateDialog = new EvaluateDialog(this);
        evaluateDialog.setOnCommitEvluateListener((evluate, stars) -> {
            evalueTaks(evluate, stars);
        });
    }


    @Override
    public void getMasterList(List<EventAllotModel> marsterList) {
        ArrayList<String> listMaster = new ArrayList<>();
        for (EventAllotModel model : marsterList) {
            listMaster.add(model.getEmp_name());
        }
        SpinnerDialog masterDialog = new SpinnerDialog(this, listMaster);
        masterDialog.setOnSpinnerItemClick((itemName, position) -> {
            this.marsterList = marsterList;
            masterPostion = position;
            tvMaster.setText(itemName);
            showSeconedCommitDialog("分配负责人:" + itemName);
            masterDialog.dismiss();

        });
        if (loadingDialog != null) loadingDialog.dismiss();
    }

    @Override
    public void getPartner(List<PartnerModel> partnerList) {
        ArrayList<String> listPartner = new ArrayList<>();
        for (PartnerModel model : partnerList) {
            listPartner.add(model.getEmp_name());
        }
        PartnerDialog contentDialog = new PartnerDialog(this, listPartner);
        contentDialog.setOnCommitClick(((itemName, sleIdList) -> {
            this.partnerList = partnerList;
            partnerSleName = itemName.replace(",","、");
            this.partnerIdList = sleIdList;
            contentDialog.dismiss();
            showPartnerSeconedCommitDialog(partnerSleName);
        }));
        if (loadingDialog != null) loadingDialog.dismiss();
    }

    @Override
    public void getAllotTaskResult(String result) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
        if (marsterList != null && !leaderId.equals(marsterList.get(masterPostion).getEmp_id())) {
            tvMate.setText("");
        }
        setResult(3);//刷新
        finish();
        if (loadingDialog != null) loadingDialog.dismiss();
    }
   //请求协助回调
    @Override
    public void getHelpTaskResult(String result) {
        requestDetail();
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
        if (loadingDialog != null) loadingDialog.dismiss();
    }

    @Override
    public void getOwnerDetail(SuperviseOwnerDetailModel ownerDetailModel) {
        SuperviseOwnerDetailModel.EventdetailsBean model = ownerDetailModel.getEventdetails();
        if (model != null) {
            leaderId = model.getShip_id();
            tvDate.setText(model.getRepair_task_date());
            tvRoom.setText(model.getContact_person_address());
            tvName.setText(model.getContact_person_name());
            tvPhone.setText(model.getContact_person_tel());
            tvType.setText(model.getRepair_task_name());
            tvDetail.setText(model.getRepair_content());
            tvMaster.setText(model.getShip_name());
            if (model.getAssistant_person() != null) {
                tvMate.setText("、"   + model.getAssistant_person());
            }
            String p1 = model.getTask_image1();
            String p2 = model.getTask_image2();
            String p3 = model.getTask_image3();
            if (p1 != null) picList.add(p1);
            if (p2 != null) picList.add(p2);
            if (p3 != null) picList.add(p3);
            if (picList.size() > 0 && state.equals("待评价")) {
                initRclPhoto();
            }
        }
        List<SuperviseOwnerDetailModel.FeedbackBean> feedbackList = ownerDetailModel.getFeedback();
        if (feedbackList != null && feedbackList.size() > 0) {
            this.feedbackList = feedbackList;
            SuperviseOwnerDetailModel.FeedbackBean feedbackBean = feedbackList.get(0);
            tvInfoDate.setText(feedbackBean.getFeedback_date());
            tvInfoTime.setText(feedbackBean.getFeedback_time());
            tvInfoName.setText(feedbackBean.getContact_person_name());
            tvInfoPhone.setText(feedbackBean.getContact_person_tel());
            tvInfoRoom.setText(feedbackBean.getContact_person_address());
            tvInfoDetail.setText(feedbackBean.getFeedback_content());
        } else {
            ivLine.setVisibility(View.GONE);
            rlInfoFoot.setVisibility(View.INVISIBLE);
        }
        if (loadingDialog != null) loadingDialog.dismiss();
    }


    @Override
    public void getEvalueResult(String result) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
        if (loadingDialog != null) loadingDialog.dismiss();
        setResult(3);
        finish();
    }

    @Override
    public void getNetError(String error) {
        if (loadingDialog != null) loadingDialog.dismiss();
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }


}
