package com.chuanglgc.wuye.activity.attendance;

import android.app.Dialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.AttendanceClassAdapter;
import com.chuanglgc.wuye.adapter.AttendanceDayAdapter;
import com.chuanglgc.wuye.adapter.AttendanceLateAdapter;
import com.chuanglgc.wuye.adapter.AttendanceLeaveEarlyAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.dialog.DatePickerDialog;
import com.chuanglgc.wuye.model.PersonalStatisticsModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.AttendanceUpDownView;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AttendanceCountActivity extends BaseActivity implements View.OnClickListener, DatePickerDialog.TimePickerDialogInterface {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.iv_calendary)
    ImageView ivCalendary;
    @BindView(R.id.sel_time)
    RelativeLayout selTime;
    @BindView(R.id.updown_day)
    AttendanceUpDownView updownDay;
    @BindView(R.id.rcl_day)
    RecyclerView rclDay;
    @BindView(R.id.updown_class)
    AttendanceUpDownView updownClass;
    @BindView(R.id.rcl_class)
    RecyclerView rclClass;
    @BindView(R.id.updown_late)
    AttendanceUpDownView updownLate;
    @BindView(R.id.rcl_late)
    RecyclerView rclLate;
    @BindView(R.id.updown_leave)
    AttendanceUpDownView updownLeave;
    @BindView(R.id.rcl_leave)
    RecyclerView rclLeave;
    @BindView(R.id.rl_date_root)
    RelativeLayout rlDateRoot;
    private PersonalStatisticsModel personalModel;
    private Dialog loadingDialog;
    private AttendanceLeaveEarlyAdapter leaveEarlyAdapter;
    private AttendanceLateAdapter lateAdapter;
    private AttendanceClassAdapter classAdapter;
    private AttendanceDayAdapter dayNumberAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_attendance_count;
    }

    @Override
    protected void initView() {
        Toolbar toolbar = myToolbar.getToolbar();
        if (toolbar != null) {
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> {
                finish();
            });
        }
        SimpleDateFormat sDateFormat1 = new SimpleDateFormat("yyyy.MM");
        String currentDate = sDateFormat1.format(new Date());
        tvTime.setText(currentDate);
        requestCheckCount(currentDate);
    }

    private void requestCheckCount(String date) {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
        closeAllUp(null);//关闭所有Rcl
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        map.put("attendancetime", date);
        RestClient.getAPIService().queryCheckStatistics(map).enqueue(new Callback<Result<PersonalStatisticsModel>>() {
            @Override
            public void onResponse(Call<Result<PersonalStatisticsModel>> call, Response<Result<PersonalStatisticsModel>> response) {
                if (response.code() == 200) {
                    Result<PersonalStatisticsModel> result = response.body();
                    if (result.isResult()) {
                        personalModel = result.getData();
                        updownDay.setCount(personalModel.getAttendance().getAttendance_totaldays() + "");
                        updownClass.setCount(personalModel.getShift().getShift_totalcount() + "");
                        updownLate.setCount(personalModel.getLate().getLate_totalcount() + "");
                        updownLeave.setCount(personalModel.getLeave_early().getEarly_total() + "");
                    }
                } else {
                    LogUtil.e("个人考勤统计", response.code());
                }
                loadingDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Result<PersonalStatisticsModel>> call, Throwable t) {
                LogUtil.e("个人考勤统计", t);
                loadingDialog.dismiss();
            }
        });
    }


    @Override
    protected void initData() {

    }

    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }


    @Override
    protected void initListener() {
        updownDay.setOnClickListener(this);
        updownClass.setOnClickListener(this);
        updownLate.setOnClickListener(this);
        updownLeave.setOnClickListener(this);

        rlDateRoot.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.updown_day:
                closeAllUp(updownDay);
                if (updownDay.setUpDown()) {
                    showRclDay(personalModel.getAttendance());
                } else {
                    rclDay.setVisibility(View.GONE);
                }
                break;
            case R.id.updown_class:
                closeAllUp(updownClass);
                if (updownClass.setUpDown()) {
                    showRclClass(personalModel.getShift());
                } else {
                    rclClass.setVisibility(View.GONE);
                }
                break;
            case R.id.updown_late:
                closeAllUp(updownLate);
                if (updownLate.setUpDown()) {
                    showRclLate(personalModel.getLate());
                } else {
                    rclLate.setVisibility(View.GONE);
                }

                break;
            case R.id.updown_leave:
                closeAllUp(updownLeave);
                if (updownLeave.setUpDown()) {
                    showRlcLeaveEearly(personalModel.getLeave_early());
                } else {
                    rclLeave.setVisibility(View.GONE);
                }
                break;
            case R.id.rl_date_root:
                showCalendary();//显示日历
                break;
        }
    }

    /**
     * 显示日历
     */
    private void showCalendary() {
        DatePickerDialog pickerDialog = new DatePickerDialog(this);
        pickerDialog.showDatePickerDialog();
    }


    /**
     * 设置早退
     */
    private void showRlcLeaveEearly(PersonalStatisticsModel.LeaveEarlyBean leaveEarlyBean) {
        if (leaveEarlyBean.getEarly_info() != null) {
            if (leaveEarlyAdapter == null) {
                leaveEarlyAdapter = new AttendanceLeaveEarlyAdapter(leaveEarlyBean.getEarly_info());
                leaveEarlyAdapter.bindToRecyclerView(rclLeave);
                rclLeave.setLayoutManager(new LinearLayoutManager(this));
            } else {
                leaveEarlyAdapter.setNewData(leaveEarlyBean.getEarly_info());
            }
            rclLeave.setVisibility(View.VISIBLE);
        }

    }

    /**
     * 设置迟到
     */
    private void showRclLate(PersonalStatisticsModel.LateBean late) {
        if (late.getLate_info() != null) {
            if (lateAdapter == null) {
                rclLate.setLayoutManager(new LinearLayoutManager(this));
                lateAdapter = new AttendanceLateAdapter(late.getLate_info());
                lateAdapter.bindToRecyclerView(rclLate);
            } else {
                lateAdapter.setNewData(late.getLate_info());
            }
        }
        rclLate.setVisibility(View.VISIBLE);
    }

    /**
     * 设置出勤班次
     *
     * @param shift
     */
    private void showRclClass(PersonalStatisticsModel.ShiftBean shift) {
        if (shift.getShift_time() != null) {
            if (classAdapter == null) {
                rclClass.setLayoutManager(new LinearLayoutManager(this));
                classAdapter = new AttendanceClassAdapter(shift.getShift_time());
                classAdapter.bindToRecyclerView(rclClass);
            } else {
                classAdapter.setNewData(shift.getShift_time());
            }
        }
        rclClass.setVisibility(View.VISIBLE);
    }

    /**
     * 设置出勤天数
     */
    private void showRclDay(PersonalStatisticsModel.AttendanceBean attendanceBean) {
        if (attendanceBean.getAttendance_date() != null) {
            if (dayNumberAdapter == null) {
                rclDay.setLayoutManager(new LinearLayoutManager(this));
                dayNumberAdapter = new AttendanceDayAdapter(attendanceBean.getAttendance_date());
                dayNumberAdapter.bindToRecyclerView(rclDay);
            } else {
                dayNumberAdapter.setNewData(attendanceBean.getAttendance_date());
            }
        }
        rclDay.setVisibility(View.VISIBLE);
    }

    @Override
    public void positiveListener(String date, String currentDate) {
        String month = currentDate.substring(0, currentDate.lastIndexOf("."));
        tvTime.setText(month);
        requestCheckCount(month);

    }

    @Override
    public void negativeListener() {

    }


    /**
     * 关闭其他的Rcl 选择器设置为false
     */
    private void closeAllUp(AttendanceUpDownView updown) {
        if (updown != updownDay) {
            rclDay.setVisibility(View.GONE);
            updownDay.setSelectFalse();
        }
        if (updown != updownClass) {
            rclClass.setVisibility(View.GONE);
            updownClass.setSelectFalse();
        }
        if (updown != updownLeave) {
            rclLeave.setVisibility(View.GONE);
            updownLeave.setSelectFalse();
        }
        if (updown != updownLate) {
            rclLate.setVisibility(View.GONE);
            updownLate.setSelectFalse();
        }

    }
}
