package com.chuanglgc.wuye.activity.event_owner;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.INormalView;
import com.chuanglgc.wuye.MVP.persent.OwnerEventPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.event.OwnerEventAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.model.LoginModel;
import com.chuanglgc.wuye.model.OwnerEventModel;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;


public class OwnerEventActivity extends BaseActivity<INormalView, OwnerEventPersent> implements INormalView<List<OwnerEventModel>> {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.rcl_owner)
    RecyclerView rclOwner;
    private Dialog loadingDialog;
    private OwnerEventAdapter ownerEventAdapter;
    private OnItemClickListener onItemClickListener;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_owner;
    }

    @Override
    protected void initView() {
        Toolbar toolbar = myToolbar.getToolbar();
        if (toolbar != null) {
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> {
                finish();
            });
        }
        requstEventList();

    }

    /**
     * 请求业主事件列表
     */
    private void requstEventList() {
        //发起请求
        LoginModel loginModel = EmployeeApplication.getInstance().getUserInfoFromCache();
        HashMap<String, String> requestMap = new HashMap<>();
        requestMap.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        requestMap.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        LogUtil.e("参数",EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id()+"|||"+EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
       if (loadingDialog==null){
           loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
       }else {
           loadingDialog.show();
       }
        persenter.requestOwnerEvent(requestMap);
    }


    private void initRecyclerOwner(List<OwnerEventModel> modelList) {
        if (ownerEventAdapter == null) {
            rclOwner.setLayoutManager(new LinearLayoutManager(this));
            ownerEventAdapter = new OwnerEventAdapter(modelList);
            ownerEventAdapter.bindToRecyclerView(rclOwner);
        } else {
            ownerEventAdapter.setNewData(modelList);
        }
        rclOwner.removeOnItemTouchListener(null);
        if (onItemClickListener != null) rclOwner.removeOnItemTouchListener(onItemClickListener);
        onItemClickListener = new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(OwnerEventActivity.this, OwnerEventDetailActivity.class);
                intent.putExtra("power", modelList.get(position).getTask_status_id());
                LogUtil.e("权限", modelList.get(position).getTask_status_id());
                intent.putExtra("task_id", modelList.get(position).getTask_id());
                intent.putExtra("repaire_id", modelList.get(position).getRepair_id());
                startActivityForResult(intent, 1);
            }
        };
        rclOwner.addOnItemTouchListener(onItemClickListener);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == 100) {//接受状态
            requstEventList();//重新请求刷新
            LogUtil.e("刷新","ssss");
        }
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected OwnerEventPersent oncreatPersenter() {
        return new OwnerEventPersent();
    }

    /**
     * 网络获取业主事件回调
     */

    @Override
    public void getNetResult(List<OwnerEventModel> modelList) {
        if (loadingDialog != null) loadingDialog.dismiss();
        initRecyclerOwner(modelList);

    }

    /**
     * 网络获取错误回调
     *
     * @param wrong
     */
    @Override
    public void getNetFauiler(String wrong) {
        if (loadingDialog != null) loadingDialog.dismiss();
        initRecyclerOwner(new ArrayList<OwnerEventModel>());
        Toast.makeText(this, wrong, Toast.LENGTH_SHORT).show();
    }
}
