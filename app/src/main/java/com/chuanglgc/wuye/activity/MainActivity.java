package com.chuanglgc.wuye.activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.IMainView;
import com.chuanglgc.wuye.MVP.persent.MainPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.activity.attendance.AttendanceActivity;
import com.chuanglgc.wuye.activity.car_release.CarReleaseActivity;
import com.chuanglgc.wuye.activity.event_cycle.CycleEventActivity;
import com.chuanglgc.wuye.activity.event_dailywork.DailyWorkActivity;
import com.chuanglgc.wuye.activity.event_handle.HandEventActivity;
import com.chuanglgc.wuye.activity.event_manager.EventManagerActivity;
import com.chuanglgc.wuye.activity.event_near.NearEventActivity;
import com.chuanglgc.wuye.activity.event_owner.OwnerEventActivity;
import com.chuanglgc.wuye.activity.event_supervise.SuperviseActivity;
import com.chuanglgc.wuye.activity.master_attendance.MasterAttendanceActivity;
import com.chuanglgc.wuye.activity.message_notify.MessageNotifyActivity;
import com.chuanglgc.wuye.activity.owner_search.OwnerSearchActivity;
import com.chuanglgc.wuye.activity.pressmoney.PressMoneyActivity;
import com.chuanglgc.wuye.activity.search_car.SearchCarActivity;
import com.chuanglgc.wuye.activity.setting.SettingActivity;
import com.chuanglgc.wuye.adapter.MainFunctionAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.broadcast.MainBroadcast;
import com.chuanglgc.wuye.model.HeadPicModel;
import com.chuanglgc.wuye.model.LoginModel;
import com.chuanglgc.wuye.network.RabbitmqManager;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.utils.recyclerchange.SimpleItemTouchHelperCallback;
import com.linchaolong.android.imagepicker.ImagePicker;
import com.linchaolong.android.imagepicker.cropper.CropImage;
import com.linchaolong.android.imagepicker.cropper.CropImageView;
import com.uuzuche.lib_zxing.activity.CaptureActivity;
import com.uuzuche.lib_zxing.activity.CodeUtils;

import java.io.File;
import java.net.URI;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class MainActivity extends BaseActivity<IMainView, MainPersent> implements IMainView {


    @BindView(R.id.iv_head)
    ImageView ivHead;
    @BindView(R.id.iv_nocheck)
    ImageView ivNocheck;
    @BindView(R.id.rcy_fuction)
    RecyclerView rcyFuction;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_number)
    TextView tvNumber;
    @BindView(R.id.tv_work_type)
    TextView tvWorkType;
    @BindView(R.id.tv_area)
    TextView tvArea;
    @BindView(R.id.tv_check)
    TextView tvCheck;
    @BindView(R.id.iv_qrcode)
    ImageView ivQrcode;

    private LoginModel userInfoFromCache;
    private MainFunctionAdapter adapter;
    private MainBroadcast mainBroadcast;
    private ImagePicker imagePicker;
    private long exitTime;
    private MediaPlayer player;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        //初始化Rabbit 实时接受消息
        RabbitmqManager instance = RabbitmqManager.getInstance();
        instance.setupConnectionFactory("139.224.191.27", 5672, "iot", "iot123", this);
        String phone = EmployeeApplication.getInstance().getUserInfoFromCache().getPhone_number();
        instance.subscribe("user_wuye_app_ex", ":KEY" + phone);

        userInfoFromCache = EmployeeApplication.getInstance().getUserInfoFromCache();
        tvName.setText(userInfoFromCache.getEmp_name());
        tvNumber.setText(userInfoFromCache.getEmp_number());
        tvArea.setText(userInfoFromCache.getCommunity_name());
        tvWorkType.setText(userInfoFromCache.getDepartment_name());
        String clock_status = userInfoFromCache.getClock_status();//判断打卡状态
        if (clock_status.equals("已打卡")) {
            ivNocheck.setVisibility(View.GONE);
        }
        tvCheck.setText(clock_status);
        //加载头像
        if (userInfoFromCache.getImage_url() != null) {
            Date date = new Date();
            long time = date.getTime();
            Glide.with(this).load(userInfoFromCache.getImage_url() + "?" + time).error(R.mipmap.person).into(ivHead);
        }

        initFunction(userInfoFromCache.getMenu());//登陆时候返回
        registBroadcast();
        photoManager();
    }

    private void registBroadcast() {
        mainBroadcast = new MainBroadcast();
        IntentFilter filter = new IntentFilter("MainBroadcast");
        registerReceiver(mainBroadcast, filter);
    }

    //用来打卡时候动态改变 通过broadcast
    public void setCheckText(String check) {
        if (check == null) {
            tvCheck.setText("未打卡");
            ivNocheck.setVisibility(View.VISIBLE);
        } else {
            tvCheck.setText(check);
            if (check.equals("未打卡")) {
                ivNocheck.setVisibility(View.VISIBLE);
            } else {
                ivNocheck.setVisibility(View.GONE);
            }
        }
    }

    //设置功能模块 Rcl
    private void initFunction(List<LoginModel.MenuBean> menus) {
        if (adapter == null) {
            rcyFuction.setLayoutManager(new GridLayoutManager(this, 3));
            adapter = new MainFunctionAdapter(menus);
            adapter.bindToRecyclerView(rcyFuction);
            SimpleItemTouchHelperCallback touchHelper = new SimpleItemTouchHelperCallback(adapter);
            ItemTouchHelper itemTouchHelper=new ItemTouchHelper(touchHelper);
            itemTouchHelper.attachToRecyclerView(rcyFuction);
            View footerView = new View(this);
            RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, DisplayUtil.dp2px(this, 15));
            footerView.setLayoutParams(params);
            adapter.addFooterView(footerView);
            rcyFuction.addOnItemTouchListener(new OnItemClickListener() {
                @Override
                public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                    String typeID = menus.get(position).getAA2100();
                    switch (typeID) {
                        case "7005":
                            startActivity(SuperviseActivity.class, null);//监督评价
                            break;
                        case "7004":
                            startActivity(CycleEventActivity.class, null);//周期事务
                            break;
                        case "7013":
                            startActivity(MasterAttendanceActivity.class, null);//员工考勤
                            break;
                        case "7002":
                            startActivity(EventManagerActivity.class, null);//事件管理
                            break;
                        case "7001":
                            startActivity(OwnerEventActivity.class, null);//业主事件
                            break;
                        case "7012":
                            startActivity(DailyWorkActivity.class, null);//每日工作
                            break;
                        case "7014":
                            startActivity(HandEventActivity.class, null); //事务处理
                            break;
                        case "7003":
                            startActivity(NearEventActivity.class, null);//临时分配
                            break;
                        case "7010":
                            startActivity(AttendanceActivity.class, null);//考勤打卡
                            break;
                        case "7009":
                            startActivity(CarReleaseActivity.class, null);//车辆放行
                            break;
                        case "7006":
                            startActivity(MessageNotifyActivity.class, null);//消息通知
                            break;
                        case "7011":
                            startActivity(SettingActivity.class, null); //系统设置
                            break;

                        case "7007":
                            startActivity(OwnerSearchActivity.class, null);//户主查询
                            break;
                        case "7008":
                            startActivity(SearchCarActivity.class, null);//车辆查询
                            break;
                        case "7015":
                            startActivity(PressMoneyActivity.class, null);//催费事务
                            break;

                    }
                }
            });
        } else {
            adapter.setNewData(menus);
        }

    }

    /**
     * 每次进来都刷新 功能模块
     */
    @Override
    protected void onResume() {
        super.onResume();
     /*   HashMap<String, String> map = new HashMap<>();
        map.put("community_id",EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("emp_id",EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        persenter.refreshFunction(map);*/
    }

    @Override
    protected void initListener() {
        ivHead.setOnClickListener(v -> upLoadPic());
    }

    private void upLoadPic() {
        addPhoto();
    }

    /**
     * 初始化相册管理器
     */
    private void photoManager() {
        imagePicker = new ImagePicker();
        // 设置标题
        imagePicker.setTitle("选择图片");
        // 设置是否裁剪图片
        imagePicker.setCropImage(true);
    }

    private void addPhoto() {
        // 启动图片选择器
        imagePicker.startChooser(this, new ImagePicker.Callback() {
            // 选择图片回调
            @Override
            public void onPickImage(Uri imageUri) {
            }

            // 裁剪图片回调
            @Override
            public void onCropImage(Uri imageUri) {
                //向服务器上传图片
                File file = new File(URI.create(String.valueOf(imageUri)));
                RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpeg"), file);
                MultipartBody.Part part = MultipartBody.Part.createFormData("photo", file.getName(), requestBody);
                persenter.uploadPic(EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id(), part);
            }

            // 自定义裁剪配置
            @Override
            public void cropConfig(CropImage.ActivityBuilder
                                           builder) {
                // 是否启动多点触摸
                builder.setMultiTouchEnabled(true)
                        // 设置网格显示模式
                        .setGuidelines(CropImageView.Guidelines.ON)
                        // 圆形/矩形
                        .setCropShape(CropImageView.CropShape
                                .OVAL)
                        // 调整裁剪后的图片最终大小
                        .setRequestedSize(960, 540)
                        // 宽高比
                        .setAspectRatio(1, 1);
            }

            // 用户拒绝授权回调
            @Override
            public void onPermissionDenied(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int
            resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 5) {
            //处理扫描结果（在界面上显示）
            if (null != data) {
                Bundle bundle = data.getExtras();
                if (bundle == null) {
                    return;
                }
                if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_SUCCESS) {
                    String result = bundle.getString(CodeUtils.RESULT_STRING);
                    Toast.makeText(this, "解析结果:" + result, Toast.LENGTH_LONG).show();
                    return;
                } else if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_FAILED) {
                   // Toast.makeText(MainActivity.this, "解析二维码失败", Toast.LENGTH_LONG).show();
                    return;
                }
            } else {
                return;
            }

        }
        imagePicker.onActivityResult(this, requestCode, resultCode, data);
    }

    @Override
    protected void initData() {
        ivQrcode.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, CaptureActivity.class);
            startActivityForResult(intent, 5);
        });

    }

    @Override
    protected MainPersent oncreatPersenter() {
        return new MainPersent();
    }


    /**
     * 网络请求功能模块回调
     */
    @Override
    public void getFunction(List<LoginModel.MenuBean> menuBeanResult) {
        initFunction(menuBeanResult);
    }

    @Override
    public void getUpPicResult(HeadPicModel model) {
        long time = new Date().getTime();
        Glide.with(this).load(model.getHttp_host() + model.getImage_url() + "?" + time).error(R.mipmap.person).into(ivHead);
    }

    @Override
    public void getFunctionErrow(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mainBroadcast != null) unregisterReceiver(mainBroadcast);
    }

    /**
     * 监听返回退出 按两次退出
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    public void exit() {
        if ((System.currentTimeMillis() - exitTime) > 1000) {
            Toast.makeText(getApplicationContext(), "再按一次退出程序", Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            finish();
            //退出之后极光扔监听 别处登录
          /*  //请求极光推送的别名
            String userId = EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id();
            JPushInterface.deleteAlias(MainActivity.this,Integer.valueOf(userId));*/
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
