package com.chuanglgc.wuye.activity.master_attendance;

import android.app.Dialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.AttendanceClassAdapter;
import com.chuanglgc.wuye.adapter.AttendanceCountDayAdapter;
import com.chuanglgc.wuye.adapter.AttendanceLeaveEarlyAdapter;
import com.chuanglgc.wuye.adapter.AttendanceMonthLateAdapter;
import com.chuanglgc.wuye.adapter.AttendanceMonthLeaveAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.dialog.DatePickerDialog;
import com.chuanglgc.wuye.model.MasterAttendMonthModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.widget.AttendanceUpDownView;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MasterAttedanceMothActiivty extends BaseActivity implements View.OnClickListener, DatePickerDialog.TimePickerDialogInterface {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.iv_calendary)
    ImageView ivCalendary;
    @BindView(R.id.sel_time)
    RelativeLayout selTime;
    @BindView(R.id.updown_day)
    AttendanceUpDownView updownDay;
    @BindView(R.id.rcl_day)
    RecyclerView rclDay;
    @BindView(R.id.updown_late)
    AttendanceUpDownView updownLate;
    @BindView(R.id.rcl_late)
    RecyclerView rclLate;
    @BindView(R.id.updown_leave)
    AttendanceUpDownView updownLeave;
    @BindView(R.id.rcl_leave)
    RecyclerView rclLeave;
    @BindView(R.id.rl_date_root)
    RelativeLayout rlDateRoot;
    private List<MasterAttendMonthModel.LateBean.LateInfoBean> lateList;
    private List<MasterAttendMonthModel.AttendanceBean.AttendanceEmpnameBean> attendanceList;
    private List<MasterAttendMonthModel.LeaveEarlyBean.EarlyInfoBean> leaveList;
    private String month;
    private Dialog loadingDialog;
    private AttendanceMonthLeaveAdapter monthLeaveAdapter;
    private AttendanceMonthLateAdapter lateAdapter;
    private AttendanceCountDayAdapter dayAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_master_attendcance_month;
    }

    @Override
    protected void initView() {
        Toolbar toolbar = myToolbar.getToolbar();
        if (toolbar != null) {
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> {
                finish();
            });
        }
        SimpleDateFormat sDateFormat1 = new SimpleDateFormat("yyyy.MM");
        month = sDateFormat1.format(new Date());
        tvTime.setText(month);
        requestAttandMonth();
    }


    @Override
    protected void initData() {

    }

    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }


    @Override
    protected void initListener() {
        updownDay.setOnClickListener(this);
        updownLate.setOnClickListener(this);
        updownLeave.setOnClickListener(this);

        rlDateRoot.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.updown_day:
                closeAllUp(updownDay);
                if (updownDay.setUpDown()) {
                    showRclDay();
                } else {
                    rclDay.setVisibility(View.GONE);
                }
                break;
            case R.id.updown_late:
                closeAllUp(updownLate);
                if (updownLate.setUpDown()) {
                    showRclLate();
                } else {
                    rclLate.setVisibility(View.GONE);
                }

                break;
            case R.id.updown_leave:
                closeAllUp(updownLeave);
                if (updownLeave.setUpDown()) {
                    showRlcLeaveEearly();
                } else {
                    rclLeave.setVisibility(View.GONE);
                }
                break;
            case R.id.rl_date_root:
                showCalendary();//显示日历
                break;
        }
    }

    /**
     * 显示日历
     */
    private void showCalendary() {
        DatePickerDialog pickerDialog = new DatePickerDialog(this);
        pickerDialog.showDatePickerDialog();
    }

    public void requestAttandMonth() {
        closeAllUp(null);
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        map.put("attendancetime", month);

        RestClient.getAPIService().queryEmpCheckStatistics(map).enqueue(new Callback<Result<MasterAttendMonthModel>>() {
            @Override
            public void onResponse(Call<Result<MasterAttendMonthModel>> call, Response<Result<MasterAttendMonthModel>> response) {
                if (response.code() == 200) {
                    Result<MasterAttendMonthModel> result = response.body();
                    if (result.isResult()) {
                        initInfo(result.getData());
                    } else {
                        Toast.makeText(MasterAttedanceMothActiivty.this, result.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MasterAttedanceMothActiivty.this, response.code(), Toast.LENGTH_SHORT).show();
                }
                loadingDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Result<MasterAttendMonthModel>> call, Throwable t) {
                loadingDialog.dismiss();
                Toast.makeText(MasterAttedanceMothActiivty.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //设置数据
    private void initInfo(MasterAttendMonthModel data) {
        MasterAttendMonthModel.AttendanceBean attendance = data.getAttendance();
        MasterAttendMonthModel.LateBean late = data.getLate();
        MasterAttendMonthModel.LeaveEarlyBean leave = data.getLeave_early();

        leaveList = leave.getEarly_info();
        lateList = late.getLate_info();
        attendanceList = attendance.getAttendance_empname();

        updownDay.setCount(attendance.getAttendance_totalperson() + "");
        updownLate.setCount(late.getLate_totalcount() + "");
        updownLeave.setCount(leave.getEarly_total() + "");

    }

    /**
     * 设置早退
     */
    private void showRlcLeaveEearly() {
        if (leaveList != null && leaveList.size() != 0) {
            if (monthLeaveAdapter == null) {
                monthLeaveAdapter = new AttendanceMonthLeaveAdapter(leaveList);
                monthLeaveAdapter.bindToRecyclerView(rclLeave);
                rclLeave.setLayoutManager(new LinearLayoutManager(this));
            } else {
                monthLeaveAdapter.setNewData(leaveList);
            }
            rclLeave.setVisibility(View.VISIBLE);
        }

    }

    /**
     * 设置迟到
     */
    private void showRclLate() {
        if (lateList != null && lateList.size() != 0) {
            if (lateAdapter == null) {
                lateAdapter = new AttendanceMonthLateAdapter(lateList);
                lateAdapter.bindToRecyclerView(rclLate);
                rclLate.setLayoutManager(new LinearLayoutManager(this));
            } else {
                lateAdapter.setNewData(lateList);
            }
            rclLate.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 设置出勤人数
     */
    private void showRclDay() {
        if (attendanceList != null && attendanceList.size() != 0) {
            if (dayAdapter == null) {
                rclDay.setLayoutManager(new LinearLayoutManager(this));
                dayAdapter = new AttendanceCountDayAdapter(attendanceList);
                dayAdapter.bindToRecyclerView(rclDay);
            } else {
                dayAdapter.setNewData(attendanceList);
            }
            rclDay.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void positiveListener(String date, String currentDate) {
        month = currentDate.substring(0, currentDate.lastIndexOf("."));
        tvTime.setText(month);
        requestAttandMonth();
    }

    @Override
    public void negativeListener() {

    }


    /**
     * 关闭其他的Rcl 选择器设置为false
     *
     * @param updown
     */
    private void closeAllUp(AttendanceUpDownView updown) {
        if (updown != updownDay) {
            rclDay.setVisibility(View.GONE);
            updownDay.setSelectFalse();
        }
        if (updown != updownLeave) {
            rclLeave.setVisibility(View.GONE);
            updownLeave.setSelectFalse();
        }
        if (updown != updownLate) {
            rclLate.setVisibility(View.GONE);
            updownLate.setSelectFalse();
        }

    }
}
