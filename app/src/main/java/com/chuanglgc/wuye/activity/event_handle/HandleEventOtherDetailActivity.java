package com.chuanglgc.wuye.activity.event_handle;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.IHandleEventDetailView;
import com.chuanglgc.wuye.MVP.persent.HandleEventDetailPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.HandleFloorNumberAdapter;
import com.chuanglgc.wuye.adapter.event.EventPhotoAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.model.EventHandleDetailModel;
import com.chuanglgc.wuye.model.PhotoModel;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;
import com.linchaolong.android.imagepicker.ImagePicker;
import com.linchaolong.android.imagepicker.cropper.CropImage;
import com.linchaolong.android.imagepicker.cropper.CropImageView;
import com.zhy.android.percent.support.PercentLinearLayout;
import com.zhy.android.percent.support.PercentRelativeLayout;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class HandleEventOtherDetailActivity extends BaseActivity<IHandleEventDetailView, HandleEventDetailPersent> implements View.OnClickListener
        , IHandleEventDetailView {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.rl_time)
    PercentRelativeLayout rlTime;
    @BindView(R.id.tv_master)
    TextView tvMaster;
    @BindView(R.id.tv_mate)
    TextView tvMate;
    @BindView(R.id.tv_level)
    TextView tvLevel;
    @BindView(R.id.tv_detail)
    TextView tvDetail;
    @BindView(R.id.rcl_pic)
    RecyclerView rclPic;
    @BindView(R.id.iv_addpic)
    ImageView ivAddpic;
    @BindView(R.id.rl_add)
    RelativeLayout rlAdd;
    @BindView(R.id.ll_pic_root)
    PercentLinearLayout llPicRoot;
    @BindView(R.id.bt_photo)
    MyPhotoBtton btPhoto;
    private HandleFloorNumberAdapter adapter;
    private ArrayList<String> list;
    private ImagePicker imagePicker;
    private EventPhotoAdapter photoAdapter;
    private ArrayList<String> listPhoto;
    private Dialog loadingDialog;
    private String tastId;

    private String photoUrl = "";
    private EventHandleDetailModel eventDetailModel;
    private List<String> moreList;
    private String state;
    private String cycle_task_type;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_handle_other_detail;
    }

    @Override
    protected void initView() {
        Toolbar toolbar = myToolbar.getToolbar();
        if (toolbar != null) {
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> {
                finish();
            });
        }


        /**
         * 权限 完成和接受
         */
        state = getIntent().getStringExtra("state");
        if (state.equals("2")) {
            btPhoto.setTxName("完成");
        } else {
            llPicRoot.setVisibility(View.INVISIBLE);
        }
        tastId = getIntent().getStringExtra("tastId");
        queryTaskDetail();
    }

    /**
     * 查询事务详情
     */
    private void queryTaskDetail() {
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("cycle_task_id", tastId);
        persenter.queryCycleTaskDetail(map);
        loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected HandleEventDetailPersent oncreatPersenter() {
        return new HandleEventDetailPersent();
    }

    @Override
    protected void initListener() {
        btPhoto.setOnClickListener(this);
        rlAdd.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_photo:
                acceptOrCommitTask();
                break;
            case R.id.rl_add:
                if (photoAdapter == null) {
                    initRclPhoto();
                }
                addPhoto();//添加相册
                break;
        }

    }

    /**
     * 判断接受还是完成
     */
    private void acceptOrCommitTask() {
        if (state.equals("2")) {//完成
            if (eventDetailModel != null && eventDetailModel.getIs_upload_image().equals("1") && photoUrl.equals("")) {
                Toast.makeText(this, "请添加事务相关图片才能提交", Toast.LENGTH_SHORT).show();
            } else if (eventDetailModel != null) {
                HashMap<String, String> finishMap = new HashMap<>();
                finishMap.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
                finishMap.put("cycle_task_id", tastId);
                finishMap.put("image_url", photoUrl);
                finishMap.put("cycle_task_type", cycle_task_type);
                persenter.finishTask(finishMap);
            }
        } else {//接受
            HashMap<String, String> acceptMap = new HashMap<>();
            acceptMap.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            acceptMap.put("cycle_task_id", tastId);
            acceptMap.put("cycle_task_type", cycle_task_type);
            persenter.acceptCycleTask(acceptMap);
        }

    }

    /**
     * 初始化相册的Rcl
     */
    private void initRclPhoto() {
        photoManager();
        listPhoto = new ArrayList<>();
        photoAdapter = new EventPhotoAdapter(listPhoto);
        rclPic.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        photoAdapter.bindToRecyclerView(rclPic);
    }

    /**
     * 初始化相册管理器
     */
    private void photoManager() {
        imagePicker = new ImagePicker();
        // 设置标题
        imagePicker.setTitle("选择图片");
        // 设置是否裁剪图片
        imagePicker.setCropImage(true);
    }

    private void addPhoto() {
        // 启动图片选择器
        imagePicker.startChooser(this, new ImagePicker.Callback() {
            // 选择图片回调
            @Override
            public void onPickImage(Uri imageUri) {
            }

            // 裁剪图片回调
            @Override
            public void onCropImage(Uri imageUri) {
                //rcl添加图片
                listPhoto.add(String.valueOf(imageUri));
                if (listPhoto.size() >= 3) rlAdd.setVisibility(View.GONE);
                photoAdapter.notifyDataSetChanged();

                PercentLinearLayout.LayoutParams layoutParams = (PercentLinearLayout.LayoutParams) rlAdd.getLayoutParams();
                layoutParams.setMargins(DisplayUtil.dp2px(HandleEventOtherDetailActivity.this, 12), 0, 0, 0);
                rlAdd.setLayoutParams(layoutParams);
                //向服务器上传图片
                File file = new File(URI.create(String.valueOf(imageUri)));
                RequestBody requestBody = RequestBody.create(MediaType.parse("image/png"), file);
                MultipartBody.Part part = MultipartBody.Part.createFormData("photo", file.getName(), requestBody);
                HashMap<String, String> map = new HashMap<>();
                map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
                map.put("cycle_task_id", tastId);
                persenter.uploadPic(map, part);
            }

            // 自定义裁剪配置
            @Override
            public void cropConfig(CropImage.ActivityBuilder
                                           builder) {
                // 是否启动多点触摸
                builder.setMultiTouchEnabled(true)
                        // 设置网格显示模式
                        .setGuidelines(CropImageView.Guidelines.ON)
                        // 圆形/矩形
                        .setCropShape(CropImageView.CropShape
                                .RECTANGLE)
                        // 调整裁剪后的图片最终大小
                        .setRequestedSize(960, 540)
                        // 宽高比
                        .setAspectRatio(16, 9);
            }

            // 用户拒绝授权回调
            @Override
            public void onPermissionDenied(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int
            resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imagePicker.onActivityResult(HandleEventOtherDetailActivity.this, requestCode, resultCode, data);
    }

    //事件详情model
    @Override
    public void getTaskDetail(EventHandleDetailModel model) {
        this.eventDetailModel = model;
        if (loadingDialog != null) loadingDialog.dismiss();
        cycle_task_type = model.getCycle_task_type();
        tvDate.setText(model.getCycle_task_date());
        tvMaster.setText(model.getLeading_person());
        if (model.getAssistant_person() != null && !model.getAssistant_person().equals(""))
            tvMate.setText("、" + model.getAssistant_person());
        tvDetail.setText(model.getCycle_task_remark());
        if (model.getCycle_task_priority().equals("1")) {
            tvLevel.setText("紧急");
        } else {
            tvLevel.setText("一般");
        }

    }

    //完成事务回调
    @Override
    public void commitTaskResult(String result) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
        setResult(1);
        finish();
    }

    /**
     * 上传图片的回调
     *
     * @param model http_host +image_url
     *              image_url 存起来 在完成时后一次行提交
     */
    @Override
    public void upLoadPhotoResult(PhotoModel model) {
        LogUtil.e("上传的图片", model.getHttp_host() + model.getImage_url());
        if (photoUrl.equals("")) {
            photoUrl += model.getImage_url();
        } else {
            photoUrl += "|" + model.getImage_url();
        }
    }

    /**
     * 接受完任务 退出并返回1，提示更新
     */
    @Override
    public void acceptTaskResult(String result) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
        setResult(1);
        finish();
    }

    @Override
    public void requestFailResult(String result) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
