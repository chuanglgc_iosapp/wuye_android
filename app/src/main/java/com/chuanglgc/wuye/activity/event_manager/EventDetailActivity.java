package com.chuanglgc.wuye.activity.event_manager;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.IOwnerDetailView;
import com.chuanglgc.wuye.MVP.persent.OwnerEventDetailPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.dialog.EventFinishDialog;
import com.chuanglgc.wuye.dialog.EventInfoDialog;
import com.chuanglgc.wuye.model.OwnerEventDetailModel;
import com.chuanglgc.wuye.model.VisitRecordModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;
import com.chuanglgc.wuye.widget.photoview.ShowImageActivity;
import com.zhy.android.percent.support.PercentLinearLayout;
import com.zhy.android.percent.support.PercentRelativeLayout;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EventDetailActivity extends BaseActivity<IOwnerDetailView, OwnerEventDetailPersent> implements View.OnClickListener, IOwnerDetailView {


    @BindView(R.id.toolbar)
    MyToolbar toolbar;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.rl_time)
    PercentRelativeLayout rlTime;
    @BindView(R.id.tv_room)
    TextView tvRoom;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_master)
    TextView tvMaster;
    @BindView(R.id.tv_mate)
    TextView tvMate;
    @BindView(R.id.ll_handle_person)
    PercentLinearLayout llHandlePerson;
    @BindView(R.id.tv_detail)
    TextView tvDetail;
    @BindView(R.id.tv_info_date)
    TextView tvInfoDate;
    @BindView(R.id.tv_info_time)
    TextView tvInfoTime;
    @BindView(R.id.tv_more)
    TextView tvMore;
    @BindView(R.id.iv_round)
    ImageView ivRound;
    @BindView(R.id.tv_info_name)
    TextView tvInfoName;
    @BindView(R.id.tv_info_phone)
    TextView tvInfoPhone;
    @BindView(R.id.tv_info_content)
    TextView tvInfoContent;
    @BindView(R.id.rl_info_foot)
    PercentRelativeLayout rlInfoFoot;
    @BindView(R.id.bt_visitor)
    TextView btVisitor;
    @BindView(R.id.bt_finish)
    TextView btFinish;
    @BindView(R.id.ll_no_allot_root)
    PercentRelativeLayout llNoAllotRoot;
    @BindView(R.id.bt_allot)
    MyPhotoBtton btAllot;
    @BindView(R.id.rl_bt_root)
    PercentRelativeLayout rlBtRoot;
    @BindView(R.id.iv_line)
    ImageView ivLine;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.rl_photo)
    RelativeLayout rlPhoto;
    @BindView(R.id.iv_photo)
    ImageView ivPhoto;
    @BindView(R.id.rl_pic)
    PercentRelativeLayout rlPic;
    private String state;
    private String repairId;
    private List<VisitRecordModel> visitModels;
    private String imageUrl;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_event_detail;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = toolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        state = getIntent().getStringExtra("state");
        repairId = getIntent().getStringExtra("repair_id");
        initPower();
        requestDetail();


    }

    @Override
    protected void initData() {

    }

    //设置权限
    private void initPower() {
        LogUtil.e("处理状态", state);
        if (state.equals("2")) {//处理中
            llHandlePerson.setVisibility(View.VISIBLE);//负责人显示
            rlInfoFoot.setVisibility(View.VISIBLE);//事件详情展示
            llNoAllotRoot.setVisibility(View.VISIBLE);//回访 解决按钮显示
            btAllot.setVisibility(View.INVISIBLE);//分配按钮消失
            requestVisitRecord();//在处理中的时候查询回访记录
        } else if (state.equals("9")) {//待分配
            llHandlePerson.setVisibility(View.GONE);//负责人显示
            rlInfoFoot.setVisibility(View.GONE);//事件详情展示
            btAllot.setVisibility(View.VISIBLE);//分配按钮显示
            llNoAllotRoot.setVisibility(View.GONE);//回访 解决按钮消失
            ivLine.setVisibility(View.GONE);
        } else if (state.equals("1")) {//未接受
            llHandlePerson.setVisibility(View.VISIBLE);//负责人显示
            rlInfoFoot.setVisibility(View.GONE);//事件详情展示
            llNoAllotRoot.setVisibility(View.GONE);//回访 解决按钮消失
            ivLine.setVisibility(View.GONE);
            btAllot.setVisibility(View.INVISIBLE);//分配按钮消失
        }
    }

    /**
     * 请求回访记录
     */
    private void requestVisitRecord() {
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("repair_id", repairId);
        persenter.requestVisitRecord(map);
    }

    /**
     * 请求事件详情
     */
    private void requestDetail() {
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("repair_id", repairId);
       LogUtil.e("请求id", "~~~" + repairId);
       LogUtil.e("请求id", "~~~" + EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
       LogUtil.e("请求id", "~~~" + EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        persenter.requestTaskDetail(map);
    }

    @Override
    protected OwnerEventDetailPersent oncreatPersenter() {
        return new OwnerEventDetailPersent();
    }

    @Override
    protected void initListener() {
        btVisitor.setOnClickListener(this);
        btAllot.setOnClickListener(this);
        btFinish.setOnClickListener(this);
        tvMore.setOnClickListener(this);
        ivPhoto.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_visitor:
                Intent intent = new Intent(this, EventVisitorActivity.class);
                intent.putExtra("repair_id", repairId);
                startActivityForResult(intent, 5);
                break;
            case R.id.bt_allot:
                Intent intent1 = new Intent(this, EventAssignActivity.class);
                intent1.putExtra("repair_id", repairId);
                startActivityForResult(intent1, 2);
                break;
            case R.id.bt_finish:
                showFinishDialog();
                break;
            case R.id.tv_more:
                showVisitInfoDialog();
                break;
            case R.id.iv_photo:
                if (imageUrl != null&&!imageUrl.equals("")) ShowImageActivity.start(this, imageUrl);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 5 && resultCode == 5) {
            requestVisitRecord();//更新事件记录
        }
        if (resultCode == 2) {
            setResult(6);
            finish();
        }
    }

    //查看更多信息
    private void showVisitInfoDialog() {
        if (visitModels != null && visitModels.size() != 0) {
            EventInfoDialog dialog = new EventInfoDialog(this, visitModels);
            dialog.show();
        }
    }

    //解决事件dialog
    private void showFinishDialog() {
        EventFinishDialog dialog = new EventFinishDialog(this, "事件解决记录");
        dialog.setOncommitListener(record -> finishEvent(record));
    }

    //解决事件
    private void finishEvent(String record) {
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("result_content", record);
        map.put("repair_id", repairId);
        RestClient.getAPIService().finishRepairTask(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code() == 200) {
                    setResult(6);
                    finish();
                } else {
                    Toast.makeText(EventDetailActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Toast.makeText(EventDetailActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void getAcceptResult(String result) {

    }

    //获取事件详情
    @Override
    public void getOwnerDetail(OwnerEventDetailModel model) {
        tvDate.setText(model.getRepair_date());
        tvMaster.setText(model.getLeading_person());
        if (!TextUtils.isEmpty(model.getAssistant_person()) && !model.getAssistant_person().equals("")) {
            String partner = "、" + model.getAssistant_person();
            tvMate.setText(partner);
        }
        if (model.getLeading_person()==null&&model.getAssistant_person()==null){
            tvMaster.setVisibility(View.GONE);
            tvMate.setText("暂无");
        }
        tvRoom.setText(model.getRepair_address());
        tvType.setText(model.getRepair_type_name());
        tvDetail.setText(model.getRepair_content());
        tvPhone.setText(model.getRepair_user_phone());
        tvName.setText(model.getRepair_user_name());
        tvTime.setText(model.getRepair_time());
        if (model.getRepair_image_url() != null && !model.getRepair_image_url().equals("")) {
            imageUrl = model.getRepair_image_url();
            Glide.with(this).load(model.getRepair_image_url()).error(R.mipmap.repairingbg).into(ivPhoto);
            rlPhoto.setVisibility(View.VISIBLE);
            rlPic.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void getVisitRecords(List<VisitRecordModel> visitModels) {
        VisitRecordModel recordModel = visitModels.get(0);
        if (recordModel != null) {
            tvInfoContent.setText(recordModel.getFeedback_content());
            tvInfoDate.setText(recordModel.getFeedback_date());
            tvInfoTime.setText(recordModel.getFeedback_time());
            tvInfoName.setText(recordModel.getContact_person());
            tvInfoPhone.setText(recordModel.getContact_info());
            this.visitModels = visitModels;//用于查看更多

        } else {
            rlInfoFoot.setVisibility(View.GONE);
        }
    }

    @Override
    public void getOwnerDetailErrow(String errow) {
        Toast.makeText(this, errow, Toast.LENGTH_SHORT).show();
    }

}
