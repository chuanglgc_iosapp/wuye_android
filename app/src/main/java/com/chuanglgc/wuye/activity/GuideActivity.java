package com.chuanglgc.wuye.activity;

import android.content.Intent;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.LoginModel;
import com.chuanglgc.wuye.network.Constant;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.DeviceInfoUtil;
import com.chuanglgc.wuye.utils.MD5Utils;
import com.chuanglgc.wuye.utils.SpUtils;

import java.util.HashMap;

import cn.jpush.android.api.JPushInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GuideActivity extends BaseActivity {
    @Override
    protected int getLayoutId() {
        return R.layout.activity_guide;
    }

    @Override
    protected void initView() {
        new Thread(() -> {
            try {
                Thread.sleep(1000);
                LoginModel userInfoFromCache = EmployeeApplication.getInstance().getUserInfoFromCache();
                String password = (String) SpUtils.get(this, Constant.PASSWORD, "");
                if (userInfoFromCache != null && password != null && !password.equals("")) {
                    loginUser(userInfoFromCache, password);
                } else {
                    Intent intent = new Intent(GuideActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    /**
     * 从缓存中拿到账号密码登陆
     * 成功更新本地缓存并跳转到MainActitiy  并设置极光推送的别名
     * 失败 跳转到登陆界面
     */
    private void loginUser(LoginModel userInfoFromCache, String password) {
        HashMap<String, String> map = new HashMap<>();
        map.put("phone_number", userInfoFromCache.getPhone_number());
        map.put("login_password", MD5Utils.md5(Constant.salt + password));
        String uuid = DeviceInfoUtil.getUUID();
        //获取设备厂商
        String deviceBrand = DeviceInfoUtil.getDeviceBrand();
        String userAgent = DeviceInfoUtil.getUserAgent();

        map.put("device_manufacturer", deviceBrand);
        map.put("device_uuid", uuid);
        map.put("device_platform", userAgent);
        RestClient.getAPIService().login(map).enqueue(new Callback<Result<LoginModel>>() {
            @Override
            public void onResponse(Call<Result<LoginModel>> call, Response<Result<LoginModel>> response) {
                if (response.code() == 200) {
                    Result<LoginModel> body = response.body();
                    if (body.isResult()) {
                        //登陆成功注册激光推送的别名
                        //先注册昵称，推送时候会根据这个昵称去推送到个人
                        String dName ="WY_"+DeviceInfoUtil.getUUID();
                        String userId = body.getData().getEmp_id();
                        JPushInterface.setAlias(GuideActivity.this,Integer.valueOf(userId),dName);

                        EmployeeApplication.getInstance().getCacheUtil().putSerializableObj(Constant.USER_KEY,  body.getData());
                        Intent intent = new Intent(GuideActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(GuideActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Intent intent = new Intent(GuideActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Result<LoginModel>> call, Throwable t) {
                Intent intent = new Intent(GuideActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }
}
