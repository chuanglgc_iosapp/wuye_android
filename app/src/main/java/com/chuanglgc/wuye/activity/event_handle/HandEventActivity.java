package com.chuanglgc.wuye.activity.event_handle;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.INormalView;
import com.chuanglgc.wuye.MVP.persent.HandleEventPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.HandleEventAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.CycleTaskModel;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;


public class HandEventActivity extends BaseActivity<INormalView, HandleEventPersent> implements INormalView<List<CycleTaskModel>> {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.rcl_cycle)
    RecyclerView rclCycle;
    private Dialog loadingDialog;
    private HandleEventAdapter cycleEventAdapter;
    private OnItemClickListener onItemClickListener;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_handle;
    }

    @Override
    protected void initView() {
        Toolbar toolbar = myToolbar.getToolbar();
        if (toolbar != null) {
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> {
                finish();
            });
        }
        requestTasks();

    }

    /**
     * 请求事务列表
     */
    private void requestTasks() {
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("emp_id",EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        LogUtil.e("事务处理", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id() + "SSSS"
                + EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        persenter.queryCycleTaskList(map);
        loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected HandleEventPersent oncreatPersenter() {
        return new HandleEventPersent();
    }

    /**
     * 获取数据列表并填充Rcl
     *
     * @param model 事件列表
     */
    @Override
    public void getNetResult(List<CycleTaskModel> model) {
        if (loadingDialog != null) loadingDialog.dismiss();
        if (cycleEventAdapter == null) {
            cycleEventAdapter = new HandleEventAdapter(model);
            rclCycle.setLayoutManager(new LinearLayoutManager(this));
            cycleEventAdapter.bindToRecyclerView(rclCycle);
        } else {
            cycleEventAdapter.setNewData(model);
        }

        if (onItemClickListener != null) {
            rclCycle.removeOnItemTouchListener(onItemClickListener);
        }
        onItemClickListener = new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                CycleTaskModel taskModel = model.get(position);
                LogUtil.e("事务处理",taskModel.getCycle_task_type()+"|||||"+taskModel.getCycle_task_id());
                Intent intent;
                if(taskModel.getCycle_task_type().equals("3")){
                    intent = new Intent(HandEventActivity.this, HandleEventOtherDetailActivity.class);
                }else {
                    intent = new Intent(HandEventActivity.this, HandleEventDetailActivity.class);

                }
                intent.putExtra("state", model.get(position).getCycle_task_status_id());
                intent.putExtra("tastId", model.get(position).getCycle_task_id());
                startActivityForResult(intent, 0);
            }
        };
        rclCycle.addOnItemTouchListener(onItemClickListener);
    }

    @Override
    public void getNetFauiler(String wrong) {
        if (loadingDialog != null) loadingDialog.dismiss();
        getNetResult(new ArrayList<CycleTaskModel>());
        LogUtil.e("事务处理", wrong);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1) {
            LogUtil.e("更新", "ssss");
            requestTasks();//更新数据
        }
    }
}
