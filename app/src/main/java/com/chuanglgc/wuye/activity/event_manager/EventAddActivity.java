package com.chuanglgc.wuye.activity.event_manager;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.IOwnerSearchView;
import com.chuanglgc.wuye.MVP.persent.OwnerSearchPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.dialog.SpinnerDialog;
import com.chuanglgc.wuye.model.BuildModel;
import com.chuanglgc.wuye.model.RepairPicModel;
import com.chuanglgc.wuye.model.RepairTypeModel;
import com.chuanglgc.wuye.model.RoomModel;
import com.chuanglgc.wuye.model.UnitModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.IsRightOrNotNumber;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.AddPhotoView;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;
import com.chuanglgc.wuye.widget.photoview.ShowImageActivity;
import com.linchaolong.android.imagepicker.ImagePicker;
import com.linchaolong.android.imagepicker.cropper.CropImage;
import com.linchaolong.android.imagepicker.cropper.CropImageView;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EventAddActivity extends BaseActivity<IOwnerSearchView, OwnerSearchPersent> implements View.OnClickListener, IOwnerSearchView {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.tv_dong)
    TextView tvDong;
    @BindView(R.id.tv_danyuan)
    TextView tvDanyuan;
    @BindView(R.id.tv_room)
    TextView tvRoom;
    @BindView(R.id.et_person)
    EditText etPerson;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.et_info)
    EditText etInfo;
    @BindView(R.id.bt_commit)
    MyPhotoBtton btCommit;
    @BindView(R.id.addPhoto)
    AddPhotoView addPhoto;
    private List<BuildModel> buildModelList;
    private List<UnitModel> unitModelList;
    private List<RoomModel> roomModelList;
    private List<RepairTypeModel> repairTypeModels;
    private int buildPosition = -1;
    private int unitPosition = -1;
    private int roomPosition = -1;
    private int repairTypeId = -1;
    private Dialog loadingDialog;
    private ImagePicker imagePicker;
    private RepairPicModel picModel;
    /**
     * 如果报修类型为为其他报修,不用填写栋单元门牌号,不是则必填
     */
    private String otherRepairType = null;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_add;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = myToolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
    }

    @Override
    protected void initListener() {
        tvDong.setOnClickListener(this);
        tvDanyuan.setOnClickListener(this);
        tvRoom.setOnClickListener(this);
        tvType.setOnClickListener(this);
        btCommit.setOnClickListener(this);
        addPhoto.setOnClickListener(this);

    }

    @Override
    protected void initData() {

    }

    @Override
    protected OwnerSearchPersent oncreatPersenter() {
        return new OwnerSearchPersent();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_dong:
                queryBuild();
                break;
            case R.id.tv_danyuan:
                queryUnit();
                break;
            case R.id.tv_room:
                queryRoom();
                break;
            case R.id.tv_type:
                queryEventType();
                break;
            case R.id.bt_commit:
                addEvent();
                break;
            case R.id.addPhoto:
                if (addPhoto.getIsHaveAddPhoto()) {
                    ShowImageActivity.start(this, picModel.getHttp_host() + picModel.getImage_url());
                } else {
                    addPhoto();
                }
                break;
        }
    }

    private void addPhoto() {
        if (imagePicker == null) photoManager();
        // 启动图片选择器
        imagePicker.startChooser(this, new ImagePicker.Callback() {
            // 选择图片回调
            @Override
            public void onPickImage(Uri imageUri) {}

            // 裁剪图片回调
            @Override
            public void onCropImage(Uri imageUri) {
                //向服务器上传图片
                File file = new File(URI.create(String.valueOf(imageUri)));
                uploadPic(file);
            }

            // 自定义裁剪配置
            @Override
            public void cropConfig(CropImage.ActivityBuilder
                                           builder) {
                // 是否启动多点触摸
                builder.setMultiTouchEnabled(true)
                        // 设置网格显示模式
                        .setGuidelines(CropImageView.Guidelines.ON)
                        // 圆形/矩形
                        .setCropShape(CropImageView.CropShape
                                .RECTANGLE)
                        // 调整裁剪后的图片最终大小
                        .setRequestedSize(960, 540)
                        // 宽高比
                        .setAspectRatio(1, 1);
            }

            // 用户拒绝授权回调
            @Override
            public void onPermissionDenied(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
            }
        });
    }

    /**
     * 上传图片
     *
     */
    private void uploadPic(File file) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpeg"), file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("photo", file.getName(), requestBody);
        RestClient.getAPIService().uploadRepairImage(EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id(), part)
                .enqueue(new Callback<Result<RepairPicModel>>() {
                    @Override
                    public void onResponse(Call<Result<RepairPicModel>> call, Response<Result<RepairPicModel>> response) {
                        if (response.code() == 200) {
                            if (response.body().isResult()) {
                                picModel = response.body().getData();
                                addPhoto.setPhoto(picModel.getHttp_host() + picModel.getImage_url());
                                if (file.exists()) file.delete();
                            }
                        } else {
                            Toast.makeText(EventAddActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<Result<RepairPicModel>> call, Throwable t) {
                        Toast.makeText(EventAddActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void photoManager() {
        imagePicker = new ImagePicker();
        // 设置标题
        imagePicker.setTitle("选择图片");
        // 设置是否裁剪图片
        imagePicker.setCropImage(true);
    }

    //添加事件
    private void addEvent() {
        String info = etInfo.getText().toString().trim();
        String person = etPerson.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();
        HashMap<String, String> map = new HashMap<>();
        if (TextUtils.isEmpty(person)) {
            Toast.makeText(EventAddActivity.this, "请输入联系人", Toast.LENGTH_SHORT).show();
            return;
        } else if (TextUtils.isEmpty(phone) || !IsRightOrNotNumber.isMobile(phone)) {
            Toast.makeText(EventAddActivity.this, "请输入正确联系电话号码", Toast.LENGTH_SHORT).show();
            return;
        } else if (repairTypeId == -1) {
            Toast.makeText(EventAddActivity.this, "请选择事件类型", Toast.LENGTH_SHORT).show();
            return;
        } else if (TextUtils.isEmpty(info)) {
            Toast.makeText(EventAddActivity.this, "请输入事件描述", Toast.LENGTH_SHORT).show();
            return;
        } else if (otherRepairType != null && !otherRepairType.equals("其他报修")) {
            if (roomPosition == -1) {
                Toast.makeText(EventAddActivity.this, "请选择详细地址", Toast.LENGTH_SHORT).show();
                return;
            } else {
                map.put("build_id", buildModelList.get(buildPosition).getBuild_id());
                map.put("unit_id", unitModelList.get(unitPosition).getUnit_id());
                map.put("room_id", roomModelList.get(roomPosition).getRoom_id());
            }
        }
        loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("contact_person", person);
        map.put("contact_phone", phone);
        map.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        map.put("repair_content", info);
        map.put("repair_type_id", repairTypeModels.get(repairTypeId).getRepair_type_id());
        if (picModel != null) {
            map.put("image_url", picModel.getImage_url());
        } else {
            map.put("image_url", "");
        }
        RestClient.getAPIService().addRepair(map).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (response.code() == 200) {
                    Result body = response.body();
                    if (body.isResult()) {
                        Toast.makeText(EventAddActivity.this, "添加成功", Toast.LENGTH_SHORT).show();
                        setResult(5);
                        finish();
                    } else {
                        Toast.makeText(EventAddActivity.this, body.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(EventAddActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                }
                loadingDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Toast.makeText(EventAddActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
                loadingDialog.dismiss();
            }
        });


    }

    //查询门牌号
    private void queryRoom() {
        if (buildModelList == null || buildModelList.size() == 0) {
            Toast.makeText(this, "请选择栋数", Toast.LENGTH_SHORT).show();
        } else if (unitModelList == null || unitModelList.size() == 0) {
            Toast.makeText(this, "请选择单元", Toast.LENGTH_SHORT).show();
        } else {
            HashMap<String, String> doorMap = new HashMap<>();
            doorMap.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            doorMap.put("build_id", buildModelList.get(buildPosition).getBuild_id());
            doorMap.put("unit_id", unitModelList.get(unitPosition).getUnit_id());
            persenter.QueryRoom(doorMap);
            if (loadingDialog == null) {
                loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
            } else {
                loadingDialog.show();
            }
        }
    }

    //查询单元
    private void queryUnit() {
        if (buildModelList == null || buildModelList.size() == 0) {
            Toast.makeText(this, "请选择栋数", Toast.LENGTH_SHORT).show();
        } else {
            Map<String, String> mapUnit = new HashMap<>();
            mapUnit.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            mapUnit.put("build_id", buildModelList.get(buildPosition).getBuild_id());
            persenter.QueryUnit(mapUnit);
            if (loadingDialog == null) {
                loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
            } else {
                loadingDialog.show();
            }
        }
    }

    /**
     * 查询栋
     */
    private void queryBuild() {
        loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        Map<String, String> mapBuild = new HashMap<>();
        mapBuild.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        persenter.QueryBuild(mapBuild);
    }

    //查询事件类型
    private void queryEventType() {
        loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        persenter.queryRepairType(EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
    }


    //获取栋的信息
    @Override
    public void getBuildList(List<BuildModel> buildModelList) {
        if (loadingDialog != null) loadingDialog.dismiss();

        ArrayList<String> listBuild = new ArrayList<>();
        for (BuildModel model : buildModelList) {
            listBuild.add(model.getBuild_name());
        }
        SpinnerDialog spinnerDialog = new SpinnerDialog(this, listBuild);
        spinnerDialog.setOnSpinnerItemClick((itemName, position) -> {
            spinnerDialog.dismiss();
            this.buildModelList = buildModelList;
            this.buildPosition = position;
            tvDong.setText(itemName);
            tvDanyuan.setText("");
            tvRoom.setText("");
        });
        spinnerDialog.show();
    }

    //获取单元
    @Override
    public void getUnitList(List<UnitModel> unitModelList) {
        if (loadingDialog != null) loadingDialog.dismiss();
        LogUtil.e("获取单元", unitModelList.size());
        ArrayList<String> listUnit = new ArrayList<>();
        for (UnitModel model : unitModelList) {
            listUnit.add(model.getUnit_name());
        }

        SpinnerDialog spinnerDialog = new SpinnerDialog(this, listUnit);
        spinnerDialog.setOnSpinnerItemClick((itemName, position) -> {
            spinnerDialog.dismiss();
            this.unitModelList = unitModelList;
            this.unitPosition = position;
            tvDanyuan.setText(itemName);
            tvRoom.setText("");
        });
        spinnerDialog.show();
    }

    //获取房间
    @Override
    public void getRoomList(List<RoomModel> roomModelList) {
        if (loadingDialog != null) loadingDialog.dismiss();

        ArrayList<String> listRoom = new ArrayList<>();
        for (RoomModel model : roomModelList) {
            listRoom.add(model.getRoom_name());
        }

        SpinnerDialog spinnerDialog = new SpinnerDialog(this, listRoom);
        spinnerDialog.setOnSpinnerItemClick((itemName, position) -> {
            spinnerDialog.dismiss();
            this.roomModelList = roomModelList;
            this.roomPosition = position;
            tvRoom.setText(itemName);
        });
        spinnerDialog.show();
    }

    //获取事件类型
    @Override
    public void getRepairTypeList(List<RepairTypeModel> repairTypeModels) {
        if (loadingDialog != null) loadingDialog.dismiss();

        ArrayList<String> listType = new ArrayList<>();
        for (RepairTypeModel model : repairTypeModels) {
            listType.add(model.getRepair_type_name());
        }
        SpinnerDialog spinnerDialog = new SpinnerDialog(this, listType);
        spinnerDialog.setOnSpinnerItemClick((itemName, position) -> {
            repairTypeId = position;
            otherRepairType = listType.get(repairTypeId);
            this.repairTypeModels = repairTypeModels;
            spinnerDialog.dismiss();
            tvType.setText(itemName);
        });
        spinnerDialog.show();

    }


    //获取网络数据失败
    @Override
    public void getNetInfoError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        if (loadingDialog != null) loadingDialog.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imagePicker.onActivityResult(this, requestCode, resultCode, data);
    }
}
