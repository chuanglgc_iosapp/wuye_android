package com.chuanglgc.wuye.activity.event_cycle;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.INormalView;
import com.chuanglgc.wuye.MVP.persent.CycleEventDetailPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.HandleFloorNumberAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.model.CycleEventDetailModel;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.widget.MyToolbar;
import com.zhy.android.percent.support.PercentRelativeLayout;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CycleEventDetailActivity extends BaseActivity<INormalView, CycleEventDetailPersent> implements INormalView<CycleEventDetailModel> {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.rl_time)
    PercentRelativeLayout rlTime;
    @BindView(R.id.tv_device_name)
    TextView tvDeviceName;

    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.rcl_number)
    RecyclerView rclNumber;
    @BindView(R.id.tv_device_content)
    TextView tvDeviceContent;
    @BindView(R.id.tv_device_type)
    TextView tvDeviceType;
    private Dialog loadingDialog;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_cycle_detail;
    }

    @Override
    protected void initView() {
        Toolbar toolbar = myToolbar.getToolbar();
        if (toolbar != null) {
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> {
                finish();
            });
        }
        requestDetailInfo();

    }

    /**
     * 请求周期详情
     */
    private void requestDetailInfo() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
        String date = getIntent().getStringExtra("date");
        tvDate.setText(date);
        String eventId = getIntent().getStringExtra("event_id");
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("cycle_task_id", eventId);
        persenter.requestDetail(map);
    }


    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected CycleEventDetailPersent oncreatPersenter() {
        return new CycleEventDetailPersent();
    }


    @Override
    public void getNetResult(CycleEventDetailModel model) {
        tvDeviceName.setText(model.getCycle_task_name());
        tvDeviceContent.setText(model.getCycle_task_content());
        tvType.setText(model.getBody_type());
        tvDeviceType.setText(model.getDevice_type());
        String device_data = model.getDevice_data();
        String[] device = device_data.split(",");
        List<String> deviceList = Arrays.asList(device);
        HandleFloorNumberAdapter adapter = new HandleFloorNumberAdapter(deviceList, true);
        rclNumber.setLayoutManager(new GridLayoutManager(this, 4));
        adapter.bindToRecyclerView(rclNumber);
        loadingDialog.dismiss();
    }


    @Override
    public void getNetFauiler(String wrong) {
        loadingDialog.dismiss();
    }


}
