package com.chuanglgc.wuye.activity.event_near;

import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.widget.MyToolbar;

import butterknife.BindView;


public class NearEventActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.card_daily)
    CardView cardDaily;
    @BindView(R.id.card_big)
    CardView cardBig;
    @BindView(R.id.card_other)
    CardView cardOther;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_event_near;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = myToolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });

    }


    @Override
    protected void initData() {

    }

    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }

    @Override
    protected void initListener() {
        cardDaily.setOnClickListener(this);
        cardBig.setOnClickListener(this);
        cardOther.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.card_daily:
                startActivity(NearDailyActivity.class);
                break;
            case R.id.card_big:
                startActivity(NearEventBigActivity.class);
                break;
            case R.id.card_other:
                startActivity(NearEventOtherActivity.class);
                break;
        }
    }
}
