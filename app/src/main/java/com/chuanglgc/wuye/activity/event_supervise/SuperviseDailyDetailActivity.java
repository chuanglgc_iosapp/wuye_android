package com.chuanglgc.wuye.activity.event_supervise;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.ISuperviseDailyView;
import com.chuanglgc.wuye.MVP.persent.SuserviseDailyPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.event.EventPhotoAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.dialog.EvaluateDialog;
import com.chuanglgc.wuye.model.SuperviseDailyDetailModel;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;
import com.zhy.android.percent.support.PercentLinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SuperviseDailyDetailActivity extends BaseActivity<ISuperviseDailyView, SuserviseDailyPersent> implements ISuperviseDailyView {
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_number)
    TextView tvNumber;
    @BindView(R.id.tv_area)
    TextView tvArea;
    @BindView(R.id.ll_pic_root)
    PercentLinearLayout llPicRoot;
    @BindView(R.id.bt_photo)
    MyPhotoBtton btPhoto;
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.iv_line)
    ImageView ivLine;
    @BindView(R.id.ll_remark_parent)
    LinearLayout llRemarkParent;
    @BindView(R.id.ll_remark_root)
    PercentLinearLayout llRemarkRoot;
    @BindView(R.id.rlc_root)
    LinearLayout rclRoot;
    @BindView(R.id.tv_ask)
    TextView tvAsk;
    @BindView(R.id.ll_ask_root)
    PercentLinearLayout llAskRoot;
    @BindView(R.id.tv_device_number)
    TextView tvDeviceNumber;
    @BindView(R.id.ll_device_number)
    PercentLinearLayout llDeviceNumber;
    private String taskId;
    private Dialog loadingDialog;

    @Override
    protected int getLayoutId() {
        return R.layout.activit_supervise_daily_detail;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = myToolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        String state = getIntent().getStringExtra("state");
        taskId = getIntent().getStringExtra("task_id");

        String work_status = "";
        switch (state) {
            case "处理中":
                myToolbar.setTvState("处理中...", getResources().getColor(R.color.bluelight));
                btPhoto.setVisibility(View.INVISIBLE);
                llPicRoot.setVisibility(View.INVISIBLE);
                ivLine.setVisibility(View.GONE);
                work_status = "2";
                break;
            case "未接受":
                myToolbar.setTvState("未接受...", getResources().getColor(R.color.colorAccent));
                btPhoto.setVisibility(View.INVISIBLE);
                llPicRoot.setVisibility(View.INVISIBLE);
                ivLine.setVisibility(View.GONE);
                work_status = "1";
                break;
            case "待评价":
                myToolbar.setTvState("待评价...", getResources().getColor(R.color.yellow));
                btPhoto.setVisibility(View.VISIBLE);
                btPhoto.setIcon(R.mipmap.start);
                llPicRoot.setVisibility(View.VISIBLE);
                work_status = "3";
                break;
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("daily_task_id", taskId);
        map.put("work_status", work_status);
        LogUtil.e("每日工作详情", state + taskId);
        showDialog();
        persenter.requestDailyDetail(map);

    }
      void  showDialog(){
          if (loadingDialog==null){
              loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
          }else {
              loadingDialog.show();
          }
      }
    //动态添加RecyclerView 图片列表 有的工作会需要多次完成 每次完成都会有备注 和图片 如果有则显示
    private void addRclPicView(ArrayList<String> photos) {
        RecyclerView rcl = new RecyclerView(this);
        LinearLayout.LayoutParams pa = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, DisplayUtil.dp2px(this, 60));
        pa.leftMargin = DisplayUtil.dp2px(this, 15);
        int childCount = rclRoot.getChildCount();
        if (childCount > 0) {//如果不是第一个 添加topMargin
            pa.topMargin = DisplayUtil.dp2px(this, 10);
        }
        rcl.setLayoutParams(pa);
        rclRoot.addView(rcl);


        initRclPic(rcl, photos);
    }

    //动态添加TextView  完成备注 有的工作会需要多次完成
    private void addRemarkView(String remark) {
        TextView tv = new TextView(this);
        LinearLayout.LayoutParams pa = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pa.leftMargin = DisplayUtil.dp2px(this, 15);
        tv.setTextColor(getResources().getColor(R.color.black));
        tv.setLayoutParams(pa);
        tv.setTextSize(18);
        tv.setText(remark);
        llRemarkParent.addView(tv);
    }

    private void initRclPic(RecyclerView rcl, ArrayList<String> listPhoto) {
        rcl.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        EventPhotoAdapter photoAdapter = new EventPhotoAdapter(listPhoto);
        photoAdapter.bindToRecyclerView(rcl);
    }

    @Override
    protected void initListener() {
        btPhoto.setOnClickListener(v -> {
            showEvaluateDialog();
        });

    }

    private void showEvaluateDialog() {
        EvaluateDialog evaluateDialog = new EvaluateDialog(this);
        evaluateDialog.setOnCommitEvluateListener((evluate, stars) -> {
            evalueTaks(evluate, stars);
        });
    }

    //评价每日工作
    private void evalueTaks(String evluate, int stars) {
        showDialog();
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("daily_task_id", taskId);
        map.put("task_type", "1");
        map.put("star_quantity", stars + "");
        map.put("evaluate_content", evluate);
        persenter.requestEvalue(map);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected SuserviseDailyPersent oncreatPersenter() {
        return new SuserviseDailyPersent();
    }


    @Override
    public void getDailyDetilModel(SuperviseDailyDetailModel model) {
        tvName.setText(model.getLeading_person());
        tvType.setText(model.getDaily_task_name());
        tvArea.setText(model.getTask_area());
        tvTime.setText(model.getSchedule_name());

        if (model.getTask_test() != 0) {
            tvNumber.setText("共" + model.getTask_total() + "次" + " / 剩" + model.getTask_test() + "次");
        } else {
            tvNumber.setText("共" + model.getTask_total() + "次");
        }

        //判断分配要求 如果没有 则Gone
        if (model.getMessage() != null && !model.getMessage().equals("")) {
            tvAsk.setText(model.getMessage());
        } else {
            llAskRoot.setVisibility(View.GONE);
        }
         //设置仪表读数
        if (model.getReading()!=null&&!model.getReading().equals("")){
            llDeviceNumber.setVisibility(View.VISIBLE);
            tvDeviceNumber.setText(model.getReading());
        }
        //解析数据
        ArrayList<String> listRemark = new ArrayList<>();
        ArrayList<ArrayList<String>> rclList = new ArrayList<>();
        List<SuperviseDailyDetailModel.EveryBean> infoBean = model.getEvery();
        for (SuperviseDailyDetailModel.EveryBean bean :
                infoBean) {
            ArrayList<String> listPhoto = new ArrayList<>();
            if (bean.getInfo() != null && !bean.getInfo().equals("")) {
                if (bean.getNumber() != null && !bean.getNumber().equals("")) {
                    listRemark.add(bean.getNumber() + bean.getInfo());
                } else {
                    listRemark.add(bean.getInfo());
                }
            }
            if (bean.getTask_image1() != null && !bean.getTask_image1().equals("")) {
                listPhoto.add(bean.getTask_image1());
            }
            if (bean.getTask_image2() != null && !bean.getTask_image2().equals("")) {
                listPhoto.add(bean.getTask_image2());
            }
            if (listPhoto.size() > 0) {
                rclList.add(listPhoto);
            }

        }


        //动态添加TextView 并设置备注
        if (listRemark.size() != 0) {
            for (String remark : listRemark) {
                addRemarkView(remark);
            }
        } else {//如果没有备注则Gone
            llRemarkRoot.setVisibility(View.GONE);
        }
        //动态添加RecyclerView 并设置Adapter
        if (rclList.size() != 0) {
            for (ArrayList<String> photos :
                    rclList) {
                addRclPicView(photos);
            }
        } else {//如果没有数据则隐藏
            llPicRoot.setVisibility(View.INVISIBLE);
            ivLine.setVisibility(View.GONE);
        }
        if (loadingDialog!=null)loadingDialog.dismiss();
    }

    @Override
    public void getEvalueResult(String result) {
        if (loadingDialog!=null)loadingDialog.dismiss();
        setResult(1);
        finish();
    }

    @Override
    public void getNetError(String error) {
        if (loadingDialog!=null)loadingDialog.dismiss();
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }


}
