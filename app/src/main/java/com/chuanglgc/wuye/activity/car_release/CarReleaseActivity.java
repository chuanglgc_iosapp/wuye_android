package com.chuanglgc.wuye.activity.car_release;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.CarReleaseAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.dialog.CarReleaseSearchDialog;
import com.chuanglgc.wuye.model.CarVisitorModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.utils.LoadingBelowSearchUtils;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CarReleaseActivity extends BaseActivity {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.ll_search_goods)
    RelativeLayout llSearchGoods;
    @BindView(R.id.rcy_car)
    RecyclerView rcyCar;
    @BindView(R.id.bt_record)
    TextView btRecord;
    private Dialog loadingDialog;
    private OnItemClickListener onItemClickListener;
    private CarReleaseAdapter carReleaseAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_car_release;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = myToolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        requestReleaseCar("");

    }

    private void showLoading() {
        if (loadingDialog == null) {
            loadingDialog = LoadingBelowSearchUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
    }

    private void requestReleaseCar(String carNumber) {
        showLoading();
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("plate_number", carNumber);
        RestClient.getAPIService().queryVisitorEntry(map).enqueue(new Callback<Result<List<CarVisitorModel>>>() {
            @Override
            public void onResponse(Call<Result<List<CarVisitorModel>>> call, Response<Result<List<CarVisitorModel>>> response) {
                if (response.code() == 200) {
                    Result<List<CarVisitorModel>> body = response.body();
                    if (body.isResult()) {
                        initCarRcl(body.getData());
                    } else {
                        rcyCar.setVisibility(View.INVISIBLE);
                        Toast.makeText(CarReleaseActivity.this, body.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    rcyCar.setVisibility(View.INVISIBLE);
                    Toast.makeText(CarReleaseActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                }
                loadingDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Result<List<CarVisitorModel>>> call, Throwable t) {
                rcyCar.setVisibility(View.INVISIBLE);
                loadingDialog.dismiss();
                Toast.makeText(CarReleaseActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initCarRcl(List<CarVisitorModel> data) {
        rcyCar.setVisibility(View.VISIBLE);
        if (carReleaseAdapter == null) {
            rcyCar.setLayoutManager(new LinearLayoutManager(this));
            carReleaseAdapter = new CarReleaseAdapter(data);
            View view = new View(this);//避免与按钮重叠
            view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, DisplayUtil.dp2px(this, 100)));
            carReleaseAdapter.addFooterView(view);
            rcyCar.setAdapter(carReleaseAdapter);
        } else {
            carReleaseAdapter.setNewData(data);
        }
        if (onItemClickListener != null) rcyCar.removeOnItemTouchListener(onItemClickListener);
        onItemClickListener = new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(CarReleaseActivity.this, CarVisitorReleaseActivity.class);
                intent.putExtra("model", data.get(position));
                startActivityForResult(intent, 2);
            }
        };

        rcyCar.addOnItemTouchListener(onItemClickListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2 & resultCode == 8) {//回复为8 车辆放行刷新列表
            requestReleaseCar("");
        }
        if (requestCode == 2 && resultCode == 5) {//回复为5，访客记录 删除本地车牌号照片
            requestReleaseCar("");
            String carPicPath = data.getStringExtra("carPic");
            File file = new File(carPicPath);
            if (file.exists()) file.delete();

        }

    }

    @Override
    protected void initListener() {
        llSearchGoods.setOnClickListener(v -> {
            CarReleaseSearchDialog searchDialog = new CarReleaseSearchDialog(this);
            searchDialog.setOnSearchClickListener(cardNumber -> {
                requestReleaseCar(cardNumber);
            });
        });

        btRecord.setOnClickListener(v -> {
            Intent intent = new Intent(this, CarVisitorRecordActivity.class);
            startActivityForResult(intent, 2);
        });

    }

    @Override
    protected void initData() {

    }

    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }


}
