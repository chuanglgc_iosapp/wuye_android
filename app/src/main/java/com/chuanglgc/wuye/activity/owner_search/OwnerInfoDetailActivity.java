package com.chuanglgc.wuye.activity.owner_search;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.CarNumberAdapter;
import com.chuanglgc.wuye.adapter.CarParkAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.OwnerInfoModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyToolbar;
import com.zhy.android.percent.support.PercentRelativeLayout;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OwnerInfoDetailActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    MyToolbar toolbar;
    @BindView(R.id.rl_time)
    PercentRelativeLayout rlTime;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_sex)
    TextView tvSex;
    @BindView(R.id.tv_bottom_room)
    TextView tvBottomRoom;
    @BindView(R.id.tv_room_info)
    TextView tvRoomInfo;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.rcl_carPark)
    RecyclerView rclCarPark;
    @BindView(R.id.rcl_car_number)
    RecyclerView rclCarNumber;
    @BindView(R.id.tv_location)
    TextView tvLocation;
    @BindView(R.id.tv_date_info)
    TextView tvDateInfo;
    @BindView(R.id.rl_line_one)
    RelativeLayout rlLineOne;
    @BindView(R.id.iv_line_two)
    ImageView ivLineTwo;
    @BindView(R.id.rl_root_rcl)
    PercentRelativeLayout rlRootRcl;
    private String buildId;
    private String unitId;
    private String roomId;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_owner_info_detail;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = toolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        Intent intent = getIntent();
        buildId = intent.getStringExtra("build_id");
        unitId = intent.getStringExtra("unit_id");
        roomId = intent.getStringExtra("room_id");

    }

    @Override
    protected void initData() {
        Dialog loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("build_id", buildId);
        map.put("unit_id", unitId);
        if (roomId.contains("室")) {
            String room = roomId.substring(0, roomId.indexOf("室"));
            map.put("room_id", room);
        } else {
            map.put("room_id", roomId);
        }

        RestClient.getAPIService().queryHouseholder(map).enqueue(new Callback<Result<OwnerInfoModel>>() {
            @Override
            public void onResponse(Call<Result<OwnerInfoModel>> call, Response<Result<OwnerInfoModel>> response) {
                if (response.code() == 200) {
                    Result<OwnerInfoModel> result = response.body();
                    if (result.isResult()) {
                        initInfo(result);
                    }
                }
                loadingDialog.dismiss();

            }

            @Override
            public void onFailure(Call<Result<OwnerInfoModel>> call, Throwable t) {
                loadingDialog.dismiss();
            }
        });
    }

    //初始化信息
    private void initInfo(Result<OwnerInfoModel> result) {
        OwnerInfoModel infoModel = result.getData();
        OwnerInfoModel.HouseholderBean householder = infoModel.getHouseholder();
        tvName.setText(householder.getHouseholder_name());
        String sex = householder.getHouseholder_gender();
        LogUtil.e("用户性别",sex);
        if (sex==null||sex.equals("")){
            tvSex.setText("");
        }else {
            tvSex.setText("("+sex+")");
        }
        if (householder.getWarehouse() == null || householder.getWarehouse().equals("")) {
            tvBottomRoom.setText("无");
        } else {
            tvBottomRoom.setText(householder.getWarehouse());
        }

        tvTime.setText(householder.getPayment_date());
        tvDateInfo.setText(householder.getPayment_date_remark());
        tvRoomInfo.setText(householder.getWarehouse_remark());
        tvPhone.setText(householder.getHouseholder_phone());
        tvLocation.setText(infoModel.getAddress());
        if (infoModel.getPark_number().size() == 0) {
            ivLineTwo.setVisibility(View.GONE);
        }
        if (infoModel.getPark_number().size() == 0 && infoModel.getPlate_number().size() == 0) {
            rlRootRcl.setVisibility(View.GONE);
            rlLineOne.setVisibility(View.GONE);
        }
        if (infoModel.getPlate_number().size() == 0) {
            ivLineTwo.setVisibility(View.GONE);
        }
        initRclCarPark(infoModel.getPark_number());
        initRclCarNumber(infoModel.getPlate_number());
    }

    //初始化车位
    private void initRclCarPark(List<OwnerInfoModel.ParkNumberBean> parkList) {
        if (parkList.size() > 2) {
            PercentRelativeLayout.LayoutParams layoutParams = (PercentRelativeLayout.LayoutParams) rclCarPark.getLayoutParams();
            layoutParams.height = DisplayUtil.dp2px(this, 200);
            rclCarPark.setLayoutParams(layoutParams);
        }
        rclCarPark.setLayoutManager(new GridLayoutManager(this, 2));
        CarParkAdapter carAdapter = new CarParkAdapter(parkList);
        rclCarPark.setAdapter(carAdapter);

    }

    private void initRclCarNumber(List<String> numberList) {
        rclCarNumber.setLayoutManager(new GridLayoutManager(this, 2));
        CarNumberAdapter numberAdapter = new CarNumberAdapter(numberList);
        numberAdapter.bindToRecyclerView(rclCarNumber);
    }

    @Override
    protected void initListener() {

    }


    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
