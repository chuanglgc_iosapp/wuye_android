package com.chuanglgc.wuye.activity.search_car;


import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.CarNumberAdapter;
import com.chuanglgc.wuye.adapter.CarParkAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchCarResultActivity extends BaseActivity {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.tv_number)
    TextView tvNumber;
    @BindView(R.id.rcl_carPark)
    RecyclerView rclCarPark;
    @BindView(R.id.rcl_car_number)
    RecyclerView rclCarNumber;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_search_car_result;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = myToolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        initRclCarPark();
        initRclCarNumber();
    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }


    private void initRclCarPark() {
        rclCarPark.setLayoutManager(new GridLayoutManager(this, 2));
        ArrayList<String> list = new ArrayList<>();
        list.add(null);
        list.add(null);
        list.add(null);
        CarParkAdapter carAdapter = new CarParkAdapter(null);
        rclCarPark.setAdapter(carAdapter);

    }

    private void initRclCarNumber() {
        rclCarNumber.setLayoutManager(new GridLayoutManager(this, 2));
        ArrayList<String> list = new ArrayList<>();
        list.add(null);
        list.add(null);
        list.add(null);
        CarNumberAdapter numberAdapter = new CarNumberAdapter(null);
        numberAdapter.bindToRecyclerView(rclCarNumber);
    }
}
