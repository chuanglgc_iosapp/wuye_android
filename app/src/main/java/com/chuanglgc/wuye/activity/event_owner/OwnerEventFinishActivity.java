package com.chuanglgc.wuye.activity.event_owner;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.IOwnerEventFinishView;
import com.chuanglgc.wuye.MVP.persent.OwnerEventFinishPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.MssageAdapter;
import com.chuanglgc.wuye.adapter.event.EventPhotoAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.PhotoModel;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;
import com.linchaolong.android.imagepicker.ImagePicker;
import com.linchaolong.android.imagepicker.cropper.CropImage;
import com.linchaolong.android.imagepicker.cropper.CropImageView;
import com.zhy.android.percent.support.PercentLinearLayout;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class OwnerEventFinishActivity extends BaseActivity<IOwnerEventFinishView, OwnerEventFinishPersent> implements View.OnClickListener
        , IOwnerEventFinishView {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.et_record)
    EditText etRecord;
    @BindView(R.id.rcl_pic)
    RecyclerView rclPic;
    @BindView(R.id.iv_addpic)
    ImageView ivAddpic;
    @BindView(R.id.rl_add)
    RelativeLayout rlAdd;
    @BindView(R.id.bt_commit)
    MyPhotoBtton btCommit;
    private ImagePicker imagePicker;
    private ArrayList<String> listPhoto;
    private EventPhotoAdapter ivAdapter;
    private String taskId;
    private String repairId;
    private String image_url = "";

    @Override
    protected int getLayoutId() {
        return R.layout.activity_owner_event_finish;
    }

    @Override
    protected void initView() {
        Toolbar toolbar = myToolbar.getToolbar();
        if (toolbar != null) {
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> {
                finish();
            });
        }
        Intent intent = getIntent();
        taskId = intent.getStringExtra("task_id");
        repairId = intent.getStringExtra("repair_id");
        LogUtil.e("参数ID", taskId + "|||||" + repairId);
        initRclPhoto();
        photoManager();

    }

    private void initRclPhoto() {
        rclPic.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        listPhoto = new ArrayList<>();
        ivAdapter = new EventPhotoAdapter(listPhoto);
        ivAdapter.bindToRecyclerView(rclPic);
    }

    private void photoManager() {
        imagePicker = new ImagePicker();
        // 设置标题
        imagePicker.setTitle("选择图片");
        // 设置是否裁剪图片
        imagePicker.setCropImage(true);
    }


    @Override
    protected void initData() {

    }

    @Override
    protected OwnerEventFinishPersent oncreatPersenter() {
        return new OwnerEventFinishPersent();
    }

    @Override
    protected void initListener() {
        rlAdd.setOnClickListener(this);
        btCommit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_add:
                openPhoto();
                break;
            case R.id.bt_commit:
                commitEvent();
                break;
        }
    }

    //提交事件解决
    private void commitEvent() {
        String record = etRecord.getText().toString().trim();
        if (TextUtils.isEmpty(record)) {
            Toast.makeText(this, "请先填写事件解决记录", Toast.LENGTH_SHORT).show();
        } else {
            HashMap<String, String> map = new HashMap<>();
            map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            map.put("task_id", taskId);
            map.put("result_content", record);
            map.put("repair_id", repairId);
            map.put("image_url_1", image_url);
            persenter.finishOwnerEvent(map);
        }

    }

    private void openPhoto() {
        // 启动图片选择器
        imagePicker.startChooser(this, new ImagePicker.Callback() {
            // 选择图片回调
            @Override
            public void onPickImage(Uri imageUri) {
            }

            // 裁剪图片回调
            @Override
            public void onCropImage(Uri imageUri) {
                listPhoto.add(String.valueOf(imageUri));
                if (listPhoto.size() >= 2) rlAdd.setVisibility(View.GONE);
                ivAdapter.notifyDataSetChanged();
                PercentLinearLayout.LayoutParams layoutParams = (PercentLinearLayout.LayoutParams) rlAdd.getLayoutParams();
                layoutParams.setMargins(DisplayUtil.dp2px(OwnerEventFinishActivity.this, 12), 0, 0, 0);
                rlAdd.setLayoutParams(layoutParams);
                //上传图片
                File file = new File(URI.create(String.valueOf(imageUri)));
                RequestBody requestBody = RequestBody.create(MediaType.parse("image/png"), file);
                MultipartBody.Part part = MultipartBody.Part.createFormData("photo", file.getName(), requestBody);

                persenter.upLoadPhoto(EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id()
                        , repairId, part);
            }

            // 自定义裁剪配置
            @Override
            public void cropConfig(CropImage.ActivityBuilder
                                           builder) {
                // 是否启动多点触摸
                builder.setMultiTouchEnabled(false)
                        // 设置网格显示模式
                        .setGuidelines(CropImageView.Guidelines.ON)
                        // 圆形/矩形
                        .setCropShape(CropImageView.CropShape
                                .RECTANGLE)
                        // 调整裁剪后的图片最终大小
                        .setRequestedSize(960, 540)
                        // 宽高比
                        .setAspectRatio(16, 9);
            }

            // 用户拒绝授权回调
            @Override
            public void onPermissionDenied(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int
            resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imagePicker.onActivityResult(OwnerEventFinishActivity.this, requestCode, resultCode, data);
    }

    @Override
    public void getUploadPhotoResult(PhotoModel photoModel) {
        if (image_url.equals("")) {
            image_url = image_url + photoModel.getImage_url();
        } else {
            image_url = image_url + "|" + photoModel.getImage_url();
        }
        LogUtil.e("图片地址", image_url);
        LogUtil.e("回调图片", photoModel.getHttp_host() + photoModel.getImage_url());
    }

    @Override
    public void getFinishEventResult(String result) {
        LogUtil.e("回调成功", result);
        setResult(10);
        finish();
    }

    @Override
    public void requestNetFail(String error) {
        Toast.makeText(this,error,Toast.LENGTH_SHORT).show();
    }
}
