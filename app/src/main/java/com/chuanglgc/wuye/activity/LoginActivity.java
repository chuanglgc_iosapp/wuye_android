package com.chuanglgc.wuye.activity;

import android.app.Dialog;
import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.ILoginView;
import com.chuanglgc.wuye.MVP.persent.LoginPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.model.LoginModel;
import com.chuanglgc.wuye.network.Constant;
import com.chuanglgc.wuye.utils.CheckManifest;
import com.chuanglgc.wuye.utils.DeviceInfoUtil;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.MD5Utils;
import com.chuanglgc.wuye.utils.SpUtils;

import java.util.HashMap;

import butterknife.BindView;
import cn.jpush.android.api.JPushInterface;


public class LoginActivity extends BaseActivity<ILoginView, LoginPersent> implements ILoginView {
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.bt_login)
    Button btLogin;
    private String username;
    private String password;
    private Dialog loadingDialog;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initView() {
        etUsername.setText((String) SpUtils.get(this, Constant.USER_NAME, ""));
        etPassword.setText((String) SpUtils.get(this, Constant.PASSWORD, ""));
        CheckManifest.initManifest(this);
    }

    @Override
    protected void initListener() {
        btLogin.setOnClickListener(v -> {
            username = etUsername.getText().toString().trim();
            password = etPassword.getText().toString().trim();
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "登录中", true);
            String password = MD5Utils.md5(Constant.salt + this.password);

            HashMap<String, String> loginMap = new HashMap<>();
            loginMap.put("phone_number", username);
            loginMap.put("login_password", password);//加密方式
            String uuid = DeviceInfoUtil.getUUID();
            //获取设备厂商
            String deviceBrand = DeviceInfoUtil.getDeviceBrand();
            String userAgent = DeviceInfoUtil.getUserAgent();

            loginMap.put("device_manufacturer", deviceBrand);
            loginMap.put("device_uuid", uuid);
            loginMap.put("device_platform", userAgent);
            persenter.requestLogin(loginMap);
        });
    }

    @Override
    protected void initData() {

    }

    @Override
    protected LoginPersent oncreatPersenter() {
        return new LoginPersent();
    }


    /**
     * 登陆成功接口的回调
     *
     * @param loginModel 内容
     */
    @Override
    public void getLoginResult(LoginModel loginModel) {
        //登陆成功注册激光推送的别名
        String dName = "WY_" + DeviceInfoUtil.getUUID();

        //本地保存
        EmployeeApplication.getInstance().getCacheUtil().putSerializableObj(Constant.USER_KEY, loginModel);
        SpUtils.put(this, Constant.USER_NAME, username);
        SpUtils.put(this, Constant.PASSWORD, password);
        //先注册昵称，推送时候会根据这个昵称去推送到个人
        String userId = loginModel.getEmp_id();

        JPushInterface.setAlias(LoginActivity.this, Integer.valueOf(userId), dName);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();

    }

    @Override
    public void finish() {
        super.finish();
        if (loadingDialog != null) loadingDialog.dismiss();
    }

    /**
     * 登陆失败 接口的回调
     *
     * @param wrong 错误信息
     */
    @Override
    public void getLoginFauiler(String wrong) {
        Toast.makeText(this, wrong, Toast.LENGTH_SHORT).show();
        if (loadingDialog != null) loadingDialog.dismiss();
    }
}
