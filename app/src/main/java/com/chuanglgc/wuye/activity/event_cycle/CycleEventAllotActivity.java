package com.chuanglgc.wuye.activity.event_cycle;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.ICycleEventAllotView;
import com.chuanglgc.wuye.MVP.persent.CycleEventAllotPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.dialog.PartnerDialog;
import com.chuanglgc.wuye.dialog.SpinnerDialog;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.PartnerModel;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CycleEventAllotActivity extends BaseActivity<ICycleEventAllotView, CycleEventAllotPersent> implements View.OnClickListener,
        ICycleEventAllotView {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.tv_master)
    TextView tvMaster;
    @BindView(R.id.sel_mast)
    RelativeLayout selMast;
    @BindView(R.id.tv_mate)
    TextView tvMate;
    @BindView(R.id.sel_mate)
    RelativeLayout selMate;
    @BindView(R.id.tv_record)
    EditText tvRecord;
    @BindView(R.id.bt_photo)
    MyPhotoBtton btPhoto;
    @BindView(R.id.rb_hurry)
    RadioButton rbHurry;
    @BindView(R.id.rb_normal)
    RadioButton rbNormal;
    @BindView(R.id.radio_group)
    RadioGroup radioGroup;
    private Dialog loadingDialog;
    private ArrayList<String> listPartner;
    private int partnerPosition;
    private int masterPostion;
    private List<EventAllotModel> masterList;
    private List<PartnerModel> partnerList;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_cycle_allot;
    }

    @Override
    protected void initView() {
        Toolbar toolbar = myToolbar.getToolbar();
        if (toolbar != null) {
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(v -> {
                finish();
            });
        }
        radioGroup.check(R.id.rb_normal);
    }


    @Override
    protected void initData() {

    }

    @Override
    protected CycleEventAllotPersent oncreatPersenter() {
        return new CycleEventAllotPersent();
    }

    @Override
    protected void initListener() {
        btPhoto.setOnClickListener(this);
        tvMaster.setOnClickListener(this);
        tvMate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_photo:
                allotTask();
                break;
            case R.id.tv_master:
                requestMaster();
                break;
            case R.id.tv_mate:
                requestPartner();
                break;
        }

    }

    /**
     * 分配任务
     */
    private void allotTask() {
        String master = tvMaster.getText().toString().trim();
        if (TextUtils.isEmpty(master)) {
            Toast.makeText(this, "请选择负责人", Toast.LENGTH_SHORT).show();
        } else {
            String slePostion = getIntent().getStringExtra("slePostion");
            String substring = slePostion.substring(0, slePostion.lastIndexOf(","));//去掉最后一个逗号
            HashMap<String, String> allotMap = new HashMap<>();
            allotMap.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            allotMap.put("cycle_task_id", substring);
            allotMap.put("director_id", masterList.get(masterPostion).getEmp_id());
            allotMap.put("cooperator_name", tvMate.getText().toString().trim());
            allotMap.put("cycle_task_remark", tvRecord.getText().toString().trim());

            int radioButtonId = radioGroup.getCheckedRadioButtonId();
            if (radioButtonId == R.id.rb_hurry) {
                allotMap.put("cycle_task_priority", "1");
            } else {
                allotMap.put("cycle_task_priority", "2");
            }
            showDialog();
            persenter.allotTask(allotMap);
        }

    }

    /**
     * 请求配合人
     */
    private void requestPartner() {
        if (TextUtils.isEmpty(tvMaster.getText().toString().trim())) {
            Toast.makeText(this, "请选择负责人", Toast.LENGTH_SHORT).show();
        } else {
            showDialog();
            HashMap<String, String> partnerMap = new HashMap<>();
            partnerMap.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            partnerMap.put("director_id", masterList.get(masterPostion).getEmp_id());
            persenter.queryPartner(partnerMap);
        }

    }

    /**
     * 请求负责人
     */
    private void requestMaster() {
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        showDialog();
        persenter.queryMaster(map);
    }


    /**
     * 获取负责人列表
     */
    @Override
    public void getAllotPerson(List<EventAllotModel> list) {
        if (loadingDialog != null) loadingDialog.dismiss();
        ArrayList<String> listMaster = new ArrayList<>();
        for (EventAllotModel model : list) {
            listMaster.add(model.getEmp_name());
        }
        SpinnerDialog masterDialog = new SpinnerDialog(this, listMaster);
        masterDialog.setOnSpinnerItemClick((itemName, position) -> {
            masterList = list;
            masterPostion = position;
            masterDialog.dismiss();
            tvMaster.setText(itemName);
            tvMate.setText("");
        });
    }

    /**
     * 获取配合人列表
     */
    @Override
    public void getAllotPartner(List<PartnerModel> list) {
        if (loadingDialog != null) loadingDialog.dismiss();
        listPartner = new ArrayList<>();
        for (PartnerModel model : list) {
            listPartner.add(model.getEmp_name());
        }
        PartnerDialog partnerrDialog = new PartnerDialog(this, listPartner);
        partnerrDialog.setOnCommitClick(((itemName, selId) -> {
            partnerList = list;
            partnerrDialog.dismiss();
            itemName = itemName.replace(",", "、");
            tvMate.setText(itemName);
        }));

    }

    @Override
    public void getAllotResult(String result) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
        setResult(2);
        finish();
    }

    @Override
    public void onRequestError(String wrong) {
        if (loadingDialog != null) loadingDialog.dismiss();
        Toast.makeText(this, wrong, Toast.LENGTH_SHORT).show();
    }

    void showDialog() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
    }
}
