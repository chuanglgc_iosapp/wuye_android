package com.chuanglgc.wuye.activity.setting;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.activity.LoginActivity;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.dialog.SecondChekDialog;
import com.chuanglgc.wuye.network.RabbitmqManager;
import com.chuanglgc.wuye.widget.MyToolbar;

import butterknife.BindView;
import cn.jpush.android.api.JPushInterface;


public class SettingActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.ll_change_password)
    LinearLayout llChangePassword;
    @BindView(R.id.ll_update)
    LinearLayout llUpdate;
    @BindView(R.id.ll_help)
    LinearLayout llHelp;
    @BindView(R.id.ll_finish)
    LinearLayout llFinish;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_setting;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = myToolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
    }


    @Override
    protected void initData() {

    }

    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }

    @Override
    protected void initListener() {
        llChangePassword.setOnClickListener(this);
        llFinish.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_change_password:
                startActivity(ChangePasswordActivity.class);
                break;
            case R.id.ll_finish:
                finishApp();
                break;

        }
    }

    private void finishApp() {
        SecondChekDialog chekDialog = new SecondChekDialog(this);
        chekDialog.setContent("确定退出?");
        chekDialog.setOnPositiveListener(v -> {
            chekDialog.dismiss();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            RabbitmqManager.getInstance().onClose();

            //请求极光推送的别名
            String userId = EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id();

            JPushInterface.deleteAlias(SettingActivity.this,Integer.valueOf(userId));

            EmployeeApplication.getInstance().removeAllActivity();
        });

        chekDialog.setOnNegativeListener(s -> {
            chekDialog.dismiss();
        });
        chekDialog.show();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }
}
