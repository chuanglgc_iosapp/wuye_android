package com.chuanglgc.wuye.activity.event_supervise;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.SupervisePagerAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePager;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.pager.SuperviseDailyPager;
import com.chuanglgc.wuye.pager.SuperviseHanldlePager;
import com.chuanglgc.wuye.pager.SuperviseOwnerPager;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.ArrayList;

import butterknife.BindView;


public class SuperviseActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.rb_daily)
    RadioButton rbDaily;
    @BindView(R.id.rb_handle)
    RadioButton rbHandle;
    @BindView(R.id.rb_owner)
    RadioButton rbOwner;
    @BindView(R.id.radio_group)
    RadioGroup radioGroup;
    @BindView(R.id.vp)
    ViewPager vp;
    private ArrayList<BasePager> pageList;
    private SuperviseDailyPager superviseDailyPager;
    private SuperviseHanldlePager superviseHanldlePager;
    private SuperviseOwnerPager superviseOwnerPager;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_supervise;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = myToolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });


        pageList = new ArrayList<>();
        superviseDailyPager = new SuperviseDailyPager(this);
        superviseHanldlePager = new SuperviseHanldlePager(this);
        superviseOwnerPager = new SuperviseOwnerPager(this);
        pageList.add(superviseDailyPager);
        pageList.add(superviseHanldlePager);
        pageList.add(superviseOwnerPager);
        vp.setAdapter(new SupervisePagerAdapter(pageList));
        radioGroup.check(R.id.rb_daily);
    }

    @Override
    protected void initListener() {
        radioGroup.setOnCheckedChangeListener(this);
        vp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        radioGroup.check(R.id.rb_daily);
                        break;
                    case 1:
                        radioGroup.check(R.id.rb_handle);
                        break;
                    case 2:
                        radioGroup.check(R.id.rb_owner);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1) {
            superviseDailyPager.initData();
        } else if (resultCode == 2) {
            superviseHanldlePager.initData();
        } else if (resultCode == 3) {
            superviseOwnerPager.initData();
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_daily:
                vp.setCurrentItem(0);
                break;
            case R.id.rb_handle:
                vp.setCurrentItem(1);
                break;
            case R.id.rb_owner:
                vp.setCurrentItem(2);
                break;
        }

    }
}
