package com.chuanglgc.wuye.activity.event_near;

import android.app.Dialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.INearBigView;
import com.chuanglgc.wuye.MVP.persent.NearBigPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.dialog.PartnerDialog;
import com.chuanglgc.wuye.dialog.SpinnerDialog;
import com.chuanglgc.wuye.model.EventAllotModel;
import com.chuanglgc.wuye.model.NearBigBodyTypeModel;
import com.chuanglgc.wuye.model.NearBigDeviceNumModel;
import com.chuanglgc.wuye.model.NearBigDeviceTypeModel;
import com.chuanglgc.wuye.model.NearDailyWorkModel;
import com.chuanglgc.wuye.model.PartnerModel;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;


public class NearEventBigActivity extends BaseActivity<INearBigView, NearBigPersent> implements View.OnClickListener, INearBigView {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.tv_master)
    TextView tvMaster;
    @BindView(R.id.sel_mast)
    RelativeLayout selMast;
    @BindView(R.id.tv_mate)
    TextView tvMate;
    @BindView(R.id.sel_mate)
    RelativeLayout selMate;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.tv_device_type)
    TextView tvDeviceType;
    @BindView(R.id.tv_divice_number)
    TextView tvDiviceNumber;
    @BindView(R.id.sel_content)
    RelativeLayout selContent;
    @BindView(R.id.tv_record)
    EditText tvRecord;
    @BindView(R.id.bt_photo)
    MyPhotoBtton btPhoto;
    @BindView(R.id.rb_one)
    RadioButton rbOne;
    @BindView(R.id.rb_two)
    RadioButton rbTwo;
    @BindView(R.id.rg_body)
    RadioGroup rgBody;
    @BindView(R.id.rb_hurry)
    RadioButton rbHurry;
    @BindView(R.id.rb_normal)
    RadioButton rbNormal;
    @BindView(R.id.rg_level)
    RadioGroup rgLevel;
    private Dialog loadingDialog;
    private List<EventAllotModel> masterList;
    private int masterPostion;
    private List<PartnerModel> partnerList;
    private String partnerSleName;
    private List<NearDailyWorkModel> workContentList;
    private int workContentPosition;
    private List<NearBigDeviceTypeModel> deviceTypeList;
    private int deviceTypePosition;
    private List<NearBigBodyTypeModel> bodyTypeList;
    private List<NearBigDeviceNumModel> deviceNumberList;
    private ArrayList<Integer> deviceNumberSleIdList;
    private String deviceNumberSle;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_near_event_big;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = myToolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        persenter.requestCycleWorkType(EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());//请求主体类型
        rgLevel.check(R.id.rb_normal);//默认选中一般
    }


    @Override
    protected void initData() {

    }

    @Override
    protected NearBigPersent oncreatPersenter() {
        return new NearBigPersent();
    }


    @Override
    protected void initListener() {
        tvContent.setOnClickListener(this);
        tvMaster.setOnClickListener(this);
        tvMate.setOnClickListener(this);
        btPhoto.setOnClickListener(this);

        tvDeviceType.setOnClickListener(this);
        tvDiviceNumber.setOnClickListener(this);
        rgBody.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                tvDiviceNumber.setText("");
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_master:
                requestMaster();
                break;
            case R.id.tv_mate:
                requestPartner();
                break;
            case R.id.tv_content:
                requestWorkContent();
                break;
            case R.id.tv_device_type:
                requestDeviceType();
                break;
            case R.id.tv_divice_number:
                requestDeviceNumber();
                break;
            case R.id.bt_photo:
                allotTask();
                break;
        }
    }

    //分配任务
    private void allotTask() {
        if (masterList == null) {
            Toast.makeText(this, "请选择负责人", Toast.LENGTH_SHORT).show();
        } else if (workContentList == null) {
            Toast.makeText(this, "请选择工作内容", Toast.LENGTH_SHORT).show();
        } else if (deviceTypeList == null) {
            Toast.makeText(this, "请选择设备类型", Toast.LENGTH_SHORT).show();
        } else if (deviceNumberList == null) {
            Toast.makeText(this, "请选择设备编号", Toast.LENGTH_SHORT).show();
        } else {
            showLoading();
            HashMap<String, String> map = new HashMap<>();
            map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            map.put("leading_person", masterList.get(masterPostion).getEmp_id());
            if (!tvMate.getText().toString().trim().equals(""))
                map.put("assistant_person", partnerSleName);
            map.put("cycle_task_type", workContentList.get(workContentPosition).getWork_id());
            map.put("cycle_task_content", workContentList.get(workContentPosition).getWork_name());
            map.put("cycle_task_device_type", deviceTypeList.get(deviceTypePosition).getDevice_type_name());
            map.put("cycle_task_device_num", deviceNumberSle);
            String deviceId = "";

            for (int i = 0; i < deviceNumberSleIdList.size(); i++) {
                if (i == 0) {
                    deviceId = deviceId + deviceNumberList.get(deviceNumberSleIdList.get(i)).getDevice_id();
                } else {
                    String append = "," + deviceNumberList.get(deviceNumberSleIdList.get(i)).getDevice_id();
                    deviceId = deviceId + append;
                }
            }
            map.put("cycle_task_device_id", deviceId);
            //设置主体类型名称
            int radioButtonId = rgBody.getCheckedRadioButtonId();
            if (radioButtonId == R.id.rb_one) {
                map.put("cycle_task_body_type", bodyTypeList.get(0).getBody_type_name());
            } else if (radioButtonId == R.id.rb_two) {
                map.put("cycle_task_body_type", bodyTypeList.get(1).getBody_type_name());
            }
            LogUtil.e("临时大事", deviceId);
            //设置优先级  1 紧急 2 一般
            int rbId = rgLevel.getCheckedRadioButtonId();
            if (rbId == R.id.rb_hurry) {
                map.put("cycle_task_priority", "1");
            } else if (rbId == R.id.rb_normal) {
                map.put("cycle_task_priority", "2");
            }
            map.put("cycle_assign_requirements", tvRecord.getText().toString().trim());
            persenter.allotTask(map);
        }
    }

    //请求设备编码
    private void requestDeviceNumber() {
        if (deviceTypeList == null) {
            Toast.makeText(this, "请选择设备类型", Toast.LENGTH_SHORT).show();
        } else {
            showLoading();
            HashMap<String, String> map = new HashMap<>();
            int radioButtonId = rgBody.getCheckedRadioButtonId();
            if (radioButtonId == R.id.rb_one) {
                map.put("body_type", bodyTypeList.get(0).getBody_type_num());
            } else if (radioButtonId == R.id.rb_two) {
                map.put("body_type", bodyTypeList.get(1).getBody_type_num());
            }
            map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            map.put("device_type", deviceTypeList.get(deviceTypePosition).getDevice_type_num());
            persenter.requestDevicekNum(map);
        }


    }

    //请求设备类型
    private void requestDeviceType() {
        showLoading();
        persenter.requestDevicekType(EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
    }

    //请求工作内容
    private void requestWorkContent() {
        showLoading();
        LogUtil.e("小区id",EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        persenter.requestWorkContent(EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
    }

    //显示loading
    private void showLoading() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
    }

    //请求配合人
    private void requestPartner() {
        if (TextUtils.isEmpty(tvMaster.getText().toString().trim())) {
            Toast.makeText(this, "请选择负责人", Toast.LENGTH_SHORT).show();
        } else {
            HashMap<String, String> partnerMap = new HashMap<>();
            partnerMap.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            partnerMap.put("director_id", masterList.get(masterPostion).getEmp_id());
            persenter.requsetPartner(partnerMap);
        }
    }


    //请求负责人
    private void requestMaster() {
        showLoading();
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        persenter.requestMaster(map);
    }

    @Override
    public void getMasterList(List<EventAllotModel> masterList) {
        ArrayList<String> listMaster = new ArrayList<>();
        for (EventAllotModel model : masterList) {
            listMaster.add(model.getEmp_name());
        }
        SpinnerDialog masterDialog = new SpinnerDialog(this, listMaster);
        masterDialog.setOnSpinnerItemClick((itemName, position) -> {
            this.masterList = masterList;
            masterPostion = position;
            tvMaster.setText(itemName);
            tvMate.setText("");
            masterDialog.dismiss();

        });
        if (loadingDialog != null) loadingDialog.dismiss();
    }

    //获取配合人
    @Override
    public void getPartnerList(List<PartnerModel> partnerList) {
        ArrayList<String> listPartner = new ArrayList<>();
        for (PartnerModel model : partnerList) {
            listPartner.add(model.getEmp_name());
        }
        PartnerDialog contentDialog = new PartnerDialog(this, listPartner);
        contentDialog.setOnCommitClick(((itemName, sleIdList) -> {
            this.partnerList = partnerList;
            partnerSleName = itemName.replace(",","、");
            tvMate.setText(partnerSleName);
            contentDialog.dismiss();
        }));
        if (loadingDialog != null) loadingDialog.dismiss();
    }

    //获取工作内容
    @Override
    public void getWorkContentList(List<NearDailyWorkModel> workModels) {
        ArrayList<String> listWork = new ArrayList<>();
        for (NearDailyWorkModel model : workModels) {
            listWork.add(model.getWork_name());
        }
        SpinnerDialog workDialog = new SpinnerDialog(this, listWork);
        workDialog.setOnSpinnerItemClick((itemName, position) -> {
            this.workContentList = workModels;
            workContentPosition = position;
            workDialog.dismiss();
            tvContent.setText(itemName);
        });
        if (loadingDialog != null) loadingDialog.dismiss();
    }

    //获取设备类型
    @Override
    public void getDeviceTypeList(List<NearBigDeviceTypeModel> workTypeList) {
        ArrayList<String> listWork = new ArrayList<>();
        for (NearBigDeviceTypeModel model : workTypeList) {
            listWork.add(model.getDevice_type_name());
        }
        SpinnerDialog deviceTypeDialog = new SpinnerDialog(this, listWork);
        deviceTypeDialog.setOnSpinnerItemClick((itemName, position) -> {
            this.deviceTypeList = workTypeList;
            deviceTypePosition = position;
            deviceTypeDialog.dismiss();

            tvDeviceType.setText(itemName);
            tvDiviceNumber.setText("");
        });
        if (loadingDialog != null) loadingDialog.dismiss();
    }


    //获取主体类型  设置RadioButton 名称
    @Override
    public void getCycleWorkType(List<NearBigBodyTypeModel> bodyTypeList) {
        for (int i = 0; i < bodyTypeList.size(); i++) {
            if (i == 0) {
                rbOne.setText(bodyTypeList.get(0).getBody_type_name());
            } else if (i == 1) {
                rbTwo.setText(bodyTypeList.get(1).getBody_type_name());
            }
        }
        this.bodyTypeList = bodyTypeList;
        rgBody.check(R.id.rb_one);//默认选中第一个
    }

    //获取设备编号
    @Override
    public void getDeviceNumberList(List<NearBigDeviceNumModel> deviceList) {
        ArrayList<String> listWork = new ArrayList<>();
        for (NearBigDeviceNumModel model : deviceList) {
            listWork.add(model.getDevice_num());
        }
        PartnerDialog deviceTypeDialog = new PartnerDialog(this, listWork);
        deviceTypeDialog.setOnCommitClick(((itemName, sleIdList) -> {
            this.deviceNumberList = deviceList;
            tvDiviceNumber.setText(itemName.replace(",","，"));
            deviceNumberSle = itemName;
            this.deviceNumberSleIdList = sleIdList;
        }));

        if (loadingDialog != null) loadingDialog.dismiss();
    }

    //分配
    @Override
    public void getAllotResult(String result) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void getNetError(String error) {
        LogUtil.e("临时大事", error);
        if (loadingDialog != null) loadingDialog.dismiss();
    }


}
