package com.chuanglgc.wuye.activity.message_notify;

import android.app.Dialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.MssageAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.dialog.MessageDialog;
import com.chuanglgc.wuye.model.MessageModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MessageNotifyActivity extends BaseActivity {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.rls_msg)
    RecyclerView rlsMsg;
    @BindView(R.id.iv_empty)
    ImageView ivEmpty;
    private Dialog loadingDialog;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_message_notfy;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = myToolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        queryMessage();
    }

    private void initRecyclerMsg(List<MessageModel> list) {
        if (list.size() == 0) {
            rlsMsg.setVisibility(View.GONE);
            ivEmpty.setVisibility(View.VISIBLE);
            return;
        }
        rlsMsg.setLayoutManager(new LinearLayoutManager(this));
        MssageAdapter mssageAdapter = new MssageAdapter(list);
        mssageAdapter.bindToRecyclerView(rlsMsg);
        rlsMsg.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                MessageModel messageModel = list.get(position);
                MessageDialog messageDialog = new MessageDialog(MessageNotifyActivity.this);
                messageDialog.setData(messageModel.getCY0201(), messageModel.getCY0202(), messageModel.getCY0206(), messageModel.getCommunity_name());
                messageDialog.show();
            }
        });

    }

    @Override
    protected void initListener() {

    }

    @Override
    protected void initData() {

    }

    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }

    //查询消息接口
    public void queryMessage() {
        loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("department_id", EmployeeApplication.getInstance().getUserInfoFromCache().getDepartment_id());
        map.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        RestClient.getAPIService().queryNoticeList(map).enqueue(new Callback<Result<List<MessageModel>>>() {
            @Override
            public void onResponse(Call<Result<List<MessageModel>>> call, Response<Result<List<MessageModel>>> response) {
                if (response.code() == 200) {
                    Result<List<MessageModel>> body = response.body();
                    if (body.isResult()) {
                        initRecyclerMsg(body.getData());
                    } else {
                        loadingDialog.dismiss();

                        rlsMsg.setVisibility(View.GONE);
                        ivEmpty.setVisibility(View.VISIBLE);
                    }
                } else {
                    rlsMsg.setVisibility(View.GONE);
                    ivEmpty.setVisibility(View.VISIBLE);
                }
                loadingDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Result<List<MessageModel>>> call, Throwable t) {
                loadingDialog.dismiss();
                rlsMsg.setVisibility(View.GONE);
                ivEmpty.setVisibility(View.VISIBLE);
            }
        });
    }
}
