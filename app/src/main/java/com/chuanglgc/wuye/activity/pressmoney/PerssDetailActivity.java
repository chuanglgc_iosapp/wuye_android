package com.chuanglgc.wuye.activity.pressmoney;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.IPressDetailView;
import com.chuanglgc.wuye.MVP.persent.PressMoneyDetailPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.PressDetailAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.dialog.EventFinishDialog;
import com.chuanglgc.wuye.dialog.PressMoneyMoreDialog;
import com.chuanglgc.wuye.model.PressMoneyDetailModel;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;
import com.zhy.android.percent.support.PercentRelativeLayout;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;


public class PerssDetailActivity extends BaseActivity<IPressDetailView, PressMoneyDetailPersent> implements IPressDetailView {
    @BindView(R.id.toolbar)
    MyToolbar toolbar;
    @BindView(R.id.tv_date)
    TextView tvAddress;
    @BindView(R.id.rl_time)
    PercentRelativeLayout rlTime;
    @BindView(R.id.rcl_press)
    RecyclerView rclPress;
    @BindView(R.id.iv_line)
    ImageView ivLine;
    @BindView(R.id.tv_info_date)
    TextView tvInfoDate;
    @BindView(R.id.tv_info_time)
    TextView tvInfoTime;
    @BindView(R.id.rc_left)
    PercentRelativeLayout rcLeft;
    @BindView(R.id.tv_more)
    TextView tvMore;
    @BindView(R.id.iv_round)
    ImageView ivRound;
    @BindView(R.id.rl_info_foot)
    PercentRelativeLayout rlInfoFoot;
    @BindView(R.id.bt_allot)
    MyPhotoBtton btRecord;
    @BindView(R.id.rl_bt_root)
    PercentRelativeLayout rlBtRoot;
    @BindView(R.id.tv_info_detail)
    TextView tvInfoDetail;
    private String estateId;
    private String arreascpstId;
    private Dialog loadingDialog;
    private PressDetailAdapter payAdapter;
    private List<PressMoneyDetailModel.UrgerecordBean> commentList;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_press_detail;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = toolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        Intent intent = getIntent();
        estateId = intent.getStringExtra("estate_id");
        arreascpstId = intent.getStringExtra("arreascpst_id");

    }

    //加载欠费数据列表
    private void initPayRcl(List<PressMoneyDetailModel.PaylistBean> paylist) {
        if (payAdapter == null) {
            payAdapter = new PressDetailAdapter(paylist);
            payAdapter.bindToRecyclerView(rclPress);
            rclPress.setLayoutManager(new LinearLayoutManager(this));
        } else {
            payAdapter.setNewData(paylist);
        }
        if (paylist.size() >= 3) {
            ViewGroup.LayoutParams layoutParams = rclPress.getLayoutParams();
            layoutParams.height = DisplayUtil.dp2px(this, 180);
        }
    }


    @Override
    protected void initListener() {
        btRecord.setOnClickListener(v -> showFinishDialog());
        tvMore.setOnClickListener(v -> showPressMoneyMore());
    }

    private void showPressMoneyMore() {
        PressMoneyMoreDialog moreDialog = new PressMoneyMoreDialog(this, commentList);
        moreDialog.show();
    }

    @Override
    protected void initData() {
        requestDetail();
    }

    private void requestDetail() {
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("estate_id", estateId);
        map.put("arreascpst_id", arreascpstId);
        showLoading();
        persenter.requestPressMoneyDetail(map);

    }


    @Override
    protected PressMoneyDetailPersent oncreatPersenter() {
        return new PressMoneyDetailPersent();
    }

    //解决事件dialog
    private void showFinishDialog() {
        EventFinishDialog dialog = new EventFinishDialog(this, "催费记录");
        dialog.setOncommitListener(this::addRecord);
    }

    //添加记录
    private void addRecord(String record) {
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("estate_id", estateId);
        map.put("arreascost_id", arreascpstId);
        map.put("entrycontent", record);
        map.put("information_entryperson", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        showLoading();
        persenter.addPressRecord(map);
    }

    //获取催费记录详情
    @Override
    public void getPressDetail(PressMoneyDetailModel moneyDetailModel) {
        List<PressMoneyDetailModel.PaylistBean> paylist = moneyDetailModel.getPaylist();
        initPayRcl(paylist);
        if (paylist.size()>0){
            tvAddress.setText(paylist.get(0).getEstate_address());
        }

        List<PressMoneyDetailModel.UrgerecordBean> commentList = moneyDetailModel.getUrgerecord();
        if (commentList.size() != 0) {//默认显示第一条记录
            rlInfoFoot.setVisibility(View.VISIBLE);
            ivLine.setVisibility(View.VISIBLE);
            this.commentList = commentList;
            PressMoneyDetailModel.UrgerecordBean urgerecordBean = commentList.get(0);
            tvInfoDate.setText(urgerecordBean.getEntrydate());
            tvInfoTime.setText(urgerecordBean.getEntrytime());
            tvInfoDetail.setText(urgerecordBean.getUrgecontent());
        } else {//没有记录不显示
            rlInfoFoot.setVisibility(View.INVISIBLE);
            ivLine.setVisibility(View.GONE);
        }
        LoadingDialogUtils.closeDialog(loadingDialog);
    }

    //添加记录成功回调
    @Override
    public void addPressResult(String result) {
        LoadingDialogUtils.closeDialog(loadingDialog);
        requestDetail();

    }

    @Override
    public void getNetError(String error) {
        LoadingDialogUtils.closeDialog(loadingDialog);
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    void showLoading() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
    }


}
