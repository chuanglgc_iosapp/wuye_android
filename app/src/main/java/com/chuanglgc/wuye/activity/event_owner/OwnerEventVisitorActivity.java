package com.chuanglgc.wuye.activity.event_owner;

import android.app.Dialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.PhotoModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.HashMap;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OwnerEventVisitorActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    MyToolbar toolbar;
    @BindView(R.id.tv_person)
    TextView tvPerson;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.tv_record)
    TextView tvRecord;
    @BindView(R.id.bt_commit)
    MyPhotoBtton btCommit;
    private String repaireId;
    private Dialog loadingDialog;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_event_owner_visitor;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = toolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        //  taskId = getIntent().getStringExtra("task_id");
        repaireId = getIntent().getStringExtra("repair_id");
    }

    @Override
    protected void initListener() {
        btCommit.setOnClickListener(v -> {
            String person = tvPerson.getText().toString().trim();
            String phone = tvPhone.getText().toString().trim();
            String record = tvRecord.getText().toString().trim();
            if (TextUtils.isEmpty(person)) {
                Toast.makeText(this, "请填写联系人", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(phone)) {
                Toast.makeText(this, "请填写联系人电话", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(record)) {
                Toast.makeText(this, "请填写回访记录", Toast.LENGTH_SHORT).show();
            } else {
                HashMap<String, String> map = new HashMap<>();
                map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
                map.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
                //map.put("task_id", taskId);
                map.put("repair_id", repaireId);
                map.put("contact_person", person);
                map.put("contact_phone", phone);
                map.put("feedback_content", record);
                LogUtil.e("回访参数", repaireId);
                LogUtil.e("回访参数", person);
                LogUtil.e("回访参数", phone);
                LogUtil.e("回访参数", record);
                loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
                RestClient.getAPIService().writeFeedbackContent(map).enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {
                        if (response.code() == 200) {
                            if (response.body().isResult()) {
                                Toast.makeText(OwnerEventVisitorActivity.this, "填写成功", Toast.LENGTH_SHORT).show();
                                setResult(5);
                                loadingDialog.dismiss();
                                finish();
                            } else {
                                loadingDialog.dismiss();
                                Toast.makeText(OwnerEventVisitorActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            loadingDialog.dismiss();
                            Toast.makeText(OwnerEventVisitorActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable t) {
                        loadingDialog.dismiss();
                        Toast.makeText(OwnerEventVisitorActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

        });
    }

    @Override
    protected void initData() {

    }

    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }


}
