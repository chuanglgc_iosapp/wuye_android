package com.chuanglgc.wuye.activity.event_manager;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.event.EventManagerAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.EventManagerModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.utils.LoadingBelowSearchUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EventManagerActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.toolbar)
    MyToolbar toolbar;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.tv_search)
    TextView tvSearch;
    @BindView(R.id.ll_search_goods)
    RelativeLayout llSearchGoods;
    @BindView(R.id.rcy_event)
    RecyclerView rcyEvent;
    @BindView(R.id.bt_add)
    TextView btAdd;
    private Dialog loadingDialog;
    private OnItemClickListener onItemClickListener;
    private EventManagerAdapter eventManagerAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_event_manager;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = toolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        requestEvents("", "", "", "", "");
    }

    /*
       搜索事件列表  不传条件为默认
     */
    public void requestEvents(String buildId, String unitId, String roomId, String repariTypeId, String repaireTime) {
        LogUtil.e("搜索时间", "栋" + buildId + "单元" + unitId + "门牌号" + roomId + "ssss" + repariTypeId + repaireTime);
        if (loadingDialog == null) {
            loadingDialog = LoadingBelowSearchUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("build_id", buildId);
        map.put("unit_id", unitId);
        map.put("room_id", roomId);
        map.put("repair_type_id", repariTypeId);
        map.put("start_time", repaireTime);
        map.put("flag", "1");//接口固定
        RestClient.getAPIService().searchRepairRecord(map).enqueue(new Callback<Result<List<EventManagerModel>>>() {
            @Override
            public void onResponse(Call<Result<List<EventManagerModel>>> call, Response<Result<List<EventManagerModel>>> response) {
                if (response.code() == 200) {
                    Result<List<EventManagerModel>> listResult = response.body();
                    if (listResult.isResult()) {
                        initRecyclerEvent(listResult.getData());
                    } else {
                        loadingDialog.dismiss();
                        Toast.makeText(EventManagerActivity.this, listResult.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(EventManagerActivity.this, response.code() + "", Toast.LENGTH_SHORT).show();
                    loadingDialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<Result<List<EventManagerModel>>> call, Throwable t) {
                loadingDialog.dismiss();
                Toast.makeText(EventManagerActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    //设置Rcl数据
    private void initRecyclerEvent(List<EventManagerModel> list) {
        if (eventManagerAdapter == null) {
            rcyEvent.setLayoutManager(new LinearLayoutManager(this));
            eventManagerAdapter = new EventManagerAdapter(list);
            eventManagerAdapter.bindToRecyclerView(rcyEvent);
            View view = new View(this);
            view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, DisplayUtil.dp2px(this, 100)));
            eventManagerAdapter.addFooterView(view);
        } else {
            eventManagerAdapter.setNewData(list);
        }

        //设置点击事件
        if (onItemClickListener != null)
            rcyEvent.removeOnItemTouchListener(onItemClickListener);//先清除点击事件在添加
        onItemClickListener = new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(EventManagerActivity.this, EventDetailActivity.class);
                intent.putExtra("state", list.get(position).getTask_status_id());
                intent.putExtra("repair_id", list.get(position).getRepair_id());
                startActivityForResult(intent, 6);
            }
        };
        rcyEvent.addOnItemTouchListener(onItemClickListener);
        loadingDialog.dismiss();
    }

    @Override
    protected void initListener() {
        tvSearch.setOnClickListener(this);
        btAdd.setOnClickListener(this);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected BasePersenter oncreatPersenter() {
        return null;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_search:
                Intent intent = new Intent(this, EventSearchActivity.class);
                startActivityForResult(intent, 5);
                break;
            case R.id.bt_add:
                Intent intentAdd = new Intent(this, EventAddActivity.class);
                startActivityForResult(intentAdd, 6);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 5 && resultCode == 6) {//查询页面返回查询条件
            String buildId = data.getStringExtra("build_id");

            String unitId = data.getStringExtra("unit_id");
            String roomId = data.getStringExtra("room_id");
            if (buildId == null) buildId = "";
            if (unitId == null) unitId = "";
            if (roomId == null) roomId = "";
            String repairTypeId = data.getStringExtra("repair_type_id");
            if (repairTypeId == null) repairTypeId = "";
            String repairTime = data.getStringExtra("repair_time");
            if (repairTime == null) repairTime = "";
            requestEvents(buildId, unitId, roomId, repairTypeId, repairTime);
        } else if (requestCode == 6) {//添加成功回调 事件解决回调
            requestEvents("", "", "", "", "");
        }
    }
}
