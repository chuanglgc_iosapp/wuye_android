package com.chuanglgc.wuye.activity.event_dailywork;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.IDailyDetailView;
import com.chuanglgc.wuye.MVP.persent.DailyDetailPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.event.EventPhotoAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.model.DailyPhotoModel;
import com.chuanglgc.wuye.model.DailyTaskModel;
import com.chuanglgc.wuye.model.PostDailyPhotoModel;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.linchaolong.android.imagepicker.ImagePicker;
import com.linchaolong.android.imagepicker.cropper.CropImage;
import com.linchaolong.android.imagepicker.cropper.CropImageView;
import com.zhy.android.percent.support.PercentLinearLayout;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okio.BufferedSink;

public class DailyDetailActivity extends BaseActivity<IDailyDetailView, DailyDetailPersent> implements View.OnClickListener, IDailyDetailView {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_number)
    TextView tvNumber;
    @BindView(R.id.tv_area)
    TextView tvArea;
    @BindView(R.id.rcl_pic)
    RecyclerView rclPic;
    @BindView(R.id.iv_addpic)
    ImageView ivAddpic;
    @BindView(R.id.rl_add)
    RelativeLayout rlAdd;
    @BindView(R.id.ll_pic_root)
    PercentLinearLayout llPicRoot;
    @BindView(R.id.ll_record_root)
    PercentLinearLayout llRecordRoot;
    @BindView(R.id.bt_photo)
    MyPhotoBtton btPhoto;
    @BindView(R.id.tv_chabiao)
    EditText tvChabiao;
    @BindView(R.id.rl_device_root)
    PercentLinearLayout rlDeviceRoot;
    @BindView(R.id.et_info)
    EditText etInfo;
    @BindView(R.id.tv_ask)
    TextView tvAsk;
    @BindView(R.id.ll_ask_root)
    PercentLinearLayout llAskRoot;
    private ArrayList<String> listPhoto;
    private EventPhotoAdapter photoAdapter;
    private ImagePicker imagePicker;
    private DailyTaskModel model;
    private JsonArray jsonArray;
    private ArrayList<PostDailyPhotoModel> postDailyPhotoModels;
    private Dialog loadingDialog;

    @Override
    protected int getLayoutId() {
        return R.layout.activit_daily_detail;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = myToolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });

    }

    private void photoManager() {
        imagePicker = new ImagePicker();
        // 设置标题
        imagePicker.setTitle("选择图片");
        // 设置是否裁剪图片
        imagePicker.setCropImage(true);
    }


    @Override
    protected void initData() {
        model = (DailyTaskModel) getIntent().getSerializableExtra("model");
        String state = model.getDaily_task_status();
        String taskType = model.getDaily_task_type();//1仪表类2区域类
        tvType.setText(model.getDaily_task_flag());
        tvTime.setText(model.getWork_schedule());

        tvArea.setText(model.getDaily_task_areaordevice());
        tvNumber.setText("共 " + model.getDaily_task_total() + " 次" + " / 剩余" + model.getDaily_task_rest() + "次");
        if (model.getMessage()!=null&&!model.getMessage().equals("")){
            tvAsk.setText(model.getMessage());
            llAskRoot.setVisibility(View.VISIBLE);
        }
        if (state.equals("处理中")) {
            rclPic.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            listPhoto = new ArrayList<>();
            photoAdapter = new EventPhotoAdapter(listPhoto);
            photoAdapter.bindToRecyclerView(rclPic);
            photoManager();
            btPhoto.setTxName("完成");
            if (taskType.equals("1")) {//显示抄表读数
                rlDeviceRoot.setVisibility(View.VISIBLE);
            }
        } else if (state.equals("未接受")) {
            llRecordRoot.setVisibility(View.INVISIBLE);
            llPicRoot.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected DailyDetailPersent oncreatPersenter() {
        return new DailyDetailPersent();
    }


    @Override
    protected void initListener() {
        rlAdd.setOnClickListener(this);
        btPhoto.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_add:
                addPhoto();
                break;
            case R.id.bt_photo:
                acceptOrFinishTask();
                break;
        }
    }

    private void acceptOrFinishTask() {
        String btName = btPhoto.getBtName();
        if (btName.equals("接受")) {
            acceptTask();
        } else if (btName.equals("完成")) {
            finishTask();
        }
    }

    void showLoading() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
    }

    //完成每日工作
    private void finishTask() {
        if (model.getIs_upload_image().equals("1") && jsonArray != null && jsonArray.size() == 0) {
            Toast.makeText(this, "请上传工作照片", Toast.LENGTH_SHORT).show();
        } else {
            showLoading();
            String info = etInfo.getText().toString().trim();
            HashMap<String, String> map = new HashMap<>();
            map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            map.put("task_id", model.getDaily_task_id());
            LogUtil.e("任务ID",model.getDaily_task_id());
            map.put("daily_task_total", model.getDaily_task_total());
            map.put("daily_task_rest", model.getDaily_task_rest() + "");
            map.put("finished_remark", info);
            map.put("daily_task_type", model.getDaily_task_type());
            if (rlDeviceRoot.getVisibility() == View.VISIBLE) {
                String chaobiao = tvChabiao.getText().toString().trim();
                map.put("instrument_readout", chaobiao);
                LogUtil.e("超标读书",chaobiao);
            }
            if (jsonArray != null && jsonArray.size() > 0) {
                 map.put("image_info", jsonArray.toString());

            }
            persenter.finishDailyTask(map);
        }

    }

    //接受任务
    private void acceptTask() {
        showLoading();
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("task_id", model.getDaily_task_id());
        persenter.acceptDailyTask(map);
    }

    private void addPhoto() {
        // 启动图片选择器
        imagePicker.startChooser(this, new ImagePicker.Callback() {
            // 选择图片回调
            @Override
            public void onPickImage(Uri imageUri) {
            }

            // 裁剪图片回调
            @Override
            public void onCropImage(Uri imageUri) {
                listPhoto.add(String.valueOf(imageUri));
                if (listPhoto.size() >= 2) rlAdd.setVisibility(View.GONE);
                photoAdapter.notifyDataSetChanged();
                PercentLinearLayout.LayoutParams layoutParams = (PercentLinearLayout.LayoutParams) rlAdd.getLayoutParams();
                layoutParams.setMargins(DisplayUtil.dp2px(DailyDetailActivity.this, 12), 0, 0, 0);
                rlAdd.setLayoutParams(layoutParams);

                showLoading();
                //向服务器上传图片
                File file = new File(URI.create(String.valueOf(imageUri)));
                RequestBody requestBody = RequestBody.create(MediaType.parse("image/png"), file);
                MultipartBody.Part part = MultipartBody.Part.createFormData("photo", file.getName(), requestBody);
                HashMap<String, String> map = new HashMap<>();
                map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
                map.put("employee_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
                map.put("task_id", model.getDaily_task_id());
                persenter.uploadDailyPhoto(map, part);
            }

            // 自定义裁剪配置
            @Override
            public void cropConfig(CropImage.ActivityBuilder
                                           builder) {
                // 是否启动多点触摸
                builder.setMultiTouchEnabled(true)
                        // 设置网格显示模式
                        .setGuidelines(CropImageView.Guidelines.ON)
                        // 圆形/矩形
                        .setCropShape(CropImageView.CropShape
                                .RECTANGLE)
                        // 调整裁剪后的图片最终大小
                        .setRequestedSize(960, 540)
                        // 宽高比
                        .setAspectRatio(16, 9);
            }

            // 用户拒绝授权回调
            @Override
            public void onPermissionDenied(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int
            resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imagePicker.onActivityResult(DailyDetailActivity.this, requestCode, resultCode, data);
    }


    @Override
    public void finishResult(String result) {
        if (loadingDialog != null) loadingDialog.dismiss();
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
        setResult(5);
        finish();
    }

    @Override
    public void acceptResult(String result) {
        if (loadingDialog != null) loadingDialog.dismiss();

        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
        setResult(5);
        finish();
    }

    @Override
    public void uploadPhotoResult(DailyPhotoModel model) {
        if (loadingDialog != null) loadingDialog.dismiss();
        if (jsonArray == null) jsonArray = new JsonArray();
        LogUtil.e("获取图片路径", model.getHttp_host() + model.getImage_url());
        JsonObject object = new JsonObject();
        object.addProperty("image_url", model.getImage_url());
        object.addProperty("time", model.getTime());
        jsonArray.add(object);


    }

    @Override
    public void requestNetError(String error) {
        if (loadingDialog != null) loadingDialog.dismiss();
        LogUtil.e("图片路径", error);
    }


}
