package com.chuanglgc.wuye.activity.search_car;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.MVP.IView.ISearchCarView;
import com.chuanglgc.wuye.MVP.persent.SearchCarPersent;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.CarViewpagerAdapter;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePager;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.dialog.CarInfoDialog;
import com.chuanglgc.wuye.model.CarModel;
import com.chuanglgc.wuye.pager.SearchCarNumberPager;
import com.chuanglgc.wuye.pager.SearchParkNumberPager;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;
import com.chuanglgc.wuye.widget.MyToolbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SearchCarActivity extends BaseActivity<ISearchCarView, SearchCarPersent> implements ISearchCarView {
    @BindView(R.id.myToolbar)
    MyToolbar myToolbar;
    @BindView(R.id.rb_carnumber)
    RadioButton rbCarnumber;
    @BindView(R.id.rb_parknumber)
    RadioButton rbParknumber;
    @BindView(R.id.vp_car)
    ViewPager vpCar;
    @BindView(R.id.radio_group)
    RadioGroup radioGroup;
    @BindView(R.id.bt_search)
    MyPhotoBtton btSearch;
    private SearchCarNumberPager searchCarNumberPager;
    private SearchParkNumberPager searchParkNumberPager;
    private Dialog loadingDialog;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_search_car;
    }

    @Override
    protected void initView() {
        Toolbar mytoolbar = myToolbar.getToolbar();
        mytoolbar.setTitle("");
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mytoolbar.setNavigationOnClickListener(v -> {
            finish();
        });
        radioGroup.check(R.id.rb_carnumber);
        ArrayList<BasePager> basePagers = new ArrayList<>();
        searchCarNumberPager = new SearchCarNumberPager(this);
        searchParkNumberPager = new SearchParkNumberPager(this);
        basePagers.add(searchCarNumberPager);
        basePagers.add(searchParkNumberPager);
        vpCar.setAdapter(new CarViewpagerAdapter(basePagers));
    }

    @Override
    protected void initListener() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_carnumber:
                        vpCar.setCurrentItem(0);
                        break;
                    case R.id.rb_parknumber:
                        vpCar.setCurrentItem(1);
                        break;
                }
            }
        });
        vpCar.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    radioGroup.check(R.id.rb_carnumber);
                } else if (position == 1) {
                    radioGroup.check(R.id.rb_parknumber);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        btSearch.setOnClickListener(v -> {
            querCarInfo();
        });
    }

    private void querCarInfo() {
        int query_type = vpCar.getCurrentItem();
        String carNumber = searchCarNumberPager.getEditCarNumber();
        String parkArea = searchParkNumberPager.getParkArea();
        String parkNumber = searchParkNumberPager.getParkNumber();
        if (query_type == 1) {
            if (parkArea.equals("")) {
                Toast.makeText(this, "请选择车位区域", Toast.LENGTH_SHORT).show();
            } else if (parkNumber.equals("")) {
                Toast.makeText(this, "请选择车位编号", Toast.LENGTH_SHORT).show();
            } else {
                showLoading();
                HashMap<String, String> map = new HashMap<>();
                map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
                map.put("query_type", "2");//1按车牌号，2按车位号
                map.put("park_num", parkNumber);
                map.put("park_area", parkArea);
                persenter.queryCarInfo(map);
            }
        }
        if (query_type == 0 && TextUtils.isEmpty(carNumber)) {
            Toast.makeText(this, "请输入车牌号", Toast.LENGTH_SHORT).show();
        } else if (query_type == 0) {
            showLoading();
            HashMap<String, String> map = new HashMap<>();
            map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            map.put("query_type", "1");//1按车牌号，2按车位号
            map.put("plate_number", carNumber);
            persenter.queryCarInfo(map);

        }


    }

    private void showLoading() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(this, "", false);
        } else {
            loadingDialog.show();
        }
    }

    /**

     */
    private void showCarDialog(String address, List<String> plateNumber, List<String> parkNumber) {
        CarInfoDialog carInfoDialog = new CarInfoDialog(this, address, plateNumber, parkNumber);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected SearchCarPersent oncreatPersenter() {
        return new SearchCarPersent();
    }

    //获取车辆信息
    @Override
    public void getCardInfo(CarModel carModel) {
        loadingDialog.dismiss();
        showCarDialog(carModel.getAddress(), carModel.getPlate_number(), carModel.getPark_number());
    }

    //获取区域
    @Override
    public void getyParkArea(List<String> parkArea) {
        loadingDialog.dismiss();
    }

    //获取车位编号
    @Override
    public void getParkNumber(List<String> parkNumber) {
        loadingDialog.dismiss();
    }

    @Override
    public void getNetError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        loadingDialog.dismiss();
    }
}
