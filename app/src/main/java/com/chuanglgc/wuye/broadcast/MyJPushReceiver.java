package com.chuanglgc.wuye.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.chuanglgc.wuye.activity.DifferentLoginActivity;

import cn.jpush.android.api.JPushInterface;

/**
 * 接受激光推送在这里  处理别处登陆 强制退出或者重新登陆
 */
public class MyJPushReceiver extends BroadcastReceiver {
    private static String TAG = "MyJPushReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        Log.d(TAG, "onReceive - " + intent.getAction());

        if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {
        } else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent
                .getAction())) {
            // 自定义消息不会展示在通知栏，完全要开发者写代码去处理

        } else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent
                .getAction())) {

            JPushInterface.clearAllNotifications(context);
            Intent intent1 = new Intent(context, DifferentLoginActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent1);
            //获取通知信息
            String extra = bundle.getString(JPushInterface.EXTRA_ALERT);
            Log.e(TAG, "onReceive - " + extra);
            // 在这里可以做些统计，或者做些其他工作
        } else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent
                .getAction())) {
            // 在这里可以自己写代码去定义用户点击后的行为
            String extra = bundle.getString(JPushInterface.EXTRA_ALERT);

        } else {
            Log.d(TAG, "Unhandled intent - " + intent.getAction());
        }
    }
}