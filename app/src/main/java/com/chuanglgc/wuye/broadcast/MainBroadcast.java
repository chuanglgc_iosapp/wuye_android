package com.chuanglgc.wuye.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.chuanglgc.wuye.activity.MainActivity;


//用来修改打卡状态
public class MainBroadcast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String isCheck = intent.getStringExtra("isCheck");
        MainActivity mainActivity= (MainActivity) context;
        mainActivity.setCheckText(isCheck);
    }
}
