package com.chuanglgc.wuye.widget;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.util.AttributeSet;



//可以设置上下滑动的GridLayoutManager
public class MyGridViewManager extends GridLayoutManager {
    private boolean isScrollEnabled;


    public MyGridViewManager(Context context, int spanCount) {
        super(context, spanCount);
    }
   /* public MyGridViewManager(Context context, int spanCount) {
        super(context);
        setSpanCount(spanCount);
    }*/
    /**
     * 设置是否可以上下滑动
     * @param flag
     */
    public void setScrollEnabled(boolean flag) {
        this.isScrollEnabled = flag;
    }
    @Override
    public boolean canScrollVertically() {
        return isScrollEnabled && super.canScrollVertically();
    }
}
