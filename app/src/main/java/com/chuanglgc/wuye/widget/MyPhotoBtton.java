package com.chuanglgc.wuye.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chuanglgc.wuye.R;

public class MyPhotoBtton extends RelativeLayout {

    private ImageView icon;
    private TextView tvName;
    private String name;
    private int iconID;
    private RelativeLayout root;
    private int backGround;

    public MyPhotoBtton(Context context) {
        this(context, null);
    }

    public MyPhotoBtton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyPhotoBtton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.MyPhotoBtton, defStyleAttr, 0);
        name = ta.getString(R.styleable.MyPhotoBtton_text);
        iconID = ta.getResourceId(R.styleable.MyPhotoBtton_icon, R.mipmap.oky);
        backGround = ta.getResourceId(R.styleable.MyPhotoBtton_backgroud, R.drawable.bt_search_bg);
        ta.recycle();
        initView(context);
    }

    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.widget_bt, this);
        icon = view.findViewById(R.id.icon);
        tvName = view.findViewById(R.id.tv_name);
        root = view.findViewById(R.id.rl_root);
        if (name == null || TextUtils.isEmpty(name)) {
            tvName.setText("接受");
        } else {
            tvName.setText(name);
        }

        icon.setBackgroundResource(iconID);
        root.setBackgroundResource(backGround);
    }

    public void setTxName(String name) {
        if (tvName != null) tvName.setText(name);
    }

    public String getBtName() {
        return tvName.getText().toString().trim();
    }

    public void setIcon(int resourceID) {
        icon.setBackgroundResource(resourceID);
    }
}
