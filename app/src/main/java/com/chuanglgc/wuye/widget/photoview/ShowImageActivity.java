package com.chuanglgc.wuye.widget.photoview;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;

/**
 * 用于展示大图片的ImageView
 */

public class ShowImageActivity extends Activity {

    private static final String KEY_PHOTO = "key_photo";
    PhotoView photoView;
    private String url = null; //图片的UR

    public static void start(Context context, String url) {
        Intent intent = new Intent(context, ShowImageActivity.class);
        intent.putExtra(KEY_PHOTO, url);
        context.startActivity(intent);
    }


    protected void initVariable() {
        url = getIntent().getStringExtra(KEY_PHOTO);
    }

    protected void initView() {
        photoView = (PhotoView) findViewById(R.id.photoView);
        if (url != null) {
            final Dialog loadingDialog = LoadingDialogUtils.createLoadingDialog(ShowImageActivity.this, "",true);

            Glide.with(this).load(url).error(R.mipmap.repairingbg).into(new GlideDrawableImageViewTarget(photoView) {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                    super.onResourceReady(resource, animation);
                    LoadingDialogUtils.closeDialog(loadingDialog);
                }

                @Override
                public void onStop() {
                    super.onStop();
                }
            });
        } else {
            Glide.with(this).load(R.mipmap.repairingbg).into(photoView);
        }
        photoView.enable();
        photoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowImageActivity.this.finish();
            }
        });
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            this.finish();
        }
        return super.onTouchEvent(event);
    }


    protected int getContentViewId() {
        //取消标题
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //取消状态栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return R.layout.acitivity_show_photo;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentViewId());
        initVariable();
        initView();

    }


}