package com.chuanglgc.wuye.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chuanglgc.wuye.R;


public class MyToolbar extends LinearLayout {

    private String titleName;
    private Toolbar toolbar;
    private TextView tvState;
    private String stateName;
    private int stateColor;

    public MyToolbar(Context context) {
        this(context, null);
    }

    public MyToolbar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.MyToolbar, defStyleAttr, 0);
        titleName = ta.getString(R.styleable.MyToolbar_title);
        stateName = ta.getString(R.styleable.MyToolbar_stateName);
        stateColor = ta.getResourceId(R.styleable.MyToolbar_stateColor, R.color.tran);
        ta.recycle();
        initView(context);
    }

    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.toolbar, this);
        toolbar = view.findViewById(R.id.mytoolbar);
        TextView title = view.findViewById(R.id.tv_title);
        tvState = view.findViewById(R.id.tv_state);

        title.setText(titleName);
        if (stateName != null) tvState.setText(stateName);
        if (stateColor != R.color.tran) tvState.setTextColor(stateColor);
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setTvState(String stateName, int tvColor) {
        if (stateName != null) tvState.setText(stateName);
        tvState.setTextColor(tvColor);
    }
    public TextView getTvState(){
        return tvState;
    }
}
