package com.chuanglgc.wuye.widget;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.utils.LogUtil;


public class AddPhotoView extends RelativeLayout {

    private final Context context;
    public RelativeLayout rl_root;
    private ImageView iv_add;
    private TextView tv_add;
    private ImageView iv_bg;

    public AddPhotoView(Context context) {
        this(context, null);

    }

    public AddPhotoView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AddPhotoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init(context);
    }

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.add_photo_layout, this);
        tv_add = (TextView) view.findViewById(R.id.tv_add_photo);
        rl_root = (RelativeLayout) view.findViewById(R.id.rl_root);
        iv_add = (ImageView) view.findViewById(R.id.iv_add);
        iv_bg = (ImageView) view.findViewById(R.id.iv_bg);

    }


   //设置提交服务器的图片
    public void setPhoto(String url) {
        LogUtil.e("评价路径",url);
        iv_add.setVisibility(GONE);
        tv_add.setVisibility(GONE);
        Glide.with(context).load(url).into(iv_bg);
        iv_bg.setVisibility(VISIBLE);
    }
    public void setPhotoDefalut(){
        iv_add.setVisibility(VISIBLE);
        tv_add.setVisibility(VISIBLE);
        iv_bg.setVisibility(INVISIBLE);

    }
    public Bitmap compressImage(Context context, String path) {
        //压缩参考尺寸(PX)
        int width = iv_bg.getWidth();
        int height = iv_bg.getHeight();
        float hh = height;
        float ww = width;
        //图片宽高
        BitmapFactory.Options options = new BitmapFactory.Options();
        //开始读入图片，此时把options.inJustDecodeBounds 设为true
        options.inJustDecodeBounds = true;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        //根据uri得到图片的具体地址
        //此时返回bm为空
        BitmapFactory.decodeFile(path, options);
        //得到图片的宽高
        float w = options.outWidth;
        float h = options.outHeight;
        //压缩倍率
        float be = 1.0f;
        if (w > h) {
            be = ((w / hh + h / ww) / 2.0f);
        } else {
            be = ((w / ww + h / hh) / 2.0f);
        }
      /*
        if (be <= 1.5f) {
            be = 1;
        }*/
        options.inSampleSize = (int) be;//设置缩放比例
        options.inJustDecodeBounds = false;
        //得到处理后的bitmap并返回
        Bitmap bitmap = BitmapFactory.decodeFile(path, options);
        return bitmap;
    }


    /**
     * 判断是否添加过图片
     *
     * @return
     */
    public boolean getIsHaveAddPhoto() {
        if (iv_add.getVisibility() == View.GONE) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 根据uri获取文件的路径URL
     */
    public String getUrlFromUri(final Context context, final Uri uri) {
        if (null == uri)
            return null;
        final String scheme = uri.getScheme();
        String data = null;
        if (scheme == null)
            data = uri.getPath();
        else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
            data = uri.getPath();
        } else if (ContentResolver.SCHEME_CONTENT.equals(scheme)) {
            Cursor cursor = context.getContentResolver().query(uri,
                    new String[]{MediaStore.Images.ImageColumns.DATA}, null, null, null);
            if (null != cursor) {
                if (cursor.moveToFirst()) {
                    int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    if (index > -1) {
                        data = cursor.getString(index);
                    }
                }
                cursor.close();
            }
        }
        return data;
    }

}
