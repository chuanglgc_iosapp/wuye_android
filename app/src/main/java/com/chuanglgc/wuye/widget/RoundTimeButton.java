package com.chuanglgc.wuye.widget;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chuanglgc.wuye.R;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2018/3/24.
 */

public class RoundTimeButton extends RelativeLayout {
    /**
     * 更新显示的文本
     */
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
            Date curDate = new Date(System.currentTimeMillis());
            String str = formatter.format(curDate);
            if (tvTime != null) tvTime.setText(str);
            handler.sendEmptyMessageDelayed(0,1000);

        }
    };
    private TextView tvClass;
    private TextView tvTime;

    public RoundTimeButton(Context context) {
        this(context, null);
    }

    public RoundTimeButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoundTimeButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.widget_roundtime_bt, this);
        tvClass = view.findViewById(R.id.tv_class);
        tvTime = view.findViewById(R.id.tv_time);
        handler.sendEmptyMessageDelayed(0, 0);
    }

    public void setTvClass(String className) {
        tvClass.setText(className);
    }

    /**
     * 退出时候调用
     */
    public void removeHandler() {
        handler.removeCallbacksAndMessages(null);
        handler = null;
    }
}
