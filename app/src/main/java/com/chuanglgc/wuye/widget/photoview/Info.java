package com.chuanglgc.wuye.widget.photoview;

import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

/**
 * 获取图片的信息（位置和窗口）
 *
 * @author YangCihang
 * @since 17/8/19.
 * email yangcihang@hrsoft.net
 */

public class Info implements Parcelable {

    // 内部图片在整个手机界面的位置
    RectF mRect = new RectF();

    // 控件在窗口的位置
    RectF mImgRect = new RectF();

    RectF mWidgetRect = new RectF();

    RectF mBaseRect = new RectF();

    PointF mScreenCenter = new PointF();

    float mScale;

    float mDegrees;

    ImageView.ScaleType mScaleType;

    public Info(RectF rect, RectF img, RectF widget, RectF base, PointF screenCenter,
                float scale, float degrees, ImageView.ScaleType scaleType) {
        mRect.set(rect);
        mImgRect.set(img);
        mWidgetRect.set(widget);
        mBaseRect.set(base);
        mScreenCenter.set(screenCenter);
        mScale = scale;
        mDegrees = degrees;
        mScaleType = scaleType;
    }

    public Info(Parcel source) {
        mBaseRect = (RectF) source.readValue(RectF.class.getClassLoader());
        mRect = (RectF) source.readValue(RectF.class.getClassLoader());
        mImgRect = (RectF) source.readValue(RectF.class.getClassLoader());
        mWidgetRect = (RectF) source.readValue(RectF.class.getClassLoader());
        mScreenCenter = (PointF) source.readValue(PointF.class.getClassLoader());
        mScale = source.readFloat();
        mDegrees = source.readFloat();
        mScaleType = ImageView.ScaleType.values()[source.readInt()];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(mRect);
        dest.writeValue(mImgRect);
        dest.writeValue(mWidgetRect);
        dest.writeValue(mBaseRect);
        dest.writeValue(mScreenCenter);
        dest.writeValue(mScale);
        dest.writeValue(mDegrees);
        dest.writeValue(mScaleType);
    }

    public static final Creator<Info> CREATOR = new Creator<Info>() {
        @Override
        public Info createFromParcel(Parcel source) {
            return new Info(source);
        }

        @Override
        public Info[] newArray(int size) {
            return new Info[size];
        }
    };
}
