package com.chuanglgc.wuye.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.media.Image;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.utils.LogUtil;


public class AttendanceUpDownView extends LinearLayout{

    private String title;
    private String count;
    private TextView tvCount;
    private ImageView ivUpDown;
    private int tvCountColor;

    public AttendanceUpDownView(Context context) {
        this(context,null);
    }

    public AttendanceUpDownView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public AttendanceUpDownView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.AttendanceUpDownView, defStyleAttr, 0);
        title = ta.getString(R.styleable.AttendanceUpDownView_titlename);
        count = ta.getString(R.styleable.AttendanceUpDownView_count);
        tvCountColor = ta.getColor(R.styleable.AttendanceUpDownView_countColor,getResources().getColor(R.color.textnormal));

        ta.recycle();
        initView(context);
    }

    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.widget_attendance_updown, this);
        TextView tvTitle = view.findViewById(R.id.tv_title);
        tvCount = view.findViewById(R.id.tv_count);
        ivUpDown = view.findViewById(R.id.iv_updown);
        tvCount = view.findViewById(R.id.tv_count);
        tvCount.setTextColor(tvCountColor);
        if (title!=null)tvTitle.setText(title);
        if (count!=null)tvTitle.setText(count);
        LogUtil.e("yanse",tvCountColor);

    }

    public void  setCount(String count){
        if (count!=null)tvCount.setText(count);
    }
    /**
     * 设置选择器
     * 返回设置后的状态
     */
    public boolean setUpDown(){
        ivUpDown.setSelected(!ivUpDown.isSelected());
        return ivUpDown.isSelected();
    }
    public  void setSelectFalse(){
        ivUpDown.setSelected(false);
    }
}
