package com.chuanglgc.wuye.model;


public class CheckModel {


    /**
     * schedule_name : 上班时间
     * define_working_time : 08:00
     * actual_working_time : 10:02
     * check_status : 迟到
     * CK8000 : 524
     */

    private String schedule_name;
    private String define_working_time;
    private String actual_working_time;
    private String check_status;
    private String CK8000;

    public String getSchedule_name() {
        return schedule_name;
    }

    public void setSchedule_name(String schedule_name) {
        this.schedule_name = schedule_name;
    }

    public String getDefine_working_time() {
        return define_working_time;
    }

    public void setDefine_working_time(String define_working_time) {
        this.define_working_time = define_working_time;
    }

    public String getActual_working_time() {
        return actual_working_time;
    }

    public void setActual_working_time(String actual_working_time) {
        this.actual_working_time = actual_working_time;
    }

    public String getCheck_status() {
        return check_status;
    }

    public void setCheck_status(String check_status) {
        this.check_status = check_status;
    }

    public String getCK8000() {
        return CK8000;
    }

    public void setCK8000(String CK8000) {
        this.CK8000 = CK8000;
    }
}
