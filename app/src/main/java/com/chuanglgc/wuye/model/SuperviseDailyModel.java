package com.chuanglgc.wuye.model;


public class SuperviseDailyModel {


    /**
     * daily_task_id : 1644
     * work_content : 小区卫生
     * ship_id : 238
     * ship_name : 谢娜
     * work_status : 待评价
     */

    private String daily_task_id;
    private String work_content;
    private String ship_id;
    private String ship_name;
    private String work_status;

    public String getDaily_task_id() {
        return daily_task_id;
    }

    public void setDaily_task_id(String daily_task_id) {
        this.daily_task_id = daily_task_id;
    }

    public String getWork_content() {
        return work_content;
    }

    public void setWork_content(String work_content) {
        this.work_content = work_content;
    }

    public String getShip_id() {
        return ship_id;
    }

    public void setShip_id(String ship_id) {
        this.ship_id = ship_id;
    }

    public String getShip_name() {
        return ship_name;
    }

    public void setShip_name(String ship_name) {
        this.ship_name = ship_name;
    }

    public String getWork_status() {
        return work_status;
    }

    public void setWork_status(String work_status) {
        this.work_status = work_status;
    }
}
