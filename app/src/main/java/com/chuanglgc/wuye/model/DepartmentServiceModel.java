package com.chuanglgc.wuye.model;

public class DepartmentServiceModel {


    /**
     * service_provider_id : 1
     * service_provider_name : 保安
     */

    private String service_provider_id;
    private String service_provider_name;

    public String getService_provider_id() {
        return service_provider_id;
    }

    public void setService_provider_id(String service_provider_id) {
        this.service_provider_id = service_provider_id;
    }

    public String getService_provider_name() {
        return service_provider_name;
    }

    public void setService_provider_name(String service_provider_name) {
        this.service_provider_name = service_provider_name;
    }
}
