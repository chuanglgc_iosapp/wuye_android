package com.chuanglgc.wuye.model;


import java.util.List;

public class SuperviseOwnerDetailModel {

    /**
     * eventdetails : {"task_id":"295","repair_id":"168","repair_task_id":"91","repair_task_name":"房屋主体","repair_content":"","CK3204":"2","task_status":"未接受","ship_id":"238","ship_name":"谢娜","assistant_person":null,"task_image1":null,"task_image2":null,"task_image3":null,"repair_task_date":"2018.01.19 11:17","contact_person_name":"玛丽","contact_person_tel":"13520660000","contact_person_address":"1-1-101","work_status":"处理中"}
     * feedback : [{"CK3300":"21","CK3304":"2","feedback_date":"2018.04.03","feedback_time":"11:49","feedback_content":"","contact_person_name":"玛丽","contact_person_tel":"13520660000","contact_person_address":"1-1-101","visit_status":"处理中"},{"CK3300":"48","CK3304":"2","feedback_date":"2018.04.08","feedback_time":"10:46","feedback_content":"AAXA","contact_person_name":"玛丽","contact_person_tel":"13520660000","contact_person_address":"1-1-101","visit_status":"处理中"}]
     */

    private EventdetailsBean eventdetails;
    private List<FeedbackBean> feedback;

    public EventdetailsBean getEventdetails() {
        return eventdetails;
    }

    public void setEventdetails(EventdetailsBean eventdetails) {
        this.eventdetails = eventdetails;
    }

    public List<FeedbackBean> getFeedback() {
        return feedback;
    }

    public void setFeedback(List<FeedbackBean> feedback) {
        this.feedback = feedback;
    }

    public static class EventdetailsBean {
        /**
         * task_id : 295
         * repair_id : 168
         * repair_task_id : 91
         * repair_task_name : 房屋主体
         * repair_content : 
         * CK3204 : 2
         * task_status : 未接受
         * ship_id : 238
         * ship_name : 谢娜
         * assistant_person : null
         * task_image1 : null
         * task_image2 : null
         * task_image3 : null
         * repair_task_date : 2018.01.19 11:17
         * contact_person_name : 玛丽
         * contact_person_tel : 13520660000
         * contact_person_address : 1-1-101
         * work_status : 处理中
         */

        private String task_id;
        private String repair_id;
        private String repair_task_id;
        private String repair_task_name;
        private String repair_content;
        private String CK3204;
        private String task_status;
        private String ship_id;
        private String ship_name;
        private String assistant_person;
        private String task_image1;
        private String task_image2;
        private String task_image3;
        private String repair_task_date;
        private String contact_person_name;
        private String contact_person_tel;
        private String contact_person_address;
        private String work_status;

        public String getTask_id() {
            return task_id;
        }

        public void setTask_id(String task_id) {
            this.task_id = task_id;
        }

        public String getRepair_id() {
            return repair_id;
        }

        public void setRepair_id(String repair_id) {
            this.repair_id = repair_id;
        }

        public String getRepair_task_id() {
            return repair_task_id;
        }

        public void setRepair_task_id(String repair_task_id) {
            this.repair_task_id = repair_task_id;
        }

        public String getRepair_task_name() {
            return repair_task_name;
        }

        public void setRepair_task_name(String repair_task_name) {
            this.repair_task_name = repair_task_name;
        }

        public String getRepair_content() {
            return repair_content;
        }

        public void setRepair_content(String repair_content) {
            this.repair_content = repair_content;
        }

        public String getCK3204() {
            return CK3204;
        }

        public void setCK3204(String CK3204) {
            this.CK3204 = CK3204;
        }

        public String getTask_status() {
            return task_status;
        }

        public void setTask_status(String task_status) {
            this.task_status = task_status;
        }

        public String getShip_id() {
            return ship_id;
        }

        public void setShip_id(String ship_id) {
            this.ship_id = ship_id;
        }

        public String getShip_name() {
            return ship_name;
        }

        public void setShip_name(String ship_name) {
            this.ship_name = ship_name;
        }

        public String getAssistant_person() {
            return assistant_person;
        }

        public void setAssistant_person(String assistant_person) {
            this.assistant_person = assistant_person;
        }

        public String getTask_image1() {
            return task_image1;
        }

        public void setTask_image1(String task_image1) {
            this.task_image1 = task_image1;
        }

        public String getTask_image2() {
            return task_image2;
        }

        public void setTask_image2(String task_image2) {
            this.task_image2 = task_image2;
        }

        public String getTask_image3() {
            return task_image3;
        }

        public void setTask_image3(String task_image3) {
            this.task_image3 = task_image3;
        }

        public String getRepair_task_date() {
            return repair_task_date;
        }

        public void setRepair_task_date(String repair_task_date) {
            this.repair_task_date = repair_task_date;
        }

        public String getContact_person_name() {
            return contact_person_name;
        }

        public void setContact_person_name(String contact_person_name) {
            this.contact_person_name = contact_person_name;
        }

        public String getContact_person_tel() {
            return contact_person_tel;
        }

        public void setContact_person_tel(String contact_person_tel) {
            this.contact_person_tel = contact_person_tel;
        }

        public String getContact_person_address() {
            return contact_person_address;
        }

        public void setContact_person_address(String contact_person_address) {
            this.contact_person_address = contact_person_address;
        }

        public String getWork_status() {
            return work_status;
        }

        public void setWork_status(String work_status) {
            this.work_status = work_status;
        }
    }

    public static class FeedbackBean {
        /**
         * CK3300 : 21
         * CK3304 : 2
         * feedback_date : 2018.04.03
         * feedback_time : 11:49
         * feedback_content : 
         * contact_person_name : 玛丽
         * contact_person_tel : 13520660000
         * contact_person_address : 1-1-101
         * visit_status : 处理中
         */

        private String CK3300;
        private String CK3304;
        private String feedback_date;
        private String feedback_time;
        private String feedback_content;
        private String contact_person_name;
        private String contact_person_tel;
        private String contact_person_address;
        private String visit_status;

        public String getCK3300() {
            return CK3300;
        }

        public void setCK3300(String CK3300) {
            this.CK3300 = CK3300;
        }

        public String getCK3304() {
            return CK3304;
        }

        public void setCK3304(String CK3304) {
            this.CK3304 = CK3304;
        }

        public String getFeedback_date() {
            return feedback_date;
        }

        public void setFeedback_date(String feedback_date) {
            this.feedback_date = feedback_date;
        }

        public String getFeedback_time() {
            return feedback_time;
        }

        public void setFeedback_time(String feedback_time) {
            this.feedback_time = feedback_time;
        }

        public String getFeedback_content() {
            return feedback_content;
        }

        public void setFeedback_content(String feedback_content) {
            this.feedback_content = feedback_content;
        }

        public String getContact_person_name() {
            return contact_person_name;
        }

        public void setContact_person_name(String contact_person_name) {
            this.contact_person_name = contact_person_name;
        }

        public String getContact_person_tel() {
            return contact_person_tel;
        }

        public void setContact_person_tel(String contact_person_tel) {
            this.contact_person_tel = contact_person_tel;
        }

        public String getContact_person_address() {
            return contact_person_address;
        }

        public void setContact_person_address(String contact_person_address) {
            this.contact_person_address = contact_person_address;
        }

        public String getVisit_status() {
            return visit_status;
        }

        public void setVisit_status(String visit_status) {
            this.visit_status = visit_status;
        }
    }
}
