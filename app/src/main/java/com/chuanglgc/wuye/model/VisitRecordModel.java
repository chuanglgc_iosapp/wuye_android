package com.chuanglgc.wuye.model;

public class VisitRecordModel {


    /**
     * feedback_date : 2018.04.02
     * feedback_time : 15:19
     * contact_person : 李小红
     * feedback_content : 开始解决当前业主事件。
     * contact_info : 1-1-101|13031300123
     * leading_person : 黄渤
     * department_name : (维修)
     */

    private String feedback_date;
    private String feedback_time;
    private String contact_person;
    private String feedback_content;
    private String contact_info;
    private String leading_person;
    private String department_name;

    public String getFeedback_date() {
        return feedback_date;
    }

    public void setFeedback_date(String feedback_date) {
        this.feedback_date = feedback_date;
    }

    public String getFeedback_time() {
        return feedback_time;
    }

    public void setFeedback_time(String feedback_time) {
        this.feedback_time = feedback_time;
    }

    public String getContact_person() {
        return contact_person;
    }

    public void setContact_person(String contact_person) {
        this.contact_person = contact_person;
    }

    public String getFeedback_content() {
        return feedback_content;
    }

    public void setFeedback_content(String feedback_content) {
        this.feedback_content = feedback_content;
    }

    public String getContact_info() {
        return contact_info;
    }

    public void setContact_info(String contact_info) {
        this.contact_info = contact_info;
    }

    public String getLeading_person() {
        return leading_person;
    }

    public void setLeading_person(String leading_person) {
        this.leading_person = leading_person;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }
}
