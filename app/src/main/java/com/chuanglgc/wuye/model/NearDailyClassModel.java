package com.chuanglgc.wuye.model;

public class NearDailyClassModel {
    /**
     * classes_id : 165
     * classes_name : 维修上午 (08:00-12:00)
     */

    private String classes_id;
    private String classes_name;

    public String getClasses_id() {
        return classes_id;
    }

    public void setClasses_id(String classes_id) {
        this.classes_id = classes_id;
    }

    public String getClasses_name() {
        return classes_name;
    }

    public void setClasses_name(String classes_name) {
        this.classes_name = classes_name;
    }
}
