package com.chuanglgc.wuye.model;



public class DailyPhotoModel {

    /**
     * http_host : http://localhost
     * image_url : /uploadpic/cms2.0/daily/DAA010101/thumb_daily_234_2188_1522811958.png
     * time : 2018-04-04 11:19:18
     */

    private String http_host;
    private String image_url;
    private String time;

    public String getHttp_host() {
        return http_host;
    }

    public void setHttp_host(String http_host) {
        this.http_host = http_host;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
