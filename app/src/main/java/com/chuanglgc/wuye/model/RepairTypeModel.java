package com.chuanglgc.wuye.model;


public class RepairTypeModel {

    /**
     * repair_type_id : 91
     * repair_type_name : 房屋主体
     */

    private String repair_type_id;
    private String repair_type_name;

    public String getRepair_type_id() {
        return repair_type_id;
    }

    public void setRepair_type_id(String repair_type_id) {
        this.repair_type_id = repair_type_id;
    }

    public String getRepair_type_name() {
        return repair_type_name;
    }

    public void setRepair_type_name(String repair_type_name) {
        this.repair_type_name = repair_type_name;
    }
}
