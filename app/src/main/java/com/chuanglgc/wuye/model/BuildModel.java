package com.chuanglgc.wuye.model;


public class BuildModel {

    /**
     * build_id : 1
     * build_name : 1栋
     */

    private String build_id;
    private String build_name;

    public String getBuild_id() {
        return build_id;
    }

    public void setBuild_id(String build_id) {
        this.build_id = build_id;
    }

    public String getBuild_name() {
        return build_name;
    }

    public void setBuild_name(String build_name) {
        this.build_name = build_name;
    }
}
