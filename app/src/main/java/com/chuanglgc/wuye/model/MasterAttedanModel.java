package com.chuanglgc.wuye.model;

import java.util.List;

public class MasterAttedanModel {


    /**
     * attendance : {"attendance_totalperson":3,"attendance_empname":[{"emp_id":"238","emp_name":"谢娜","info":"维修下午"},{"emp_id":"239","emp_name":"武勋","info":"维修上午"},{"emp_id":"240","emp_name":"海涛","info":"水表抄表"}]}
     * shift : {"shift_totalcount":3,"shift_time":[{"shift_id":"170","shift_name":"水表抄表(08:00-18:00)","shift_empnumber":"1"},{"shift_id":"165","shift_name":"维修上午(08:00-12:00)","shift_empnumber":"1"},{"shift_id":"166","shift_name":"维修下午(13:30-18:00)","shift_empnumber":"1"}]}
     * late : {"late_totalcount":2,"late_info":[{"punch_time":"08:29","emp_id":"240","emp_name":"海涛","shift_id":"170","shift_name":"水表抄表(08:00-18:00)","timediff":"29"},{"punch_time":"08:31","emp_id":"239","emp_name":"武勋","shift_id":"165","shift_name":"维修上午(08:00-12:00)","timediff":"31"}]}
     * leave_early : {"early_total":2,"early_info":[{"punch_time":"15:19","emp_id":"240","emp_name":"海涛","shift_id":"170","shift_name":"水表抄表(08:00-18:00)","timediff":"161"},{"punch_time":"15:19","emp_id":"238","emp_name":"谢娜","shift_id":"166","shift_name":"维修下午(13:30-18:00)","timediff":"161"}]}
     */

    private AttendanceBean attendance;
    private ShiftBean shift;
    private LateBean late;
    private LeaveEarlyBean leave_early;

    public AttendanceBean getAttendance() {
        return attendance;
    }

    public void setAttendance(AttendanceBean attendance) {
        this.attendance = attendance;
    }

    public ShiftBean getShift() {
        return shift;
    }

    public void setShift(ShiftBean shift) {
        this.shift = shift;
    }

    public LateBean getLate() {
        return late;
    }

    public void setLate(LateBean late) {
        this.late = late;
    }

    public LeaveEarlyBean getLeave_early() {
        return leave_early;
    }

    public void setLeave_early(LeaveEarlyBean leave_early) {
        this.leave_early = leave_early;
    }

    public static class AttendanceBean {
        /**
         * attendance_totalperson : 3
         * attendance_empname : [{"emp_id":"238","emp_name":"谢娜","info":"维修下午"},{"emp_id":"239","emp_name":"武勋","info":"维修上午"},{"emp_id":"240","emp_name":"海涛","info":"水表抄表"}]
         */

        private int attendance_totalperson;
        private List<AttendanceEmpnameBean> attendance_empname;

        public int getAttendance_totalperson() {
            return attendance_totalperson;
        }

        public void setAttendance_totalperson(int attendance_totalperson) {
            this.attendance_totalperson = attendance_totalperson;
        }

        public List<AttendanceEmpnameBean> getAttendance_empname() {
            return attendance_empname;
        }

        public void setAttendance_empname(List<AttendanceEmpnameBean> attendance_empname) {
            this.attendance_empname = attendance_empname;
        }

        public static class AttendanceEmpnameBean {
            /**
             * emp_id : 238
             * emp_name : 谢娜
             * info : 维修下午
             */

            private String emp_id;
            private String emp_name;
            private String info;

            public String getEmp_id() {
                return emp_id;
            }

            public void setEmp_id(String emp_id) {
                this.emp_id = emp_id;
            }

            public String getEmp_name() {
                return emp_name;
            }

            public void setEmp_name(String emp_name) {
                this.emp_name = emp_name;
            }

            public String getInfo() {
                return info;
            }

            public void setInfo(String info) {
                this.info = info;
            }
        }
    }

    public static class ShiftBean {
        /**
         * shift_totalcount : 3
         * shift_time : [{"shift_id":"170","shift_name":"水表抄表(08:00-18:00)","shift_empnumber":"1"},{"shift_id":"165","shift_name":"维修上午(08:00-12:00)","shift_empnumber":"1"},{"shift_id":"166","shift_name":"维修下午(13:30-18:00)","shift_empnumber":"1"}]
         */

        private int shift_totalcount;
        private List<ShiftTimeBean> shift_time;

        public int getShift_totalcount() {
            return shift_totalcount;
        }

        public void setShift_totalcount(int shift_totalcount) {
            this.shift_totalcount = shift_totalcount;
        }

        public List<ShiftTimeBean> getShift_time() {
            return shift_time;
        }

        public void setShift_time(List<ShiftTimeBean> shift_time) {
            this.shift_time = shift_time;
        }

        public static class ShiftTimeBean {
            /**
             * shift_id : 170
             * shift_name : 水表抄表(08:00-18:00)
             * shift_empnumber : 1
             */

            private String shift_id;
            private String shift_name;
            private String shift_empnumber;

            public String getShift_id() {
                return shift_id;
            }

            public void setShift_id(String shift_id) {
                this.shift_id = shift_id;
            }

            public String getShift_name() {
                return shift_name;
            }

            public void setShift_name(String shift_name) {
                this.shift_name = shift_name;
            }

            public String getShift_empnumber() {
                return shift_empnumber;
            }

            public void setShift_empnumber(String shift_empnumber) {
                this.shift_empnumber = shift_empnumber;
            }
        }
    }

    public static class LateBean {
        /**
         * late_totalcount : 2
         * late_info : [{"punch_time":"08:29","emp_id":"240","emp_name":"海涛","shift_id":"170","shift_name":"水表抄表(08:00-18:00)","timediff":"29"},{"punch_time":"08:31","emp_id":"239","emp_name":"武勋","shift_id":"165","shift_name":"维修上午(08:00-12:00)","timediff":"31"}]
         */

        private int late_totalcount;
        private List<LateInfoBean> late_info;

        public int getLate_totalcount() {
            return late_totalcount;
        }

        public void setLate_totalcount(int late_totalcount) {
            this.late_totalcount = late_totalcount;
        }

        public List<LateInfoBean> getLate_info() {
            return late_info;
        }

        public void setLate_info(List<LateInfoBean> late_info) {
            this.late_info = late_info;
        }

        public static class LateInfoBean {
            /**
             * punch_time : 08:29
             * emp_id : 240
             * emp_name : 海涛
             * shift_id : 170
             * shift_name : 水表抄表(08:00-18:00)
             * timediff : 29
             */

            private String punch_time;
            private String emp_id;
            private String emp_name;
            private String shift_id;
            private String shift_name;
            private String timediff;

            public String getPunch_time() {
                return punch_time;
            }

            public void setPunch_time(String punch_time) {
                this.punch_time = punch_time;
            }

            public String getEmp_id() {
                return emp_id;
            }

            public void setEmp_id(String emp_id) {
                this.emp_id = emp_id;
            }

            public String getEmp_name() {
                return emp_name;
            }

            public void setEmp_name(String emp_name) {
                this.emp_name = emp_name;
            }

            public String getShift_id() {
                return shift_id;
            }

            public void setShift_id(String shift_id) {
                this.shift_id = shift_id;
            }

            public String getShift_name() {
                return shift_name;
            }

            public void setShift_name(String shift_name) {
                this.shift_name = shift_name;
            }

            public String getTimediff() {
                return timediff;
            }

            public void setTimediff(String timediff) {
                this.timediff = timediff;
            }
        }
    }

    public static class LeaveEarlyBean {
        /**
         * early_total : 2
         * early_info : [{"punch_time":"15:19","emp_id":"240","emp_name":"海涛","shift_id":"170","shift_name":"水表抄表(08:00-18:00)","timediff":"161"},{"punch_time":"15:19","emp_id":"238","emp_name":"谢娜","shift_id":"166","shift_name":"维修下午(13:30-18:00)","timediff":"161"}]
         */

        private int early_total;
        private List<EarlyInfoBean> early_info;

        public int getEarly_total() {
            return early_total;
        }

        public void setEarly_total(int early_total) {
            this.early_total = early_total;
        }

        public List<EarlyInfoBean> getEarly_info() {
            return early_info;
        }

        public void setEarly_info(List<EarlyInfoBean> early_info) {
            this.early_info = early_info;
        }

        public static class EarlyInfoBean {
            /**
             * punch_time : 15:19
             * emp_id : 240
             * emp_name : 海涛
             * shift_id : 170
             * shift_name : 水表抄表(08:00-18:00)
             * timediff : 161
             */

            private String punch_time;
            private String emp_id;
            private String emp_name;
            private String shift_id;
            private String shift_name;
            private String timediff;

            public String getPunch_time() {
                return punch_time;
            }

            public void setPunch_time(String punch_time) {
                this.punch_time = punch_time;
            }

            public String getEmp_id() {
                return emp_id;
            }

            public void setEmp_id(String emp_id) {
                this.emp_id = emp_id;
            }

            public String getEmp_name() {
                return emp_name;
            }

            public void setEmp_name(String emp_name) {
                this.emp_name = emp_name;
            }

            public String getShift_id() {
                return shift_id;
            }

            public void setShift_id(String shift_id) {
                this.shift_id = shift_id;
            }

            public String getShift_name() {
                return shift_name;
            }

            public void setShift_name(String shift_name) {
                this.shift_name = shift_name;
            }

            public String getTimediff() {
                return timediff;
            }

            public void setTimediff(String timediff) {
                this.timediff = timediff;
            }
        }
    }
}
