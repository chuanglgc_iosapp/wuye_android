package com.chuanglgc.wuye.model;

public class MessageModel {


    /**
     * CY0200 : 162
     * CY0201 : 催费关注
     * CY0202 : 您好，1-1-101住户于2018-05-31 14:18:30通过道闸
     * CY0203 : 9
     * CY0205 : 2018.06.30
     * CY0206 : 2018.05.31
     * community_name : 达润.上邦物业
     */

    private String CY0200;
    private String CY0201;
    private String CY0202;
    private String CY0203;
    private String CY0205;
    private String CY0206;
    private String community_name;

    public String getCY0200() {
        return CY0200;
    }

    public void setCY0200(String CY0200) {
        this.CY0200 = CY0200;
    }

    public String getCY0201() {
        return CY0201;
    }

    public void setCY0201(String CY0201) {
        this.CY0201 = CY0201;
    }

    public String getCY0202() {
        return CY0202;
    }

    public void setCY0202(String CY0202) {
        this.CY0202 = CY0202;
    }

    public String getCY0203() {
        return CY0203;
    }

    public void setCY0203(String CY0203) {
        this.CY0203 = CY0203;
    }

    public String getCY0205() {
        return CY0205;
    }

    public void setCY0205(String CY0205) {
        this.CY0205 = CY0205;
    }

    public String getCY0206() {
        return CY0206;
    }

    public void setCY0206(String CY0206) {
        this.CY0206 = CY0206;
    }

    public String getCommunity_name() {
        return community_name;
    }

    public void setCommunity_name(String community_name) {
        this.community_name = community_name;
    }
}
