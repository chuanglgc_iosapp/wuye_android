package com.chuanglgc.wuye.model;

import java.io.Serializable;
import java.util.List;


public class LoginModel implements Serializable{


    /**
     * emp_id : 234
     * emp_number : 00000009
     * emp_name : 黄渤
     * department_id : 3
     * department_name : 维修
     * phone_number : 18712733333
     * community_id : DAA010101
     * community_name : 白塔岭新区
     * clock_status : 未打卡
     * image_url : http://192.168.0.210/uploadpic/cms2.0/avatar/thumb_emp_234.png
     * device_manufacturer : 123456
     * device_uuid : 123456
     * device_platform : Android
     * menu : [{"AA2100":"7001","AA2101":"业主事件"},{"AA2100":"7002","AA2101":"事件管理"},{"AA2100":"7003","AA2101":"临时分配"},{"AA2100":"7004","AA2101":"周期事务"},{"AA2100":"7005","AA2101":"监督评价"},{"AA2100":"7006","AA2101":"消息通知"},{"AA2100":"7007","AA2101":"户主查询"},{"AA2100":"7008","AA2101":"车辆查询"},{"AA2100":"7009","AA2101":"车辆放行"},{"AA2100":"7010","AA2101":"考勤打卡"},{"AA2100":"7011","AA2101":"系统设置"},{"AA2100":"7012","AA2101":"每日工作"},{"AA2100":"7013","AA2101":"员工考勤"},{"AA2100":"7014","AA2101":"事务处理"}]
     */

    private String emp_id;
    private String emp_number;
    private String emp_name;
    private String department_id;
    private String department_name;
    private String phone_number;
    private String community_id;
    private String community_name;
    private String clock_status;
    private String image_url;
    private String device_manufacturer;
    private String device_uuid;
    private String device_platform;
    private List<MenuBean> menu;

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getEmp_number() {
        return emp_number;
    }

    public void setEmp_number(String emp_number) {
        this.emp_number = emp_number;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }

    public String getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(String department_id) {
        this.department_id = department_id;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getCommunity_id() {
        return community_id;
    }

    public void setCommunity_id(String community_id) {
        this.community_id = community_id;
    }

    public String getCommunity_name() {
        return community_name;
    }

    public void setCommunity_name(String community_name) {
        this.community_name = community_name;
    }

    public String getClock_status() {
        return clock_status;
    }

    public void setClock_status(String clock_status) {
        this.clock_status = clock_status;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getDevice_manufacturer() {
        return device_manufacturer;
    }

    public void setDevice_manufacturer(String device_manufacturer) {
        this.device_manufacturer = device_manufacturer;
    }

    public String getDevice_uuid() {
        return device_uuid;
    }

    public void setDevice_uuid(String device_uuid) {
        this.device_uuid = device_uuid;
    }

    public String getDevice_platform() {
        return device_platform;
    }

    public void setDevice_platform(String device_platform) {
        this.device_platform = device_platform;
    }

    public List<MenuBean> getMenu() {
        return menu;
    }

    public void setMenu(List<MenuBean> menu) {
        this.menu = menu;
    }

    public static class MenuBean implements Serializable {
        /**
         * AA2100 : 7001
         * AA2101 : 业主事件
         */

        private String AA2100;
        private String AA2101;

        public String getAA2100() {
            return AA2100;
        }

        public void setAA2100(String AA2100) {
            this.AA2100 = AA2100;
        }

        public String getAA2101() {
            return AA2101;
        }

        public void setAA2101(String AA2101) {
            this.AA2101 = AA2101;
        }
    }
}
