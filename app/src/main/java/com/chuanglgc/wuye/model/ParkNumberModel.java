package com.chuanglgc.wuye.model;



public class ParkNumberModel {

    /**
     * park_num : 1031
     */

    private String park_num;

    public String getPark_num() {
        return park_num;
    }

    public void setPark_num(String park_num) {
        this.park_num = park_num;
    }
}
