package com.chuanglgc.wuye.model;


public class PostDailyPhotoModel {
    private String image_url;
    private String time;

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
