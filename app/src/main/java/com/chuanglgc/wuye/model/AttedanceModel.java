package com.chuanglgc.wuye.model;

import java.util.List;



public class AttedanceModel {


    /**
     * schedule_id : 170
     * schedule_name : 水表抄表(08:00-18:00)
     * is_cross_date : 1
     * define_working_time : 08:00
     * define_closing_time : 18:00
     * schedule_check : [{"schedule_name":"上班时间","define_working_time":"08:00","actual_working_time":"10:33","check_status":"迟到","CK8000":"20"}]
     * check_button_flag : true
     * clock_status : 已打卡
     */

    private String schedule_id;
    private String schedule_name;
    private String is_cross_date;
    private String define_working_time;
    private String define_closing_time;
    private boolean check_button_flag;
    private String clock_status;
    private List<ScheduleCheckBean> schedule_check;

    public String getSchedule_id() {
        return schedule_id;
    }

    public void setSchedule_id(String schedule_id) {
        this.schedule_id = schedule_id;
    }

    public String getSchedule_name() {
        return schedule_name;
    }

    public void setSchedule_name(String schedule_name) {
        this.schedule_name = schedule_name;
    }

    public String getIs_cross_date() {
        return is_cross_date;
    }

    public void setIs_cross_date(String is_cross_date) {
        this.is_cross_date = is_cross_date;
    }

    public String getDefine_working_time() {
        return define_working_time;
    }

    public void setDefine_working_time(String define_working_time) {
        this.define_working_time = define_working_time;
    }

    public String getDefine_closing_time() {
        return define_closing_time;
    }

    public void setDefine_closing_time(String define_closing_time) {
        this.define_closing_time = define_closing_time;
    }

    public boolean isCheck_button_flag() {
        return check_button_flag;
    }

    public void setCheck_button_flag(boolean check_button_flag) {
        this.check_button_flag = check_button_flag;
    }

    public String getClock_status() {
        return clock_status;
    }

    public void setClock_status(String clock_status) {
        this.clock_status = clock_status;
    }

    public List<ScheduleCheckBean> getSchedule_check() {
        return schedule_check;
    }

    public void setSchedule_check(List<ScheduleCheckBean> schedule_check) {
        this.schedule_check = schedule_check;
    }

    public static class ScheduleCheckBean {
        /**
         * schedule_name : 上班时间
         * define_working_time : 08:00
         * actual_working_time : 10:33
         * check_status : 迟到
         * CK8000 : 20
         */

        private String schedule_name;
        private String define_working_time;
        private String actual_working_time;
        private String check_status;
        private String CK8000;

        public String getSchedule_name() {
            return schedule_name;
        }

        public void setSchedule_name(String schedule_name) {
            this.schedule_name = schedule_name;
        }

        public String getDefine_working_time() {
            return define_working_time;
        }

        public void setDefine_working_time(String define_working_time) {
            this.define_working_time = define_working_time;
        }

        public String getActual_working_time() {
            return actual_working_time;
        }

        public void setActual_working_time(String actual_working_time) {
            this.actual_working_time = actual_working_time;
        }

        public String getCheck_status() {
            return check_status;
        }

        public void setCheck_status(String check_status) {
            this.check_status = check_status;
        }

        public String getCK8000() {
            return CK8000;
        }

        public void setCK8000(String CK8000) {
            this.CK8000 = CK8000;
        }
    }
}
