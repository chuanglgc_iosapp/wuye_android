package com.chuanglgc.wuye.model;


import java.io.Serializable;

public class CarVisitorModel implements Serializable {

    /**
     * visitor_id : 3
     * plate_number : 冀C44444
     * visit_address : 1-1-101
     * entry_date : 2018.03.30
     * entry_time : 16:22
     * visitor_name : 王宝青
     * visitor_phone : 13633062515
     * remark : 三男两女,特斯拉911
     */

    private String visitor_id;
    private String plate_number;
    private String visit_address;
    private String entry_date;
    private String entry_time;
    private String visitor_name;
    private String visitor_phone;
    private String remark;

    private String visitor_image;

    public String getVisitor_image() {
        return visitor_image;
    }

    public void setVisitor_image(String visitor_image) {
        this.visitor_image = visitor_image;
    }

    public String getVisitor_id() {
        return visitor_id;
    }

    public void setVisitor_id(String visitor_id) {
        this.visitor_id = visitor_id;
    }

    public String getPlate_number() {
        return plate_number;
    }

    public void setPlate_number(String plate_number) {
        this.plate_number = plate_number;
    }

    public String getVisit_address() {
        return visit_address;
    }

    public void setVisit_address(String visit_address) {
        this.visit_address = visit_address;
    }

    public String getEntry_date() {
        return entry_date;
    }

    public void setEntry_date(String entry_date) {
        this.entry_date = entry_date;
    }

    public String getEntry_time() {
        return entry_time;
    }

    public void setEntry_time(String entry_time) {
        this.entry_time = entry_time;
    }

    public String getVisitor_name() {
        return visitor_name;
    }

    public void setVisitor_name(String visitor_name) {
        this.visitor_name = visitor_name;
    }

    public String getVisitor_phone() {
        return visitor_phone;
    }

    public void setVisitor_phone(String visitor_phone) {
        this.visitor_phone = visitor_phone;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "CarVisitorModel{" +
                "visitor_id='" + visitor_id + '\'' +
                ", plate_number='" + plate_number + '\'' +
                ", visit_address='" + visit_address + '\'' +
                ", entry_date='" + entry_date + '\'' +
                ", entry_time='" + entry_time + '\'' +
                ", visitor_name='" + visitor_name + '\'' +
                ", visitor_phone='" + visitor_phone + '\'' +
                ", remark='" + remark + '\'' +
                ", visitor_image='" + visitor_image + '\'' +
                '}';
    }
}
