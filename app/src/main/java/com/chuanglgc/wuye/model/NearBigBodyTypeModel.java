package com.chuanglgc.wuye.model;


public class NearBigBodyTypeModel {

    /**
     * body_type_num : 1
     * body_type_name : 房产
     */

    private String body_type_num;
    private String body_type_name;

    public String getBody_type_num() {
        return body_type_num;
    }

    public void setBody_type_num(String body_type_num) {
        this.body_type_num = body_type_num;
    }

    public String getBody_type_name() {
        return body_type_name;
    }

    public void setBody_type_name(String body_type_name) {
        this.body_type_name = body_type_name;
    }
}
