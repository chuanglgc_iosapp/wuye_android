package com.chuanglgc.wuye.model;


public class CycleTaskModel {


    /**
     * cycle_task_id : 282
     * cycle_task_name : 临时烟雾报警器保养
     * cycle_task_date : 2018-04-11
     * cycle_task_status_id : 1
     * facilities_list : FE-0001、1-1-001
     * cycle_task_type : 2
     * cycle_task_status_name : 未接受
     */

    private String cycle_task_id;
    private String cycle_task_name;
    private String cycle_task_date;
    private String cycle_task_status_id;
    private String facilities_list;
    private String cycle_task_type;
    private String cycle_task_status_name;

    public String getCycle_task_id() {
        return cycle_task_id;
    }

    public void setCycle_task_id(String cycle_task_id) {
        this.cycle_task_id = cycle_task_id;
    }

    public String getCycle_task_name() {
        return cycle_task_name;
    }

    public void setCycle_task_name(String cycle_task_name) {
        this.cycle_task_name = cycle_task_name;
    }

    public String getCycle_task_date() {
        return cycle_task_date;
    }

    public void setCycle_task_date(String cycle_task_date) {
        this.cycle_task_date = cycle_task_date;
    }

    public String getCycle_task_status_id() {
        return cycle_task_status_id;
    }

    public void setCycle_task_status_id(String cycle_task_status_id) {
        this.cycle_task_status_id = cycle_task_status_id;
    }

    public String getFacilities_list() {
        return facilities_list;
    }

    public void setFacilities_list(String facilities_list) {
        this.facilities_list = facilities_list;
    }

    public String getCycle_task_type() {
        return cycle_task_type;
    }

    public void setCycle_task_type(String cycle_task_type) {
        this.cycle_task_type = cycle_task_type;
    }

    public String getCycle_task_status_name() {
        return cycle_task_status_name;
    }

    public void setCycle_task_status_name(String cycle_task_status_name) {
        this.cycle_task_status_name = cycle_task_status_name;
    }
}
