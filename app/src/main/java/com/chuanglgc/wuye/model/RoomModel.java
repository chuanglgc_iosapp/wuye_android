package com.chuanglgc.wuye.model;


public class RoomModel {


    /**
     * room_id : 3278
     * room_name : 101室
     */

    private String room_id;
    private String room_name;

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }
}
