package com.chuanglgc.wuye.model;

public class CycleEventDetailModel {

    /**
     * cycle_task_id : 41
     * cycle_task_name : null
     * cycle_task_content : null
     * body_type : 房产
     * device_type : 电梯
     * trigger_time : null
     * device_data : FE-0001,2-2-1
     */

    private String cycle_task_id;
    private String cycle_task_name;
    private String cycle_task_content;
    private String body_type;
    private String device_type;
    private String trigger_time;
    private String device_data;

    public String getCycle_task_id() {
        return cycle_task_id;
    }

    public void setCycle_task_id(String cycle_task_id) {
        this.cycle_task_id = cycle_task_id;
    }

    public String getCycle_task_name() {
        return cycle_task_name;
    }

    public void setCycle_task_name(String cycle_task_name) {
        this.cycle_task_name = cycle_task_name;
    }

    public String getCycle_task_content() {
        return cycle_task_content;
    }

    public void setCycle_task_content(String cycle_task_content) {
        this.cycle_task_content = cycle_task_content;
    }

    public String getBody_type() {
        return body_type;
    }

    public void setBody_type(String body_type) {
        this.body_type = body_type;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getTrigger_time() {
        return trigger_time;
    }

    public void setTrigger_time(String trigger_time) {
        this.trigger_time = trigger_time;
    }

    public String getDevice_data() {
        return device_data;
    }

    public void setDevice_data(String device_data) {
        this.device_data = device_data;
    }
}
