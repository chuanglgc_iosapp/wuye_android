package com.chuanglgc.wuye.model;

import java.util.List;

public class PressMoneyDetailModel {

    private List<PaylistBean> paylist;
    private List<UrgerecordBean> urgerecord;

    public List<PaylistBean> getPaylist() {
        return paylist;
    }

    public void setPaylist(List<PaylistBean> paylist) {
        this.paylist = paylist;
    }

    public List<UrgerecordBean> getUrgerecord() {
        return urgerecord;
    }

    public void setUrgerecord(List<UrgerecordBean> urgerecord) {
        this.urgerecord = urgerecord;
    }

    public static class PaylistBean {
        /**
         * estate_address : 1栋1单元105室
         * estate_id : 3282
         * payablemoney : 26000.00
         * arrearscostname : 物业服务费
         * startdate : 2017-11-09
         * enddate : 2018-04-25
         */

        private String estate_address;
        private String estate_id;
        private String payablemoney;
        private String arrearscostname;
        private String startdate;
        private String enddate;

        public String getEstate_address() {
            return estate_address;
        }

        public void setEstate_address(String estate_address) {
            this.estate_address = estate_address;
        }

        public String getEstate_id() {
            return estate_id;
        }

        public void setEstate_id(String estate_id) {
            this.estate_id = estate_id;
        }

        public String getPayablemoney() {
            return payablemoney;
        }

        public void setPayablemoney(String payablemoney) {
            this.payablemoney = payablemoney;
        }

        public String getArrearscostname() {
            return arrearscostname;
        }

        public void setArrearscostname(String arrearscostname) {
            this.arrearscostname = arrearscostname;
        }

        public String getStartdate() {
            return startdate;
        }

        public void setStartdate(String startdate) {
            this.startdate = startdate;
        }

        public String getEnddate() {
            return enddate;
        }

        public void setEnddate(String enddate) {
            this.enddate = enddate;
        }
    }

    public static class UrgerecordBean {
        /**
         * entrydate : 2018-04-28
         * entrytime : 15:19
         * urgecontent : dddddddddddddddddddddddddddd
         */

        private String entrydate;
        private String entrytime;
        private String urgecontent;

        public String getEntrydate() {
            return entrydate;
        }

        public void setEntrydate(String entrydate) {
            this.entrydate = entrydate;
        }

        public String getEntrytime() {
            return entrytime;
        }

        public void setEntrytime(String entrytime) {
            this.entrytime = entrytime;
        }

        public String getUrgecontent() {
            return urgecontent;
        }

        public void setUrgecontent(String urgecontent) {
            this.urgecontent = urgecontent;
        }
    }
}
