package com.chuanglgc.wuye.model;


public class PartnerModel {

    /**
     * emp_id : 224
     * emp_name : 张素娇
     */

    private String emp_id;
    private String emp_name;

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }
}
