package com.chuanglgc.wuye.model;



public class RepairPicModel {

    /**
     * http_host : http://192.168.0.210
     * image_url : /uploadpic/cms2.0/cycle/DAA010101/thumb_cycle_263_1522650928.png
     * time : 2018-04-02 14:35:28
     */

    private String http_host;
    private String image_path;


    public String getHttp_host() {
        return http_host;
    }

    public void setHttp_host(String http_host) {
        this.http_host = http_host;
    }

    public String getImage_url() {
        return image_path;
    }

    public void setImage_url(String image_url) {
        this.image_path = image_url;
    }


}
