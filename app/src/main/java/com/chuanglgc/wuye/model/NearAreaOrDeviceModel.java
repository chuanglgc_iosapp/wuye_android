package com.chuanglgc.wuye.model;

public class NearAreaOrDeviceModel {

    /**
     * area_or_device_id : 1114
     * area_or_device_name : W-0002
     */

    private String area_or_device_id;
    private String area_or_device_name;

    public String getArea_or_device_id() {
        return area_or_device_id;
    }

    public void setArea_or_device_id(String area_or_device_id) {
        this.area_or_device_id = area_or_device_id;
    }

    public String getArea_or_device_name() {
        return area_or_device_name;
    }

    public void setArea_or_device_name(String area_or_device_name) {
        this.area_or_device_name = area_or_device_name;
    }
}
