package com.chuanglgc.wuye.model;



public class EventHandleDetailModel {


    /**
     * cycle_task_id : 271
     * cycle_task_date : 2018-04-03 17:28:07
     * cycle_task_name : 临时分配
     * cycle_task_content : null
     * cycle_task_body : null
     * cycle_task_facilities_type : 灭火器
     * cycle_task_priority : 1
     * cycle_task_facilities_list : null
     * cycle_task_remark : 反反复复付
     * leading_person : 黄渤
     * assistant_person : 吴君如,吴孟达
     * is_upload_image : null
     * cycle_task_type : 3
     */

    private String cycle_task_id;
    private String cycle_task_date;
    private String cycle_task_name;
    private String cycle_task_content;
    private String cycle_task_body;
    private String cycle_task_facilities_type;
    private String cycle_task_priority;
    private String cycle_task_facilities_list;
    private String cycle_task_remark;
    private String leading_person;
    private String assistant_person;
    private String is_upload_image;
    private String cycle_task_type;

    public String getCycle_task_id() {
        return cycle_task_id;
    }

    public void setCycle_task_id(String cycle_task_id) {
        this.cycle_task_id = cycle_task_id;
    }

    public String getCycle_task_date() {
        return cycle_task_date;
    }

    public void setCycle_task_date(String cycle_task_date) {
        this.cycle_task_date = cycle_task_date;
    }

    public String getCycle_task_name() {
        return cycle_task_name;
    }

    public void setCycle_task_name(String cycle_task_name) {
        this.cycle_task_name = cycle_task_name;
    }

    public String getCycle_task_content() {
        return cycle_task_content;
    }

    public void setCycle_task_content(String cycle_task_content) {
        this.cycle_task_content = cycle_task_content;
    }

    public String getCycle_task_body() {
        return cycle_task_body;
    }

    public void setCycle_task_body(String cycle_task_body) {
        this.cycle_task_body = cycle_task_body;
    }

    public String getCycle_task_facilities_type() {
        return cycle_task_facilities_type;
    }

    public void setCycle_task_facilities_type(String cycle_task_facilities_type) {
        this.cycle_task_facilities_type = cycle_task_facilities_type;
    }

    public String getCycle_task_priority() {
        return cycle_task_priority;
    }

    public void setCycle_task_priority(String cycle_task_priority) {
        this.cycle_task_priority = cycle_task_priority;
    }

    public String getCycle_task_facilities_list() {
        return cycle_task_facilities_list;
    }

    public void setCycle_task_facilities_list(String cycle_task_facilities_list) {
        this.cycle_task_facilities_list = cycle_task_facilities_list;
    }

    public String getCycle_task_remark() {
        return cycle_task_remark;
    }

    public void setCycle_task_remark(String cycle_task_remark) {
        this.cycle_task_remark = cycle_task_remark;
    }

    public String getLeading_person() {
        return leading_person;
    }

    public void setLeading_person(String leading_person) {
        this.leading_person = leading_person;
    }

    public String getAssistant_person() {
        return assistant_person;
    }

    public void setAssistant_person(String assistant_person) {
        this.assistant_person = assistant_person;
    }

    public String getIs_upload_image() {
        return is_upload_image;
    }

    public void setIs_upload_image(String is_upload_image) {
        this.is_upload_image = is_upload_image;
    }

    public String getCycle_task_type() {
        return cycle_task_type;
    }

    public void setCycle_task_type(String cycle_task_type) {
        this.cycle_task_type = cycle_task_type;
    }
}
