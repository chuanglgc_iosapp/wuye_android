package com.chuanglgc.wuye.model;


import java.util.List;

public class CarModel {


    /**
     * address : 1-1-107
     * plate_number : ["京P88888","湘A55555"]
     * park_number : ["A1001","H1007","K1001"]
     */

    private String address;
    private List<String> plate_number;
    private List<String> park_number;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<String> getPlate_number() {
        return plate_number;
    }

    public void setPlate_number(List<String> plate_number) {
        this.plate_number = plate_number;
    }

    public List<String> getPark_number() {
        return park_number;
    }

    public void setPark_number(List<String> park_number) {
        this.park_number = park_number;
    }
}
