package com.chuanglgc.wuye.model;


public class NearBigDeviceNumModel {

    /**
     * device_id : 1109
     * device_num : L-0002
     */

    private String device_id;
    private String device_num;

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_num() {
        return device_num;
    }

    public void setDevice_num(String device_num) {
        this.device_num = device_num;
    }
}
