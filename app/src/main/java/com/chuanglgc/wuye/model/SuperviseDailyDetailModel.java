package com.chuanglgc.wuye.model;


import java.util.List;

public class SuperviseDailyDetailModel {


    /**
     * daily_task_id : 2233
     * daily_task_name : 小区卫生
     * leading_person : 谢娜
     * work_status : 处理中
     * schedule_name : 维修上午(08:00-12:00)
     * task_total : 2
     * message : 分配要求：测试卫生;第1次备注：完成
     * task_test : 1
     * task_area :
     * every : [{"number":"第1次","task_image1":"","task_image2":"","info":null}]
     */

    private String daily_task_id;
    private String daily_task_name;
    private String leading_person;
    private String work_status;
    private String schedule_name;
    private String task_total;
    private String message;
    private int task_test;
    private String task_area;
    private String reading;
    private List<EveryBean> every;

    public String getReading() {
        return reading;
    }

    public void setReading(String reading) {
        this.reading = reading;
    }


    public String getDaily_task_id() {
        return daily_task_id;
    }

    public void setDaily_task_id(String daily_task_id) {
        this.daily_task_id = daily_task_id;
    }

    public String getDaily_task_name() {
        return daily_task_name;
    }

    public void setDaily_task_name(String daily_task_name) {
        this.daily_task_name = daily_task_name;
    }

    public String getLeading_person() {
        return leading_person;
    }

    public void setLeading_person(String leading_person) {
        this.leading_person = leading_person;
    }

    public String getWork_status() {
        return work_status;
    }

    public void setWork_status(String work_status) {
        this.work_status = work_status;
    }

    public String getSchedule_name() {
        return schedule_name;
    }

    public void setSchedule_name(String schedule_name) {
        this.schedule_name = schedule_name;
    }

    public String getTask_total() {
        return task_total;
    }

    public void setTask_total(String task_total) {
        this.task_total = task_total;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTask_test() {
        return task_test;
    }

    public void setTask_test(int task_test) {
        this.task_test = task_test;
    }

    public String getTask_area() {
        return task_area;
    }

    public void setTask_area(String task_area) {
        this.task_area = task_area;
    }

    public List<EveryBean> getEvery() {
        return every;
    }

    public void setEvery(List<EveryBean> every) {
        this.every = every;
    }

    public static class EveryBean {
        /**
         * number : 第1次
         * task_image1 :
         * task_image2 :
         * info : null
         */

        private String number;
        private String task_image1;
        private String task_image2;
        private String info;

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getTask_image1() {
            return task_image1;
        }

        public void setTask_image1(String task_image1) {
            this.task_image1 = task_image1;
        }

        public String getTask_image2() {
            return task_image2;
        }

        public void setTask_image2(String task_image2) {
            this.task_image2 = task_image2;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }
    }
}
