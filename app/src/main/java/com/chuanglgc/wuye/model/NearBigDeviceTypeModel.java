package com.chuanglgc.wuye.model;


public class NearBigDeviceTypeModel {

    /**
     * device_type_num : 21
     * device_type_name : 消防栓
     */

    private String device_type_num;
    private String device_type_name;

    public String getDevice_type_num() {
        return device_type_num;
    }

    public void setDevice_type_num(String device_type_num) {
        this.device_type_num = device_type_num;
    }

    public String getDevice_type_name() {
        return device_type_name;
    }

    public void setDevice_type_name(String device_type_name) {
        this.device_type_name = device_type_name;
    }
}
