package com.chuanglgc.wuye.model;


import java.util.List;

public class SuperviseHandleDetailModel {

    /**
     * cycle_task_id : 189
     * cycle_task_date : 2017.12.18
     * cycle_task_type : 区域
     * cycle_task_facilities_type : 消防栓
     * cycle_task_content : 消防保养
     * ship_id : 238
     * ship_name : 谢娜
     * assistant_person : null
     * cycle_task_priority : 一般
     * cycle_task_remark : ccvvvv
     * cycle_task_facilities_id : L-0002,L-0003
     * task_image1 : ["http://192.168.0.210/uploadpic/cms2.0/cycle//thumb_cycle__1522733806.jpg"]
     * work_status : 未接受
     */

    private String cycle_task_id;
    private String cycle_task_date;
    private String cycle_task_type;
    private String cycle_task_facilities_type;
    private String cycle_task_content;
    private String ship_id;
    private String ship_name;
    private String assistant_person;
    private String cycle_task_priority;
    private String cycle_task_remark;
    private String cycle_task_facilities_id;
    private String work_status;
    private List<String> task_image1;

    public String getCycle_task_id() {
        return cycle_task_id;
    }

    public void setCycle_task_id(String cycle_task_id) {
        this.cycle_task_id = cycle_task_id;
    }

    public String getCycle_task_date() {
        return cycle_task_date;
    }

    public void setCycle_task_date(String cycle_task_date) {
        this.cycle_task_date = cycle_task_date;
    }

    public String getCycle_task_type() {
        return cycle_task_type;
    }

    public void setCycle_task_type(String cycle_task_type) {
        this.cycle_task_type = cycle_task_type;
    }

    public String getCycle_task_facilities_type() {
        return cycle_task_facilities_type;
    }

    public void setCycle_task_facilities_type(String cycle_task_facilities_type) {
        this.cycle_task_facilities_type = cycle_task_facilities_type;
    }

    public String getCycle_task_content() {
        return cycle_task_content;
    }

    public void setCycle_task_content(String cycle_task_content) {
        this.cycle_task_content = cycle_task_content;
    }

    public String getShip_id() {
        return ship_id;
    }

    public void setShip_id(String ship_id) {
        this.ship_id = ship_id;
    }

    public String getShip_name() {
        return ship_name;
    }

    public void setShip_name(String ship_name) {
        this.ship_name = ship_name;
    }

    public String getAssistant_person() {
        return assistant_person;
    }

    public void setAssistant_person(String assistant_person) {
        this.assistant_person = assistant_person;
    }

    public String getCycle_task_priority() {
        return cycle_task_priority;
    }

    public void setCycle_task_priority(String cycle_task_priority) {
        this.cycle_task_priority = cycle_task_priority;
    }

    public String getCycle_task_remark() {
        return cycle_task_remark;
    }

    public void setCycle_task_remark(String cycle_task_remark) {
        this.cycle_task_remark = cycle_task_remark;
    }

    public String getCycle_task_facilities_id() {
        return cycle_task_facilities_id;
    }

    public void setCycle_task_facilities_id(String cycle_task_facilities_id) {
        this.cycle_task_facilities_id = cycle_task_facilities_id;
    }

    public String getWork_status() {
        return work_status;
    }

    public void setWork_status(String work_status) {
        this.work_status = work_status;
    }

    public List<String> getTask_image1() {
        return task_image1;
    }

    public void setTask_image1(List<String> task_image1) {
        this.task_image1 = task_image1;
    }
}
