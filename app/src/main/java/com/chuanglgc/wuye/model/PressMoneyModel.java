package com.chuanglgc.wuye.model;

public class PressMoneyModel {

    /**
     * arreascost_id : 145
     * estate_id : 3282
     * estate_address : 1-1-105
     * payablemoney : 26000.00
     */

    private String arreascost_id;
    private String estate_id;
    private String estate_address;
    private String payablemoney;

    public String getArreascost_id() {
        return arreascost_id;
    }

    public void setArreascost_id(String arreascost_id) {
        this.arreascost_id = arreascost_id;
    }

    public String getEstate_id() {
        return estate_id;
    }

    public void setEstate_id(String estate_id) {
        this.estate_id = estate_id;
    }

    public String getEstate_address() {
        return estate_address;
    }

    public void setEstate_address(String estate_address) {
        this.estate_address = estate_address;
    }

    public String getPayablemoney() {
        return payablemoney;
    }

    public void setPayablemoney(String payablemoney) {
        this.payablemoney = payablemoney;
    }
}
