package com.chuanglgc.wuye.model;
public class EventManagerModel {

    /**
     * task_id : 116
     * task_status_id : 2
     * task_status_name : 处理中
     * repair_id : 121
     * repair_date : 2017.11.19
     * repair_time : 13:54
     * repair_address : 1-1-101
     * repair_type_id : 91
     * repair_type_name : 房屋主体
     */

    private String task_id;
    private String task_status_id;
    private String task_status_name;
    private String repair_id;
    private String repair_date;
    private String repair_time;
    private String repair_address;
    private String repair_type_id;
    private String repair_type_name;

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getTask_status_id() {
        return task_status_id;
    }

    public void setTask_status_id(String task_status_id) {
        this.task_status_id = task_status_id;
    }

    public String getTask_status_name() {
        return task_status_name;
    }

    public void setTask_status_name(String task_status_name) {
        this.task_status_name = task_status_name;
    }

    public String getRepair_id() {
        return repair_id;
    }

    public void setRepair_id(String repair_id) {
        this.repair_id = repair_id;
    }

    public String getRepair_date() {
        return repair_date;
    }

    public void setRepair_date(String repair_date) {
        this.repair_date = repair_date;
    }

    public String getRepair_time() {
        return repair_time;
    }

    public void setRepair_time(String repair_time) {
        this.repair_time = repair_time;
    }

    public String getRepair_address() {
        return repair_address;
    }

    public void setRepair_address(String repair_address) {
        this.repair_address = repair_address;
    }

    public String getRepair_type_id() {
        return repair_type_id;
    }

    public void setRepair_type_id(String repair_type_id) {
        this.repair_type_id = repair_type_id;
    }

    public String getRepair_type_name() {
        return repair_type_name;
    }

    public void setRepair_type_name(String repair_type_name) {
        this.repair_type_name = repair_type_name;
    }
}
