package com.chuanglgc.wuye.model;



public class NearDailyWorkModel {


    /**
     * work_id : 209
     * work_name : 小区卫生
     */

    private String work_id;
    private String work_name;

    public String getWork_id() {
        return work_id;
    }

    public void setWork_id(String work_id) {
        this.work_id = work_id;
    }

    public String getWork_name() {
        return work_name;
    }

    public void setWork_name(String work_name) {
        this.work_name = work_name;
    }
}
