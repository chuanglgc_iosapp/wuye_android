package com.chuanglgc.wuye.model;



public class UnitModel {

    /**
     * unit_id : 1
     * unit_name : 1单元
     */

    private String unit_id;
    private String unit_name;

    public String getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }

    public String getUnit_name() {
        return unit_name;
    }

    public void setUnit_name(String unit_name) {
        this.unit_name = unit_name;
    }
}
