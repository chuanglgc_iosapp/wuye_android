package com.chuanglgc.wuye.model;



public class ScheduleModel {

    /**
     * schedule_id : 165
     * schedule_no : 301
     * schedule_name : 维修上午(08:00-12:00)
     * is_cross_date : 1
     */


    /**
     * define_working_time : 08:00
     * define_closing_time : 12:00
     * define_working_name : 上班时间
     * define_closing_name : 下班时间
     */
    private String schedule_id;
    private String schedule_no;
    private String schedule_name;
    private String is_cross_date;
    private String define_working_time;
    private String define_closing_time;
    private String define_working_name;
    private String define_closing_name;

    public String getSchedule_id() {
        return schedule_id;
    }

    public void setSchedule_id(String schedule_id) {
        this.schedule_id = schedule_id;
    }

    public String getSchedule_no() {
        return schedule_no;
    }

    public void setSchedule_no(String schedule_no) {
        this.schedule_no = schedule_no;
    }

    public String getSchedule_name() {
        return schedule_name;
    }

    public void setSchedule_name(String schedule_name) {
        this.schedule_name = schedule_name;
    }

    public String getIs_cross_date() {
        return is_cross_date;
    }

    public void setIs_cross_date(String is_cross_date) {
        this.is_cross_date = is_cross_date;
    }

    public String getDefine_working_time() {
        return define_working_time;
    }

    public void setDefine_working_time(String define_working_time) {
        this.define_working_time = define_working_time;
    }

    public String getDefine_closing_time() {
        return define_closing_time;
    }

    public void setDefine_closing_time(String define_closing_time) {
        this.define_closing_time = define_closing_time;
    }

    public String getDefine_working_name() {
        return define_working_name;
    }

    public void setDefine_working_name(String define_working_name) {
        this.define_working_name = define_working_name;
    }

    public String getDefine_closing_name() {
        return define_closing_name;
    }

    public void setDefine_closing_name(String define_closing_name) {
        this.define_closing_name = define_closing_name;
    }
}
