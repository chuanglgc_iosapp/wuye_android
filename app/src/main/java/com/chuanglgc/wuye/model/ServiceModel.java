package com.chuanglgc.wuye.model;



public class ServiceModel {

    /**
     * service_type_num : 1
     * service_type_name : 物业公司
     */

    private String service_type_num;
    private String service_type_name;

    public String getService_type_num() {
        return service_type_num;
    }

    public void setService_type_num(String service_type_num) {
        this.service_type_num = service_type_num;
    }

    public String getService_type_name() {
        return service_type_name;
    }

    public void setService_type_name(String service_type_name) {
        this.service_type_name = service_type_name;
    }
}
