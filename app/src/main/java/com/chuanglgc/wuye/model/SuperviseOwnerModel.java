package com.chuanglgc.wuye.model;


public class SuperviseOwnerModel {

    /**
     * task_id : 119
     * repair_task_name : 房屋主体
     * repair_task_date : 2017.11.20
     * repair_task_time : 09:14
     * CK3204 : 2
     * status : 处理中
     * emp_name : 黄渤
     */

    private String task_id;
    private String repair_task_name;
    private String repair_task_date;
    private String repair_task_time;
    private String CK3204;
    private String status;
    private String emp_name;

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getRepair_task_name() {
        return repair_task_name;
    }

    public void setRepair_task_name(String repair_task_name) {
        this.repair_task_name = repair_task_name;
    }

    public String getRepair_task_date() {
        return repair_task_date;
    }

    public void setRepair_task_date(String repair_task_date) {
        this.repair_task_date = repair_task_date;
    }

    public String getRepair_task_time() {
        return repair_task_time;
    }

    public void setRepair_task_time(String repair_task_time) {
        this.repair_task_time = repair_task_time;
    }

    public String getCK3204() {
        return CK3204;
    }

    public void setCK3204(String CK3204) {
        this.CK3204 = CK3204;
    }



    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }
}
