package com.chuanglgc.wuye.model;

import java.util.List;



public class PersonalStatisticsModel {

    /**
     * attendance : {"attendance_totaldays":1,"attendance_date":[{"attendance_date":"2018.04.09"}]}
     * shift : {"shift_totalcount":4,"shift_time":[{"shift_id":"165","shift_name":"维修上午(08:00-12:00)","shift_dayscount":"2"},{"shift_id":"172","shift_name":"维修全天(08:00-17:00)","shift_dayscount":"1"},{"shift_id":"175","shift_name":"维修晚上(16:00-18:00)","shift_dayscount":"1"},{"shift_id":"178","shift_name":"自动打卡跨天(18:00-08:00)","shift_dayscount":"1"}]}
     * late : {"late_totalcount":3,"late_info":[{"attendance_date":"2018.04.09","punch_time":"13:27","shift_name":"维修上午(08:00-12:00)","timediff":"327","weekday":"星期四"},{"attendance_date":"2018.04.09","punch_time":"13:27","shift_name":"自动打卡跨天(18:00-08:00)","timediff":"","weekday":"星期四"},{"attendance_date":"2018.04.09","punch_time":"13:27","shift_name":"维修上午(08:00-12:00)","timediff":"327","weekday":"星期四"}]}
     * leave_early : {"early_total":2,"early_info":[{"attendance_date":"2018.04.09","punch_time":"13:31","CM9806":"1","shift_name":"维修晚上(16:00-18:00)","timediff":"269","weekday":"星期四"},{"attendance_date":"2018.04.09","punch_time":"13:31","CM9806":"1","shift_name":"维修全天(08:00-17:00)","timediff":"209","weekday":"星期四"}]}
     */

    private AttendanceBean attendance;
    private ShiftBean shift;
    private LateBean late;
    private LeaveEarlyBean leave_early;

    public AttendanceBean getAttendance() {
        return attendance;
    }

    public void setAttendance(AttendanceBean attendance) {
        this.attendance = attendance;
    }

    public ShiftBean getShift() {
        return shift;
    }

    public void setShift(ShiftBean shift) {
        this.shift = shift;
    }

    public LateBean getLate() {
        return late;
    }

    public void setLate(LateBean late) {
        this.late = late;
    }

    public LeaveEarlyBean getLeave_early() {
        return leave_early;
    }

    public void setLeave_early(LeaveEarlyBean leave_early) {
        this.leave_early = leave_early;
    }

    public static class AttendanceBean {
        /**
         * attendance_totaldays : 1
         * attendance_date : [{"attendance_date":"2018.04.09"}]
         */

        private int attendance_totaldays;
        private List<AttendanceDateBean> attendance_date;

        public int getAttendance_totaldays() {
            return attendance_totaldays;
        }

        public void setAttendance_totaldays(int attendance_totaldays) {
            this.attendance_totaldays = attendance_totaldays;
        }

        public List<AttendanceDateBean> getAttendance_date() {
            return attendance_date;
        }

        public void setAttendance_date(List<AttendanceDateBean> attendance_date) {
            this.attendance_date = attendance_date;
        }

        public static class AttendanceDateBean {
            /**
             * attendance_date : 2018.04.09
             */

            private String attendance_date;

            public String getAttendance_date() {
                return attendance_date;
            }

            public void setAttendance_date(String attendance_date) {
                this.attendance_date = attendance_date;
            }
        }
    }

    public static class ShiftBean {
        /**
         * shift_totalcount : 4
         * shift_time : [{"shift_id":"165","shift_name":"维修上午(08:00-12:00)","shift_dayscount":"2"},{"shift_id":"172","shift_name":"维修全天(08:00-17:00)","shift_dayscount":"1"},{"shift_id":"175","shift_name":"维修晚上(16:00-18:00)","shift_dayscount":"1"},{"shift_id":"178","shift_name":"自动打卡跨天(18:00-08:00)","shift_dayscount":"1"}]
         */

        private int shift_totalcount;
        private List<ShiftTimeBean> shift_time;

        public int getShift_totalcount() {
            return shift_totalcount;
        }

        public void setShift_totalcount(int shift_totalcount) {
            this.shift_totalcount = shift_totalcount;
        }

        public List<ShiftTimeBean> getShift_time() {
            return shift_time;
        }

        public void setShift_time(List<ShiftTimeBean> shift_time) {
            this.shift_time = shift_time;
        }

        public static class ShiftTimeBean {
            /**
             * shift_id : 165
             * shift_name : 维修上午(08:00-12:00)
             * shift_dayscount : 2
             */

            private String shift_id;
            private String shift_name;
            private String shift_dayscount;

            public String getShift_id() {
                return shift_id;
            }

            public void setShift_id(String shift_id) {
                this.shift_id = shift_id;
            }

            public String getShift_name() {
                return shift_name;
            }

            public void setShift_name(String shift_name) {
                this.shift_name = shift_name;
            }

            public String getShift_dayscount() {
                return shift_dayscount;
            }

            public void setShift_dayscount(String shift_dayscount) {
                this.shift_dayscount = shift_dayscount;
            }
        }
    }

    public static class LateBean {
        /**
         * late_totalcount : 3
         * late_info : [{"attendance_date":"2018.04.09","punch_time":"13:27","shift_name":"维修上午(08:00-12:00)","timediff":"327","weekday":"星期四"},{"attendance_date":"2018.04.09","punch_time":"13:27","shift_name":"自动打卡跨天(18:00-08:00)","timediff":"","weekday":"星期四"},{"attendance_date":"2018.04.09","punch_time":"13:27","shift_name":"维修上午(08:00-12:00)","timediff":"327","weekday":"星期四"}]
         */

        private int late_totalcount;
        private List<LateInfoBean> late_info;

        public int getLate_totalcount() {
            return late_totalcount;
        }

        public void setLate_totalcount(int late_totalcount) {
            this.late_totalcount = late_totalcount;
        }

        public List<LateInfoBean> getLate_info() {
            return late_info;
        }

        public void setLate_info(List<LateInfoBean> late_info) {
            this.late_info = late_info;
        }

        public static class LateInfoBean {
            /**
             * attendance_date : 2018.04.09
             * punch_time : 13:27
             * shift_name : 维修上午(08:00-12:00)
             * timediff : 327
             * weekday : 星期四
             */

            private String attendance_date;
            private String punch_time;
            private String shift_name;
            private String timediff;
            private String weekday;

            public String getAttendance_date() {
                return attendance_date;
            }

            public void setAttendance_date(String attendance_date) {
                this.attendance_date = attendance_date;
            }

            public String getPunch_time() {
                return punch_time;
            }

            public void setPunch_time(String punch_time) {
                this.punch_time = punch_time;
            }

            public String getShift_name() {
                return shift_name;
            }

            public void setShift_name(String shift_name) {
                this.shift_name = shift_name;
            }

            public String getTimediff() {
                return timediff;
            }

            public void setTimediff(String timediff) {
                this.timediff = timediff;
            }

            public String getWeekday() {
                return weekday;
            }

            public void setWeekday(String weekday) {
                this.weekday = weekday;
            }
        }
    }

    public static class LeaveEarlyBean {
        /**
         * early_total : 2
         * early_info : [{"attendance_date":"2018.04.09","punch_time":"13:31","CM9806":"1","shift_name":"维修晚上(16:00-18:00)","timediff":"269","weekday":"星期四"},{"attendance_date":"2018.04.09","punch_time":"13:31","CM9806":"1","shift_name":"维修全天(08:00-17:00)","timediff":"209","weekday":"星期四"}]
         */

        private int early_total;
        private List<EarlyInfoBean> early_info;

        public int getEarly_total() {
            return early_total;
        }

        public void setEarly_total(int early_total) {
            this.early_total = early_total;
        }

        public List<EarlyInfoBean> getEarly_info() {
            return early_info;
        }

        public void setEarly_info(List<EarlyInfoBean> early_info) {
            this.early_info = early_info;
        }

        public static class EarlyInfoBean {
            /**
             * attendance_date : 2018.04.09
             * punch_time : 13:31
             * CM9806 : 1
             * shift_name : 维修晚上(16:00-18:00)
             * timediff : 269
             * weekday : 星期四
             */

            private String attendance_date;
            private String punch_time;
            private String CM9806;
            private String shift_name;
            private String timediff;
            private String weekday;

            public String getAttendance_date() {
                return attendance_date;
            }

            public void setAttendance_date(String attendance_date) {
                this.attendance_date = attendance_date;
            }

            public String getPunch_time() {
                return punch_time;
            }

            public void setPunch_time(String punch_time) {
                this.punch_time = punch_time;
            }

            public String getCM9806() {
                return CM9806;
            }

            public void setCM9806(String CM9806) {
                this.CM9806 = CM9806;
            }

            public String getShift_name() {
                return shift_name;
            }

            public void setShift_name(String shift_name) {
                this.shift_name = shift_name;
            }

            public String getTimediff() {
                return timediff;
            }

            public void setTimediff(String timediff) {
                this.timediff = timediff;
            }

            public String getWeekday() {
                return weekday;
            }

            public void setWeekday(String weekday) {
                this.weekday = weekday;
            }
        }
    }
}
