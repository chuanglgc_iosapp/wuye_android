package com.chuanglgc.wuye.model;

import java.util.List;


public class MasterCheckWorkInfoModel {

    /**
     * emp_id : 234
     * emp_name : 黄渤
     * clockin_work_date : 2018.04.12
     * weekday : 星期四
     * work_info : [{"go_to_work_time":"上班时间(08:00)","go_off_work_time":"下班时间(12:00)","goto_work_punch_time":"上班打卡时间 08:29","gooff_work_punch_times":"下班打卡时间 11:04","goto_work_punch_state":"迟到","goto_work_punch_state_number":"1","gooff_work_punch_state":"正常","gooff_work_state_punch_number":"0","shift_nameAtime":"维修上午(08:00~12:00)"},{"go_to_work_time":"上班时间(13:30)","go_off_work_time":"下班时间(18:00)","goto_work_punch_time":"上班打卡时间 08:29","gooff_work_punch_times":"下班打卡时间 11:04","goto_work_punch_state":"正常","goto_work_punch_state_number":"0","gooff_work_punch_state":"正常","gooff_work_state_punch_number":"0","shift_nameAtime":"维修下午(13:30~18:00)"},{"go_to_work_time":"上班时间(08:00)","go_off_work_time":"下班时间(18:00)","goto_work_punch_time":"上班打卡时间 08:29","gooff_work_punch_times":"下班打卡时间 11:19","goto_work_punch_state":"迟到","goto_work_punch_state_number":"1","gooff_work_punch_state":"正常","gooff_work_state_punch_number":"0","shift_nameAtime":"水表抄表(08:00~18:00)"},{"go_to_work_time":"上班时间(08:00)","go_off_work_time":"下班时间(12:00)","goto_work_punch_time":"上班打卡时间 08:29","gooff_work_punch_times":"下班打卡时间 17:23","goto_work_punch_state":"迟到","goto_work_punch_state_number":"1","gooff_work_punch_state":"正常","gooff_work_state_punch_number":"0","shift_nameAtime":"维修上午(08:00~12:00)"},{"go_to_work_time":"上班时间(08:00)","go_off_work_time":"下班时间(18:00)","goto_work_punch_time":"上班打卡时间 16:59","gooff_work_punch_times":"下班打卡时间 16:59","goto_work_punch_state":"迟到","goto_work_punch_state_number":"1","gooff_work_punch_state":"正常","gooff_work_state_punch_number":"0","shift_nameAtime":"水表抄表(08:00~18:00)"}]
     */

    private String emp_id;
    private String emp_name;
    private String clockin_work_date;
    private String weekday;
    private List<WorkInfoBean> work_info;

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }

    public String getClockin_work_date() {
        return clockin_work_date;
    }

    public void setClockin_work_date(String clockin_work_date) {
        this.clockin_work_date = clockin_work_date;
    }

    public String getWeekday() {
        return weekday;
    }

    public void setWeekday(String weekday) {
        this.weekday = weekday;
    }

    public List<WorkInfoBean> getWork_info() {
        return work_info;
    }

    public void setWork_info(List<WorkInfoBean> work_info) {
        this.work_info = work_info;
    }

    public static class WorkInfoBean {
        /**
         * go_to_work_time : 上班时间(08:00)
         * go_off_work_time : 下班时间(12:00)
         * goto_work_punch_time : 上班打卡时间 08:29
         * gooff_work_punch_times : 下班打卡时间 11:04
         * goto_work_punch_state : 迟到
         * goto_work_punch_state_number : 1
         * gooff_work_punch_state : 正常
         * gooff_work_state_punch_number : 0
         * shift_nameAtime : 维修上午(08:00~12:00)
         */

        private String go_to_work_time;
        private String go_off_work_time;
        private String goto_work_punch_time;
        private String gooff_work_punch_times;
        private String goto_work_punch_state;
        private String goto_work_punch_state_number;
        private String gooff_work_punch_state;
        private String gooff_work_state_punch_number;
        private String shift_nameAtime;

        public String getGo_to_work_time() {
            return go_to_work_time;
        }

        public void setGo_to_work_time(String go_to_work_time) {
            this.go_to_work_time = go_to_work_time;
        }

        public String getGo_off_work_time() {
            return go_off_work_time;
        }

        public void setGo_off_work_time(String go_off_work_time) {
            this.go_off_work_time = go_off_work_time;
        }

        public String getGoto_work_punch_time() {
            return goto_work_punch_time;
        }

        public void setGoto_work_punch_time(String goto_work_punch_time) {
            this.goto_work_punch_time = goto_work_punch_time;
        }

        public String getGooff_work_punch_times() {
            return gooff_work_punch_times;
        }

        public void setGooff_work_punch_times(String gooff_work_punch_times) {
            this.gooff_work_punch_times = gooff_work_punch_times;
        }

        public String getGoto_work_punch_state() {
            return goto_work_punch_state;
        }

        public void setGoto_work_punch_state(String goto_work_punch_state) {
            this.goto_work_punch_state = goto_work_punch_state;
        }

        public String getGoto_work_punch_state_number() {
            return goto_work_punch_state_number;
        }

        public void setGoto_work_punch_state_number(String goto_work_punch_state_number) {
            this.goto_work_punch_state_number = goto_work_punch_state_number;
        }

        public String getGooff_work_punch_state() {
            return gooff_work_punch_state;
        }

        public void setGooff_work_punch_state(String gooff_work_punch_state) {
            this.gooff_work_punch_state = gooff_work_punch_state;
        }

        public String getGooff_work_state_punch_number() {
            return gooff_work_state_punch_number;
        }

        public void setGooff_work_state_punch_number(String gooff_work_state_punch_number) {
            this.gooff_work_state_punch_number = gooff_work_state_punch_number;
        }

        public String getShift_nameAtime() {
            return shift_nameAtime;
        }

        public void setShift_nameAtime(String shift_nameAtime) {
            this.shift_nameAtime = shift_nameAtime;
        }
    }
}
