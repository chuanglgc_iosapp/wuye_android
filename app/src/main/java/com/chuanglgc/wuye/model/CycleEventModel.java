package com.chuanglgc.wuye.model;


public class CycleEventModel {


    /**
     * cycle_task_id : 41
     * cycle_task_name : null
     * trigger_time : null
     */

    private String cycle_task_id;
    private String cycle_task_name;
    private String trigger_time;

    public String getCycle_task_id() {
        return cycle_task_id;
    }

    public void setCycle_task_id(String cycle_task_id) {
        this.cycle_task_id = cycle_task_id;
    }

    public String getCycle_task_name() {
        return cycle_task_name;
    }

    public void setCycle_task_name(String cycle_task_name) {
        this.cycle_task_name = cycle_task_name;
    }

    public String getTrigger_time() {
        return trigger_time;
    }

    public void setTrigger_time(String trigger_time) {
        this.trigger_time = trigger_time;
    }
}
