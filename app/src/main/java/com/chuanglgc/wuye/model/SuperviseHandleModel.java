package com.chuanglgc.wuye.model;


public class SuperviseHandleModel {

    /**
     * cycle_task_id : 189
     * ship_name : 谢娜
     * ship_id : 238
     * assistant_person : null
     * cycle_task_date : 2017.12.18
     * cycle_task_time : 09:41
     * cycle_task_status_name : 待评价
     * cycle_task_content : 消防保养
     */

    private String cycle_task_id;
    private String ship_name;
    private String ship_id;
    private String assistant_person;
    private String cycle_task_date;
    private String cycle_task_time;
    private String cycle_task_status_name;
    private String cycle_task_content;

    public String getCycle_task_id() {
        return cycle_task_id;
    }

    public void setCycle_task_id(String cycle_task_id) {
        this.cycle_task_id = cycle_task_id;
    }

    public String getShip_name() {
        return ship_name;
    }

    public void setShip_name(String ship_name) {
        this.ship_name = ship_name;
    }

    public String getShip_id() {
        return ship_id;
    }

    public void setShip_id(String ship_id) {
        this.ship_id = ship_id;
    }

    public String getAssistant_person() {
        return assistant_person;
    }

    public void setAssistant_person(String assistant_person) {
        this.assistant_person = assistant_person;
    }

    public String getCycle_task_date() {
        return cycle_task_date;
    }

    public void setCycle_task_date(String cycle_task_date) {
        this.cycle_task_date = cycle_task_date;
    }

    public String getCycle_task_time() {
        return cycle_task_time;
    }

    public void setCycle_task_time(String cycle_task_time) {
        this.cycle_task_time = cycle_task_time;
    }

    public String getCycle_task_status_name() {
        return cycle_task_status_name;
    }

    public void setCycle_task_status_name(String cycle_task_status_name) {
        this.cycle_task_status_name = cycle_task_status_name;
    }

    public String getCycle_task_content() {
        return cycle_task_content;
    }

    public void setCycle_task_content(String cycle_task_content) {
        this.cycle_task_content = cycle_task_content;
    }
}
