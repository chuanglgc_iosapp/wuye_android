package com.chuanglgc.wuye.model;

import java.util.List;


public class MasterAttendMonthModel {
    /**
     * attendance : {"attendance_totalperson":3,"attendance_empname":[{"emp_name":"谢娜","info":"水表抄表(1)、保安全天(1)、维修上午(2)、维修下午(1)","total":"4"},{"emp_name":"海涛","info":"水表抄表(1)","total":"1"},{"emp_name":"武勋","info":"维修上午(1)","total":"1"}]}
     * late : {"late_totalcount":5,"late_info":[{"attendance_date":"2018.04.11","emp_id":"238","emp_name":"谢娜","shift_id":"165","shift_name":"维修上午(08:00-12:00)","timediff":"329","weekday":"星期三"},{"attendance_date":"2018.04.10","emp_id":"238","emp_name":"谢娜","shift_id":"165","shift_name":"维修上午(08:00-12:00)","timediff":"29","weekday":"星期二"},{"attendance_date":"2018.04.09","emp_id":"238","emp_name":"谢娜","shift_id":"171","shift_name":"保安全天(08:00-18:00)","timediff":"29","weekday":"星期一"},{"attendance_date":"2018.04.12","emp_id":"240","emp_name":"海涛","shift_id":"170","shift_name":"水表抄表(08:00-18:00)","timediff":"29","weekday":"星期四"},{"attendance_date":"2018.04.12","emp_id":"239","emp_name":"武勋","shift_id":"165","shift_name":"维修上午(08:00-12:00)","timediff":"31","weekday":"星期四"}]}
     * leave_early : {"early_total":4,"early_info":[{"attendance_date":"2018.04.12","emp_id":"238","emp_name":"谢娜","shift_id":"170","shift_name":"水表抄表(08:00-18:00)","timediff":"401","weekday":"星期四"},{"attendance_date":"2018.04.09","emp_id":"238","emp_name":"谢娜","shift_id":"171","shift_name":"保安全天(08:00-18:00)","timediff":"161","weekday":"星期一"},{"attendance_date":"2018.04.09","emp_id":"240","emp_name":"海涛","shift_id":"170","shift_name":"水表抄表(08:00-18:00)","timediff":"161","weekday":"星期一"},{"attendance_date":"2018.04.12","emp_id":"238","emp_name":"谢娜","shift_id":"166","shift_name":"维修下午(13:30-18:00)","timediff":"161","weekday":"星期四"}]}
     */

    private AttendanceBean attendance;
    private LateBean late;
    private LeaveEarlyBean leave_early;

    public AttendanceBean getAttendance() {
        return attendance;
    }

    public void setAttendance(AttendanceBean attendance) {
        this.attendance = attendance;
    }

    public LateBean getLate() {
        return late;
    }

    public void setLate(LateBean late) {
        this.late = late;
    }

    public LeaveEarlyBean getLeave_early() {
        return leave_early;
    }

    public void setLeave_early(LeaveEarlyBean leave_early) {
        this.leave_early = leave_early;
    }

    public static class AttendanceBean {
        /**
         * attendance_totalperson : 3
         * attendance_empname : [{"emp_name":"谢娜","info":"水表抄表(1)、保安全天(1)、维修上午(2)、维修下午(1)","total":"4"},{"emp_name":"海涛","info":"水表抄表(1)","total":"1"},{"emp_name":"武勋","info":"维修上午(1)","total":"1"}]
         */

        private int attendance_totalperson;
        private List<AttendanceEmpnameBean> attendance_empname;

        public int getAttendance_totalperson() {
            return attendance_totalperson;
        }

        public void setAttendance_totalperson(int attendance_totalperson) {
            this.attendance_totalperson = attendance_totalperson;
        }

        public List<AttendanceEmpnameBean> getAttendance_empname() {
            return attendance_empname;
        }

        public void setAttendance_empname(List<AttendanceEmpnameBean> attendance_empname) {
            this.attendance_empname = attendance_empname;
        }

        public static class AttendanceEmpnameBean {
            /**
             * emp_name : 谢娜
             * info : 水表抄表(1)、保安全天(1)、维修上午(2)、维修下午(1)
             * total : 4
             */

            private String emp_name;
            private String info;
            private String total;

            public String getEmp_name() {
                return emp_name;
            }

            public void setEmp_name(String emp_name) {
                this.emp_name = emp_name;
            }

            public String getInfo() {
                return info;
            }

            public void setInfo(String info) {
                this.info = info;
            }

            public String getTotal() {
                return total;
            }

            public void setTotal(String total) {
                this.total = total;
            }
        }
    }

    public static class LateBean {
        /**
         * late_totalcount : 5
         * late_info : [{"attendance_date":"2018.04.11","emp_id":"238","emp_name":"谢娜","shift_id":"165","shift_name":"维修上午(08:00-12:00)","timediff":"329","weekday":"星期三"},{"attendance_date":"2018.04.10","emp_id":"238","emp_name":"谢娜","shift_id":"165","shift_name":"维修上午(08:00-12:00)","timediff":"29","weekday":"星期二"},{"attendance_date":"2018.04.09","emp_id":"238","emp_name":"谢娜","shift_id":"171","shift_name":"保安全天(08:00-18:00)","timediff":"29","weekday":"星期一"},{"attendance_date":"2018.04.12","emp_id":"240","emp_name":"海涛","shift_id":"170","shift_name":"水表抄表(08:00-18:00)","timediff":"29","weekday":"星期四"},{"attendance_date":"2018.04.12","emp_id":"239","emp_name":"武勋","shift_id":"165","shift_name":"维修上午(08:00-12:00)","timediff":"31","weekday":"星期四"}]
         */

        private int late_totalcount;
        private List<LateInfoBean> late_info;

        public int getLate_totalcount() {
            return late_totalcount;
        }

        public void setLate_totalcount(int late_totalcount) {
            this.late_totalcount = late_totalcount;
        }

        public List<LateInfoBean> getLate_info() {
            return late_info;
        }

        public void setLate_info(List<LateInfoBean> late_info) {
            this.late_info = late_info;
        }

        public static class LateInfoBean {
            /**
             * attendance_date : 2018.04.11
             * emp_id : 238
             * emp_name : 谢娜
             * shift_id : 165
             * shift_name : 维修上午(08:00-12:00)
             * timediff : 329
             * weekday : 星期三
             */

            private String attendance_date;
            private String emp_id;
            private String emp_name;
            private String shift_id;
            private String shift_name;
            private String timediff;
            private String weekday;

            public String getAttendance_date() {
                return attendance_date;
            }

            public void setAttendance_date(String attendance_date) {
                this.attendance_date = attendance_date;
            }

            public String getEmp_id() {
                return emp_id;
            }

            public void setEmp_id(String emp_id) {
                this.emp_id = emp_id;
            }

            public String getEmp_name() {
                return emp_name;
            }

            public void setEmp_name(String emp_name) {
                this.emp_name = emp_name;
            }

            public String getShift_id() {
                return shift_id;
            }

            public void setShift_id(String shift_id) {
                this.shift_id = shift_id;
            }

            public String getShift_name() {
                return shift_name;
            }

            public void setShift_name(String shift_name) {
                this.shift_name = shift_name;
            }

            public String getTimediff() {
                return timediff;
            }

            public void setTimediff(String timediff) {
                this.timediff = timediff;
            }

            public String getWeekday() {
                return weekday;
            }

            public void setWeekday(String weekday) {
                this.weekday = weekday;
            }
        }
    }

    public static class LeaveEarlyBean {
        /**
         * early_total : 4
         * early_info : [{"attendance_date":"2018.04.12","emp_id":"238","emp_name":"谢娜","shift_id":"170","shift_name":"水表抄表(08:00-18:00)","timediff":"401","weekday":"星期四"},{"attendance_date":"2018.04.09","emp_id":"238","emp_name":"谢娜","shift_id":"171","shift_name":"保安全天(08:00-18:00)","timediff":"161","weekday":"星期一"},{"attendance_date":"2018.04.09","emp_id":"240","emp_name":"海涛","shift_id":"170","shift_name":"水表抄表(08:00-18:00)","timediff":"161","weekday":"星期一"},{"attendance_date":"2018.04.12","emp_id":"238","emp_name":"谢娜","shift_id":"166","shift_name":"维修下午(13:30-18:00)","timediff":"161","weekday":"星期四"}]
         */

        private int early_total;
        private List<EarlyInfoBean> early_info;

        public int getEarly_total() {
            return early_total;
        }

        public void setEarly_total(int early_total) {
            this.early_total = early_total;
        }

        public List<EarlyInfoBean> getEarly_info() {
            return early_info;
        }

        public void setEarly_info(List<EarlyInfoBean> early_info) {
            this.early_info = early_info;
        }

        public static class EarlyInfoBean {
            /**
             * attendance_date : 2018.04.12
             * emp_id : 238
             * emp_name : 谢娜
             * shift_id : 170
             * shift_name : 水表抄表(08:00-18:00)
             * timediff : 401
             * weekday : 星期四
             */

            private String attendance_date;
            private String emp_id;
            private String emp_name;
            private String shift_id;
            private String shift_name;
            private String timediff;
            private String weekday;

            public String getAttendance_date() {
                return attendance_date;
            }

            public void setAttendance_date(String attendance_date) {
                this.attendance_date = attendance_date;
            }

            public String getEmp_id() {
                return emp_id;
            }

            public void setEmp_id(String emp_id) {
                this.emp_id = emp_id;
            }

            public String getEmp_name() {
                return emp_name;
            }

            public void setEmp_name(String emp_name) {
                this.emp_name = emp_name;
            }

            public String getShift_id() {
                return shift_id;
            }

            public void setShift_id(String shift_id) {
                this.shift_id = shift_id;
            }

            public String getShift_name() {
                return shift_name;
            }

            public void setShift_name(String shift_name) {
                this.shift_name = shift_name;
            }

            public String getTimediff() {
                return timediff;
            }

            public void setTimediff(String timediff) {
                this.timediff = timediff;
            }

            public String getWeekday() {
                return weekday;
            }

            public void setWeekday(String weekday) {
                this.weekday = weekday;
            }
        }
    }
}
