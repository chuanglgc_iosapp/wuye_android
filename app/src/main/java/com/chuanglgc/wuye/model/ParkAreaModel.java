package com.chuanglgc.wuye.model;


public class ParkAreaModel {


    /**
     * area : A
     */

    private String area;

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
