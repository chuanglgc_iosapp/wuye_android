package com.chuanglgc.wuye.model;


public class DailyWorkTypeModel {

    /**
     * work_type_num : 1
     * work_type_name : 仪表类
     */

    private String work_type_num;
    private String work_type_name;

    public String getWork_type_num() {
        return work_type_num;
    }

    public void setWork_type_num(String work_type_num) {
        this.work_type_num = work_type_num;
    }

    public String getWork_type_name() {
        return work_type_name;
    }

    public void setWork_type_name(String work_type_name) {
        this.work_type_name = work_type_name;
    }
}
