package com.chuanglgc.wuye.model;


import java.io.Serializable;

public class DailyTaskModel implements Serializable {

    /**
     * daily_task_id : 2235
     * daily_task_flag : 小区卫生
     * daily_task_total : 2
     * work_schedule : 维修上午 (08:00-12:00)
     * daily_task_areaordevice : 2-1-1-001
     * is_upload_image : 0
     * daily_task_type : 2
     * message : 分配要求：测试卫生;
     * daily_task_rest : 1
     * daily_task_status : 处理中
     */

    private String daily_task_id;
    private String daily_task_flag;
    private String daily_task_total;
    private String work_schedule;
    private String daily_task_areaordevice;
    private String is_upload_image;
    private String daily_task_type;
    private String message;
    private int daily_task_rest;
    private String daily_task_status;

    public String getDaily_task_id() {
        return daily_task_id;
    }

    public void setDaily_task_id(String daily_task_id) {
        this.daily_task_id = daily_task_id;
    }

    public String getDaily_task_flag() {
        return daily_task_flag;
    }

    public void setDaily_task_flag(String daily_task_flag) {
        this.daily_task_flag = daily_task_flag;
    }

    public String getDaily_task_total() {
        return daily_task_total;
    }

    public void setDaily_task_total(String daily_task_total) {
        this.daily_task_total = daily_task_total;
    }

    public String getWork_schedule() {
        return work_schedule;
    }

    public void setWork_schedule(String work_schedule) {
        this.work_schedule = work_schedule;
    }

    public String getDaily_task_areaordevice() {
        return daily_task_areaordevice;
    }

    public void setDaily_task_areaordevice(String daily_task_areaordevice) {
        this.daily_task_areaordevice = daily_task_areaordevice;
    }

    public String getIs_upload_image() {
        return is_upload_image;
    }

    public void setIs_upload_image(String is_upload_image) {
        this.is_upload_image = is_upload_image;
    }

    public String getDaily_task_type() {
        return daily_task_type;
    }

    public void setDaily_task_type(String daily_task_type) {
        this.daily_task_type = daily_task_type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getDaily_task_rest() {
        return daily_task_rest;
    }

    public void setDaily_task_rest(int daily_task_rest) {
        this.daily_task_rest = daily_task_rest;
    }

    public String getDaily_task_status() {
        return daily_task_status;
    }

    public void setDaily_task_status(String daily_task_status) {
        this.daily_task_status = daily_task_status;
    }
}
