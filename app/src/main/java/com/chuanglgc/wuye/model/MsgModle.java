package com.chuanglgc.wuye.model;


public class MsgModle {

    /**
     * data : 您好，1-1-101住户于2018-05-28 12:51:46通过大门/单元门/道闸
     */
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
