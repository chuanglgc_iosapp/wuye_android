package com.chuanglgc.wuye.model;


import java.util.List;

public class OwnerInfoModel {

    /**
     * address : 1栋1单元101室
     * householder : {"householder_name":"玛丽","householder_phone":"13520660000","householder_gender":"(女)","payment_date":"2018.04.03","payment_date_remark":"(房产缴费开始日期)","warehouse":"","warehouse_remark":"(下房编号)"}
     * plate_number : ["京P88888","湘A55555"]
     * park_number : [{"park_number":"A1002","payment_date":"2018.04.05"},{"park_number":"A1003","payment_date":"2018.04.05"},{"park_number":"A1006","payment_date":"2018.04.14"},{"park_number":"A1007","payment_date":"2018.04.05"},{"park_number":"B1004","payment_date":"2018.03.31"},{"park_number":"B1005","payment_date":"2018.03.31"},{"park_number":"B1006","payment_date":"2018.03.31"},{"park_number":"B1007","payment_date":"2018.03.31"},{"park_number":"C1002","payment_date":"2018.04.08"},{"park_number":"C1004","payment_date":"2018.04.08"},{"park_number":"C1007","payment_date":"2018.04.08"},{"park_number":"C1008","payment_date":"2018.04.08"},{"park_number":"C10009","payment_date":"2018.04.08"},{"park_number":"C1010","payment_date":"2018.04.08"},{"park_number":"D1001","payment_date":"2018.04.27"},{"park_number":"E1002","payment_date":"2018.04.26"},{"park_number":"E1003","payment_date":"2018.04.26"},{"park_number":"E1005","payment_date":"2018.04.26"},{"park_number":"F1002","payment_date":"2018.04.03"},{"park_number":"A1009","payment_date":"2018.04.05"},{"park_number":"F1004","payment_date":"2018.04.03"},{"park_number":"D1005","payment_date":"2018.04.27"},{"park_number":"G1009","payment_date":"2018.04.27"},{"park_number":"A101","payment_date":"2018.04.12"},{"park_number":"BB105","payment_date":"2018.04.05"}]
     */

    private String address;
    private HouseholderBean householder;
    private List<String> plate_number;
    private List<ParkNumberBean> park_number;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public HouseholderBean getHouseholder() {
        return householder;
    }

    public void setHouseholder(HouseholderBean householder) {
        this.householder = householder;
    }

    public List<String> getPlate_number() {
        return plate_number;
    }

    public void setPlate_number(List<String> plate_number) {
        this.plate_number = plate_number;
    }

    public List<ParkNumberBean> getPark_number() {
        return park_number;
    }

    public void setPark_number(List<ParkNumberBean> park_number) {
        this.park_number = park_number;
    }

    public static class HouseholderBean {
        /**
         * householder_name : 玛丽
         * householder_phone : 13520660000
         * householder_gender : (女)
         * payment_date : 2018.04.03
         * payment_date_remark : (房产缴费开始日期)
         * warehouse :
         * warehouse_remark : (下房编号)
         */

        private String householder_name;
        private String householder_phone;
        private String householder_gender;
        private String payment_date;
        private String payment_date_remark;
        private String warehouse;
        private String warehouse_remark;

        public String getHouseholder_name() {
            return householder_name;
        }

        public void setHouseholder_name(String householder_name) {
            this.householder_name = householder_name;
        }

        public String getHouseholder_phone() {
            return householder_phone;
        }

        public void setHouseholder_phone(String householder_phone) {
            this.householder_phone = householder_phone;
        }

        public String getHouseholder_gender() {
            return householder_gender;
        }

        public void setHouseholder_gender(String householder_gender) {
            this.householder_gender = householder_gender;
        }

        public String getPayment_date() {
            return payment_date;
        }

        public void setPayment_date(String payment_date) {
            this.payment_date = payment_date;
        }

        public String getPayment_date_remark() {
            return payment_date_remark;
        }

        public void setPayment_date_remark(String payment_date_remark) {
            this.payment_date_remark = payment_date_remark;
        }

        public String getWarehouse() {
            return warehouse;
        }

        public void setWarehouse(String warehouse) {
            this.warehouse = warehouse;
        }

        public String getWarehouse_remark() {
            return warehouse_remark;
        }

        public void setWarehouse_remark(String warehouse_remark) {
            this.warehouse_remark = warehouse_remark;
        }
    }

    public static class ParkNumberBean {
        /**
         * park_number : A1002
         * payment_date : 2018.04.05
         */

        private String park_number;
        private String payment_date;

        public String getPark_number() {
            return park_number;
        }

        public void setPark_number(String park_number) {
            this.park_number = park_number;
        }

        public String getPayment_date() {
            return payment_date;
        }

        public void setPayment_date(String payment_date) {
            this.payment_date = payment_date;
        }
    }
}
