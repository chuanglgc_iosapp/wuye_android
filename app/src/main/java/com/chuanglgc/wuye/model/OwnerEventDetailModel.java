package com.chuanglgc.wuye.model;



public class OwnerEventDetailModel {

    /**
     * repair_id : 121
     * repair_type_id : 91
     * repair_type_name : 房屋主体
     * repair_date : 2017-11-19
     * repair_time : 13:54
     * repair_address : 1-1-101
     * repair_image_url : http://192.168.0.210/uploadpic/cms/repair/thumb_repair_571_1511070874.jpg
     * repair_user_name : 李小红
     * repair_user_phone : 13031300123
     * repair_content : 做最人做最也破也破也
     * assistant_person : null
     * leading_person : 黄渤
     */

    private String repair_id;
    private String repair_type_id;
    private String repair_type_name;
    private String repair_date;
    private String repair_time;
    private String repair_address;
    private String repair_image_url;
    private String repair_user_name;
    private String repair_user_phone;
    private String repair_content;
    private String assistant_person;
    private String leading_person;

    public String getRepair_id() {
        return repair_id;
    }

    public void setRepair_id(String repair_id) {
        this.repair_id = repair_id;
    }

    public String getRepair_type_id() {
        return repair_type_id;
    }

    public void setRepair_type_id(String repair_type_id) {
        this.repair_type_id = repair_type_id;
    }

    public String getRepair_type_name() {
        return repair_type_name;
    }

    public void setRepair_type_name(String repair_type_name) {
        this.repair_type_name = repair_type_name;
    }

    public String getRepair_date() {
        return repair_date;
    }

    public void setRepair_date(String repair_date) {
        this.repair_date = repair_date;
    }

    public String getRepair_time() {
        return repair_time;
    }

    public void setRepair_time(String repair_time) {
        this.repair_time = repair_time;
    }

    public String getRepair_address() {
        return repair_address;
    }

    public void setRepair_address(String repair_address) {
        this.repair_address = repair_address;
    }

    public String getRepair_image_url() {
        return repair_image_url;
    }

    public void setRepair_image_url(String repair_image_url) {
        this.repair_image_url = repair_image_url;
    }

    public String getRepair_user_name() {
        return repair_user_name;
    }

    public void setRepair_user_name(String repair_user_name) {
        this.repair_user_name = repair_user_name;
    }

    public String getRepair_user_phone() {
        return repair_user_phone;
    }

    public void setRepair_user_phone(String repair_user_phone) {
        this.repair_user_phone = repair_user_phone;
    }

    public String getRepair_content() {
        return repair_content;
    }

    public void setRepair_content(String repair_content) {
        this.repair_content = repair_content;
    }

    public String getAssistant_person() {
        return assistant_person;
    }

    public void setAssistant_person(String assistant_person) {
        this.assistant_person = assistant_person;
    }

    public String getLeading_person() {
        return leading_person;
    }

    public void setLeading_person(String leading_person) {
        this.leading_person = leading_person;
    }
}
