package com.chuanglgc.wuye.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.PartnerDialogAdapter;
import com.chuanglgc.wuye.adapter.SpinnerAdapter;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.utils.LogUtil;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


public class PartnerDialog extends Dialog {

    private final ArrayList<String> list;
    private RecyclerView rclPartner;
    private OnCommitClick itemClick;
    private ArrayList<Integer> selIdList;

    public PartnerDialog(@NonNull Context context, ArrayList<String> list) {
        super(context, R.style.dialog_spinner);

        this.list = list;
        setContentView(initView());
        show();
        //先show 在设置属性 getAttributes得到属性
        Window window = getWindow();
        DisplayMetrics metrics = new DisplayMetrics();
        Activity activity = (Activity) context;
        Display defaultDisplay = activity.getWindowManager().getDefaultDisplay();
        defaultDisplay.getMetrics(metrics);
        WindowManager.LayoutParams lp = window.getAttributes();

        lp.gravity = Gravity.CENTER_HORIZONTAL;
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;

        if (list != null && list.size() < 9) {
            lp.height = DisplayUtil.dp2px(context, list.size() * 50 + 60);
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(DisplayUtil.dp2px(context, 250), DisplayUtil.dp2px(context, 400));
            rclPartner.setLayoutParams(layoutParams);
            lp.height = DisplayUtil.dp2px(context, 460); // 高度  50*8+60 条目*数量+bt高度
        }
        lp.dimAmount = 0.4f;//变暗程度
        window.setAttributes(lp);
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//设置背景变暗

    }

    private View initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_partner, null);  //通过LayoutInflater获取布局
        rclPartner = view.findViewById(R.id.rcl_partner);
        Button btCommit = view.findViewById(R.id.bt_commit);

        if (list != null) {
            rclPartner.setLayoutManager(new LinearLayoutManager(getContext()));
            PartnerDialogAdapter adapter = new PartnerDialogAdapter(list);
            adapter.bindToRecyclerView(rclPartner);
            //返回选中的名字
            selIdList = new ArrayList<>();
            //获取选中的条目集合 遍历获取名称
            btCommit.setOnClickListener(v -> {
                String slcName = "";
                TreeMap<Integer, Boolean> sleMap = adapter.getSleMap();
                Iterator iter = sleMap.entrySet().iterator();
                while (iter.hasNext()) {//将所选中的条目position添加到 ArrayList中
                    Map.Entry entry = (Map.Entry) iter.next();
                    Integer key = (Integer) entry.getKey();
                    Boolean val = (Boolean) entry.getValue();
                    if (val) {
                        selIdList.add(key);
                    }
                }
                for (int i = 0; i < selIdList.size(); i++) {//循环position 取出名字
                    if (i != selIdList.size() - 1) {
                        slcName += list.get(selIdList.get(i)) + ",";
                    } else {
                        slcName += list.get(selIdList.get(i));
                    }
                    LogUtil.e("集合名称", list.get(selIdList.get(i)));
                    LogUtil.e("集合名称", slcName);
                }
                if (itemClick != null) itemClick.onCommitClick(slcName, selIdList);
                dismiss();
            });

        }
        return view;
    }

    public interface OnCommitClick {
        void onCommitClick(String itemName, ArrayList<Integer> sleIdList);
    }

    public void setOnCommitClick(OnCommitClick itemClick) {
        this.itemClick = itemClick;
    }
}
