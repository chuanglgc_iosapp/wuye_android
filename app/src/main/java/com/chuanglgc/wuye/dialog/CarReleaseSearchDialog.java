package com.chuanglgc.wuye.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.chuanglgc.wuye.R;


public class CarReleaseSearchDialog extends Dialog {
    private OnSearchClick onSearchClick;

    public CarReleaseSearchDialog(@NonNull Context context) {
        super(context, R.style.dialog_spinner);
        setContentView(R.layout.dialog_car_release_search);
        show();
        Window window = getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();

        lp.gravity = Gravity.CENTER_HORIZONTAL;
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        lp.dimAmount = 0.6f;//变暗程度
        window.setAttributes(lp);
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//设置背景变暗
        initView();
    }

    private void initView() {
        EditText etCarNumber = findViewById(R.id.et_carnumber);
        View bt = findViewById(R.id.bt_search);
        bt.setOnClickListener(v -> {
            if (onSearchClick!=null){
                String cardNumber = etCarNumber.getText().toString().trim();
                if (TextUtils.isEmpty(cardNumber)){
                    Toast.makeText(getContext(),"请输入车牌号",Toast.LENGTH_SHORT).show();
                }else {
                    onSearchClick.onSearch(cardNumber);
                    dismiss();
                }

            }
        });
    }

    public void setOnSearchClickListener(OnSearchClick onSearchClick) {
        this.onSearchClick = onSearchClick;
    }

    public interface OnSearchClick {
        public void onSearch(String cardNumber);
    }
}
