package com.chuanglgc.wuye.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.DialogSingleMasterAdapter;
import com.chuanglgc.wuye.adapter.SpinnerAdapter;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.utils.LogUtil;

import java.util.ArrayList;


//多选条目Dialog
public class MasterDialog extends Dialog {

    private final ArrayList<String> list;
    private RecyclerView rclMaster;
    private OnMasterItemClick itemClick;

    public MasterDialog(@NonNull Context context, ArrayList<String> list) {
        super(context, R.style.dialog_spinner);
        this.list = list;
        setContentView(initView());
        show();
        //先show 在设置属性 getAttributes得到属性
        Window window = getWindow();
        DisplayMetrics metrics = new DisplayMetrics();
        Activity activity = (Activity) context;
        Display defaultDisplay = activity.getWindowManager().getDefaultDisplay();
        defaultDisplay.getMetrics(metrics);
        WindowManager.LayoutParams lp = window.getAttributes();

        lp.gravity = Gravity.CENTER_HORIZONTAL;
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;

        if (list != null && list.size() < 10) {
            lp.height = DisplayUtil.dp2px(context, list.size()*50+55);
        } else {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(DisplayUtil.dp2px(context, 250), DisplayUtil.dp2px(context, 445));
            rclMaster.setLayoutParams(layoutParams);
            lp.height = DisplayUtil.dp2px(context, 500); // 高度
        }
        lp.dimAmount = 0.4f;//变暗程度
        window.setAttributes(lp);
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//设置背景变暗

    }

    private View initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_master_with_dialog, null);  //通过LayoutInflater获取布局
        rclMaster = view.findViewById(R.id.rcl_master);
        Button btCommit = view.findViewById(R.id.bt_commit);
        if (list != null) {
            rclMaster.setLayoutManager(new LinearLayoutManager(getContext()));
            DialogSingleMasterAdapter adapter = new DialogSingleMasterAdapter(list);
            adapter.bindToRecyclerView(rclMaster);
            btCommit.setOnClickListener(v->{
                int sleItemPositon = adapter.getCurrentSlcItemPositon();
                LogUtil.e("选中的条目",sleItemPositon);
                if (itemClick != null) itemClick.onItemClick(sleItemPositon);
            });
        }
        return view;
    }

    public interface OnMasterItemClick {
        void onItemClick(int slePosition);
    }

    public void setOnMasterItemClick(OnMasterItemClick itemClick) {
        this.itemClick = itemClick;
    }
}
