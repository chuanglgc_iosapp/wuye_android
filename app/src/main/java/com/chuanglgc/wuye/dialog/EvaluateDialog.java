package com.chuanglgc.wuye.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyPhotoBtton;


public class EvaluateDialog extends Dialog {

    private OnCommitEvluate onCommitEvluate;

    public EvaluateDialog(@NonNull Context context) {
        super(context, R.style.dialog_spinner);
        setContentView(initView(context));
        show();
        //getAttributes得到属性
        Window window = getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.x = 0;
        lp.y = 0;
        //设置dialog的狂傲
        lp.height = DisplayUtil.dp2px(context, 400);
        lp.width = DisplayUtil.dp2px(context, 330);

        lp.dimAmount = 0.4f;//变暗程度
        window.setAttributes(lp);
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//设置背景变暗

    }

    private View initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_evaluate, null);
        MyPhotoBtton btCommit = view.findViewById(R.id.bt_commit);
        RatingBar ratingBar = view.findViewById(R.id.rb_start);
        TextView tvStart = view.findViewById(R.id.tv_star);
        EditText etRecord = view.findViewById(R.id.et_record);
        btCommit.setOnClickListener(v -> {
            if (onCommitEvluate != null) {
                onCommitEvluate.OnCommit(etRecord.getText().toString().trim(), (int) ratingBar.getRating());
            }
        });
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                LogUtil.e("星星", rating + "");
                switch ((int) rating) {
                    case 0:
                        tvStart.setTextColor(getContext().getResources().getColor(R.color.colorAccent));
                        tvStart.setText("非常差");
                    case 1:
                        tvStart.setTextColor(getContext().getResources().getColor(R.color.colorAccent));
                        tvStart.setText("非常差");
                        break;
                    case 2:
                        tvStart.setTextColor(getContext().getResources().getColor(R.color.yellow));
                        tvStart.setText("很差");
                        break;
                    case 3:
                        tvStart.setTextColor(getContext().getResources().getColor(R.color.bluelight));
                        tvStart.setText("一般");
                        break;
                    case 4:
                        tvStart.setTextColor(getContext().getResources().getColor(R.color.blue));
                        tvStart.setText("很好");
                        break;
                    case 5:
                        tvStart.setTextColor(getContext().getResources().getColor(R.color.blueDark));
                        tvStart.setText("非常好");
                        break;
                }
            }
        });
        return view;
    }

    public interface OnCommitEvluate {
        void OnCommit(String evluate, int stars);
    }

    public void setOnCommitEvluateListener(OnCommitEvluate onCommitEvluate) {
        this.onCommitEvluate = onCommitEvluate;
    }
}
