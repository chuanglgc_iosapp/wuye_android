package com.chuanglgc.wuye.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.SpinnerAdapter;
import com.chuanglgc.wuye.utils.DisplayUtil;


import java.util.ArrayList;


public class SpinnerDialog extends Dialog {

    private final ArrayList<String> list;
    private RecyclerView rclSpinner;
    private OnSpinnerItemClick itemClick;

    public SpinnerDialog(@NonNull Context context, ArrayList<String> list) {
        super(context, R.style.dialog_spinner);
        this.list = list;
        setContentView(initView());
        show();
        //先show 在设置属性 getAttributes得到属性
        Window window = getWindow();
        DisplayMetrics metrics = new DisplayMetrics();
        Activity activity = (Activity) context;
        Display defaultDisplay = activity.getWindowManager().getDefaultDisplay();
        defaultDisplay.getMetrics(metrics);
        WindowManager.LayoutParams lp = window.getAttributes();

        lp.gravity = Gravity.CENTER_HORIZONTAL;
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        if (list!=null&&list.size()<8){
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        }else {
            lp.height = DisplayUtil.dp2px(context, 400); // 高度
    }
        lp.dimAmount = 0.2f;//变暗程度
        window.setAttributes(lp);
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//设置背景变暗
        //float density = metrics.density;
        //getWindow().setLayout((int) (300 * density), 400);


    }

    private View initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_spinner, null);  //通过LayoutInflater获取布局
        rclSpinner = view.findViewById(R.id.rcl_spinner);
        if (list != null) {
            rclSpinner.setLayoutManager(new LinearLayoutManager(getContext()));
            SpinnerAdapter adapter = new SpinnerAdapter(list);
            adapter.bindToRecyclerView(rclSpinner);
            adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    view.setPressed(true);
                    if (itemClick != null) itemClick.onItemClick(list.get(position),position);
                }
            });

        }
        return view;
    }

    public interface OnSpinnerItemClick {
        void onItemClick(String itemName, int position);
    }

    public void setOnSpinnerItemClick(OnSpinnerItemClick itemClick) {
        this.itemClick = itemClick;
    }
}
