package com.chuanglgc.wuye.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.chuanglgc.wuye.R;

public class DiffLoginDialog extends Dialog {

    private TextView contenttv;
    private TextView positiveButton;
    private TextView negativeButton;
    private Activity activity;

    public DiffLoginDialog(@NonNull Context context) {
        super(context, R.style.dialog_spinner);
        setContentView(initView());
        show();
        //getAttributes得到属性
        Window window = getWindow();
        DisplayMetrics metrics = new DisplayMetrics();
        activity = (Activity) context;
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.dimAmount = 0.6f;//变暗程度
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//设置背景变暗
        setCanceledOnTouchOutside(false);
        setCancelable(false);

    }

    private View initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_diff_login, null);  //通过LayoutInflater获取布局
        contenttv = (TextView) view.findViewById(R.id.tv_content);
        positiveButton = (TextView) view.findViewById(R.id.bt_commit);
        negativeButton = (TextView) view.findViewById(R.id.bt_cancle);
        return view;
    }

    public DiffLoginDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    protected DiffLoginDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    //设置内容
    public void setContent(String content) {
        contenttv.setText(content);
    }

    public void setPositiveButtonText(String text) {
        positiveButton.setText(text);
    }

    //确定按钮监听
    public void setOnPositiveListener(View.OnClickListener listener) {
        positiveButton.setOnClickListener(listener);
        dismiss();
    }

    //否定按钮监听
    public void setOnNegativeListener(View.OnClickListener listener) {
        negativeButton.setOnClickListener(listener);
        dismiss();

    }
}
