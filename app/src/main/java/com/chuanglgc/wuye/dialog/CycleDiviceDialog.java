package com.chuanglgc.wuye.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.HandleFloorNumberAdapter;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.widget.MyGridViewManager;

import java.util.ArrayList;
import java.util.List;


public class CycleDiviceDialog extends Dialog {
    private final String content;

    public CycleDiviceDialog(@NonNull Context context, String content) {
        super(context, R.style.dialog_spinner);

        this.content = content;
        setContentView(initView());
        show();
        //getAttributes得到属性
        Window window = getWindow();
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager.LayoutParams lp = window.getAttributes();
        int height = metrics.heightPixels;
        lp.x = 0;
        lp.y = height / 2;
        //保证按钮可以水平铺满
       /* lp.height = DisplayUtil.dp2px(context, 400);*/
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;

        lp.dimAmount = 0.2f;//变暗程度
        window.setAttributes(lp);
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//设置背景变暗


    }

    private View initView() {
   /*     View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_handle_divice, null);  //通过LayoutInflater获取布局
        RecyclerView rclDivice = view.findViewById(R.id.rcl_divice);
        rclDivice.setLayoutManager(new MyGridViewManager(getContext(),4));
        HandleFloorNumberAdapter diviceAdapter = new HandleFloorNumberAdapter(list,true);
        diviceAdapter.bindToRecyclerView(rclDivice); */
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_handle_divice, null);  //通过LayoutInflater获取布局
        TextView textView = view.findViewById(R.id.tv_divice);
        textView.setText(content);

        return view;
    }

}
