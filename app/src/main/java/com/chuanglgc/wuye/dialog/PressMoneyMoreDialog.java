package com.chuanglgc.wuye.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.PressMoneyMoreAdapter;
import com.chuanglgc.wuye.model.PressMoneyDetailModel;

import java.util.List;


public class PressMoneyMoreDialog extends Dialog {

    private final List<PressMoneyDetailModel.UrgerecordBean> pressModels ;
    private RecyclerView rclSpinner;
    private OnSpinnerItemClick itemClick;

    public PressMoneyMoreDialog(@NonNull Context context, List<PressMoneyDetailModel.UrgerecordBean> pressModels) {
        super(context,R.style.dialog_spinner);
        this.pressModels=pressModels;
        setContentView(initView());
        show();
        //getAttributes得到属性
        Window window = getWindow();
        DisplayMetrics metrics = new DisplayMetrics();
        Activity activity = (Activity) context;
        Display defaultDisplay = activity.getWindowManager().getDefaultDisplay();
        defaultDisplay .getMetrics(metrics);
        WindowManager.LayoutParams lp = window.getAttributes();

        lp.gravity = Gravity.CENTER_HORIZONTAL;
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        if (pressModels!=null&&pressModels.size()>3){
            lp.height = (int) (metrics.heightPixels*0.75); // 高度
        }else if (pressModels!=null){
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT; // 高度
        }
        lp.dimAmount = 0.2f;//变暗程度
        window.setAttributes(lp);
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//设置背景变暗


    }

    private View initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_eventinfo, null);  //通过LayoutInflater获取布局
        rclSpinner = view.findViewById(R.id.rcl_spinner);
        if (pressModels!=null){
            rclSpinner.setLayoutManager(new LinearLayoutManager(getContext()));
            PressMoneyMoreAdapter adapter = new PressMoneyMoreAdapter(pressModels);
            adapter.bindToRecyclerView(rclSpinner);
        }
        return view;
    }
    public  interface OnSpinnerItemClick{
        void onItemClick(String itemName);
    }
    public void setOnSpinnerItemClick(OnSpinnerItemClick itemClick){
        this.itemClick=itemClick;
    }
}
