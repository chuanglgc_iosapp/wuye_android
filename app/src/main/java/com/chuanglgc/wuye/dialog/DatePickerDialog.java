package com.chuanglgc.wuye.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CalendarView;
import android.widget.DatePicker;

import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.utils.LogUtil;


public class DatePickerDialog {
    private Context context;
    private AlertDialog.Builder alertDialog;
    private TimePickerDialogInterface timePickerDialogInterface;
    private DatePicker datePicker;
    private int year, day, month;
    private int firstDayOfWeek;

    public DatePickerDialog(Context context) {
        super();
        this.context = context;
        //把这个上下文强制转化为一个接口
        timePickerDialogInterface = (TimePickerDialogInterface) context;
    }

    private View initDatePicker() {
        View inflate = LayoutInflater.from(context).inflate(R.layout.dialog_datepicker, null);
        datePicker = (DatePicker) inflate.findViewById(R.id.datePicker);
        return inflate;
    }

    private void initDialog(View view) {
        alertDialog.setPositiveButton("确定",
                new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        year = datePicker.getYear();
                        month = datePicker.getMonth();
                        LogUtil.e("月", month);
                        month += 1;
                        day = datePicker.getDayOfMonth();
                        String date = year + "年" + month + "月" + day + "日";

                        String currentDate = "";
                        if (year < 10) {
                            currentDate += "0" + year;
                        } else {
                            currentDate += year;
                        }
                        if (month < 10) {
                            currentDate += "."+"0" + month;
                        } else {
                            currentDate += "."+ month;
                        }
                        if (day < 10) {
                            currentDate +=  "."+"0" + day;
                        } else {
                            currentDate += "."+ day;
                        }
                        timePickerDialogInterface.positiveListener(date,currentDate);

                    }
                });
        alertDialog.setNegativeButton("取消",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        timePickerDialogInterface.negativeListener();
                        dialog.dismiss();
                    }
                });
        alertDialog.setView(view);
    }

    public void showDatePickerDialog() {
        View view = initDatePicker();
        alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("选择日期");
        initDialog(view);
        alertDialog.show();
    }

    public int getYear() {
        return year;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private String getDatePickerValue() {

        return year + "年" + month + "月" + day + "日";
    }

    public interface TimePickerDialogInterface {
        public void positiveListener(String date,String current);

        public void negativeListener();
    }

}
