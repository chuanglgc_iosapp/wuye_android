package com.chuanglgc.wuye.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.utils.DisplayUtil;


public class EventFinishDialog extends Dialog {


    private final String hintText;
    private OnCommitListener listener;

    /**
     *
     * @param context
     * @param hintText 设置EditText的hint 在事件中为默认，在催费中为催费记录
     */
    public EventFinishDialog(@NonNull Context context,String hintText) {
        super(context, R.style.dialog_spinner);
        this.hintText=hintText;
        setContentView(initView());
        show();
        //getAttributes得到属性
        Window window = getWindow();
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager.LayoutParams lp = window.getAttributes();
        int height = metrics.heightPixels;
        lp.x = 0;
        lp.y = height / 2;
        //保证按钮可以水平铺满
        lp.height = DisplayUtil.dp2px(context, 400);
        // lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = (int) (DisplayUtil.getScreenWidth(context)*0.85);

        lp.dimAmount = 0.2f;//变暗程度
        window.setAttributes(lp);
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//设置背景变暗


    }

    private View initView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_eventfinish, null);  //通过LayoutInflater获取布局
        View rlcommit = view.findViewById(R.id.rl_commit);
        EditText etRecord = view.findViewById(R.id.et_record);
        etRecord.setHint(hintText);
        rlcommit.setOnClickListener(v -> {
            if (listener != null) {
                String record = etRecord.getText().toString().trim();
                if (TextUtils.isEmpty(record)) {
                    Toast.makeText(getContext(), "请填写事件解决记录", Toast.LENGTH_SHORT).show();
                } else {
                    listener.onCommit(record);
                    this.dismiss();
                }

            }
        });
        return view;
    }

    public interface OnCommitListener {
        void onCommit(String record);
    }

    public void setOncommitListener(OnCommitListener listener) {
        this.listener = listener;
    }
}
