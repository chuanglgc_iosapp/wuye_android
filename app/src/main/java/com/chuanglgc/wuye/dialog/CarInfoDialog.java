package com.chuanglgc.wuye.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.CarNumberAdapter;
import com.chuanglgc.wuye.adapter.CarParkAdapter;
import com.chuanglgc.wuye.adapter.CarParkNoDateAdapter;
import com.chuanglgc.wuye.model.CarModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.utils.LogUtil;
import com.zhy.android.percent.support.PercentRelativeLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CarInfoDialog extends Dialog {


    private final List<String> plateNumber;
    private final String address;
    private List<String> parkNumber;
    private RecyclerView rclCarPark;
    private RecyclerView rclCarNumber;

    public CarInfoDialog(@NonNull Context context, String address, List<String> plateNumber, List<String> parkNumber) {
        super(context, R.style.dialog_spinner);
        setContentView(R.layout.dialog_car_info);
        this.plateNumber = plateNumber;
        this.parkNumber = parkNumber;
        this.address = address;
        initView();
        show();
        Window window = getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();

        lp.gravity = Gravity.CENTER_HORIZONTAL;
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        lp.dimAmount = 0.6f;//变暗程度
        window.setAttributes(lp);
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//设置背景变暗
        initRclCarPark(parkNumber);
        initRclCarNumber(plateNumber);

    }

    private void initView() {
        rclCarPark = findViewById(R.id.rcl_carPark);
        rclCarNumber = findViewById(R.id.rcl_car_number);
        TextView tvAddress = findViewById(R.id.tv_number);
        tvAddress.setText(address);
        //设置rcl的大小 当大于六条数据的时候 设置固定高度
        if (parkNumber.size() > 6) {
            PercentRelativeLayout.LayoutParams layoutParams = (PercentRelativeLayout.LayoutParams) rclCarPark.getLayoutParams();
            layoutParams.height = DisplayUtil.dp2px(getContext(), 260);
            rclCarPark.setLayoutParams(layoutParams);
        }
        if (plateNumber.size() > 6) {
            PercentRelativeLayout.LayoutParams layoutParams = (PercentRelativeLayout.LayoutParams) rclCarNumber.getLayoutParams();
            layoutParams.height = DisplayUtil.dp2px(getContext(), 150);
            rclCarNumber.setLayoutParams(layoutParams);
        }
    }


    private void initRclCarPark(List<String> parkNumber) {
        rclCarPark.setLayoutManager(new GridLayoutManager(getContext(), 2));
        CarParkNoDateAdapter carAdapter = new CarParkNoDateAdapter(parkNumber);
        rclCarPark.setAdapter(carAdapter);

    }

    private void initRclCarNumber(List<String> plateNumber) {
        rclCarNumber.setLayoutManager(new GridLayoutManager(getContext(), 2));
        CarNumberAdapter numberAdapter = new CarNumberAdapter(plateNumber);
        numberAdapter.bindToRecyclerView(rclCarNumber);
    }
}
