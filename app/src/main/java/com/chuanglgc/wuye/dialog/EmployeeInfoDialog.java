package com.chuanglgc.wuye.dialog;


import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.adapter.EmployeeCheckDialogAdapter;
import com.chuanglgc.wuye.model.MasterCheckWorkInfoModel;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.utils.LogUtil;

import java.util.ArrayList;

//领导查看员工考勤信息Dialog
public class EmployeeInfoDialog extends Dialog {
    private final MasterCheckWorkInfoModel model;
    private EmployeeCheckDialogAdapter checkAdapter;

    public EmployeeInfoDialog(@NonNull Context context, MasterCheckWorkInfoModel model) {
        super(context, R.style.dialog_spinner);
        this.model = model;
        setContentView(initView(context));
        show();
        Window window = getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();

        lp.gravity = Gravity.CENTER;
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        int size = model.getWork_info().size();
        if (size>=2){
            lp.height = (int) (DisplayUtil.getScreenHeight(context) * 0.8);
        }else {
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;;
        }

        lp.dimAmount = 0.4f;//变暗程度
        window.setAttributes(lp);
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//设置背景变暗
    }

    private View initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_employeeinfo, null);
        RecyclerView rclCheck = view.findViewById(R.id.rcl_check);
        TextView name = view.findViewById(R.id.tv_name);
        TextView date = view.findViewById(R.id.tv_date);
        TextView week = view.findViewById(R.id.tv_week);
        name.setText(model.getEmp_name());
        date.setText(model.getClockin_work_date());
        week.setText(model.getWeekday());

        if (checkAdapter == null) {
            checkAdapter = new EmployeeCheckDialogAdapter(model.getWork_info());
            rclCheck.setLayoutManager(new LinearLayoutManager(context));
            checkAdapter.bindToRecyclerView(rclCheck);
        } else {
            checkAdapter.setNewData(model.getWork_info());
        }
        return view;
    }
}
