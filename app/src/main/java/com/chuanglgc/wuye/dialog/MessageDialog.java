package com.chuanglgc.wuye.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.widget.AlignTextView;


public class MessageDialog extends Dialog {


    Context context;
    private TextView tvTitle;

    private AlignTextView tvContent;
    private TextView tvTime;
    private TextView tvLocation;

    public MessageDialog(Context context) {
        super(context, R.style.dialog_spinner);
        //先show 在设置属性 getAttributes得到属性
        this.setContentView(R.layout.dialog_msg);
        initview();
        show();
        Window window = getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();

        lp.gravity = Gravity.CENTER_HORIZONTAL;
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        lp.dimAmount = 0.3f;//变暗程度
        window.setAttributes(lp);
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//设置背景变暗
    }

    private void initview() {
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvContent = (AlignTextView) findViewById(R.id.tv_content);
        tvTime = (TextView) findViewById(R.id.tv_time);
        tvLocation = (TextView) findViewById(R.id.tv_location);

    }

    public void setData(String title, String content, String time, String location) {
        tvTitle.setText(title);
        tvContent.setText("\u3000\u3000" + content);
        tvContent.post(new Runnable() {
            @Override
            public void run() {
                int lineCount = tvContent.getLineCount();
                if (lineCount < 2) {

                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tvContent.getLayoutParams();
                    params.leftMargin=0;
                    tvContent.setLayoutParams(params);
                    //tvContent.setText( content);
                }
            }
        });
        tvTime.setText(time);
        tvLocation.setText(location);
    }
}
