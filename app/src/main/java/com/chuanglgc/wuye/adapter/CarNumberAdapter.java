package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;

import java.util.List;


public class CarNumberAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    public CarNumberAdapter(@Nullable List<String> data) {
        super(R.layout.adapter_card_number, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        helper.setText(R.id.tv_number, item);
    }
}
