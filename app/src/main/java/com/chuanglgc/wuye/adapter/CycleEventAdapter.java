package com.chuanglgc.wuye.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.activity.event_cycle.CycleEventDetailActivity;
import com.chuanglgc.wuye.model.CycleEventModel;

import java.util.List;
import java.util.TreeMap;


public class CycleEventAdapter extends BaseQuickAdapter<CycleEventModel, BaseViewHolder> {
    private TreeMap scaMap = new TreeMap<Integer, Boolean> ();

    public CycleEventAdapter(@Nullable List<CycleEventModel> data) {
        super(R.layout.adapter_cycle_event, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, CycleEventModel item) {
        helper.setText(R.id.tv_type, item.getCycle_task_name())
                .setText(R.id.tv_date, item.getTrigger_time());

        View view = helper.getView(R.id.rl_item);
        view.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, CycleEventDetailActivity.class);
            intent.putExtra("event_id",item.getCycle_task_id());
            intent.putExtra("date",item.getTrigger_time());
            mContext.startActivity(intent);
        });
        int position = helper.getLayoutPosition();
        CheckBox cb = helper.getView(R.id.cb);
        cb.setOnClickListener(v -> {
            if (cb.isChecked()) {
                scaMap.put(position, true);
            } else {
                scaMap.put(position, false);
            }
        });
    }

    /**
     * 获取选中的条目集合
     *
     * @return
     */
    public TreeMap<Integer, Boolean> getSleMap() {
        return scaMap;
    }

}
