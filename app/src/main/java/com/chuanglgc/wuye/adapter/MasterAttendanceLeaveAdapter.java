package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.MasterAttedanModel;
import com.chuanglgc.wuye.utils.LogUtil;

import java.util.List;


public class MasterAttendanceLeaveAdapter extends BaseQuickAdapter<MasterAttedanModel.LeaveEarlyBean.EarlyInfoBean, BaseViewHolder> {
    public MasterAttendanceLeaveAdapter(@Nullable List<MasterAttedanModel.LeaveEarlyBean.EarlyInfoBean> data) {
        super(R.layout.adapter_master_attendance_leave, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MasterAttedanModel.LeaveEarlyBean.EarlyInfoBean item) {

        if (item.getTimediff() != null && !TextUtils.isEmpty(item.getTimediff())) {
            Integer time = Integer.parseInt(item.getTimediff());
            int hours = time / 60;
            int day = time / 60 / 24;
            int minutes = time % 60;
            String leaveTime = "";
            if (day != 0) {
                leaveTime = day + "天";
            }
            if (hours != 0) {
                leaveTime = leaveTime + hours + "小时";
            }
            if (minutes != 0) {
                leaveTime = leaveTime + minutes + "分钟";
            }
            LogUtil.e("时间", time);
            LogUtil.e("时间", leaveTime);
            helper.setText(R.id.tv_late_info, "早退" + leaveTime);
        }
        helper.setText(R.id.tv_name, item.getEmp_name())
                .setText(R.id.tv_time, item.getShift_name())
                .setText(R.id.tv_late_time, "下班打卡"+item.getPunch_time());

    }
}
