package com.chuanglgc.wuye.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.chuanglgc.wuye.base.BasePager;

import java.util.ArrayList;


public class CarViewpagerAdapter extends PagerAdapter {
    private final ArrayList<BasePager> list;

    public CarViewpagerAdapter(ArrayList<BasePager>list) {
        this.list=list;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        BasePager basePager = list.get(position);
        container.addView(basePager.rootView);
        return basePager.rootView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(list.get(position).rootView);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }
}
