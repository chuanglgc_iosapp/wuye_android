package com.chuanglgc.wuye.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.activity.attendance.AttendanceActivity;
import com.chuanglgc.wuye.dialog.SpinnerDialog;
import com.chuanglgc.wuye.model.AttedanceModel;
import com.chuanglgc.wuye.model.BuildModel;
import com.chuanglgc.wuye.model.ScheduleModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.RoundTimeButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


//打卡记录
public class AttendanceAdapter extends BaseQuickAdapter<AttedanceModel, BaseViewHolder> {

    private TextView tvTime;
    public String scheduleId;
    private List<AttedanceModel.ScheduleCheckBean> checkBeans;

    public AttendanceAdapter(@Nullable List<AttedanceModel> data) {
        super(R.layout.adapter_attendance_list, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, AttedanceModel item) {
        int position = helper.getLayoutPosition();
        RecyclerView rclDay = helper.getView(R.id.rcl_day);
        RelativeLayout rlDayRoot = helper.getView(R.id.rl_day_root);
        RelativeLayout rlClassRoot = helper.getView(R.id.rc_class_root);
        TextView currentClass = helper.getView(R.id.tv_class_name);

        if (item == null) {
            rlClassRoot.setVisibility(View.VISIBLE);
            rlDayRoot.setVisibility(View.GONE);
            checkBeans = new ArrayList<AttedanceModel.ScheduleCheckBean>();
        } else {
            checkBeans = item.getSchedule_check();
            helper.setText(R.id.tv_class_name, item.getSchedule_name());
        }

        rclDay.setLayoutManager(new LinearLayoutManager(mContext));
        LogUtil.e("当前条目",checkBeans.size());
        AttendanceItemAdapter adapter = new AttendanceItemAdapter(checkBeans);
        if (getData().size() - 1 == position&&item!=null&&!item.isCheck_button_flag()){
            adapter.bindToRecyclerView(rclDay);
            return;
        }
        LogUtil.e("当前条目2222",checkBeans.size());
        if (getData().size() - 1 == position && checkBeans.size() < 2) {//  为最后一个条目，打卡次数小于2 添加Footer
            if (item != null) scheduleId = item.getSchedule_id();
            if (checkBeans.size() == 0) {//没有条目时候 显示选择班次
                rlDayRoot.setVisibility(View.GONE);
                rlClassRoot.setVisibility(View.VISIBLE);
            }
            if (checkBeans.size() == 1) {//有一个条目时候，显示当前班次
                rlDayRoot.setVisibility(View.VISIBLE);
                rlClassRoot.setVisibility(View.GONE);
            }
            View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_attendance_footer, null);
            RoundTimeButton timeButtom = view.findViewById(R.id.tb_time);
            tvTime = view.findViewById(R.id.tv_class_time);
            View needLine = view.findViewById(R.id.needline);
            if (checkBeans.size() != 0)
                needLine.setVisibility(View.VISIBLE);//如果Footer不是第一个条目则显示小竖线与之前连接上
            adapter.addFooterView(view);
            if (checkBeans.size() == 0) {//没有条目为上班打卡
                timeButtom.setTvClass("上班打卡");
            } else {//有一个条目为下班打卡
                timeButtom.setTvClass("下班打卡");
                tvTime.setText("下班时间" + item.getDefine_closing_time());
                needLine.setVisibility(View.VISIBLE);
            }
            timeButtom.setOnClickListener(v -> {
                //是否选择班次
                if (rlClassRoot.getVisibility() != View.GONE) {
                    Toast.makeText(mContext, "请先选择班次", Toast.LENGTH_SHORT).show();
                } else {
                    AttendanceActivity activity = (AttendanceActivity) mContext;
                    activity.checkCard(scheduleId);
                }
            });
        } else {//不是最后一个条目 显示出上下班打卡
            rlDayRoot.setVisibility(View.VISIBLE);
            rlClassRoot.setVisibility(View.GONE);
            adapter.setNewData(checkBeans);
        }
        adapter.bindToRecyclerView(rclDay);
        //选择班次 并给上班时间fuzhi
        TextView tvClass = helper.getView(R.id.tv_class);
        tvClass.setOnClickListener(v -> {
            showClassSpinner(rlClassRoot, rlDayRoot, currentClass, tvTime);
        });
    }

    /**
     * @param rlClassRoot 选择上班Spinner Gone
     * @param rlDayRoot   班次时间显示出来 8:00-12:00
     * @param tvTime      设置选择的上班时间
     */
    private void showClassSpinner(RelativeLayout rlClassRoot, RelativeLayout rlDayRoot, TextView tvClass, TextView tvTime) {
        Dialog loadingDialog = LoadingDialogUtils.createLoadingDialog(mContext, "", false);
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        map.put("department_id", EmployeeApplication.getInstance().getUserInfoFromCache().getDepartment_id());
        RestClient.getAPIService().queryScheduleList(map).enqueue(new Callback<Result<List<ScheduleModel>>>() {
            @Override
            public void onResponse(Call<Result<List<ScheduleModel>>> call, Response<Result<List<ScheduleModel>>> response) {
                if (response.code() == 200) {
                    Result<List<ScheduleModel>> listResult = response.body();
                    if (listResult.isResult()) {
                        List<ScheduleModel> scheduleModels = listResult.getData();
                        ArrayList<String> listClass = new ArrayList<>();
                        for (ScheduleModel model : scheduleModels) {
                            listClass.add(model.getSchedule_name());
                        }
                        SpinnerDialog classrDialog = new SpinnerDialog(mContext, listClass);
                        classrDialog.setOnSpinnerItemClick((names, position) -> {
                            classrDialog.dismiss();
                            //添加上班时间
                            String time = "上班时间" + scheduleModels.get(position).getDefine_working_time();
                            tvTime.setText(time);
                            tvClass.setText(scheduleModels.get(position).getSchedule_name());//设置班次
                            rlDayRoot.setVisibility(View.VISIBLE);
                            scheduleId = scheduleModels.get(position).getSchedule_id();
                            rlClassRoot.setVisibility(View.GONE);
                        });
                        classrDialog.show();
                    }

                }
                loadingDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Result<List<ScheduleModel>>> call, Throwable t) {
                loadingDialog.dismiss();
            }
        });
    }
}
