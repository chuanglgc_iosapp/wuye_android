package com.chuanglgc.wuye.adapter.event;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.OwnerEventModel;

import java.util.List;


public class OwnerEventAdapter extends BaseQuickAdapter<OwnerEventModel, BaseViewHolder> {
    public OwnerEventAdapter(@Nullable List<OwnerEventModel> data) {
        super(R.layout.adapter_owner_event, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, OwnerEventModel model) {
        String statusName = model.getTask_status_name();
        if (statusName.equals("处理中")) {
            helper.setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.bluelight))
                    .setText(R.id.tv_state, "处理中...");
        } else if (statusName.equals("未接受")) {
            helper.setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.colorAccent))
                    .setText(R.id.tv_state, "未接受...");
        }
        helper.setText(R.id.tv_room, model.getRepair_address())
                .setText(R.id.tv_type, model.getRepair_type())
                .setText(R.id.tv_date, model.getRepair_date())
                .setText(R.id.tv_time, model.getRepair_time());


    }
}
