package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.MasterAttendMonthModel;

import java.util.List;


public class AttendanceCountDayAdapter extends BaseQuickAdapter<MasterAttendMonthModel.AttendanceBean.AttendanceEmpnameBean, BaseViewHolder> {
    public AttendanceCountDayAdapter(@Nullable List<MasterAttendMonthModel.AttendanceBean.AttendanceEmpnameBean> data) {
        super(R.layout.adapter_attendance_count_day, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MasterAttendMonthModel.AttendanceBean.AttendanceEmpnameBean item) {
        helper.setText(R.id.tv_name, item.getEmp_name())
                .setText(R.id.tv_info, item.getInfo())
                .setText(R.id.tv_number, "出勤"+item.getTotal()+"天");

    }
}
