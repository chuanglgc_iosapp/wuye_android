package com.chuanglgc.wuye.adapter;


import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.PressMoneyModel;

import java.util.List;

public class PressMoneyAdapter extends BaseQuickAdapter<PressMoneyModel,BaseViewHolder> {
    public PressMoneyAdapter( @Nullable List<PressMoneyModel> data) {
        super(R.layout.adapter_press_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, PressMoneyModel item) {
        helper.setText(R.id.tv_address,item.getEstate_address())
                .setText(R.id.tv_money,item.getPayablemoney());
    }
}
