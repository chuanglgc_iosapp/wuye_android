package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.MasterAttendMonthModel;

import java.util.List;


public class AttendanceMonthLateAdapter extends BaseQuickAdapter<MasterAttendMonthModel.LateBean.LateInfoBean,BaseViewHolder> {
    public AttendanceMonthLateAdapter(@Nullable List<MasterAttendMonthModel.LateBean.LateInfoBean> data) {
        super(R.layout.adapter_attendance_month_leave, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MasterAttendMonthModel.LateBean.LateInfoBean item) {
        if (item.getTimediff() != null && !TextUtils.isEmpty(item.getTimediff())) {
            Integer time = Integer.parseInt(item.getTimediff());
            int hours = time / 60;
            int day = time / 60 / 24;
            int minutes = time % 60;
            String lateTime = "";
            if (day != 0) {
                lateTime = day + "天";
            }
            if (hours != 0) {
                lateTime = lateTime + hours + "小时";
            }
            if (minutes != 0) {
                lateTime = lateTime + minutes + "分钟";
            }

            helper.setText(R.id.tv_late_info, "迟到" + lateTime);
        }
        helper.setText(R.id.tv_name,item.getEmp_name())
                .setText(R.id.tv_time,item.getShift_name())
                .setText(R.id.tv_leave_date,item.getAttendance_date()+"  "+item.getWeekday());
    }
}
