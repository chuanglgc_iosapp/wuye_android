package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.MasterCheckWorkInfoModel;

import java.util.List;


public class EmployeeCheckDialogAdapter extends BaseQuickAdapter<MasterCheckWorkInfoModel.WorkInfoBean, BaseViewHolder> {
    public EmployeeCheckDialogAdapter(@Nullable List<MasterCheckWorkInfoModel.WorkInfoBean> data) {
        super(R.layout.adapter_employee_check_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MasterCheckWorkInfoModel.WorkInfoBean item) {
        String workState = item.getGoto_work_punch_state_number();
        String offState = item.getGooff_work_state_punch_number();
        if (workState.equals("0")){
            helper.getView(R.id.tv_state).setBackgroundResource(R.drawable.attendance_normal_bg);
        }else {
            helper.getView(R.id.tv_state).setBackgroundResource(R.drawable.attendance_late_bg);
        }
        if (offState.equals("0")){
            helper.getView(R.id.tv_off_state).setBackgroundResource(R.drawable.attendance_normal_bg);
        }else {
            helper.getView(R.id.tv_off_state).setBackgroundResource(R.drawable.attendance_late_bg);
        }
        helper.setText(R.id.tv_taskName, item.getShift_nameAtime())
                .setText(R.id.tv_work, item.getGo_to_work_time())
                .setText(R.id.tv_call_time, item.getGoto_work_punch_time())
                .setText(R.id.tv_state, item.getGoto_work_punch_state())

                .setText(R.id.tv_off_time, item.getGooff_work_punch_times())
                .setText(R.id.tv_offwork, item.getGo_off_work_time())
                .setText(R.id.tv_off_state, item.getGooff_work_punch_state());
    }
}
