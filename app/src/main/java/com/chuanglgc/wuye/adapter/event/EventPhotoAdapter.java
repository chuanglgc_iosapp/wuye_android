package com.chuanglgc.wuye.adapter.event;

import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.widget.photoview.ShowImageActivity;

import java.util.List;

public class EventPhotoAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    public EventPhotoAdapter(@Nullable List<String> data) {
        super(R.layout.adapter_eventadd_photo, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        int position = helper.getLayoutPosition();
        ImageView iv = helper.getView(R.id.iv);
        if (position != 0) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) iv.getLayoutParams();
            layoutParams.setMargins(DisplayUtil.dp2px(mContext, 12), 0, 0, 0);
            iv.setLayoutParams(layoutParams);
        }
        Glide.with(mContext).load(item).error(R.mipmap.ic_launcher).into(iv);
        iv.setOnClickListener(v -> {
            ShowImageActivity.start(mContext, item);
        });


    }
}
