package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;

import java.util.List;



public class SpinnerAdapter extends BaseQuickAdapter<String,BaseViewHolder> {
    public SpinnerAdapter( @Nullable List<String> data) {
        super(R.layout.adapter_spinner_dialog, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
       helper.setText(R.id.tv_dong_number,item);
    }
}
