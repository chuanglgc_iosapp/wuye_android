package com.chuanglgc.wuye.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.chuanglgc.wuye.base.BasePager;

import java.util.List;


public class SupervisePagerAdapter extends PagerAdapter {
    private final List<BasePager> pagerList;

    public SupervisePagerAdapter(List<BasePager>pagerList) {
        this.pagerList=pagerList;
    }

    @Override
    public int getCount() {
        return pagerList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        BasePager pager = pagerList.get(position);
        container.addView(pager.rootView);
        return pager.rootView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(pagerList.get(position).rootView);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }
}
