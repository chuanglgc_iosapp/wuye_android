package com.chuanglgc.wuye.adapter.event;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.VisitRecordModel;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.utils.LogUtil;
import com.zhy.android.percent.support.PercentRelativeLayout;

import java.util.List;


public class EventInfoAdapter extends BaseQuickAdapter<VisitRecordModel, BaseViewHolder> {
    public EventInfoAdapter(@Nullable List<VisitRecordModel> data) {
        super(R.layout.layout_event_detail, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, VisitRecordModel item) {
        int position = helper.getLayoutPosition();
        if (position == 0) {
            helper.getView(R.id.needline).setVisibility(View.INVISIBLE);//设置间距线不展示 并设置marginTop1 0
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) helper.getView(R.id.rootview).getLayoutParams();
            layoutParams.setMargins(0, DisplayUtil.dp2px(mContext, 10), 0, 0);
        } else if (position == getData().size() - 1) {//最后一个条目,设置marginBottom 10
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) helper.getView(R.id.rootview).getLayoutParams();
            layoutParams.setMargins(0, 0, 0, DisplayUtil.dp2px(mContext, 10));
        }

        helper.setText(R.id.tv_date, item.getFeedback_date())
                .setText(R.id.tv_time, item.getFeedback_time())
                .setText(R.id.tv_name, item.getContact_person())
                .setText(R.id.tv_call, item.getContact_info())
                .setText(R.id.tv_info, item.getFeedback_content());
    }
}
