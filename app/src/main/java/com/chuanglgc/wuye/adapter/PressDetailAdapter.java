package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.PressMoneyDetailModel;

import java.util.List;


public class PressDetailAdapter extends BaseQuickAdapter<PressMoneyDetailModel.PaylistBean, BaseViewHolder> {
    public PressDetailAdapter(@Nullable List<PressMoneyDetailModel.PaylistBean> data) {
        super(R.layout.adapter_perss_detail, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, PressMoneyDetailModel.PaylistBean item) {
        //设置布局
        int position = helper.getLayoutPosition();
        if (position == getData().size() - 1) {
            helper.getView(R.id.line).setVisibility(View.INVISIBLE);
        }
        if (position == 0) {
            View view = helper.getView(R.id.tv_type);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
            layoutParams.topMargin = 0;
            view.setLayoutParams(layoutParams);
        }

        helper.setText(R.id.tv_type, item.getArrearscostname())
                .setText(R.id.tv_time, item.getStartdate() + "~" + item.getEnddate())
                .setText(R.id.tv_money, item.getPayablemoney());

    }
}
