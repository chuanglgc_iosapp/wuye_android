package com.chuanglgc.wuye.adapter.event;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.EventManagerModel;

import java.util.List;

public class EventManagerAdapter extends BaseQuickAdapter<EventManagerModel, BaseViewHolder> {

    public EventManagerAdapter(List<EventManagerModel> data) {
        super(R.layout.adapter_event_manager, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, EventManagerModel item) {

        switch (item.getTask_status_id()) {
            case "2":
                helper.setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.bluelight));
                break;
            case "1":
                helper.setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.colorAccent));
                break;
            case "9":
                helper.setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.colorAccent));
                break;
        }
        helper.setText(R.id.tv_room, item.getRepair_address())
                .setText(R.id.tv_time, item.getRepair_time())
                .setText(R.id.tv_date, item.getRepair_date())
                .setText(R.id.tv_type, item.getRepair_type_name())
                .setText(R.id.tv_state, item.getTask_status_name()+"...");;
    }
}
