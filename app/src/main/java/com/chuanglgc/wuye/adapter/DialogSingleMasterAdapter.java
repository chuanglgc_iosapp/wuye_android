package com.chuanglgc.wuye.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.utils.LogUtil;

import org.w3c.dom.Text;

import java.util.List;


public class DialogSingleMasterAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    private int slcItem = 0;
    private boolean isSle=false;//记录是否选中


    public DialogSingleMasterAdapter(@Nullable List<String> data) {
        super(R.layout.adapter_dialog_master_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        int position = helper.getLayoutPosition();
        helper.setText(R.id.tv_master_name, item);
        View view = helper.getView(R.id.tv_master_name);
        //设置选中条目
        if (position==slcItem){
            view.setSelected(true);
        }else {
            view.setSelected(false);//其他条目不选选中 不设置会导致第二页的条目也选中的bug
        }
        helper.getConvertView().setOnClickListener(v -> {
            if (slcItem==position){//点击同一个条目是设置相反的选择器
                isSle=!view.isSelected();
                view.setSelected(!view.isSelected());
            }else {
                isSle=true;
                slcItem = position;
                notifyDataSetChanged();
            }
        });
    }

    /**
     *
     * @return 如果为-1表示没有选中任何条目
     */
 public int getCurrentSlcItemPositon(){
        if (isSle){
            return slcItem;
        }else {
            return -1;
        }
 }
}
