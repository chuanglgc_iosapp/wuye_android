package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.utils.LogUtil;

import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class PartnerDialogAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    private TreeMap<Integer, Boolean> sleMap = new TreeMap<>();

    public PartnerDialogAdapter(@Nullable List<String> data) {
        super(R.layout.dialog_partner_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        helper.setText(R.id.cb, item);
        CheckBox cb = helper.getView(R.id.cb);
        int position = helper.getLayoutPosition();

        if (sleMap.containsKey(position)) {
            cb.setChecked(sleMap.get(position));
        } else {
            cb.setChecked(false);
        }
        LogUtil.e("集合数量",sleMap.size());
        LogUtil.e("集合数量",position);
        helper.setOnClickListener(R.id.cb, v -> {
            if (cb.isChecked()) {
                sleMap.put(position, true);
            } else {
                sleMap.put(position, false);
            }
            notifyDataSetChanged();//每次记录当前状态 更新  否者有第二页加载有些条目选中bug
        });

    }

    public TreeMap<Integer, Boolean> getSleMap() {
        return sleMap;
    }
}
