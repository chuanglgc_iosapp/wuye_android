package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.PersonalStatisticsModel;

import java.util.List;


public class AttendanceClassAdapter extends BaseQuickAdapter<PersonalStatisticsModel.ShiftBean.ShiftTimeBean, BaseViewHolder> {
    public AttendanceClassAdapter(@Nullable List<PersonalStatisticsModel.ShiftBean.ShiftTimeBean> data) {
        super(R.layout.adapter_attendance_daynumber_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, PersonalStatisticsModel.ShiftBean.ShiftTimeBean item) {
        helper.setText(R.id.tv_content, item.getShift_name())
                .setText(R.id.tv_count, item.getShift_dayscount());

    }
}
