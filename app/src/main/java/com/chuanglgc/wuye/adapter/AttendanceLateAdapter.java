package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.PersonalStatisticsModel;
import com.chuanglgc.wuye.utils.LogUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class AttendanceLateAdapter extends BaseQuickAdapter<PersonalStatisticsModel.LateBean.LateInfoBean, BaseViewHolder> {
    public AttendanceLateAdapter(@Nullable List<PersonalStatisticsModel.LateBean.LateInfoBean> data) {
        super(R.layout.adapter_attendance_leave_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, PersonalStatisticsModel.LateBean.LateInfoBean item) {
        LogUtil.e("时间",item.getTimediff());
        if (item.getTimediff()!=null&&!TextUtils.isEmpty(item.getTimediff())){
            Integer time = Integer.parseInt(item.getTimediff());
            int hours = time / 60;
            int day = time / 60 / 24;
            int  minutes= time % 60;
            String lateTime = "";
            if (day!=0){
                lateTime=day+"天";
            }
            if (hours!=0){
                lateTime=lateTime+hours+"小时";
            }
            if (minutes!=0){
                lateTime=lateTime+minutes+"分钟";
            }
            LogUtil.e("时间",time);
            LogUtil.e("时间",lateTime);
            helper  .setText(R.id.tv_late_info,"迟到"+lateTime);
        }


        helper.setText(R.id.tv_date,item.getAttendance_date()+"  "+item.getWeekday())
                       .setText(R.id.tv_time,item.getShift_name())
                       .setText(R.id.tv_late_time,"上班打卡"+item.getPunch_time());

    }
}
