package com.chuanglgc.wuye.adapter;


import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.CycleTaskModel;

import java.util.List;


public class HandleEventAdapter extends BaseQuickAdapter<CycleTaskModel, BaseViewHolder> {



    public HandleEventAdapter(List<CycleTaskModel> data) {
        super(R.layout.adapter_handle_event, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, CycleTaskModel item) {
        if (item.getCycle_task_status_id().equals("1")) {
            helper.setText(R.id.tv_state, "未接受...")
                    .setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.colorAccent));
        } else {
            helper.setText(R.id.tv_state, "处理中...")
                    .setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.bluelight));
        }
        if (item.getFacilities_list() != null) {
            String  devices = item.getFacilities_list().replace(",", "，");
            helper.setText(R.id.tv_area, devices);
        }
        helper.setText(R.id.tv_date, item.getCycle_task_date())
                .setText(R.id.tv_type, item.getCycle_task_name());
    }
}
