package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.PersonalStatisticsModel;

import java.util.List;


public class AttendanceDayAdapter extends BaseQuickAdapter<PersonalStatisticsModel.AttendanceBean.AttendanceDateBean, BaseViewHolder> {
    public AttendanceDayAdapter(@Nullable List<PersonalStatisticsModel.AttendanceBean.AttendanceDateBean> data) {
        super(R.layout.adapter_attendance_daynumber_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, PersonalStatisticsModel.AttendanceBean.AttendanceDateBean item) {
        helper.setText(R.id.tv_content, item.getAttendance_date());
        helper.getView(R.id.tv_count).setVisibility(View.GONE);
    }
}
