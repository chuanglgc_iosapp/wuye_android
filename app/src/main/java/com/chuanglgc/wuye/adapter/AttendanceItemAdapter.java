package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.AttedanceModel;
import com.chuanglgc.wuye.utils.DisplayUtil;
import com.chuanglgc.wuye.utils.LogUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


//打卡记录
public class AttendanceItemAdapter extends BaseQuickAdapter<AttedanceModel.ScheduleCheckBean, BaseViewHolder> {
    public AttendanceItemAdapter(@Nullable List<AttedanceModel.ScheduleCheckBean> data) {
        super(R.layout.adapter_attendance_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, AttedanceModel.ScheduleCheckBean item) {
        helper.setText(R.id.tv_call_time, "打卡时间" + item.getActual_working_time())
                .setText(R.id.tv_work, item.getSchedule_name() + item.getDefine_working_time());
        String checkStatus = item.getCheck_status();
        TextView tvState = helper.getView(R.id.tv_state);
        tvState.setText(checkStatus);
        if (checkStatus!=null&&checkStatus.equals("正常")) {
            tvState.setBackgroundResource(R.drawable.attendance_normal_bg);
        } else {
            tvState.setBackgroundResource(R.drawable.attendance_late_bg);
        }


        int position = helper.getLayoutPosition();
        if (position == 1) {//如果为下班条目 则高度设置110dp 设置下班时间
            View rootView = helper.getView(R.id.rl_attendance_item_root);
            ViewGroup.LayoutParams layoutParams = rootView.getLayoutParams();
            layoutParams.height = DisplayUtil.dp2px(mContext, 70);
            rootView.setLayoutParams(layoutParams);

        }
         LogUtil.e("footer",getFooterLayoutCount());
        if (getFooterLayoutCount() == 0) {//如果没有Footer
            if (position == getData().size() - 1) {//为最后一个条目
                helper.getView(R.id.line).setVisibility(View.INVISIBLE);
                if (position!=0){
                    helper.getView(R.id.needline).setVisibility(View.VISIBLE);//如果不是第一个条目 显示
                }
                //  helper.getView(R.id.round).setBackground(mContext.getResources().getDrawable(R.drawable.round));
            }
        } else {
            if (getData().size() != 1 && position != 0) {
                helper.getView(R.id.line).setVisibility(View.VISIBLE);
                helper.getView(R.id.needline).setVisibility(View.VISIBLE);
            }
        }

    }
}
