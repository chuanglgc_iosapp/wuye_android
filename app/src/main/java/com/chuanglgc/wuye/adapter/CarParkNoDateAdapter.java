package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;

import java.util.List;


public class CarParkNoDateAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    public CarParkNoDateAdapter(@Nullable List<String> data) {
        super(R.layout.adapter_carpark_nodate, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        helper.setText(R.id.tv_number, item);
    }
}
