package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.OwnerInfoModel;

import java.util.List;


public class CarParkAdapter extends BaseQuickAdapter<OwnerInfoModel.ParkNumberBean,BaseViewHolder> {
    public CarParkAdapter( @Nullable List<OwnerInfoModel.ParkNumberBean> data) {
        super(R.layout.adapter_carpark, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, OwnerInfoModel.ParkNumberBean item) {
             helper.setText(R.id.tv_number,item.getPark_number())
                     .setText(R.id.tv_date,"("+item.getPayment_date()+")");

    }
}
