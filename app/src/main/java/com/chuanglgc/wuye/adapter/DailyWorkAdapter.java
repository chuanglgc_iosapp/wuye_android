package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.DailyTaskModel;

import java.util.List;


public class DailyWorkAdapter extends BaseQuickAdapter<DailyTaskModel, BaseViewHolder> {
    public DailyWorkAdapter(@Nullable List<DailyTaskModel> data) {
        super(R.layout.adapter_dailywork, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, DailyTaskModel item) {
        if (item.getDaily_task_status().equals("处理中")) {
            helper.setText(R.id.tv_state, "处理中...")
                    .setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.bluelight));
        } else if (item.getDaily_task_status().equals("未接受")) {
            helper.setText(R.id.tv_state, "未接受...")
                    .setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.colorAccent));

        }
        helper.setText(R.id.tv_area, item.getDaily_task_areaordevice())
                .setText(R.id.tv_type, item.getDaily_task_flag())
                .setText(R.id.tv_number, "共" + item.getDaily_task_total() + "次/"
                        + "剩" + item.getDaily_task_rest() + "次");
    }
}
