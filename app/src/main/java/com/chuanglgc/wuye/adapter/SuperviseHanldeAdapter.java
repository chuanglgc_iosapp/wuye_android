package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.SuperviseDailyModel;
import com.chuanglgc.wuye.model.SuperviseHandleModel;

import java.util.List;


public class SuperviseHanldeAdapter extends BaseQuickAdapter<SuperviseHandleModel, BaseViewHolder> {
    public SuperviseHanldeAdapter(@Nullable List<SuperviseHandleModel> data) {
        super(R.layout.adapter_supervise_handle, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SuperviseHandleModel item) {
        switch (item.getCycle_task_status_name()) {
            case "处理中":
                helper.setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.bluelight))
                        .setText(R.id.tv_state, "处理中...");
                break;
            case "未接受":
                helper.setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.colorAccent))
                        .setText(R.id.tv_state, "未接受...");
                break;
            case "待评价":
                helper.setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.yellow))
                        .setText(R.id.tv_state, "待评价...");
                break;
        }
        helper.setText(R.id.tv_name, item.getShip_name())
                .setText(R.id.tv_type, item.getCycle_task_content())
                .setText(R.id.tv_date, item.getCycle_task_date())
                .setText(R.id.tv_time, item.getCycle_task_time());

    }
}
