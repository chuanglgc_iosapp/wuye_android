package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.SuperviseDailyModel;

import java.util.List;


public class SuperviseDailyAdapter extends BaseQuickAdapter<SuperviseDailyModel, BaseViewHolder> {
    public SuperviseDailyAdapter(@Nullable List<SuperviseDailyModel> data) {
        super(R.layout.adapter_supervise_daily, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SuperviseDailyModel item) {
        switch (item.getWork_status()) {
            case "处理中":
                helper.setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.bluelight))
                        .setText(R.id.tv_state, "处理中...");
                break;
            case "未接受":
                helper.setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.colorAccent))
                        .setText(R.id.tv_name, "于得水")
                        .setText(R.id.tv_state, "未接受...");
                break;
            case "待评价":
                helper.setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.yellow))
                        .setText(R.id.tv_state, "待评价...");
                break;
        }
        helper.setText(R.id.tv_name, item.getShip_name())
                .setText(R.id.tv_work, item.getWork_content());

    }
}
