package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.SuperviseOwnerModel;

import java.util.List;


public class SuperviseOwnerAdapter extends BaseQuickAdapter<SuperviseOwnerModel, BaseViewHolder> {
    public SuperviseOwnerAdapter(@Nullable List<SuperviseOwnerModel> data) {
        super(R.layout.adapter_supervise_owner, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SuperviseOwnerModel item) {
        if (item.getStatus() != null) {
            switch (item.getStatus()) {
                case "处理中":
                    helper.setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.bluelight))
                            .setText(R.id.tv_state, "处理中...");
                    break;
                case "未接受":
                    helper.setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.colorAccent))
                            .setText(R.id.tv_state, "未接受...");
                    break;
                case "待评价":
                    helper.setTextColor(R.id.tv_state, mContext.getResources().getColor(R.color.yellow))
                            .setText(R.id.tv_state, "待评价..");
                    break;
            }
        }
        helper.setText(R.id.tv_name, item.getEmp_name())
                .setText(R.id.tv_work, item.getRepair_task_name())
                .setText(R.id.tv_date, item.getRepair_task_date())
                .setText(R.id.tv_time, item.getRepair_task_time());

    }
}
