package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.MasterAttedanModel;
import com.chuanglgc.wuye.model.PersonalStatisticsModel;

import java.util.List;


public class MasterAttendanceClassAdapter extends BaseQuickAdapter<MasterAttedanModel.ShiftBean.ShiftTimeBean, BaseViewHolder> {
    public MasterAttendanceClassAdapter(@Nullable List<MasterAttedanModel.ShiftBean.ShiftTimeBean> data) {
        super(R.layout.adapter_attendance_daynumber_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MasterAttedanModel.ShiftBean.ShiftTimeBean item) {
        helper.setText(R.id.tv_content, item.getShift_name())
                .setText(R.id.tv_count, item.getShift_empnumber());

    }
}
