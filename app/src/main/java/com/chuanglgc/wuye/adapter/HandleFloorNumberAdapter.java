package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.utils.LogUtil;
import com.chuanglgc.wuye.widget.MyGridViewManager;

import java.text.SimpleDateFormat;
import java.util.List;

public class HandleFloorNumberAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    private final boolean visible;

    /**
     * @param visiable true 不隐藏 第三行和第四行后两个数据 false 隐藏
     */
    public HandleFloorNumberAdapter(@Nullable List<String> data, boolean visiable) {
        super(R.layout.adapter_floor_number, data);
        this.visible = visiable;

    }


    @Override
    protected void convert(BaseViewHolder helper, String item) {
        int position = helper.getLayoutPosition();
        helper.setText(R.id.tv_device, item);
        //从零开始书 第三行和第四行后两个隐藏 显示更多按钮
        if (!visible) {
            if (position == 10 || position == 11 || position == 14 || position == 15) {
                helper.getConvertView().setVisibility(View.INVISIBLE);
            }
            if (position > 15) {
                MyGridViewManager layoutManager = (MyGridViewManager) getRecyclerView().getLayoutManager();
                layoutManager.setScrollEnabled(false);//设置不可滑动
                helper.getConvertView().setVisibility(View.GONE);
            }
        }
    }
}
