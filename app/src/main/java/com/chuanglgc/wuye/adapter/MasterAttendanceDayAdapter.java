package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.MasterAttedanModel;

import java.util.List;



public class MasterAttendanceDayAdapter extends BaseQuickAdapter<MasterAttedanModel.AttendanceBean.AttendanceEmpnameBean,BaseViewHolder> {
    public MasterAttendanceDayAdapter(@Nullable List<MasterAttedanModel.AttendanceBean.AttendanceEmpnameBean> data) {
        super(R.layout.adapter_master_attendance_day, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MasterAttedanModel.AttendanceBean.AttendanceEmpnameBean item) {
        helper.setText(R.id.tv_name,item.getEmp_name())
        .setText(R.id.tv_content,"("+item.getInfo()+")");

    }
}
