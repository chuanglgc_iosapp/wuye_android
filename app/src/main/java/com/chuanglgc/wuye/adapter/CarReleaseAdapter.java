package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.CarVisitorModel;

import java.util.List;


public class CarReleaseAdapter extends BaseQuickAdapter<CarVisitorModel, BaseViewHolder> {
    public CarReleaseAdapter(@Nullable List<CarVisitorModel> data) {
        super(R.layout.adapter_card_release, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, CarVisitorModel item) {
        helper.setText(R.id.tv_car_number, item.getPlate_number())
                .setText(R.id.tv_date, item.getEntry_date())
                .setText(R.id.tv_car_time, item.getEntry_time());
        if (item.getRemark()==null||item.getRemark().equals("")){
            helper .setText(R.id.tv_car_info, "无备注");
        }else {
            helper .setText(R.id.tv_car_info, item.getRemark());
        }

    }
}
