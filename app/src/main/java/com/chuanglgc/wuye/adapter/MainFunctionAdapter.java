package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.LoginModel;
import com.chuanglgc.wuye.utils.recyclerchange.ItemTouchHelperAdapter;

import java.util.Collections;
import java.util.List;


public class MainFunctionAdapter extends BaseQuickAdapter<LoginModel.MenuBean, BaseViewHolder>implements
        ItemTouchHelperAdapter {

    public MainFunctionAdapter(@Nullable List<LoginModel.MenuBean> data) {
        super(R.layout.adapter_mainfunction, data);

    }


    @Override
    protected void convert(BaseViewHolder helper, LoginModel.MenuBean item) {
    /*    if (helper.getLayoutPosition()<=2){
            View view = helper.getView(R.id.rl_func);
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) view.getLayoutParams();
            params.topMargin=0;
            view.setLayoutParams(params);
        }*/
        helper.setText(R.id.tv_function, item.getAA2101());
        switch (item.getAA2100()) {
            case "7001":
                helper.setImageResource(R.id.iv_function, R.mipmap.ownerevent);
                break;
            case "7002":
                helper.setImageResource(R.id.iv_function, R.mipmap.managerevent);
                break;
            case "7003":
                helper.setImageResource(R.id.iv_function, R.mipmap.nearhandle);
                break;
            case "7004":
                helper.setImageResource(R.id.iv_function, R.mipmap.cycleevent);
                break;
            case "7005":
                helper.setImageResource(R.id.iv_function, R.mipmap.jiandu);
                break;
            case "7006":
                helper.setImageResource(R.id.iv_function, R.mipmap.message);
                break;
            case "7007":
                helper.setImageResource(R.id.iv_function, R.mipmap.ownersearch);
                break;
            case "7008":
                helper.setImageResource(R.id.iv_function, R.mipmap.carsearch);
                break;
            case "7009":
                helper.setImageResource(R.id.iv_function, R.mipmap.carrelease);
                break;
            case "7010":
                helper.setImageResource(R.id.iv_function, R.mipmap.daka);
                break;
            case "7011":
                helper.setImageResource(R.id.iv_function, R.mipmap.systemset);
                break;
            case "7012":
                helper.setImageResource(R.id.iv_function, R.mipmap.dailywork);
                break;
            case "7013":
                helper.setImageResource(R.id.iv_function, R.mipmap.kaoqin);
                break;
            case "7014":
                helper.setImageResource(R.id.iv_function, R.mipmap.eventhandle);
                break;
            case "7015":
                helper.setImageResource(R.id.iv_function, R.mipmap.press_money);
                break;

        }

    }


    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        //交换位置 如果位置超过了 不交换
        if (toPosition==mData.size())return;
        Collections.swap(mData, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemDissmiss(int position) {
        mData.remove(position);
        notifyItemRemoved(position);
    }
}
