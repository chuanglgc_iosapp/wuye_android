package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.model.PressMoneyDetailModel;
import com.chuanglgc.wuye.utils.DisplayUtil;

import java.util.List;


public class PressMoneyMoreAdapter extends BaseQuickAdapter<PressMoneyDetailModel.UrgerecordBean, BaseViewHolder> {
    public PressMoneyMoreAdapter(@Nullable List<PressMoneyDetailModel.UrgerecordBean> data) {
        super(R.layout.adapter_perss_detail_more, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, PressMoneyDetailModel.UrgerecordBean item) {
        int position = helper.getLayoutPosition();
        if (position == 0) {
            helper.getView(R.id.needline).setVisibility(View.INVISIBLE);//设置间距线不展示 并设置marginTop1 0
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) helper.getView(R.id.rootview).getLayoutParams();
            layoutParams.setMargins(0, DisplayUtil.dp2px(mContext, 10), 0, 0);
        } else if (position == getData().size() - 1) {//最后一个条目,设置marginBottom 10
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) helper.getView(R.id.rootview).getLayoutParams();
            layoutParams.setMargins(0, 0, 0, DisplayUtil.dp2px(mContext, 10));
        }

        helper.setText(R.id.tv_info_date, item.getEntrydate())
                .setText(R.id.tv_info_time, item.getEntrytime())
                .setText(R.id.tv_info_detail, item.getUrgecontent());
    }
}
