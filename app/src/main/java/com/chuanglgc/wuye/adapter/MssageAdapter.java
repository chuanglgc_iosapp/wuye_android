package com.chuanglgc.wuye.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BaseActivity;
import com.chuanglgc.wuye.base.BasePersenter;
import com.chuanglgc.wuye.model.MessageModel;

import java.util.List;


public class MssageAdapter extends BaseQuickAdapter<MessageModel, BaseViewHolder> {


    public MssageAdapter(@Nullable List<MessageModel> data) {
        super(R.layout.adapter_msg_item, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, MessageModel item) {
        helper.setText(R.id.tv_level, item.getCY0201())
                .setText(R.id.tv_date, item.getCY0206())
                .setText(R.id.tv_content, item.getCY0202());
    }
}
