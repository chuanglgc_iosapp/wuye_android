package com.chuanglgc.wuye.pager;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.activity.event_supervise.SuperviseActivity;
import com.chuanglgc.wuye.activity.event_supervise.SuperviseOwnerDetailActivity;
import com.chuanglgc.wuye.adapter.SuperviseOwnerAdapter;
import com.chuanglgc.wuye.base.BasePager;
import com.chuanglgc.wuye.model.SuperviseOwnerModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SuperviseOwnerPager extends BasePager {
    RecyclerView rclOwner;
    private OnItemClickListener onItemClickListener;

    public SuperviseOwnerPager(Context context) {
        super(context);
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {
        Dialog loadingDialog = LoadingDialogUtils.createLoadingDialog(mActivity, "", false);
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());

        RestClient.getAPIService().querySuperviseRepairTask(map).enqueue(new Callback<Result<List<SuperviseOwnerModel>>>() {
            @Override
            public void onResponse(Call<Result<List<SuperviseOwnerModel>>> call, Response<Result<List<SuperviseOwnerModel>>> response) {
                if (response.code() == 200) {
                    Result<List<SuperviseOwnerModel>> result = response.body();
                    if (result.isResult()) {
                        List<SuperviseOwnerModel> ownerModels = result.getData();
                        initRclDaily(ownerModels);
                    } else {
                        LogUtil.e("每日任务", result.getMsg());
                    }
                } else {
                    LogUtil.e("每日任务", response.code());
                }
                loadingDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Result<List<SuperviseOwnerModel>>> call, Throwable t) {
                LogUtil.e("每日任务", t.toString());
                loadingDialog.dismiss();
            }
        });
    }

    @Override
    public View initView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.pager_supervise_owner, null);
        rclOwner = view.findViewById(R.id.rcl_owner);
        return view;
    }

    private void initRclDaily(List<SuperviseOwnerModel> ownerModels) {
        rclOwner.setLayoutManager(new LinearLayoutManager(mActivity));

        SuperviseOwnerAdapter handleAdapter = new SuperviseOwnerAdapter(ownerModels);
        rclOwner.setAdapter(handleAdapter);
        if (onItemClickListener != null) rclOwner.removeOnItemTouchListener(onItemClickListener);
        onItemClickListener = new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(mActivity, SuperviseOwnerDetailActivity.class);
                intent.putExtra("state", ownerModels.get(position).getStatus());
                intent.putExtra("task_id", ownerModels.get(position).getTask_id());
                SuperviseActivity activity = (SuperviseActivity) mActivity;
                activity.startActivityForResult(intent, 2);
            }
        };
        rclOwner.addOnItemTouchListener(onItemClickListener);
    }

    @Override
    public void onClick(View v) {

    }
}
