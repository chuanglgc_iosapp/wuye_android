package com.chuanglgc.wuye.pager;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.activity.event_supervise.SuperviseActivity;
import com.chuanglgc.wuye.activity.event_supervise.SuperviseHandleDetailActivity;
import com.chuanglgc.wuye.adapter.SuperviseHanldeAdapter;
import com.chuanglgc.wuye.base.BasePager;
import com.chuanglgc.wuye.model.SuperviseHandleModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SuperviseHanldlePager extends BasePager {
    private RecyclerView rclHandle;
    private OnItemClickListener onItemClickListener;

    public SuperviseHanldlePager(Context context) {
        super(context);
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {
        Dialog loadingDialog = LoadingDialogUtils.createLoadingDialog(mActivity, "", false);
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        RestClient.getAPIService().querySuperviseCycleTaskList(map).enqueue(new Callback<Result<List<SuperviseHandleModel>>>() {
            @Override
            public void onResponse(Call<Result<List<SuperviseHandleModel>>> call, Response<Result<List<SuperviseHandleModel>>> response) {
                if (response.code() == 200) {
                    Result<List<SuperviseHandleModel>> result = response.body();
                    if (result.isResult()) {
                        List<SuperviseHandleModel> dailyModels = result.getData();
                        initRclDaily(dailyModels);
                    } else {
                        LogUtil.e("事务处理", result.getMsg());
                    }
                } else {
                    LogUtil.e("事务处理", response.code());
                }
                loadingDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Result<List<SuperviseHandleModel>>> call, Throwable t) {
                LogUtil.e("事务处理", t.toString());
                loadingDialog.dismiss();
            }
        });
    }

    @Override
    public View initView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.pager_supervise_daily, null);
        rclHandle = view.findViewById(R.id.rcl_daily);
        return view;
    }

    private void initRclDaily(List<SuperviseHandleModel> dailyModels) {
        rclHandle.setLayoutManager(new LinearLayoutManager(mActivity));
        SuperviseHanldeAdapter handleAdapter = new SuperviseHanldeAdapter(dailyModels);
        rclHandle.setAdapter(handleAdapter);
        if (onItemClickListener != null) rclHandle.removeOnItemTouchListener(onItemClickListener);
        onItemClickListener = new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(mActivity, SuperviseHandleDetailActivity.class);
                intent.putExtra("state", dailyModels.get(position).getCycle_task_status_name());
                intent.putExtra("task_id", dailyModels.get(position).getCycle_task_id());
                intent.putExtra("task_name", dailyModels.get(position).getCycle_task_content());
                intent.putExtra("task_time", dailyModels.get(position).getCycle_task_time());
                LogUtil.e("状态", dailyModels.get(position).getCycle_task_id());
                SuperviseActivity activity= (SuperviseActivity) mActivity;
                activity.startActivityForResult(intent,2);
            }
        };
        rclHandle.addOnItemTouchListener(onItemClickListener);
    }

    @Override
    public void onClick(View v) {

    }
}
