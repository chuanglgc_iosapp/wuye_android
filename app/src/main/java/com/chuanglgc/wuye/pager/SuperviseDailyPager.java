package com.chuanglgc.wuye.pager;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.activity.event_supervise.SuperviseActivity;
import com.chuanglgc.wuye.activity.event_supervise.SuperviseDailyDetailActivity;
import com.chuanglgc.wuye.adapter.SuperviseDailyAdapter;
import com.chuanglgc.wuye.base.BasePager;
import com.chuanglgc.wuye.model.SuperviseDailyModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;
import com.chuanglgc.wuye.utils.LogUtil;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SuperviseDailyPager extends BasePager {
    RecyclerView rclDaily;
    private OnItemClickListener onItemClickListener;

    public SuperviseDailyPager(Context context) {
        super(context);
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {
        Dialog loadingDialog = LoadingDialogUtils.createLoadingDialog(mActivity, "", false);
        HashMap<String, String> map = new HashMap<>();
        map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
        map.put("emp_id", EmployeeApplication.getInstance().getUserInfoFromCache().getEmp_id());
        RestClient.getAPIService().queryDailyWorkStatusList(map).enqueue(new Callback<Result<List<SuperviseDailyModel>>>() {
            @Override
            public void onResponse(Call<Result<List<SuperviseDailyModel>>> call, Response<Result<List<SuperviseDailyModel>>> response) {
                if (response.code() == 200) {
                    Result<List<SuperviseDailyModel>> result = response.body();
                    if (result.isResult()) {
                        List<SuperviseDailyModel> dailyModels = result.getData();
                        initRclDaily(dailyModels);
                    } else {
                        LogUtil.e("每日任务", result.getMsg());
                    }
                } else {
                    LogUtil.e("每日任务", response.code());
                }
                loadingDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Result<List<SuperviseDailyModel>>> call, Throwable t) {
                LogUtil.e("每日任务", t.toString());
                loadingDialog.dismiss();
            }
        });
    }

    @Override
    public View initView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.pager_supervise_daily, null);
        rclDaily = view.findViewById(R.id.rcl_daily);
        return view;
    }

    private void initRclDaily(List<SuperviseDailyModel> dailyModels) {
        rclDaily.setLayoutManager(new LinearLayoutManager(mActivity));
        SuperviseDailyAdapter dailyAdapter = new SuperviseDailyAdapter(dailyModels);
        rclDaily.setAdapter(dailyAdapter);
        if (onItemClickListener!=null)rclDaily.removeOnItemTouchListener(onItemClickListener);
        onItemClickListener = new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(mActivity, SuperviseDailyDetailActivity.class);
                intent.putExtra("state", dailyModels.get(position).getWork_status());
                intent.putExtra("task_id", dailyModels.get(position).getDaily_task_id());
                SuperviseActivity mActivity = (SuperviseActivity) SuperviseDailyPager.this.mActivity;
                mActivity.startActivityForResult(intent,1);
            }
        };
        rclDaily.addOnItemTouchListener(onItemClickListener);
    }

    @Override
    public void onClick(View v) {

    }
}
