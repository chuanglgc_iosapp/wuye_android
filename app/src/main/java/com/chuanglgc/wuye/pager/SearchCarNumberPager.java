package com.chuanglgc.wuye.pager;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.activity.search_car.SearchCarResultActivity;
import com.chuanglgc.wuye.base.BasePager;


public class SearchCarNumberPager extends BasePager {

    private View search;
    private EditText etCardNumber;

    public SearchCarNumberPager(Context context) {
        super(context);
    }

    @Override
    public void initListener() {

    }

    @Override
    public void initData() {

    }

    @Override
    public View initView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.pager_search_car_number, null, false);
        etCardNumber = view.findViewById(R.id.et_carnumber);
        search = view.findViewById(R.id.bt_search);
        return view;
    }

    /**
     * 获取输入的车牌号
     * @return
     */
    public String getEditCarNumber() {
        if (etCardNumber != null) {
            return etCardNumber.getText().toString().trim();
        } else {
            return null;
        }
    }

    @Override
    public void onClick(View v) {

    }
}
