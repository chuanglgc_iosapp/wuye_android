package com.chuanglgc.wuye.pager;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.chuanglgc.wuye.EmployeeApplication;
import com.chuanglgc.wuye.R;
import com.chuanglgc.wuye.base.BasePager;
import com.chuanglgc.wuye.dialog.SpinnerDialog;
import com.chuanglgc.wuye.model.CarModel;
import com.chuanglgc.wuye.model.ParkAreaModel;
import com.chuanglgc.wuye.model.ParkNumberModel;
import com.chuanglgc.wuye.network.RestClient;
import com.chuanglgc.wuye.network.Result;
import com.chuanglgc.wuye.utils.LoadingDialogUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SearchParkNumberPager extends BasePager {

    private TextView tvParkArea;
    private TextView tvParkNumber;
    private Dialog loadingDialog;

    public SearchParkNumberPager(Context context) {
        super(context);
    }

    @Override
    public void initListener() {
        tvParkArea.setOnClickListener(this);
        tvParkNumber.setOnClickListener(this);
    }

    @Override
    public void initData() {

    }

    @Override
    public View initView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.pager_search_park_number, null, false);
        tvParkArea = view.findViewById(R.id.tv_parkarea);
        tvParkNumber = view.findViewById(R.id.tv_parknumber);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_parkarea:
                requestParkArea();
                break;
            case R.id.tv_parknumber:
                requestParkNumber();
                break;
        }
    }

    private void requestParkNumber() {
        String parkArea = tvParkArea.getText().toString().trim();
        if (parkArea.equals("")) {
            Toast.makeText(mActivity, "请选择车位区域", Toast.LENGTH_SHORT).show();
        } else {
            showLoading();
            HashMap<String, String> map = new HashMap<>();
            map.put("community_id", EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id());
            map.put("area", parkArea);
            RestClient.getAPIService().queryParkNum(map).enqueue(new Callback<Result<List<ParkNumberModel>>>() {
                @Override
                public void onResponse(Call<Result<List<ParkNumberModel>>> call, Response<Result<List<ParkNumberModel>>> response) {
                    if (response.code() == 200) {
                        Result<List<ParkNumberModel>> result = response.body();
                        if (result.isResult()) {
                            showParkNumberSpinner(result.getData());
                        } else {
                            Toast.makeText(mActivity, result.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(mActivity, response.code(), Toast.LENGTH_SHORT).show();
                    }
                    loadingDialog.dismiss();
                }

                @Override
                public void onFailure(Call<Result<List<ParkNumberModel>>> call, Throwable t) {
                    Toast.makeText(mActivity, t.toString(), Toast.LENGTH_SHORT).show();
                    loadingDialog.dismiss();
                }
            });
        }

    }

    //请求车位区域
    private void requestParkArea() {
        showLoading();
        RestClient.getAPIService().queryParkArea(EmployeeApplication.getInstance().getUserInfoFromCache().getCommunity_id()).enqueue(new Callback<Result<List<ParkAreaModel>>>() {
            @Override
            public void onResponse(Call<Result<List<ParkAreaModel>>> call, Response<Result<List<ParkAreaModel>>> response) {
                if (response.code() == 200) {
                    Result<List<ParkAreaModel>> result = response.body();
                    if (result.isResult()) {
                        showParkAreaSpinner(result.getData());
                    } else {
                        Toast.makeText(mActivity, result.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mActivity, response.code(), Toast.LENGTH_SHORT).show();
                }
                loadingDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Result<List<ParkAreaModel>>> call, Throwable t) {
                Toast.makeText(mActivity, t.toString(), Toast.LENGTH_SHORT).show();
                loadingDialog.dismiss();
            }
        });
    }

    private void showLoading() {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogUtils.createLoadingDialog(mActivity, "", false);
        } else {
            loadingDialog.show();
        }
    }

    public String getParkArea() {
        return tvParkArea.getText().toString().trim();
    }

    /**
     * 获取选择车位编号
     */
    public String getParkNumber() {
        return tvParkNumber.getText().toString().trim();
    }

    private void showParkNumberSpinner(List<ParkNumberModel> data) {
        ArrayList<String> list = new ArrayList<>();
        for (ParkNumberModel model : data) {
            list.add(model.getPark_num());
        }
        SpinnerDialog spinnerDialog = new SpinnerDialog(mActivity, list);
        spinnerDialog.setOnSpinnerItemClick((itemName, position) -> {
            spinnerDialog.dismiss();
            tvParkNumber.setText(itemName);
        });
        spinnerDialog.show();
    }

    private void showParkAreaSpinner(List<ParkAreaModel> data) {
        ArrayList<String> list = new ArrayList<>();
        for (ParkAreaModel model : data) {
            list.add(model.getArea());
        }
        SpinnerDialog spinnerDialog = new SpinnerDialog(mActivity, list);
        spinnerDialog.setOnSpinnerItemClick((itemName, position) -> {
            spinnerDialog.dismiss();
            tvParkArea.setText(itemName);
            tvParkNumber.setText("");
        });
        spinnerDialog.show();
    }
}
